package com.bigsteptech.realtimechat.conversation;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bigsteptech.realtimechat.Constants;
import com.bigsteptech.realtimechat.R;
import com.bigsteptech.realtimechat.Utils;
import com.bigsteptech.realtimechat.adapter.RecyclerViewAdapter;
import com.bigsteptech.realtimechat.conversation.data_model.Conversation;
import com.bigsteptech.realtimechat.interfaces.OnItemClickListener;
import com.bigsteptech.realtimechat.utils.MessengerDatabaseUtils;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecentConversationListFragment extends Fragment {

    private View rootView;
    private RecyclerView mRecyclerView;
    private List<Object> mConversationList;
    private RecyclerViewAdapter mRecyclerViewAdapter;
    private Context mContext;
    private DatabaseReference chatRoomsDb, chatMessagesDb, userDb, chatRoomMembersDb, duplicateChatRoomsDb, filesDb;

    private String chatRoomId, messageId, toId, userName, lastMessage, userImage, conversationTitle, conversationImage;
    int isOnline, typeOfChat;
    private long lastSeen, timeStamp;

    private ProgressBar progressBar;
    private String selfUid;
    private String typeOfLastMessage;

    private int visibility;
    final int[] i = {0};
    private int counter = 0;
    private MessengerDatabaseUtils messengerDatabaseUtils;
    private HashMap<DatabaseReference, ChildEventListener> mListenerMap = new HashMap<>();
    private HashMap<String, HashMap<DatabaseReference, ChildEventListener>> mStatusListeners = new HashMap<>();
    private boolean isScrolling = false, isDataReceived = false;


    public RecentConversationListFragment() {
        // Required empty public constructor
    }

    public static RecentConversationListFragment newInstance(Bundle bundle) {
        // Required  public constructor
        RecentConversationListFragment fragment = new RecentConversationListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        mContext = getActivity();
        messengerDatabaseUtils = MessengerDatabaseUtils.getInstance();
        rootView = inflater.inflate(R.layout.recycler_view, null);
        mConversationList = new ArrayList<>();

        initViews();
        initDatabaseRef();
        getRecentConversations();

        mRecyclerViewAdapter = new RecyclerViewAdapter(mContext, mConversationList, selfUid, new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                openConversationActivity(position);
            }
        });
        mRecyclerView.setAdapter(mRecyclerViewAdapter);

        return rootView;
    }

    /**
     * Function to initialize the views of this fragment
     */
    private void initViews() {

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setNestedScrollingEnabled(false);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        mRecyclerView.setHasFixedSize(true);
        // Disable Animation on Item change.
        RecyclerView.ItemAnimator animator = mRecyclerView.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        GridLayoutManager linearLayoutManager = new GridLayoutManager(mContext, 1);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.addItemDecoration(new com.bigsteptech.realtimechat.ui.VerticalSpaceItemDecoration(8));

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        isScrolling = false;
                        // reloading data after scrolling done
                        if(isDataReceived){
                            reloadData();
                        }
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        isScrolling = true;
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        System.out.println("Scroll Settling");
                        isScrolling = false;
                        // reload Data after scrolling done
                        if(isDataReceived){
                            reloadData();
                        }
                        break;

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    /**
     * Function to initialize the database references that are being used in this fragment
     */
    private void initDatabaseRef() {

        chatRoomsDb = messengerDatabaseUtils.getChatsDatabase();
        chatRoomMembersDb = messengerDatabaseUtils.getChatMembersDatabase();
        chatMessagesDb = messengerDatabaseUtils.getChatMessagesDatabase();
        userDb = messengerDatabaseUtils.getUsersDatabase();
        duplicateChatRoomsDb = messengerDatabaseUtils.getDuplicateChatsDatabase();
        filesDb = messengerDatabaseUtils.getFilesDatabase();

        selfUid = messengerDatabaseUtils.getCurrentUserId();
    }

    /**
     * Function to open conversation page
     * @param position clicked position of chats
     */
    private void openConversationActivity (int position) {

        Conversation conversation = (Conversation) mConversationList.get(position);
        String chatRoomId = conversation.getmConversationId();
        Intent intent = new Intent(mContext, ConversationActivity.class);
        intent.putExtra("chatRoomId", chatRoomId);
        intent.putExtra("typeOfChat", conversation.getTypeOfChat());
        intent.putExtra(Constants.SENDER, selfUid);
        intent.putExtra(Constants.DESTINATION, conversation.getToId());

        startActivity(intent);
    }

    /**
     * Function to get the recent conversation of the logged-in user
     */
    private void getRecentConversations() {

        chatRoomsDb.orderByChild("participants/" + selfUid + "/isDeleted").equalTo("0").
                addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

//                removeAllListeners();
                if(dataSnapshot.getChildrenCount() > 0){

                    rootView.findViewById(R.id.message_layout).setVisibility(View.GONE);
                    mConversationList.clear();
                    counter = 0;
                    i[0] = 0;

                    for(final DataSnapshot chatRoom: dataSnapshot.getChildren()) {

                        if (chatRoom.hasChild("lastMessageId") && !chatRoom.child("lastMessageId").getValue(String.class).isEmpty()) {
                            toId = null;
                            getChatMembers(chatRoom);
                        }
                    }

                } else {
                    progressBar.setVisibility(View.GONE);

                    rootView.findViewById(R.id.message_layout).setVisibility(View.VISIBLE);
                    ImageView errorIcon = (ImageView) rootView.findViewById(R.id.error_icon);
                    errorIcon.setImageResource(R.drawable.ic_access_time_black_24dp);
//                    errorIcon.setColorFilter(ContextCompat.getColor(mContext, R.color.icon_color),
//                            PorterDuff.Mode.SRC_ATOP);
                    TextView errorMessage = (TextView) rootView.findViewById(R.id.error_message);
                    errorMessage.setText(mContext.getResources().getString(R.string.new_user_message));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void fetchChatMessageDetails(Conversation recentConversation, String lastMessageId){


        final Conversation conversation;

        if(mConversationList.contains(recentConversation)){
            mConversationList.set(mConversationList.indexOf(recentConversation),
                    recentConversation);
            conversation = (Conversation) mConversationList.get(mConversationList.
                    indexOf(recentConversation));
        } else {
            mConversationList.add(recentConversation);
            conversation = (Conversation) mConversationList.get(i[0]);
            i[0]++;
        }

        chatMessagesDb.child(conversation.getmConversationId()).child(lastMessageId).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        typeOfLastMessage = dataSnapshot.child("attachmentType").getValue(String.class);

                        lastMessage = dataSnapshot.child("body").getValue(String.class);
                        String ownerId = dataSnapshot.child("ownerId").getValue(String.class);
                        DataSnapshot recipients = dataSnapshot.child("recipients");

                        conversation.setmMessageId(dataSnapshot.getKey());
                        conversation.setTypeOfLastMessage(typeOfLastMessage);
                        conversation.setmChatMessage(lastMessage);
                        conversation.setLastMessageCreatorId(ownerId);

                        if (ownerId != null && !ownerId.equals(selfUid) && recipients.hasChild(selfUid) && recipients.child(selfUid).child("status").
                                getValue(String.class).equals("1")) {

                            chatMessagesDb.child(conversation.getmConversationId())
                                    .child(conversation.getmMessageId()).child("recipients").child(selfUid).
                                    child("status").setValue("2");
                        }

                        toId = (conversation.getTypeOfChat() == 1) ? dataSnapshot.child("ownerId").
                                getValue(String.class) : conversation.getToId();
                        conversation.setToId(toId);

                        // store the status of last message If the last message is sent by the current logged-in user
                        if(ownerId != null && ownerId.equals(selfUid)){
                            conversation.setLastMessageStatus(getStatusOfMessage(recipients));
                        } else {
                            conversation.setLastMessageStatus(0);
                        }

                        getUserDetails(conversation);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void getUserDetails (final Conversation conversation) {

        if(conversation.getToId() != null) {

            userDb.child(conversation.getToId()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    userName = dataSnapshot.exists() ? dataSnapshot.child("name").getValue(String.class)
                            : mContext.getResources().getString(R.string.deleted_member);

                    if (conversation.getTypeOfChat() == 1) {

                        conversation.setmUserTitle(conversation.getConversationTitle());
                        conversation.setmUserImage(conversation.getConversationImage());
                        if(!conversation.getToId().equals(selfUid)) {
                            conversation.setLastMessageCreator(userName);
                        }
                    } else {

                        if(dataSnapshot.exists()){
                            userImage = dataSnapshot.child(Constants.FIREBASE_IMAGE_KEY).getValue(String.class);
                            isOnline = dataSnapshot.child("isOnline").getValue(Integer.class);
                            lastSeen = dataSnapshot.child("lastSeen").getValue(Long.class);
                            visibility = dataSnapshot.child("visibility").getValue(Integer.class);
                        } else {
                            userImage = null;
                            isOnline = 0;
                            lastSeen = 0;
                            visibility = 0;
                        }
                        conversation.setmUserTitle(userName);
                        conversation.setmUserImage(userImage);
                        conversation.setOnlineStatus(isOnline);
                        conversation.setLastSeen(lastSeen);
                        conversation.setVisibility(visibility);
                    }
                    conversation.setmChatMessage(getChatMessage(conversation.getmChatMessage(),
                            conversation.getLastMessageCreator(), conversation.getTypeOfLastMessage()));

                    notifyAdapter();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void notifyAdapter() {

        if(counter <  mConversationList.size()) {
            counter ++;
        }

        // Sort the List  and notify adapter when all the chatRooms are iterated
        if(counter == mConversationList.size()){

            reloadData();
            removeObservers();
            setObservers();
        }
    }

    private void reloadData() {

        if(isScrolling) {
            isDataReceived = true;
        } else {
            isDataReceived = false;
            sortListWithTimeStamp();
            mRecyclerViewAdapter.notifyDataSetChanged();
            progressBar.setVisibility(View.GONE);
        }
    }

    private void setObservers() {

        for(int i = 0; i < mConversationList.size(); i++) {

            final Conversation conversation = (Conversation) mConversationList.get(i);

            setConversationChangeObserver(conversation);
            addNewMessageCountListener(conversation.getmConversationId());
            setMessageStatusChangeObserver(conversation);
            setUserChangeObserver(conversation);
        }

        DatabaseReference databaseReference = chatRoomsDb.orderByChild("participants/" + selfUid + "/isDeleted").equalTo("0").getRef();
        ChildEventListener childEventListener = chatRoomsDb.orderByChild("participants/" + selfUid + "/isDeleted").equalTo("0").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Conversation conversation = new Conversation(dataSnapshot.getKey());
                if(!mConversationList.contains(conversation)){
                    if(dataSnapshot.hasChild("lastMessageId")){
                        getChatMembers(dataSnapshot);
                    } else {
                        setConversationAddObserver(conversation);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mListenerMap.put(databaseReference, childEventListener);
    }

    private void removeObservers() {


        if(mListenerMap != null && mListenerMap.size() != 0) {
            // Remove Listeners of messages whose status has been changed to 3
            for (Map.Entry<DatabaseReference, ChildEventListener> entry : mListenerMap.entrySet()) {
                DatabaseReference ref = entry.getKey();
                ChildEventListener listener = entry.getValue();
                ref.removeEventListener(listener);
            }
        }
    }

    private void removePreviousMessageChangeListener(String messageId) {

        // Remove Listeners of messages whose status has been changed to 3
        if(mStatusListeners.containsKey(messageId)){
            HashMap<DatabaseReference, ChildEventListener> listenerMap = mStatusListeners.get(messageId);
            for (Map.Entry<DatabaseReference, ChildEventListener> entry : listenerMap.entrySet()) {
                DatabaseReference ref = entry.getKey();
                ChildEventListener listener = entry.getValue();
                ref.removeEventListener(listener);
            }
        }
    }

    private void setConversationAddObserver(final Conversation conversation) {

        DatabaseReference databaseReference = chatRoomsDb.child(conversation.getmConversationId()).getRef();
        ChildEventListener childEventListener = databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if(dataSnapshot.getKey().equals("lastMessageId")) {
                    chatRoomsDb.child(conversation.getmConversationId()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            getChatMembers(dataSnapshot);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mListenerMap.put(databaseReference, childEventListener);
    }

    private void setConversationChangeObserver(final Conversation conversation) {

        DatabaseReference databaseReference = chatRoomsDb.child(conversation.getmConversationId()).getRef();
        ChildEventListener childEventListener = databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                switch (dataSnapshot.getKey()) {

                    case "lastMessageId":
                        removePreviousMessageChangeListener(conversation.getmMessageId());
                        fetchChatMessageDetails(conversation, dataSnapshot.getValue(String.class));
                        break;

                    case "title":
                        if(conversation.getTypeOfChat() == 1){
                            conversation.setmUserTitle(dataSnapshot.getValue(String.class));
                            mRecyclerViewAdapter.notifyItemChanged(mConversationList.indexOf(conversation));
                        }

                        break;

                    case "profileImageUrl":
                        if(conversation.getTypeOfChat() == 1){
                            conversation.setmUserTitle(dataSnapshot.getValue(String.class));
                            mRecyclerViewAdapter.notifyItemChanged(mConversationList.indexOf(conversation));
                        }
                        break;
                }

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mListenerMap.put(databaseReference, childEventListener);
    }

    private void setMessageStatusChangeObserver(final Conversation conversation) {

        if(conversation.getLastMessageStatus() != 3){
            HashMap<DatabaseReference, ChildEventListener> listenerMap = new HashMap<>();
            DatabaseReference messageRef = chatMessagesDb.child(conversation.getmConversationId()).
                    child(conversation.getmMessageId()).getRef();
            ChildEventListener listener = messageRef.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    if(conversation.getLastMessageCreatorId().equals(selfUid)) {
                        conversation.setLastMessageStatus(getStatusOfMessage(dataSnapshot));
                    } else {
                        conversation.setLastMessageStatus(0);
                    }

                    // notify adapter
                    if(mConversationList.contains(conversation)){
                        mRecyclerViewAdapter.notifyItemChanged(mConversationList.indexOf(conversation));
                    }

                    // Remove Listeners of messages whose status has been changed to 3
                    if(conversation.getLastMessageStatus() == 3 && mStatusListeners.containsKey(conversation.getmMessageId())){
                        HashMap<DatabaseReference, ChildEventListener> listenerMap = mStatusListeners.get(conversation.getmMessageId());
                        for (Map.Entry<DatabaseReference, ChildEventListener> entry : listenerMap.entrySet()) {
                            DatabaseReference ref = entry.getKey();
                            ChildEventListener listener = entry.getValue();
                            ref.removeEventListener(listener);
                        }
                    }
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            listenerMap.put(messageRef, listener);
            mStatusListeners.put(conversation.getmMessageId(), listenerMap);
            mListenerMap.put(messageRef,  listener);

        }
    }

    private void setUserChangeObserver(final Conversation conversation) {

        if(conversation.getToId() != null) {

            DatabaseReference databaseReference = userDb.child(conversation.getToId()).getRef();
            ChildEventListener childEventListener = databaseReference.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    switch (dataSnapshot.getKey()) {

                        case "name":
                            if(conversation.getTypeOfChat() != 1) {
                                conversation.setmUserTitle(dataSnapshot.getValue(String.class));
                            } else if(conversation.getLastMessageCreator() != null){
                                conversation.setmChatMessage(getChatMessage(conversation.getmChatMessage(),
                                        conversation.getLastMessageCreator(), conversation.getTypeOfLastMessage()));
                            }
                            mRecyclerViewAdapter.notifyItemChanged(mConversationList.indexOf(conversation));
                            break;

                        case "profileImageUrl":
                            if(conversation.getTypeOfChat() != 1) {
                                conversation.setmUserImage(dataSnapshot.getValue(String.class));
                                mRecyclerViewAdapter.notifyItemChanged(mConversationList.indexOf(conversation));
                            }
                            break;

                        case "isOnline":
                            conversation.setOnlineStatus(dataSnapshot.getValue(Integer.class));
                            mRecyclerViewAdapter.notifyItemChanged(mConversationList.indexOf(conversation));
                            break;

                        case "visibility":
                            conversation.setVisibility(dataSnapshot.getValue(Integer.class));
                            mRecyclerViewAdapter.notifyItemChanged(mConversationList.indexOf(conversation));
                            break;

                        case "lastSeen":
                            conversation.setLastSeen(dataSnapshot.getValue(Long.class));
                            mRecyclerViewAdapter.notifyItemChanged(mConversationList.indexOf(conversation));
                            break;
                    }
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            mListenerMap.put(databaseReference, childEventListener);
        }
    }

    private void getChatMembers(final DataSnapshot chatRoom) {

        final Conversation[] recentConversation = new Conversation[1];

        chatRoomMembersDb.child(chatRoom.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {

                if(dataSnapshot.child(selfUid).hasChild("duplicateChatId")){

                    String duplicateChatRoomId = dataSnapshot.child(selfUid)
                            .child("duplicateChatId").getValue(String.class);
                    duplicateChatRoomsDb.child(duplicateChatRoomId).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot chatDataSnapshot) {

                            if(chatDataSnapshot.hasChild("lastMessageId")) {
                                chatRoomId = chatRoom.getKey();
                                typeOfChat = chatRoom.child("isGroup").getValue(String.class).equals("1") ? 1 : 0;

                                toId = getToIdOfChat(typeOfChat, chatRoom);

                                messageId = chatDataSnapshot.child("lastMessageId").getValue(String.class);

                                conversationTitle = chatDataSnapshot.child("title").getValue(String.class);
                                conversationImage = chatDataSnapshot.child(Constants.FIREBASE_IMAGE_KEY).getValue(String.class);

                                timeStamp = chatDataSnapshot.child("updatedAt").getValue(Long.class);

                                recentConversation[0] = new Conversation(chatRoomId, messageId, timeStamp, toId,
                                        typeOfChat, conversationTitle, conversationImage, getTimeStampString(timeStamp));

                                if(dataSnapshot.child(selfUid).hasChild("newMessageCount")){
                                    int unReadMessageCount = dataSnapshot.child(selfUid).child("newMessageCount").
                                            getValue(Integer.class);
                                    recentConversation[0].setUnreadMessageCount(unReadMessageCount);
                                } else {
                                    recentConversation[0].setUnreadMessageCount(0);
                                }

                                fetchChatMessageDetails(recentConversation[0], messageId);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                } else {
                    chatRoomId = chatRoom.getKey();
                    typeOfChat = chatRoom.child("isGroup").getValue(String.class).equals("1") ? 1 : 0;

                    toId = getToIdOfChat(typeOfChat, chatRoom);

                    messageId = chatRoom.child("lastMessageId").getValue(String.class);

                    conversationTitle = chatRoom.child("title").getValue(String.class);
                    conversationImage = chatRoom.child(Constants.FIREBASE_IMAGE_KEY).getValue(String.class);

                    timeStamp = chatRoom.child("updatedAt").getValue(Long.class);

                    recentConversation[0] = new Conversation(chatRoomId, messageId, timeStamp, toId,
                            typeOfChat, conversationTitle, conversationImage, getTimeStampString(timeStamp));

                    if(dataSnapshot.child(selfUid).hasChild("newMessageCount")){
                        int unReadMessageCount = dataSnapshot.child(selfUid).child("newMessageCount").
                                getValue(Integer.class);
                        recentConversation[0].setUnreadMessageCount(unReadMessageCount);
                    } else {
                        recentConversation[0].setUnreadMessageCount(0);
                    }

                    fetchChatMessageDetails(recentConversation[0], messageId);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    /**
     * Listener for newMessageCount in members table
     * will be called if newMessageCount will be added/changed/removed from a particular chat for current logged-in user
     * @param chatRoomId
     */
    private void addNewMessageCountListener(final String chatRoomId) {
        DatabaseReference databaseReference = chatRoomMembersDb.child(chatRoomId).child(selfUid).getRef();
        ChildEventListener childEventListener = databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                // Show New Message Count here
                if(dataSnapshot.getKey().equals("newMessageCount")){
                    Conversation conversation = new Conversation(chatRoomId);
                    if(mConversationList.contains(conversation)) {
                        int indexOfItem = mConversationList.indexOf(conversation);
                        conversation = (Conversation) mConversationList.get(indexOfItem);
                        conversation.setUnreadMessageCount(dataSnapshot.getValue(Integer.class));
                        mRecyclerViewAdapter.notifyItemChanged(indexOfItem);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                // Show Updated Message Count here
                if(dataSnapshot.getKey().equals("newMessageCount")){
                    Conversation conversation = new Conversation(chatRoomId);
                    if(mConversationList.contains(conversation)) {
                        int indexOfItem = mConversationList.indexOf(conversation);
                        conversation = (Conversation) mConversationList.get(indexOfItem);
                        conversation.setUnreadMessageCount(dataSnapshot.getValue(Integer.class));
                        mRecyclerViewAdapter.notifyItemChanged(indexOfItem);
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                // Hide count ui if newMessageCount is removed
                if(dataSnapshot.getKey().equals("newMessageCount")){
                    Conversation conversation = new Conversation(chatRoomId);
                    if(mConversationList.contains(conversation)) {
                        int indexOfItem = mConversationList.indexOf(conversation);
                        conversation = (Conversation) mConversationList.get(indexOfItem);
                        conversation.setUnreadMessageCount(0);
                        mRecyclerViewAdapter.notifyItemChanged(indexOfItem);
                    }
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mListenerMap.put(databaseReference, childEventListener);
    }

    private String getChatMessage(String message, String lastMessageCreator, String typeOfLastMessage) {

        lastMessageCreator = lastMessageCreator != null ? lastMessageCreator + ": " : "";

        if(typeOfLastMessage != null) {
            switch (typeOfLastMessage){

                case "text":
                    // Text Message
                    return lastMessageCreator + message;

                case "image":
                    // Photo Message
                    return lastMessageCreator + "\uf030" + " " + mContext.getResources().
                            getString(R.string.photo_attachment);

                case "video":
                    // Video Message
                    return lastMessageCreator + "\uf03d" + " " +  mContext.getResources().
                            getString(R.string.video_attachment);

                case "audio":
                    // Video Message
                    return lastMessageCreator + "\uf001" + " " +  mContext.getResources().
                            getString(R.string.audio_attachment);

                case "file":
                    // Video Message
                    return lastMessageCreator + "\uf15c" + " " +  mContext.getResources().
                            getString(R.string.document_attachment);

                default:
                    return lastMessageCreator + "\uf15c" + " " +  typeOfLastMessage;
            }
        } else {
            return message;
        }
    }

    private int getStatusOfMessage(DataSnapshot recipients) {

        int statusOfMessage = 1;
        int countOfDelivered = 0, countOfRead = 0;

        if(recipients.getChildrenCount() > 1){
            for(DataSnapshot recipient : recipients.getChildren()){
                if(!recipient.getKey().equals(selfUid)){
                    if(recipient.child("status").getValue(String.class).equals("2")){
                        countOfDelivered++;
                    } else if(recipient.child("status").getValue(String.class).equals("3")){
                        countOfDelivered++;
                        countOfRead++;
                    }
                }
            }

            if(countOfRead == recipients.getChildrenCount() -1 ){
                statusOfMessage = 3;
            } else if(countOfDelivered == recipients.getChildrenCount() -1){
                statusOfMessage = 2;
            }
        } else {
            statusOfMessage = 1;
        }

        return statusOfMessage;

    }

    private String getToIdOfChat(int typeOfChat, DataSnapshot chatRoomSnapshot) {

        if (typeOfChat != 1) {
            for (DataSnapshot member : chatRoomSnapshot.child("participants").getChildren()) {
                if (!member.getKey().equals(selfUid)) {
                    return member.getKey();
                }
            }
        }
        return null;
    }

    private String getTimeStampString(Long timeStamp) {

        String time = Utils.getTimestamp(timeStamp, mContext);
        String date = Utils.getDateFromTimeStamp(timeStamp);
        String today = Utils.getCurrentTimestamp();

        String[] time1 = date.split("/");
        String[] time2 = today.split("/");
        if ((time1[0]+time1[1]+time1[2]).equals(time2[0]+time2[1]+time2[2])) {
            return time;
        } else {
            return date;
        }
    }

    private void sortListWithTimeStamp() {

        Collections.sort(mConversationList, new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                Conversation conversation1 = (Conversation) o1;
                Conversation conversation2 = (Conversation) o2;

                return Long.valueOf(conversation2.getLastUpdated()).compareTo(conversation1.getLastUpdated());
            }

        });
    }
}
