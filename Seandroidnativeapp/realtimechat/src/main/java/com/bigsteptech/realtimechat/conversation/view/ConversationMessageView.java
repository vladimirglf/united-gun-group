package com.bigsteptech.realtimechat.conversation.view;


import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bigsteptech.realtimechat.R;
import com.bigsteptech.realtimechat.Utils;
import com.bigsteptech.realtimechat.conversation.adapter.ConversationMessageAdapter;
import com.bigsteptech.realtimechat.conversation.data_model.Message;
import com.bigsteptech.realtimechat.interfaces.OnRetryClicked;
import com.bigsteptech.realtimechat.interfaces.OnSongPlay;
import com.bigsteptech.realtimechat.ui.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;

public class ConversationMessageView extends LinearLayout {

    private TextView dateTextView, userNameTextView;
    private TextView messageTextView;
    private TextView timestampTextView;
    private ImageView imageView, videoThumbnail;
    private RelativeLayout mVideoLayout;
    private CircularImageView userImage;
    private RelativeLayout audioLayout;
    private TextView playButton, musicIcon, textDuration, textBufferDuration;
    private SeekBar mSeekBar;
    private TextView documentTextView;
    private ImageView statusImageView;
    private ProgressBar progressBar;
    private TextView retryTextView;

    private int layoutResId;
    private Context mContext;
    private long mediaFileLengthInMilliseconds;

    private final Handler handler = new Handler();
    private boolean isPaused = false;
    private MediaPlayer mPlayer;
    private String selfUid;

    public ConversationMessageView(Context context, AttributeSet attrs) {

        super(context, attrs);
        mContext = context;
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            int[] attrsArray = {
                    android.R.attr.layout
            };
            TypedArray array = context.obtainStyledAttributes(attrs, attrsArray);
            layoutResId = array.getResourceId(0, R.layout.merge_conversation_message_item_destination);
            array.recycle();
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), layoutResId, this);
        this.dateTextView = (TextView) this.findViewById(R.id.dateTextView);
        this.messageTextView = (TextView) this.findViewById(R.id.message);
        this.timestampTextView = (TextView) this.findViewById(R.id.time);
        this.imageView = (ImageView) this.findViewById(R.id.image);
        this.videoThumbnail = (ImageView) this.findViewById(R.id.videoThumbnail);
        this.mVideoLayout = (RelativeLayout) this.findViewById(R.id.videoLayout);
        this.userImage = (CircularImageView) this.findViewById(R.id.user_image);
        this.userNameTextView = (TextView) this.findViewById(R.id.user_name);

        this.audioLayout = (RelativeLayout) this.findViewById(R.id.audioLayout);
        this.playButton = (TextView) this.findViewById(R.id.playButtonIcon);
        this.musicIcon = (TextView) this.findViewById(R.id.musicIcon);
        this.textBufferDuration = (TextView) this.findViewById(R.id.textBufferDuration);
        this.textDuration = (TextView) this.findViewById(R.id.textDuration);
        this.mSeekBar = (SeekBar) this.findViewById(R.id.playback_view_seekbar);
        this.progressBar = (ProgressBar) this.findViewById(R.id.progress);
        this.retryTextView = (TextView) findViewById(R.id.retryText);

        if(playButton != null){
            playButton.setTypeface(Typeface.createFromAsset(mContext.getAssets(),
                    "fontIcons/fontawesome-webfont.ttf"));
            playButton.setText("\uf04b");
        }

        if(musicIcon != null){
            musicIcon.setTypeface(Typeface.createFromAsset(mContext.getAssets(),
                    "fontIcons/fontawesome-webfont.ttf"));
            musicIcon.setText("\uf001");
        }

        documentTextView = (TextView) this.findViewById(R.id.document);
        if(documentTextView != null){
            documentTextView.setTypeface(Typeface.createFromAsset(mContext.getAssets(),
                    "fontIcons/fontawesome-webfont.ttf"));
            documentTextView.setText("\uf15c");
        }

        statusImageView = (ImageView) findViewById(R.id.status);

    }

    public void display(final int position, final Message message, final OnSongPlay onSongPlay,
                        final OnRetryClicked onRetryClicked, String selfUid) {
        final long timestamp = message.getCreatedAt();
        if (dateTextView != null) {
            dateTextView.setText(Utils.getDateOfMessage(mContext, timestamp));
        }

        messageTextView.setMovementMethod(LinkMovementMethod.getInstance());

        if(message.getOwnerId().equals(selfUid)){
            messageTextView.setBackgroundResource(R.drawable.bg_msg_to);
        } else {
            messageTextView.setBackgroundResource(R.drawable.bg_msg_from);
        }

        switch (message.getAttachmentType()){


            case "text":

                findViewById(R.id.imageLayout).setVisibility(GONE);
                audioLayout.setVisibility(GONE);
                mVideoLayout.setVisibility(GONE);
                messageTextView.setVisibility(VISIBLE);

                String messageBody = message.getBody().replace("\n", "<br>");
                messageTextView.setText(Html.fromHtml(Html.fromHtml(Html.fromHtml(messageBody).toString()).toString()));

                break;

            case "image":

                findViewById(R.id.imageLayout).setVisibility(VISIBLE);
                audioLayout.setVisibility(GONE);
                mVideoLayout.setVisibility(GONE);
                messageTextView.setVisibility(GONE);

                if(message.getTemporaryPath() != null && !message.getTemporaryPath().isEmpty()){

                    if(retryTextView != null){
                        retryTextView.setVisibility(VISIBLE);
                        retryTextView.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                progressBar.setVisibility(VISIBLE);
                                retryTextView.setVisibility(GONE);
                                onRetryClicked.onRetryViewClicked(message);
                            }
                        });
                    }
                } else if(message.getFileUrl() != null && !message.getFileUrl().isEmpty()){

                    progressBar.setVisibility(VISIBLE);
                    Picasso.with(mContext)
                            .load(message.getFileUrl())
                            .placeholder(R.color.grey)
                            .into(imageView, new com.squareup.picasso.Callback() {
                                    @Override
                                    public void onSuccess() {
                                        progressBar.setVisibility(GONE);
                                    }

                                    @Override
                                    public void onError() {

                                    }
                            });
                    // Open PhotoPreviewActivity to View the full Photo.
                    imageView.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(mContext, PhotoPreviewActivity.class);
                            intent.putExtra("imageUrl", message.getFileUrl());
                            mContext.startActivity(intent);
                        }
                    });
                    if(retryTextView != null){
                        retryTextView.setVisibility(GONE);
                    }
                } else {
                    imageView.setBackgroundResource(R.color.grey);
                    progressBar.setVisibility(VISIBLE);
                }

                break;

            case "video":

                findViewById(R.id.imageLayout).setVisibility(GONE);
                audioLayout.setVisibility(GONE);
                messageTextView.setVisibility(GONE);

                if(message.getVideoThumbnail() != null && !message.getVideoThumbnail().isEmpty()){
                    Picasso.with(mContext)
                            .load(message.getVideoThumbnail())
                            .placeholder(R.color.grey)
                            .into(videoThumbnail);
                } else {
                    videoThumbnail.setImageResource(R.color.grey);
                }
                if(message.getFileUrl() != null){

                    videoThumbnail.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(mContext, PhotoPreviewActivity.class);
                            intent.putExtra("imageUrl", message.getVideoThumbnail());
                            intent.putExtra("videoUrl", message.getFileUrl());
                            mContext.startActivity(intent);

                        }
                    });

                    mVideoLayout.setVisibility(VISIBLE);
                } else {
                    mVideoLayout.setVisibility(GONE);
                }
                break;

            case "audio":
                if(audioLayout != null) {
                    findViewById(R.id.imageLayout).setVisibility(GONE);
                    mVideoLayout.setVisibility(GONE);
                    messageTextView.setVisibility(GONE);

                    if (message.getFileUrl() != null) {
                        audioLayout.setVisibility(VISIBLE);

                        if (message.getSeekBarProgress() != 0) {
                            this.mSeekBar.setProgress(message.getSeekBarProgress());
                            mSeekBar.setEnabled(true);
                        }


                        try {
                            mPlayer = new MediaPlayer();
                            ConversationMessageAdapter.medialPlayers.add(mPlayer);
                            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                            mPlayer.setDataSource(message.getFileUrl());
                            mPlayer.prepareAsync();
                        } catch (IllegalArgumentException | IllegalStateException | IOException e) {
                            e.printStackTrace();
                        }

                        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mediaPlayer) {
                            }
                        });

                        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mediaPlayer) {
//                                mediaPlayer.release();
                                playButton.setText("\uF04B");

                            }
                        });



                        playButton.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (onSongPlay != null) {
                                    if (!mPlayer.isPlaying()) {

                                        playButton.setText("\uf04c");
                                        mPlayer.start();
                                        mediaFileLengthInMilliseconds = (long) (message.getAudioDuration() != 0 ?
                                                message.getAudioDuration() * 1000 : 0);
                                        primarySeekBarProgressUpdater();
                                        isPaused = false;
                                    } else {

                                        mPlayer.pause();
                                        isPaused = true;
                                        playButton.setText("\uF04B");
                                    }
                                }
                            }
                        });
                    } else {
                        audioLayout.setVisibility(GONE);
                    }
                }
                break;

            case "file":

                if(message.getFileUrl() != null){
                    documentTextView.setText("\uf15c " + "Document");
                    documentTextView.setVisibility(VISIBLE);
                    documentTextView.setMovementMethod(LinkMovementMethod.getInstance());
                    documentTextView.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            openDocument(message.getFileUrl());
                        }
                    });
                } else {
                    if(documentTextView != null)
                        documentTextView.setVisibility(GONE);
                }
                break;

        }

        if(userImage != null){
            Picasso.with(mContext)
                    .load(message.getUserImage())
                    .placeholder(R.drawable.person_image_empty)
                    .into(userImage);
        }

        if(userNameTextView != null){
            userNameTextView.setText(message.getUserName());
        }

        timestampTextView.setText(Utils.getTimestamp(timestamp, mContext));

        if(statusImageView != null){

            switch (message.getStatus()){

                case 1:
                    statusImageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_status_sent_18dp));
                    statusImageView.setColorFilter(ContextCompat.getColor(mContext, R.color.grey));
                    break;

                case 2:
                    statusImageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_status_delivered_15dp));
                    statusImageView.setColorFilter(ContextCompat.getColor(mContext, R.color.grey));
                    break;

                case 3:
                    statusImageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_status_delivered_15dp));
                    statusImageView.setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimary));
                    break;
            }
        }
    }

    private void primarySeekBarProgressUpdater() {
        if(mSeekBar != null){
            textBufferDuration.setVisibility(VISIBLE);
            textBufferDuration.setText(Utils.getDuration(mPlayer.getCurrentPosition()));

            textDuration.setVisibility(VISIBLE);
            textDuration.setText(Utils.getDuration(mediaFileLengthInMilliseconds));
            mSeekBar.setEnabled(true);
            mSeekBar.setProgress((int)(((float)mPlayer.getCurrentPosition()/mediaFileLengthInMilliseconds)*100)); // This math construction give a percentage of "was playing"/"song length"
            if (mPlayer.isPlaying()) {
                Runnable notification = new Runnable() {
                    public void run() {
                        primarySeekBarProgressUpdater();
                    }
                };
                ConversationMessageAdapter.medialPlayerHandlers.put(handler, notification);
                handler.postDelayed(notification, 1000);
            }
        }
    }

    public void openDocument(String name) {

//        Intent intent = new Intent(Intent.ACTION_VIEW);
//        intent.setDataAndType(Uri.parse(name), "application/pdf");

        Log.d(ConversationMessageView.class.getSimpleName(), "Inside openDocument method..");
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
        File file = new File("/storage/emulated/0/Download/pdf-sample.pdf");
        String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString());
        String mimetype = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

        Log.d(ConversationMessageView.class.getSimpleName(), "extension " + extension + " mimetype " + mimetype);
        if (extension.equalsIgnoreCase("") || mimetype == null) {
            // if there is no extension or there is no definite mimetype, still try to open the file
            intent.setDataAndType(Uri.fromFile(file), "text/*");
        } else {
            intent.setDataAndType(Uri.fromFile(file), mimetype);
        }
        // custom message for the intent
        mContext.startActivity(Intent.createChooser(intent, "Choose an Application:"));
    }

}
