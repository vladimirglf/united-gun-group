package com.bigsteptech.realtimechat.groups;


import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bigsteptech.realtimechat.Constants;
import com.bigsteptech.realtimechat.R;
import com.bigsteptech.realtimechat.Utils;
import com.bigsteptech.realtimechat.adapter.RecyclerViewAdapter;
import com.bigsteptech.realtimechat.conversation.ConversationActivity;
import com.bigsteptech.realtimechat.groups.data_model.GroupsList;
import com.bigsteptech.realtimechat.interfaces.OnItemClickListener;
import com.bigsteptech.realtimechat.ui.VerticalSpaceItemDecoration;
import com.bigsteptech.realtimechat.utils.MessengerDatabaseUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupsListFragment extends Fragment {


    private View rootView;
    private RecyclerView mRecyclerView;
    public static List<Object> mGroupsList = new ArrayList<>();
    private RecyclerViewAdapter mRecyclerViewAdapter;
    private Context mContext;
    private DatabaseReference chatRoomDb, usersDb, chatMembersDb;
    private String selfUid;

    private String groupTitle, groupImage;
    private String lastMessageTimeStamp;
    boolean isAlreadyExists;
    private ProgressBar progressBar;
    private MessengerDatabaseUtils messengerDatabaseUtils;

    public GroupsListFragment() {
        // Required empty public constructor
    }

    public static GroupsListFragment newInstance(Bundle bundle) {
        // Required  public constructor
        GroupsListFragment fragment = new GroupsListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        mContext = getActivity();
        messengerDatabaseUtils = MessengerDatabaseUtils.getInstance();
        rootView = inflater.inflate(R.layout.recycler_view, null);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);

        mGroupsList = new ArrayList<>();

        selfUid = messengerDatabaseUtils.getCurrentUserId();

        mRecyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return 1;
            }
        });
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(8));

        chatRoomDb = messengerDatabaseUtils.getChatsDatabase();
        chatMembersDb = messengerDatabaseUtils.getChatMembersDatabase();
        usersDb = messengerDatabaseUtils.getUsersDatabase();

        mRecyclerViewAdapter = new RecyclerViewAdapter(mContext, mGroupsList, selfUid, new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

                GroupsList groupsList = (GroupsList) mGroupsList.get(position);
                String chatRoomId = groupsList.getmGroupId();
                Intent intent = new Intent(mContext, ConversationActivity.class);
                intent.putExtra("chatRoomId", chatRoomId);
                intent.putExtra("typeOfChat", 1);
                intent.putExtra(Constants.SENDER, selfUid);


                startActivity(intent);
            }
        });

        mRecyclerView.setAdapter(mRecyclerViewAdapter);

        getGroupsList();

        return rootView;
    }

    private void getGroupsList() {

        progressBar.setVisibility(View.VISIBLE);
        chatMembersDb.orderByChild(selfUid + "/isActive").equalTo("1").addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.d(GroupsListFragment.class.getSimpleName(), "onDataChange  of Members db called.." +
                        dataSnapshot.getChildrenCount());

                mGroupsList.clear();

                int j = 0;
                int k = 0;

                if(dataSnapshot.getChildrenCount() > 0) {

                    rootView.findViewById(R.id.message_layout).setVisibility(View.GONE);
                    for(final DataSnapshot childDataSnapshot: dataSnapshot.getChildren()){

                        k++;
                        final int finalK = k;
                        isAlreadyExists = false;

                        // Todo for next release
//                    final int[] i = {1};
//                    final LinkedHashMap<String, String> userNames = new LinkedHashMap<>();


                        if(childDataSnapshot.getChildrenCount() > 2){
                            hideErrorMessage();

                            j++;
                            chatRoomDb.child(childDataSnapshot.getKey()).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {

                                    Log.d(GroupsListFragment.class.getSimpleName(), "chatRoomId= " + childDataSnapshot.getKey());

                                    int memberCount = dataSnapshot.child("memberCount").getValue(Integer.class);
                                    groupImage = dataSnapshot.hasChild(Constants.FIREBASE_IMAGE_KEY) ?
                                            dataSnapshot.child(Constants.FIREBASE_IMAGE_KEY).getValue(String.class) : null;
                                    groupTitle = dataSnapshot.child("title").getValue(String.class);

                                    lastMessageTimeStamp = Utils.getRelativeTimeString(dataSnapshot.child("updatedAt").
                                            getValue(Long.class));

                                    GroupsList groupsList = new GroupsList(dataSnapshot.getKey(), groupImage,
                                            groupTitle, lastMessageTimeStamp, dataSnapshot.child("updatedAt").
                                            getValue(Long.class), memberCount);

                                    if(mGroupsList.contains(groupsList)){
                                        isAlreadyExists = true;
                                        mGroupsList.set(mGroupsList.indexOf(groupsList), groupsList);
                                    } else {
                                        mGroupsList.add(groupsList);
                                    }

                                    if(finalK >= mGroupsList.size() || isAlreadyExists){
                                        progressBar.setVisibility(View.GONE);
                                        sortGroupsListWithTimeStamp();
                                        mRecyclerViewAdapter.notifyDataSetChanged();
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }
                    }

                    if(j == 0){
                        // There are no groups available for this user
                        showErrorMessage();
                    }

                } else {
                    showErrorMessage();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void showErrorMessage() {

        // Show No Contacts View here
        progressBar.setVisibility(View.GONE);
        mGroupsList.clear();
        mRecyclerViewAdapter.update(mGroupsList);
        rootView.findViewById(R.id.message_layout).setVisibility(View.VISIBLE);
        ImageView errorIcon = (ImageView) rootView.findViewById(R.id.error_icon);
        errorIcon.setImageResource(R.drawable.ic_people_white_24dp);
        TextView errorMessage = (TextView) rootView.findViewById(R.id.error_message);
        errorMessage.setText(mContext.getResources().getString(R.string.no_groups_available));
    }

    private void hideErrorMessage(){
        rootView.findViewById(R.id.message_layout).setVisibility(View.GONE);
    }

    private void sortGroupsListWithTimeStamp() {

        Collections.sort(mGroupsList, new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                GroupsList group1 = (GroupsList) o1;
                GroupsList group2 = (GroupsList) o2;

                return Long.valueOf(group2.getLastUpdated()).compareTo(group1.getLastUpdated());
            }

        });
    }

}
