/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *    You may not use this file except in compliance with the
 *    SocialEngineAddOns License Agreement.
 *    You may obtain a copy of the License at:
 *    https://www.socialengineaddons.com/android-app-license
 *    The full copyright and license information is also mentioned
 *    in the LICENSE file that was distributed with this
 *    source code.
 */

package com.bigsteptech.realtimechat.contacts;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bigsteptech.realtimechat.Constants;
import com.bigsteptech.realtimechat.R;
import com.bigsteptech.realtimechat.adapter.RecyclerViewAdapter;
import com.bigsteptech.realtimechat.contacts.data_model.ContactsList;
import com.bigsteptech.realtimechat.conversation.ConversationActivity;
import com.bigsteptech.realtimechat.interfaces.OnItemClickListener;
import com.bigsteptech.realtimechat.ui.VerticalSpaceItemDecoration;
import com.bigsteptech.realtimechat.user.User;
import com.bigsteptech.realtimechat.user.interfaces.OnContactSelectListener;
import com.bigsteptech.realtimechat.utils.MessengerDatabaseUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactsListFragment extends Fragment {


    private View rootView;
    private RecyclerView mRecyclerView;
    public static List<Object> mContactsList = new ArrayList<>();
    public static List<Object> mBlockedContactsList = new ArrayList<>();
    public static List<Object> mNewConvContactsList = new ArrayList<>();
    private List<ContactsList> mOnlineMembers = new ArrayList<>(), mOfflineMembers = new ArrayList<>();
    private RecyclerViewAdapter contactsListAdapter, newConversationPageAdapter, usersFilterAdapter, blockedContactsAdapter;
    private Context mContext;
    private DatabaseReference friendsDb, userDb, chatMembersDb, duplicateChatRef;
    private String selfUid;
    private ProgressBar progressBar;
    private OnContactSelectListener mContactSelectedListener;
    private boolean isNewConversationPage, blockedContacts;
    private ArrayList<String> participantsKeys = new ArrayList<>();
    private MessengerDatabaseUtils messengerDatabaseUtils;


    public ContactsListFragment() {
        // Required empty public constructor
    }

    public static ContactsListFragment newInstance(Bundle bundle) {
        // Required  public constructor
        ContactsListFragment fragment = new ContactsListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(ContactsListFragment.class.getSimpleName(), "onCreateView function called..");

        // Inflate the layout for this fragment
        mContext = getActivity();
        messengerDatabaseUtils = MessengerDatabaseUtils.getInstance();
        rootView = inflater.inflate(R.layout.recycler_view, null);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(8));

        if(getArguments() != null){
            isNewConversationPage = getArguments().getBoolean("isNewGroupsPage");
            blockedContacts = getArguments().getBoolean("blockedContacts");
            participantsKeys = getArguments().getStringArrayList("participants");
        }

        selfUid = messengerDatabaseUtils.getCurrentUserId();

        friendsDb = blockedContacts ? messengerDatabaseUtils.getBlockedListDatabase().child(selfUid) :
                messengerDatabaseUtils.getFriendsDatabase().child(selfUid);

        userDb = messengerDatabaseUtils.getUsersDatabase();
        chatMembersDb = messengerDatabaseUtils.getChatMembersDatabase();
        duplicateChatRef = messengerDatabaseUtils.getDuplicateChatsDatabase();

        contactsListAdapter = new RecyclerViewAdapter(mContext, isNewConversationPage, blockedContacts, selfUid, new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

                ContactsList contactsList = (ContactsList) contactsListAdapter.getmConversationList().get(position);
                Intent intent = new Intent(mContext, ConversationActivity.class);
                intent.putExtra(Constants.SENDER, selfUid);
                intent.putExtra(Constants.DESTINATION, contactsList.getmUserId());
                startActivity(intent);

            }
        });

        blockedContactsAdapter = new RecyclerViewAdapter(mContext, isNewConversationPage, blockedContacts, selfUid, new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

                ContactsList contactsList = (ContactsList) blockedContactsAdapter.getmConversationList().get(position);

                if(mContactSelectedListener != null){
                    mContactSelectedListener.onContactSelected(contactsList);
                }
            }
        });

        newConversationPageAdapter = new RecyclerViewAdapter(mContext, isNewConversationPage, blockedContacts, selfUid, new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

                ContactsList contactsList = (ContactsList) newConversationPageAdapter.getmConversationList().get(position);
                Log.d(ContactsListFragment.class.getSimpleName(), "OnItemClicked called.. " +  contactsList.isContactSelected());
                if(mContactSelectedListener != null){
                    mContactSelectedListener.onContactSelected(contactsList);
                }
            }
        });

        usersFilterAdapter = new RecyclerViewAdapter(mContext, isNewConversationPage, false, selfUid, new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

                ContactsList contactsList = (ContactsList) usersFilterAdapter.getmConversationList().get(position);

                if(isNewConversationPage && mContactSelectedListener != null){
                    mContactSelectedListener.onContactSelected(contactsList);
                } else {
                    Intent intent = new Intent(mContext, ConversationActivity.class);
                    intent.putExtra(Constants.SENDER, selfUid);
                    intent.putExtra(Constants.DESTINATION, contactsList.getmUserId());
                    startActivity(intent);
                }
            }
        });

        if(blockedContacts){
            mRecyclerView.setAdapter(blockedContactsAdapter);
        } else if(isNewConversationPage || (participantsKeys != null && participantsKeys.size() != 0)){
            mRecyclerView.setAdapter(newConversationPageAdapter);
        } else {
            mRecyclerView.setAdapter(contactsListAdapter);
        }
        addDataToList();

        return rootView;
    }

    // Set The Selected Filters
    public void setmContactSelectedListener(OnContactSelectListener mFilterSelectedListener) {
        this.mContactSelectedListener = mFilterSelectedListener;
    }

    public void notifyAdapter(ContactsList contact) {

        Log.d(ContactsListFragment.class.getSimpleName(), " notifyAdapter called..");
        contact.setContactSelected(false);
        newConversationPageAdapter.notifyDataSetChanged();
    }

    private void addDataToList(){

        // Attach a listener to read the data at our posts reference
        friendsDb.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot usersDataSnapshot) {

                int size;
                if(usersDataSnapshot.getChildrenCount() > 0){
                    rootView.findViewById(R.id.message_layout).setVisibility(View.GONE);
                    if(blockedContacts){
                        mBlockedContactsList.clear();
                    } else if (isNewConversationPage || (participantsKeys != null && participantsKeys.size() != 0)){
                        mNewConvContactsList.clear();
                    } else {
                        mContactsList.clear();
                    }
                    mOnlineMembers.clear();
                    mOfflineMembers.clear();
                    final int[] i = {0};
                    final int[] j = {0};

                    for (final DataSnapshot childDataSnapshot : usersDataSnapshot.getChildren()) {
                        if(participantsKeys != null && participantsKeys.size() != 0){
                            if(!participantsKeys.contains(childDataSnapshot.getKey())){
                                mNewConvContactsList.add(new ContactsList(childDataSnapshot.getKey()));
                            }
                            size = mNewConvContactsList.size();
                        } else {
                            if(blockedContacts) {
                                mBlockedContactsList.add(new ContactsList(childDataSnapshot.getKey()));
                                size = mBlockedContactsList.size();
                            } else if(isNewConversationPage) {
                                mNewConvContactsList.add(new ContactsList(childDataSnapshot.getKey()));
                                size = mNewConvContactsList.size();
                            } else {
                                mContactsList.add(new ContactsList(childDataSnapshot.getKey()));
                                size = mContactsList.size();
                            }
                        }

                        if(size > 0 && i[0] <= size - 1){

                        final ContactsList contactsList;

                        if(blockedContacts){
                            contactsList = (ContactsList) mBlockedContactsList.get(i[0]);
                        } else if (isNewConversationPage || (participantsKeys != null && participantsKeys.size() != 0)){
                            contactsList = (ContactsList) mNewConvContactsList.get(i[0]);
                        } else {
                            contactsList = (ContactsList) mContactsList.get(i[0]);
                        }
                        i[0]++;
                        userDb.child(childDataSnapshot.getKey()).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                j[0]++;

                                if(dataSnapshot.exists()){
                                    User user = dataSnapshot.getValue(User.class);
                                    contactsList.setmUserTitle(user.getName());
                                    contactsList.setmUserImage(user.getProfileImageUrl());
                                    contactsList.setmOnlineStatus(user.getIsOnline());
                                    contactsList.setLastSeen(user.getLastSeen());

                                    if(user.getIsOnline() == 1){
                                        if(mOfflineMembers.contains(contactsList)){
                                            mOfflineMembers.remove(contactsList);
                                        }
                                        if(mOnlineMembers.contains(contactsList)){
                                            mOnlineMembers.remove(contactsList);
                                        }
                                        mOnlineMembers.add(contactsList);
                                    } else {
                                        if(mOnlineMembers.contains(contactsList)){
                                            mOnlineMembers.remove(contactsList);
                                        }
                                        if (mOfflineMembers.contains(contactsList)) {
                                            mOfflineMembers.remove(contactsList);
                                        }
                                        mOfflineMembers.add(contactsList);
                                    }
                                } else {
                                    mContactsList.remove(contactsList);
                                }

                                if(j[0] >= usersDataSnapshot.getChildrenCount() || (participantsKeys != null &&
                                        participantsKeys.size() != 0)){
                                    progressBar.setVisibility(View.GONE);

                                    if(isNewConversationPage || (participantsKeys != null && participantsKeys.size() != 0)){
                                        newConversationPageAdapter.update(mNewConvContactsList);
                                    } else if(blockedContacts){
                                        blockedContactsAdapter.update(mBlockedContactsList);
                                    } else {
                                        mContactsList = new ArrayList<>();
                                        mContactsList.addAll(mOnlineMembers);
                                        mContactsList.addAll(mOfflineMembers);
                                        contactsListAdapter.update(mContactsList);
                                    }
                                }

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                        }
                    }
                } else {
                    // Show No Contacts View here
                    progressBar.setVisibility(View.GONE);
                    if(blockedContacts){
                        mBlockedContactsList.clear();
                        blockedContactsAdapter.update(mBlockedContactsList);
                    } else if(isNewConversationPage || (participantsKeys != null && participantsKeys.size() != 0)) {
                        mNewConvContactsList.clear();
                        newConversationPageAdapter.update(mContactsList);
                    } else {
                        mContactsList.clear();
                        contactsListAdapter.update(mContactsList);
                    }

                    rootView.findViewById(R.id.message_layout).setVisibility(View.VISIBLE);
                    ImageView errorIcon = (ImageView) rootView.findViewById(R.id.error_icon);
                    errorIcon.setImageResource(R.drawable.ic_assignment_ind_black_24dp);
//                    errorIcon.setColorFilter(ContextCompat.getColor(mContext, R.color.icon_color),
//                            PorterDuff.Mode.SRC_ATOP);
                    TextView errorMessage = (TextView) rootView.findViewById(R.id.error_message);
                    if(blockedContacts){
                        errorMessage.setText(mContext.getResources().getString(R.string.no_blocked_contacts_available));
                    } else {
                        errorMessage.setText(mContext.getResources().getString(R.string.no_contacts_available));
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
    }

    public void filterUsers(String text){
        if (text.equals("")){
            mRecyclerView.setAdapter(newConversationPageAdapter);
        } else {
            usersFilterAdapter.update(new ArrayList<>(newConversationPageAdapter.getmConversationList()));
            usersFilterAdapter.filter(text);
            mRecyclerView.setAdapter(usersFilterAdapter);
        }
    }

    public void unBlockSelectedMember(final String userId){

        chatMembersDb.orderByChild(selfUid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                boolean isChatRoomExist = false;

                for (DataSnapshot groupDetails : dataSnapshot.getChildren()) {

                    if(groupDetails.getChildrenCount() == 2 && groupDetails.hasChild(userId) &&
                            groupDetails.hasChild(selfUid)){

                        isChatRoomExist = true;

                        String duplicateChatId = groupDetails.child(selfUid).child("duplicateChatId").getValue(String.class);

                        duplicateChatRef.child(duplicateChatId).removeValue();

                        HashMap<String, Object> chatMembersUpdates = new HashMap<>();
                        chatMembersUpdates.put("isActive", "1");
                        chatMembersUpdates.put("duplicateChatId", null);

                        chatMembersDb.child(groupDetails.getKey()).child(selfUid).updateChildren(chatMembersUpdates);


                        // Remove the user from BlockedList table
                        friendsDb.child(userId).removeValue();
                        break;
                    }
                }

                // Check if there is no chat room exist for this user, then just remove entry from block list table
                if(!isChatRoomExist){
                    friendsDb.child(userId).removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // todo send an API request to unblock the same user in socialEngine as well

    }

}
