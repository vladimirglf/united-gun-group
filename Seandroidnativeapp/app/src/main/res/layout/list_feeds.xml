<?xml version="1.0" encoding="utf-8"?>
<!--
  ~   Copyright (c) 2016 BigStep Technologies Private Limited.
  ~
  ~   You may not use this file except in compliance with the
  ~   SocialEngineAddOns License Agreement.
  ~   You may obtain a copy of the License at:
  ~   https://www.socialengineaddons.com/android-app-license
  ~   The full copyright and license information is also mentioned
  ~   in the LICENSE file that was distributed with this
  ~   source code.
  -->

<android.support.v7.widget.CardView
    android:id="@+id/card"
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginLeft="@dimen/element_spacing_small"
    android:layout_marginStart="@dimen/element_spacing_small"
    android:layout_marginRight="@dimen/element_spacing_small"
    android:layout_marginEnd="@dimen/element_spacing_small"
    app:cardBackgroundColor="?cardItemBackgroundColor"
    app:cardCornerRadius="1dp"
    app:cardElevation="1dp">

    <LinearLayout
        android:id="@+id/activityFeedBlock"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical"
        android:background="?selectableItemBackground"
        android:focusable="true"
        android:paddingTop="@dimen/element_spacing_small"
        app:ignorePadding="true">

        <RelativeLayout
            android:id="@+id/profile_container"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="@dimen/margin_6dp"
            android:paddingEnd="0dp"
            android:paddingLeft="@dimen/element_spacing_normal"
            android:paddingRight="0dp"
            android:paddingStart="@dimen/element_spacing_normal">

            <com.unitedgungroup.mobiapp.classes.common.ui.BezelImageView
                android:id="@+id/profile_image"
                android:layout_width="@dimen/icon_size_status_profile_image"
                android:layout_height="@dimen/icon_size_status_profile_image"
                android:layout_centerVertical="true"
                android:layout_marginBottom="@dimen/element_spacing_small"
                android:layout_marginEnd="@dimen/element_spacing_small"
                android:layout_marginRight="@dimen/element_spacing_small"
                android:contentDescription="@string/image_caption"
                android:scaleType="centerCrop"
                android:clickable="true"
                android:focusable="true"
                app:maskDrawable="@drawable/circle_mask"
                app:borderDrawable="@drawable/circle_border"/>

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_centerVertical="true"
                android:layout_marginLeft="@dimen/element_spacing_normal"
                android:layout_marginStart="@dimen/element_spacing_normal"
                android:layout_toEndOf="@id/profile_image"
                android:layout_toRightOf="@id/profile_image"
                android:layout_toLeftOf="@+id/feed_menu"
                android:layout_toStartOf="@+id/feed_menu"
                android:orientation="vertical">

                <TextView
                    android:id="@+id/feed_title"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:textSize="@dimen/body_default_font_size"
                    android:layout_marginEnd="@dimen/element_spacing_small"
                    android:layout_marginRight="@dimen/element_spacing_small"
                    android:orientation="horizontal"
                    android:textColor="@color/black"
                    android:maxLines="4"
                    android:ellipsize="end"/>

                <com.unitedgungroup.mobiapp.classes.common.ui.SelectableTextView
                    android:id="@+id/feed_time"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:paddingTop="@dimen/element_spacing_xsmall"
                    android:textAppearance="?android:textAppearanceSmall"
                    tools:textSize="@dimen/text_size_extra_small"
                    android:maxLines="1" />
            </LinearLayout>

            <com.unitedgungroup.mobiapp.classes.common.ui.ActionIconButton
                android:id="@+id/feed_menu"
                style="?cardActionButtonStyle"
                android:layout_width="@dimen/element_size_normal"
                android:layout_height="wrap_content"
                android:layout_alignParentEnd="true"
                android:layout_alignParentRight="true"
                android:layout_centerVertical="true"
                android:layout_marginBottom="@dimen/element_spacing_normal"
                android:layout_marginLeft="@dimen/element_spacing_normal"
                android:layout_marginRight="@dimen/element_spacing_normal"
                android:layout_marginStart="@dimen/element_spacing_normal"
                android:layout_marginEnd="@dimen/element_spacing_normal"
                android:color="?android:textColorTertiary"
                android:focusable="false"
                android:src="@drawable/ic_item_overflow" />

        </RelativeLayout>

        <github.ankushsachdeva.emojicon.EmojiconTextView
            android:id="@+id/feed_body"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:autoLink="web"
            android:layout_marginBottom="@dimen/element_spacing_small"
            android:layout_marginTop="@dimen/element_spacing_normal"
            android:paddingLeft="@dimen/element_spacing_normal"
            android:paddingRight="@dimen/element_spacing_normal"
            android:paddingStart="@dimen/element_spacing_normal"
            android:paddingEnd="@dimen/element_spacing_normal"
            android:textIsSelectable="true"
            android:focusable="true"
            android:focusableInTouchMode="true"
            android:textAppearance="?android:attr/textAppearanceSmall"
            android:textColor="?android:attr/textColorPrimary"
            android:visibility="gone"/>

        <RelativeLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:visibility="gone"
            android:background="@color/grey_light"
            android:id="@+id/feed_edit_layout">

            <github.ankushsachdeva.emojicon.EmojiconEditText
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_margin="@dimen/margin_10dp"
                android:layout_marginEnd="@dimen/margin_10dp"
                android:layout_marginStart="@dimen/margin_10dp"
                android:paddingBottom="@dimen/padding_10dp"
                android:paddingTop="@dimen/padding_10dp"
                android:paddingLeft="@dimen/padding_10dp"
                android:paddingStart="@dimen/padding_10dp"
                android:paddingEnd="@dimen/padding_30dp"
                android:paddingRight="@dimen/padding_30dp"
                android:textAppearance="?android:attr/textAppearanceSmall"
                android:textColor="?android:attr/textColorPrimary"
                android:background="@color/white"
                android:id="@+id/feed_body_edit_text" />

            <TextView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_alignParentRight="true"
                android:layout_alignParentEnd="true"
                android:padding="@dimen/padding_15dp"
                android:paddingStart="@dimen/padding_15dp"
                android:paddingEnd="@dimen/padding_15dp"
                style="@style/TitleLargeView"
                android:id="@+id/emojiIcon">

            </TextView>

            <com.unitedgungroup.mobiapp.classes.common.ui.BaseButton
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:id="@+id/edit_feed_submit"
                android:paddingEnd="@dimen/padding_5dp"
                android:paddingStart="@dimen/padding_5dp"
                android:padding="@dimen/padding_5dp"
                android:layout_below="@id/feed_body_edit_text"
                android:layout_alignParentLeft="true"
                android:layout_alignParentStart="true"
                android:textColor="@color/textColorPrimary"
                android:textAlignment="center"
                app:cornerRadius="@dimen/base_button_default_corner_radius"
                android:layout_marginRight="@dimen/login_button_left_right_margin"
                android:layout_marginEnd="@dimen/login_button_left_right_margin"
                android:layout_marginLeft="@dimen/login_button_top_bottom_padding"
                android:layout_marginStart="@dimen/login_button_top_bottom_padding"
                android:layout_marginBottom="@dimen/margin_10dp"
                app:normalStateColor="@color/colorPrimary"
                style="@style/BodyMediumView"
                android:text="@string/feed_edit_button_text"/>

            <com.unitedgungroup.mobiapp.classes.common.ui.BaseButton
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:paddingEnd="@dimen/padding_5dp"
                android:paddingStart="@dimen/padding_5dp"
                android:padding="@dimen/padding_5dp"
                android:layout_below="@id/feed_body_edit_text"
                android:id="@+id/edit_feed_cancel"
                android:layout_toRightOf="@+id/edit_feed_submit"
                android:layout_toEndOf="@+id/edit_feed_submit"
                android:textColor="@color/textColorPrimary"
                android:textAlignment="center"
                app:cornerRadius="@dimen/base_button_default_corner_radius"
                android:layout_marginLeft="@dimen/margin_10dp"
                android:layout_marginStart="@dimen/margin_10dp"
                android:layout_marginBottom="@dimen/margin_10dp"
                app:normalStateColor="@color/colorPrimary"
                style="@style/BodyMediumView"
                android:text="@string/cancel"/>

        </RelativeLayout>


        <LinearLayout
            android:id="@+id/attachment_view"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical"
            android:visibility="gone"
            android:layout_marginBottom="@dimen/element_spacing_normal"
            android:layout_marginTop="@dimen/element_spacing_normal"
            android:layout_marginLeft="@dimen/element_spacing_normal"
            android:layout_marginStart="@dimen/element_spacing_normal"
            android:layout_marginRight="@dimen/element_spacing_normal"
            android:layout_marginEnd="@dimen/element_spacing_normal"
            android:background="@drawable/custom_border">

            <RelativeLayout
                android:layout_width="match_parent"
                android:id="@+id/attachment_preview_layout"
                android:visibility="gone"
                android:layout_height="@dimen/feed_attachment_image_height">
                <ImageView
                    android:id="@+id/attachment_preview"
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:scaleType="fitXY"
                    android:visibility="gone"
                    android:contentDescription="@string/image_caption"/>
                <ImageView
                    android:id="@+id/play_button"
                    android:layout_width="@dimen/play_button_width"
                    android:layout_height="@dimen/play_button_width"
                    android:layout_centerInParent="true"
                    android:src="@drawable/ic_play_normal"
                    android:shadowDx="1"
                    android:shadowDy="1"
                    android:shadowRadius="2"
                    android:visibility="gone"/>
            </RelativeLayout>

            <com.unitedgungroup.mobiapp.classes.common.ui.NameView
                android:id="@+id/attachment_title"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:paddingLeft="@dimen/element_spacing_normal"
                android:paddingStart="@dimen/element_spacing_normal"
                android:paddingRight="@dimen/element_spacing_normal"
                android:paddingEnd="@dimen/element_spacing_normal"
                android:layout_marginBottom="@dimen/element_spacing_normal"
                android:layout_marginTop="@dimen/element_spacing_normal"
                android:textStyle="bold"
                android:textSize="@dimen/body_default_font_size"
                app:nv_primaryTextColor="?android:textColorPrimary"
                app:nv_primaryTextStyle="bold"
                android:visibility="gone"
                app:nv_secondaryTextColor="?android:textColorSecondary" />

            <com.unitedgungroup.mobiapp.classes.common.ui.SelectableTextView
                android:id="@+id/attachment_body"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginBottom="@dimen/element_spacing_normal"
                android:layout_marginTop="@dimen/element_spacing_normal"
                android:paddingLeft="@dimen/element_spacing_normal"
                android:paddingStart="@dimen/element_spacing_normal"
                android:paddingRight="@dimen/element_spacing_normal"
                android:paddingEnd="@dimen/element_spacing_normal"
                android:textAppearance="?android:attr/textAppearanceSmall"
                android:textColor="?android:attr/textColorPrimary"
                android:visibility="gone" />

            <com.unitedgungroup.mobiapp.classes.common.ui.SelectableTextView
                android:id="@+id/attachment_url_view"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginBottom="@dimen/element_spacing_normal"
                android:layout_marginTop="@dimen/element_spacing_normal"
                android:paddingLeft="@dimen/element_spacing_normal"
                android:paddingStart="@dimen/element_spacing_normal"
                android:paddingRight="@dimen/element_spacing_normal"
                android:paddingEnd="@dimen/element_spacing_normal"
                android:textAppearance="?android:attr/textAppearanceSmall"
                android:textColor="@color/body_text_3"
                android:visibility="gone"
                android:singleLine="true"
                android:maxLines="1"
                android:ellipsize="end" />

        </LinearLayout>

        <LinearLayout
            android:id="@+id/attachment_view_link_music"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="horizontal"
            android:visibility="gone"
            android:layout_marginBottom="@dimen/margin_5dp"
            android:layout_marginTop="@dimen/margin_5dp"
            android:layout_marginLeft="@dimen/element_spacing_normal"
            android:layout_marginStart="@dimen/element_spacing_normal"
            android:layout_marginRight="@dimen/element_spacing_normal"
            android:layout_marginEnd="@dimen/element_spacing_normal"
            android:background="@drawable/custom_border">

            <ImageView
                android:id="@+id/attachment_preview_link_music"
                android:layout_width="@dimen/attachment_small_image_size"
                android:layout_height="@dimen/attachment_small_image_size"
                android:scaleType="fitCenter"
                android:paddingLeft="@dimen/element_spacing_normal"
                android:paddingStart="@dimen/element_spacing_normal"
                android:paddingRight="@dimen/element_spacing_normal"
                android:paddingEnd="@dimen/element_spacing_normal"
                android:visibility="gone"
                android:contentDescription="@string/image_caption"/>

            <LinearLayout
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="center_vertical"
                android:orientation="vertical">

            <com.unitedgungroup.mobiapp.classes.common.ui.NameView
                android:id="@+id/attachment_title_link_music"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:ellipsize="end"
                android:layout_marginEnd="@dimen/element_spacing_normal"
                android:layout_marginRight="@dimen/element_spacing_normal"
                android:textStyle="bold"
                android:textSize="@dimen/body_default_font_size"
                app:nv_primaryTextColor="?android:textColorPrimary"
                app:nv_primaryTextStyle="bold"
                android:visibility="gone"
                app:nv_secondaryTextColor="?android:textColorSecondary"
                android:maxLines="1" />

            <TextView
                android:id="@+id/attachment_body_link_music"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:maxLines="2"
                android:ellipsize="end"
                android:layout_marginTop="@dimen/margin_2dp"
                android:layout_marginBottom="@dimen/element_spacing_normal"
                android:layout_marginEnd="@dimen/element_spacing_normal"
                android:layout_marginRight="@dimen/element_spacing_normal"
                android:textAppearance="?android:attr/textAppearanceSmall"
                android:textColor="?android:attr/textColorPrimary"
                android:visibility="gone" />
            </LinearLayout>

        </LinearLayout>

        <ImageView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:visibility="gone"
            android:adjustViewBounds="true"
            android:scaleType="fitXY"
            android:src="@color/grey_light"
            android:id="@+id/singleAlbumPhoto"/>

        <com.google.android.gms.maps.MapView
            android:layout_width="match_parent"
            android:layout_height="@dimen/feed_attachment_image_height"
            android:id="@+id/map_view"
            android:visibility="gone">
        </com.google.android.gms.maps.MapView>

        <!-- Recycler view to show Images in Feeds -->
        <android.support.v7.widget.RecyclerView
            android:id="@+id/image_preview"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="@dimen/margin_5dp">

        </android.support.v7.widget.RecyclerView>

        <TextView
            android:id="@+id/hashTag_view"
            android:layout_width="match_parent"
            android:textSize="@dimen/body_default_font_size"
            android:textStyle="bold"
            android:layout_height="wrap_content"
            android:padding="@dimen/element_spacing_normal"
            android:paddingStart="@dimen/element_spacing_normal"
            android:paddingEnd="@dimen/element_spacing_normal"/>

        <RelativeLayout
            android:id="@+id/counts_container"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="?selectableItemBackground"
            android:padding="@dimen/element_spacing_normal"
            android:paddingStart="@dimen/element_spacing_normal"
            android:paddingEnd="@dimen/element_spacing_normal"
            android:gravity="center_vertical"
            android:splitMotionEvents="false">

            <LinearLayout
                android:layout_width="wrap_content"
                android:id="@+id/popularReactionIcons"
                android:orientation="horizontal"
                android:layout_alignParentLeft="true"
                android:layout_alignParentStart="true"
                android:visibility="gone"
                android:layout_height="wrap_content">

            </LinearLayout>

            <com.unitedgungroup.mobiapp.classes.common.ui.ThemedTextView
                android:id="@+id/like_count"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_toRightOf="@+id/popularReactionIcons"
                android:layout_toEndOf="@+id/popularReactionIcons"
                android:layout_marginLeft="@dimen/margin_2dp"
                android:layout_marginStart="@dimen/margin_2dp"
                android:text="@string/like_text"
                android:layout_marginTop="@dimen/margin_1dp"
                android:textAppearance="?android:textAppearanceSmall"
                android:textColor="?android:textColorSecondary"
                android:maxLines="1" />

            <com.unitedgungroup.mobiapp.classes.common.ui.ThemedTextView
                android:id="@+id/comment_count"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="@string/comment_count_text"
                android:layout_marginTop="@dimen/margin_1dp"
                android:textAppearance="?android:textAppearanceSmall"
                android:textColor="?android:textColorSecondary"
                android:maxLines="1" />

            <View android:layout_height=".5dp"
                android:layout_below="@+id/popularReactionIcons"
                android:layout_marginTop="@dimen/margin_5dp"
                android:layout_width="match_parent"
                android:visibility="gone"
                android:id="@+id/counts_saperator"
                android:background="@color/light_gray" />

        </RelativeLayout>

        <LinearLayout
            android:id="@+id/feedFooterMenusBlock"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_gravity="center"
            android:layout_marginTop="@dimen/element_spacing_minus_msmall"
            android:orientation="horizontal">

            <include layout="@layout/layout_like_comment" />

        </LinearLayout>

    </LinearLayout>
</android.support.v7.widget.CardView>
