/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */

package com.unitedgungroup.mobiapp.classes.modules.user;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.adapters.BrowseMemberAdapter;
import com.unitedgungroup.mobiapp.classes.common.dialogs.AlertDialogWithAction;
import com.unitedgungroup.mobiapp.classes.common.ui.BaseButton;
import com.unitedgungroup.mobiapp.classes.common.ui.SelectableTextView;
import com.unitedgungroup.mobiapp.classes.common.utils.GlobalFunctions;
import com.unitedgungroup.mobiapp.classes.common.utils.PreferencesUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.UrlUtil;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;
import com.unitedgungroup.mobiapp.classes.core.ConstantVariables;
import com.unitedgungroup.mobiapp.classes.modules.peopleSuggestion.GetContactsAsync;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.utils.SnackbarUtils;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnResponseListener;
import com.unitedgungroup.mobiapp.classes.modules.user.profile.userProfile;
import com.unitedgungroup.mobiapp.classes.common.utils.BrowseListItems;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class BrowseMemberFragment extends Fragment implements AbsListView.OnScrollListener,
        AdapterView.OnItemClickListener, GetContactsAsync.OnContactLoadResponseListener {

    private String mBrowseMemberUrl;
    private View mRootView, footerView;
    private Context mContext;
    private int mLoadingPageNo = 1, canAddToList = 0, isSiteMember;
    private AppConstant mAppConst;
    private ListView mMemeresListView;
    private SelectableTextView mMembersCountInfo;
    private View mSeparator;
    private LinearLayout llContactInfoView;
    private JSONObject mBody;
    private JSONArray mResponseArray;
    private int mTotalMembers;
    private BrowseMemberAdapter mBrowseMemberAdapter;
    private List<BrowseListItems> mBrowseListItems;
    private boolean isMemberFriends = false, isContactTab = false, isSearchTab = false,
            isOutGoingTab = false, isFindFriends = false, isVisibleToUser = false;;
    private int mUserId;
    private boolean isLoading = false;
    private HashMap<String, String> searchParams = new HashMap<>();
    private Map<String, String> mPostParams = new HashMap<>();
    private AlertDialogWithAction mAlertDialogWithAction;
    private String userTitle;
    private AppCompatActivity mActivity;


    public BrowseMemberFragment() {
        // Required empty public constructor
    }

    public static BrowseMemberFragment newInstance(Bundle bundle) {
        BrowseMemberFragment fragment = new BrowseMemberFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible && !isVisibleToUser && mContext != null) {
            makeRequest();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mContext = getContext();
        mActivity = (AppCompatActivity) getActivity();
        mAppConst = new AppConstant(mContext);
        mAlertDialogWithAction = new AlertDialogWithAction(mContext);

        mBrowseListItems = new ArrayList<>();

        Bundle bundle = getArguments();
        if (bundle != null) {
            isMemberFriends = bundle.getBoolean("isMemberFriends");
            mUserId = bundle.getInt("user_id");
            userTitle = bundle.getString(ConstantVariables.CONTENT_TITLE);
            isContactTab = bundle.getBoolean("contact_tab");
            isSearchTab = bundle.getBoolean("search_tab");
            isOutGoingTab = bundle.getBoolean("outgoing_tab");
            isFindFriends = bundle.getBoolean("isFindFriends");
        }

        mBrowseMemberUrl = getMembersListUrl() + "&page=" + mLoadingPageNo;

        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_browse_member, container, false);
        footerView = CustomViews.getFooterView(inflater);
        mMemeresListView = (ListView) mRootView.findViewById(R.id.membersList);
        mMembersCountInfo = (SelectableTextView) mRootView.findViewById(R.id.memberCountInfo);
        mSeparator = mRootView.findViewById(R.id.saperator);
        mBrowseMemberAdapter = new BrowseMemberAdapter(mContext, R.layout.list_members,
                mBrowseListItems, isMemberFriends);

        mMemeresListView.setAdapter(mBrowseMemberAdapter);

        mMemeresListView.setOnItemClickListener(this);
        mMemeresListView.setOnScrollListener(this);
        ViewCompat.setNestedScrollingEnabled(mMemeresListView, true);


        /**
         * isMemberFriends will be true when Friends tab will be loaded from user profile page.
         * Searching members will not be working when Friends tab will be clicked from user profile page.
         */
        if (!isMemberFriends && !isContactTab && !isSearchTab && !isOutGoingTab) {
            if (getArguments() != null) {

                Set<String> searchArgumentSet = getArguments().keySet();

                for (String key : searchArgumentSet) {
                    String value = getArguments().getString(key);

                    if (value != null && !value.isEmpty()) {
                        searchParams.put(key, value);
                    }
                }

                if (searchParams != null && searchParams.size() != 0) {
                    mBrowseMemberUrl = mAppConst.buildQueryString(mBrowseMemberUrl, searchParams);
                }
            }
        }

        // If it is browser member request then send the request immediately.
        if (!isOutGoingTab && !isSearchTab && !isContactTab && !isMemberFriends) {
            makeRequest(mBrowseMemberUrl);
        }
        return mRootView;
    }

    public void makeRequest() {
        if (isContactTab) {
            //Checking for the contact tab if the contacts are already synced or not.
            //If already synced then add contacts directly otherwise show the contact sync info view.
            if (PreferencesUtils.isContactSynced(mContext)) {
                addContactsWithApp();
            } else {
                createContactSyncInfoView();
            }
        } else if (isOutGoingTab) {
            loadOutGoingMember(mBrowseMemberUrl, false);
        } else {
            makeRequest(mBrowseMemberUrl);
        }
    }

    private void makeRequest(String url) {

        mLoadingPageNo = 1;

        mRootView.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);

        mAppConst.getJsonResponseFromUrl(url, new OnResponseListener() {
            @Override
            public void onTaskCompleted(JSONObject jsonObject) {
                mRootView.findViewById(R.id.progressBar).setVisibility(View.GONE);
                isVisibleToUser = true;

                if (jsonObject != null && jsonObject.length() != 0) {
                    try {
                        mBrowseListItems.clear();
                        mBody = jsonObject;
                        mTotalMembers = mBody.optInt("totalItemCount");
                        isSiteMember = mBody.optInt("isSitemember");

                        if (isMemberFriends) {
                            canAddToList = mBody.optInt("canAddToList");
                            mResponseArray = mBody.optJSONArray("friends");
                            if (!isFindFriends) {
                                if (mTotalMembers != 0) {
                                    mActivity.getSupportActionBar().setTitle(
                                            String.format("%s (%d): %s", getResources().getString(
                                                    R.string.action_bar_title_friend), mTotalMembers, userTitle));
                                } else {
                                    mActivity.getSupportActionBar().setTitle(String.format("%s: %s", getResources().getString(R.string.
                                            action_bar_title_friend), userTitle));
                                }
                            }
                        } else{
                            mResponseArray = mBody.optJSONArray("response");
                        }

                        addDataToList(mResponseArray);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                mRootView.findViewById(R.id.progressBar).setVisibility(View.GONE);
                SnackbarUtils.displaySnackbar(mRootView, message);
            }
        });

    }

    /**
     * Method to add data to the list view.
     *
     * @param mResponseArray Response array
     */
    public void addDataToList(JSONArray mResponseArray) {

        isVisibleToUser = true;
        if (mResponseArray != null && mResponseArray.length() > 0) {
            mMembersCountInfo.setVisibility(View.VISIBLE);
            String membersText = mContext.getResources().getQuantityString(R.plurals.member_text,
                    mTotalMembers);
            mMembersCountInfo.setText(String.format(
                    mContext.getResources().getString(R.string.browse_member_count_text_format), mTotalMembers,
                    membersText, mContext.getResources().getString(R.string.browse_member_count_text)
            ));
            mSeparator.setVisibility(View.VISIBLE);
            for (int i = 0; i < mResponseArray.length(); i++) {

                JSONObject memberInfoObject = mResponseArray.optJSONObject(i);
                String displayName = memberInfoObject.optString("displayname");
                String imageUrl = memberInfoObject.optString("image_profile");
                JSONObject menusObject = memberInfoObject.optJSONObject("menus");
                int userId = memberInfoObject.optInt("user_id");
                int isVerified = memberInfoObject.optInt("isVerified");
                if (isSiteMember == 1) {
                    int mutualFriendCount = memberInfoObject.optInt("mutualFriendCount");
                    int age = memberInfoObject.optInt("age");
                    int verified = memberInfoObject.optInt("verified");
                    int isMemberOnline = memberInfoObject.optInt("memberStatus");
                    String location = memberInfoObject.optString("location");
                    mBrowseListItems.add(new BrowseListItems(isSiteMember, userId, imageUrl, displayName,
                            menusObject, mutualFriendCount, age, verified, location, isMemberOnline, canAddToList, isVerified));
                } else {
                    mBrowseListItems.add(new BrowseListItems(isSiteMember, userId, imageUrl, displayName,
                            menusObject, 0, 0, 0, null, 0, canAddToList, isVerified));
                }
            }
        } else {
            showError();
        }
        mBrowseMemberAdapter.notifyDataSetChanged();
    }

    /**
     * Method to show error.
     */
    public void showError() {
        mRootView.findViewById(R.id.message_layout).setVisibility(View.VISIBLE);
        TextView errorIcon = (TextView) mRootView.findViewById(R.id.error_icon);
        SelectableTextView errorMessage = (SelectableTextView) mRootView.findViewById
                (R.id.error_message);
        errorIcon.setTypeface(GlobalFunctions.getFontIconTypeFace(mContext));
        errorIcon.setText("\uf007");
        if (isOutGoingTab) {
            errorMessage.setText(mContext.getResources().getString(R.string.no_outgoing_request));
        } else {
            errorMessage.setText(mContext.getResources().getString(R.string.no_member_to_display));
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        int limit = firstVisibleItem + visibleItemCount;
        String url;

        if (limit == totalItemCount && !isLoading) {

            if (limit >= AppConstant.LIMIT && (AppConstant.LIMIT * mLoadingPageNo) <
                    mTotalMembers) {

                CustomViews.addFooterView(mMemeresListView, footerView);
                mLoadingPageNo += 1;

                url = getMembersListUrl() + "&page=" + mLoadingPageNo;

                isLoading = true;
                // Adding Search Params in the scrolling url
                if (searchParams != null && searchParams.size() != 0) {
                    url = mAppConst.buildQueryString(url, searchParams);
                }
                if (isOutGoingTab) {
                    loadOutGoingMember(url, true);
                } else if (isContactTab) {
                    addContactsWithApp();
                } else {
                    loadMoreMembers(url);
                }
            }

        }
    }

    public void loadMoreMembers(String url) {

        mAppConst.getJsonResponseFromUrl(url, new OnResponseListener() {
            @Override
            public void onTaskCompleted(JSONObject jsonObject) {
                mBody = jsonObject;
                CustomViews.removeFooterView(mMemeresListView, footerView);
                try {
                    if (isMemberFriends)
                        mResponseArray = mBody.getJSONArray("friends");
                    else
                        mResponseArray = mBody.getJSONArray("response");
                    addDataToList(mResponseArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(mContext.getApplicationContext(),
                            mContext.getResources().getString(R.string.no_data_available),
                            Toast.LENGTH_LONG).show();
                }
                isLoading = false;
            }

            @Override
            public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                SnackbarUtils.displaySnackbar(mRootView, message);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        if (mMemeresListView != null) {
            mMemeresListView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        BrowseListItems clickedMember = mBrowseListItems.get(i);
        int userId = clickedMember.getmUserId();
        Intent userIntent = new Intent(mContext, userProfile.class);
        userIntent.putExtra("user_id", userId);
        ((Activity) mContext).startActivityForResult(userIntent, ConstantVariables.USER_PROFILE_CODE);
        ((Activity) mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ConstantVariables.READ_CONTACTS:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, proceed to the normal flow.
                    addContactsWithApp();

                } else {
                    // If user deny the permission popup
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.READ_CONTACTS)) {

                        // Show an explanation to the user, After the user
                        // sees the explanation, try again to request the permission.

                        mAlertDialogWithAction.showDialogForAccessPermission(Manifest.permission.READ_CONTACTS,
                                ConstantVariables.READ_CONTACTS);
                    } else {
                        // If user pressed never ask again on permission popup
                        // show snackbar with open app info button
                        // user can revoke the permission from Permission section of App Info.
                        SnackbarUtils.displaySnackbarOnPermissionResult(mContext, mRootView,
                                ConstantVariables.READ_CONTACTS);
                    }
                }
                break;
        }
    }

    /**
     * Method to send out going member request on server then load the data into list.
     */
    public void loadOutGoingMember(String url, final boolean isLoadMoreRequest) {
        mPostParams.clear();
        mPostParams.put("membershipType", "cancel_request");

        // Showing progress bar when it is loading outgoing member request.
        if (!isLoadMoreRequest) {
            mLoadingPageNo = 1;
            mRootView.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
        }
        mAppConst.postJsonResponseForUrl(url, mPostParams, new OnResponseListener() {
            @Override
            public void onTaskCompleted(JSONObject jsonObject) {

                // Hiding footer view when it is load more member request.
                if (isLoadMoreRequest) {
                    isLoading = false;
                    CustomViews.removeFooterView(mMemeresListView, footerView);
                } else {
                    mBrowseListItems.clear();
                }
                mRootView.findViewById(R.id.progressBar).setVisibility(View.GONE);
                mTotalMembers = jsonObject.optInt("totalItemCount");
                mResponseArray = jsonObject.optJSONArray("users");
                addDataToList(mResponseArray);
            }

            @Override
            public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                mRootView.findViewById(R.id.progressBar).setVisibility(View.GONE);
                SnackbarUtils.displaySnackbar(mRootView, message);
            }
        });
    }

    /**
     * Method to create contact sync info view.
     */
    public void createContactSyncInfoView() {
        isVisibleToUser = true;
        PreferencesUtils.setContactSyncedInfo(mContext, false);
        llContactInfoView = new LinearLayout(mContext);
        llContactInfoView.removeAllViews();
        llContactInfoView.setOrientation(LinearLayout.VERTICAL);
        llContactInfoView.setId(R.id.add_description);

        // Layout params for linear layout.
        LinearLayout.LayoutParams llParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        llParams.gravity = Gravity.CENTER;
        llContactInfoView.setLayoutParams(llParams);
        llParams.setMargins(mContext.getResources().getDimensionPixelSize(R.dimen.margin_10dp),
                mContext.getResources().getDimensionPixelSize(R.dimen.margin_5dp),
                mContext.getResources().getDimensionPixelSize(R.dimen.margin_10dp),
                mContext.getResources().getDimensionPixelSize(R.dimen.margin_5dp));

        // Adding Contact add title view.
        try {
            if (!mAppConst.isLoggedOutUser() && PreferencesUtils.getUserDetail(mContext) != null) {
                JSONObject jsonObject = new JSONObject(PreferencesUtils.getUserDetail(mContext));
                String userName = jsonObject.optString("displayname");
                TextView contactTitle = new TextView(mContext);
                contactTitle.setLayoutParams(llParams);
                contactTitle.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                contactTitle.setTextSize(mContext.getResources().getDimensionPixelSize(R.dimen.body_default_font_size));
                contactTitle.setGravity(Gravity.CENTER);
                contactTitle.setText(userName);
                llContactInfoView.addView(contactTitle);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Adding contact's demo image.
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                mContext.getResources().getDimensionPixelSize(R.dimen.small_image_art_width),
                mContext.getResources().getDimensionPixelSize(R.dimen.small_image_art_width));
        ImageView imageView = new ImageView(mContext);
        imageView.setImageResource(R.drawable.contact_tab);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        layoutParams.gravity = Gravity.CENTER;
        layoutParams.setMargins(0, mContext.getResources().getDimensionPixelSize(R.dimen.margin_10dp), 0, 0);
        imageView.setLayoutParams(layoutParams);
        llContactInfoView.addView(imageView);

        // Adding subtitle info.
        TextView subTitle = new TextView(mContext);
        subTitle.setGravity(Gravity.CENTER);
        subTitle.setLayoutParams(llParams);
        subTitle.setTextColor(ContextCompat.getColor(mContext, R.color.black));
        subTitle.setTextSize(mContext.getResources().getDimensionPixelSize(R.dimen.caption_font_size));
        subTitle.setText(mContext.getResources().getString(R.string.better_with_friends));
        llContactInfoView.addView(subTitle);

        // Adding brief description of contact tab.
        TextView info = new TextView(mContext);
        info.setGravity(Gravity.CENTER);
        info.setText(mContext.getResources().getString(R.string.see_who) + " "
                + mContext.getResources().getString(R.string.app_name) + " "
                + mContext.getResources().getString(R.string.upload_contacts));
        info.setLayoutParams(llParams);
        llContactInfoView.addView(info);

        // Adding contact sync start button
        BaseButton contactAddButton = new BaseButton(mContext);
        contactAddButton.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
        contactAddButton.setText(mContext.getResources().getString(R.string.get_started));
        contactAddButton.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        contactAddButton.setId(R.id.add_description);
        contactAddButton.setBackgroundResource(R.drawable.background_app_theme_color);
        contactAddButton.setLayoutParams(llParams);
        contactAddButton.setPadding(0, mContext.getResources().getDimensionPixelSize(R.dimen.padding_20dp),
                0, mContext.getResources().getDimensionPixelSize(R.dimen.padding_20dp));
        llContactInfoView.addView(contactAddButton);
        RelativeLayout mainView = (RelativeLayout) mRootView.findViewById(R.id.main_content);
        mainView.addView(llContactInfoView);

        // Apply click listener on add contact button.
        contactAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mAppConst.checkManifestPermission(Manifest.permission.READ_CONTACTS)) {
                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS},
                            ConstantVariables.READ_CONTACTS);
                } else {
                    addContactsWithApp();
                }
            }
        });

    }

    /**
     * Method to Send contact on server then add the member's into list which are not in friend list.
     */
    public void addContactsWithApp() {
        if (mLoadingPageNo == 1) {
            isVisibleToUser = true;
            if (llContactInfoView != null) {
                llContactInfoView.setVisibility(View.GONE);
            }
            mRootView.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
        }
        GetContactsAsync getContactsAsync = new GetContactsAsync(mContext, "contact", mLoadingPageNo);
        getContactsAsync.setOnResponseListener(this);
        getContactsAsync.execute();
    }

    @Override
    public void onContactLoaded(JSONObject jsonObject, boolean isRequestSuccessful) {
        try {
            mRootView.findViewById(R.id.progressBar).setVisibility(View.GONE);
            PreferencesUtils.setContactSyncedInfo(mContext, true);
            // Hiding progress bar from footer when loading more members.
            if (mLoadingPageNo > 1) {
                CustomViews.removeFooterView(mMemeresListView, footerView);
                isLoading = false;
            }
            if (jsonObject != null) {
                if (isRequestSuccessful) {
                    mTotalMembers = jsonObject.optInt("totalItemCount");
                    mResponseArray = jsonObject.optJSONArray("users");
                    addDataToList(mResponseArray);
                } else {
                    SnackbarUtils.displaySnackbar(mRootView, jsonObject.optString("message"));
                }
            } else {
                showError();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getMembersListUrl(){

        String url;
        if (isMemberFriends) {
            url = AppConstant.DEFAULT_URL + "user/get-friend-list?user_id=" + mUserId + "&limit=" + AppConstant.LIMIT;
        } else if (isOutGoingTab) {
            url = UrlUtil.MEMBERSHIP_DEFAULT_URL + "?limit=" + AppConstant.LIMIT;
        } else {
            url = AppConstant.DEFAULT_URL + "members?limit=" + AppConstant.LIMIT;
        }
        return url;
    }

}
