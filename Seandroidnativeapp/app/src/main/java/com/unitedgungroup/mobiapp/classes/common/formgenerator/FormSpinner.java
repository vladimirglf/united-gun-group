/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 *
 */

package com.unitedgungroup.mobiapp.classes.common.formgenerator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.activities.CreateNewEntry;
import com.unitedgungroup.mobiapp.classes.common.adapters.SpinnerAdapter;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.ui.SelectableTextView;
import com.unitedgungroup.mobiapp.classes.common.utils.BrowseListItems;
import com.unitedgungroup.mobiapp.classes.common.utils.CustomSpinner;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;
import com.unitedgungroup.mobiapp.classes.core.ConstantVariables;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FormSpinner extends FormWidget implements AdapterView.OnItemSelectedListener, CompoundButton.OnCheckedChangeListener, View.OnClickListener {
    protected JSONObject _options;
    AppConstant mAppConst;
    protected SelectableTextView _label;
    protected CustomSpinner _spinner;
    protected Map<String, String> _propmap, sortMap;
    protected SpinnerAdapter _adapter;
    protected String mCurrentSelectedOption,mLabel,TAG = "FormSpinner";
    Context mContext;
    protected LinearLayout linearLayout;
    protected TextView cancelTextView, addNewTextView;
    protected LinearLayout.LayoutParams layoutParams;

    JSONObject mSubCategory, mCategoryFields, mRepeatOccurences, mSubSubCategory, mJsonObject, mFormValuesJsonObject;
    FormActivity formActivity;
    FormWidget widget;
    ArrayList<FormWidget> _widgetsNew, mWidgets, mEditWidget;
    Map<String, FormWidget> _mapNew;
    Map<String,String> positionMap = new HashMap<>();
    String selectedType, fieldCategoryLevel, mAdvancedMemberWhatWhereWithinmile;
    int sposition = 0, eposition= 0, repeat_e_position = 0;
    int size = 0, defaultPosition;
    static int sPosition = 0, ePosition = 0, subSPosition = 0, subSubSPosition = 0;
    static boolean isCategoryProfileFields = false, isSubCategoryProfileFields = false,
            isSubSubCategoryProfileFields = false, hasSubCategory = false, hasSubSubCategory = false;
    static int repeat_s_position = 0;
    CheckBox isMonthlyTypeCheck;

    boolean isRemoveRepeatEvent = true, createForm, isCreateForm, isDefaultValueSet = false, isRemoveFields = true;
    boolean canChangeProfileFields = true, isFirstTimeLoadingFromEdit = true, isFirstTimeSubCategorySelection = true,
            isFirstTimeSubSubCategorySelection = true, isFirstTimeNoCateFieldSelection = true,
            isFirstTimeCateFieldSelection = true, isFirstTimeSelectedTypeCategory = true, mIsSpinnerWithIcon = false;

    int mSelectedItem = -1;
    public static Boolean sIsChangeCategory = false, sIsChangeSubCategory = false, sIsChangeOccurence = false;
    public static Boolean sIsRemoveCalled = false, sIsChangedMonthlyField = false, sIsEventRepeatTypeMonthly = false;
    public static int sEditPageLoadingTime = 0, sEditPageLoadingTimeRepeatEvent = 0;
    private String mWidgetProperty;


    public FormSpinner(Context context, String property, String defaultValue, JSONObject options, JSONObject subCategory,
                       JSONObject categoryFields, JSONObject repeatOccurences, String label, boolean hasValidator,
                       String selectedModule, boolean createForm, final ArrayList<FormWidget> _widgets,
                       Map<String, FormWidget> _map, JSONObject subSubCategory, JSONObject jsonObject, JSONObject formValues, String mAdvancedMemberWhatWhereWithinmile, boolean isSpinnerWithIcon) {
        super(context, property, hasValidator);
        mContext = context;
        _options = options;

        mLabel = label;
        this.createForm = isCreateForm = isRemoveRepeatEvent =  isRemoveFields = createForm;
        this.mIsSpinnerWithIcon = isSpinnerWithIcon;
        mCurrentSelectedOption = selectedModule;
        getWidgetPropertyName();
        mAppConst = new AppConstant(mContext);
        _label = new SelectableTextView(context);
        _label.setText(label);
        _label.setLayoutParams(FormActivity.defaultLayoutParams);

        _spinner = new CustomSpinner(context);
        _spinner.setLayoutParams(FormActivity.defaultLayoutParams);
        _spinner.setTag(property);

        _spinner.setSpinnerEventsListener(new CustomSpinner.OnSpinnerEventsListener() {
            @Override
            public void onSpinnerOpened() {
                mAppConst.hideKeyboard();
            }
        });

        switch (property){
            case "eventrepeat_id":
                _spinner.setId(R.id.eventrepeat_id);
                break;
            case "category_id":
                _spinner.setId(R.id.category_id);
                break;
            case "host_type_select":
                _spinner.setId(R.id.host_select);

                linearLayout = new LinearLayout(mContext);
                linearLayout.setLayoutParams(CustomViews.getWrapLayoutParams());

                linearLayout.setOrientation(LinearLayout.HORIZONTAL);

                _label = new SelectableTextView(context);
                _label.setText(label);
                _label.setLayoutParams(FormActivity.defaultLayoutParams);

                cancelTextView = new TextView(context);
                cancelTextView.setId(R.id.change_host);
                cancelTextView.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                cancelTextView.setLayoutParams(FormActivity.defaultLayoutParams);
                cancelTextView.setText("[ " + mContext.getResources().getString(R.string.cancel_dialogue_message)+ " ]");


                addNewTextView = new TextView(context);
                addNewTextView.setId(R.id.add_new_host);
                addNewTextView.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                addNewTextView.setLayoutParams(FormActivity.defaultLayoutParams);
                addNewTextView.setText("[ " + mContext.getResources().getString(R.string.add_new_text)+ " ]");


                addNewTextView.setOnClickListener(this);
                cancelTextView.setOnClickListener(this);

                linearLayout.addView(_label);
                linearLayout.addView(addNewTextView);
                linearLayout.addView(cancelTextView);
                break;

            case "main_channel_id":
                linearLayout = new LinearLayout(mContext);
                linearLayout.setLayoutParams(CustomViews.getWrapLayoutParams());

                linearLayout.setOrientation(LinearLayout.HORIZONTAL);

                _label = new SelectableTextView(context);
                _label.setText(label);
                _label.setLayoutParams(FormActivity.defaultLayoutParams);

                TextView tvCreateChannel = new TextView(context);
                tvCreateChannel.setId(R.id.create_channel);
                tvCreateChannel.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                tvCreateChannel.setLayoutParams(FormActivity.defaultLayoutParams);
                tvCreateChannel.setText("(" + mContext.getResources().getString(R.string.title_activity_create_new_channel)+ ")");
                tvCreateChannel.setOnClickListener(this);

                linearLayout.addView(_label);
                linearLayout.addView(tvCreateChannel);
                break;
        }

        mSubCategory = subCategory;
        mSubSubCategory = subSubCategory;
        mJsonObject = jsonObject;
        mFormValuesJsonObject = formValues;
        mCategoryFields = categoryFields;
        mRepeatOccurences = repeatOccurences;
        _widgetsNew = _widgets;
        mWidgets = new ArrayList<>();
        mEditWidget = new ArrayList<>();
        _mapNew = _map;
        formActivity = new FormActivity();
        this.mAdvancedMemberWhatWhereWithinmile = mAdvancedMemberWhatWhereWithinmile;

        String key;
        String name;

        JSONArray propertyNames = options.names();
        sortMap = new HashMap<>();

        ArrayList arrayList = new ArrayList();
        List<BrowseListItems> browseListItemsList = new ArrayList<>();

        for (int i = 0; i < options.length(); i++) {
            name = propertyNames.optString(i);
            key = options.optString(name);
            sortMap.put(key, name);
            arrayList.add(key);
        }

        // Todo Check this code for the client for whom this work has been done and uncommennt this
        if (mCurrentSelectedOption.equals("signup_fields") && property.contains("alias_country")) {
            Collections.sort(arrayList);
        }

        _propmap = new HashMap<>();
        if (mIsSpinnerWithIcon) {
            for (int i = 0; i < arrayList.size(); i++) {
                key = (String) arrayList.get(i);
                name = sortMap.get(key);
                browseListItemsList.add(new BrowseListItems(name, key));
                _propmap.put(key, name);
                if (name.equals(defaultValue)) {
                    defaultValue = key;
                }
            }
            _adapter = new SpinnerAdapter(context, R.layout.spinner_value_layout, mSelectedItem,
                    browseListItemsList, arrayList);
            _spinner.setAdapter(_adapter);
            _adapter.notifyDataSetChanged();

        } else {
            _adapter = new SpinnerAdapter(context, R.layout.simple_text_view, mSelectedItem);
            _adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            _spinner.setAdapter(_adapter);

            for (int i = 0; i < arrayList.size(); i++) {
                key = (String) arrayList.get(i);
                name = sortMap.get(key);

                switch (mCurrentSelectedOption){
                    case "create_new_diary":
                        if(name.equals(defaultValue))
                            defaultValue = key;
                        _adapter.add(key);
                        break;

                    default:

                        if ((mCurrentSelectedOption.equals(ConstantVariables.VIDEO_MENU_TITLE)
                                || mCurrentSelectedOption.equals(ConstantVariables.ADV_VIDEO_MENU_TITLE))
                                && defaultValue != null && defaultValue.equals("3") && createForm
                                && property.equals("type")) {

                            // When the video creation form is called from aaf to upload from device
                            // then show the my device option already selected.
                            if (name.equals(defaultValue)) {
                                defaultValue = key;
                            }

                        } else if (mCurrentSelectedOption.equals(ConstantVariables.ADV_VIDEO_MENU_TITLE)
                                && defaultValue != null && !defaultValue.isEmpty() && createForm
                                && property.equals("main_channel_id")) {

                            // When the video creation form is called after channel creation
                            // then show the created channel id already selected.
                            if (name.equals(defaultValue)) {
                                defaultValue = key;
                            }
                        } else if (mCurrentSelectedOption.equals(ConstantVariables.ALBUM_MENU_TITLE)
                                && defaultValue != null && !defaultValue.isEmpty() && createForm
                                && property.equals("category_id")) {
                            if (name.equals(defaultValue)) {
                                defaultValue = key;
                            }
                        }
                        // Checking is there is any default value.(name.isEmpty())

                        // If default value found for name.isEmpty() then don't check for name,equals('0').
                        // If default value found in this condition then don't check for name.equals("1").
                        else if ((name.isEmpty() || name.equals("0") || name.equals("1") || name.equals(defaultValue) ||
                                name.equals("everyone")) && !isDefaultValueSet) {
                            defaultValue = key;
                            isDefaultValueSet = true;
                        }

                        if (property.equals("album")) {
                            if (name.equals("0"))
                                _adapter.add(key);
                        } else if(property.equals("draft")){
                            if (createForm)
                                _adapter.add(key);
                        } else {
                            _adapter.add(key);
                        }
                        break;
                }
                _propmap.put(key, name);
            }
        }

        //Setting up default value
        if (defaultValue != null) {
            defaultPosition = _adapter.getPosition(defaultValue);
            _spinner.setSelection(defaultPosition);
        }
        _spinner.setOnItemSelectedListener(this);

        if (property.equals("host_type_select") || property.equals("main_channel_id")) {
            _layout.addView(linearLayout);
        } else {
            _layout.addView(_label);
        }
        _layout.addView(_spinner);


        if (property.equals("eventrepeat_id")) {
            _layout.setTag(R.id.eventrepeat_id, property);
        } else if (property.equals("category_id")) {
            _layout.setTag(R.id.category_id, property);
        }

    }

    @Override
    public String getValue() {
        return _propmap.get( _adapter.getItem( _spinner.getSelectedItemPosition() ) );
    }

    @Override
    public void setValue(String value) {
        String name;
        JSONArray names = _options.names();
        for (int i = 0; i < names.length(); i++) {
            name = names.optString(i);

            if (name.equals(value)) {
                String item = _options.optString(name);
                _spinner.setSelection(_adapter.getPosition(item));

            }
        }
    }

    @Override
    public void setToggleHandler( FormActivity.FormWidgetToggleHandler handler ) {
        super.setToggleHandler(handler);
        _spinner.setOnItemSelectedListener(new SelectionHandler(this));
    }

    /**
     * Function to show error message on Spinner
     * @param errorMessage Error Message to show
     */
    public void setErrorMessage(String errorMessage){
        TextView errorText = (TextView) _spinner.getSelectedView();
        errorText.setError("");
        errorText.setTextColor(ContextCompat.getColor(mContext, R.color.black));
        errorText.setText(errorMessage + " ");
        errorText.setFocusable(true);
        errorText.requestFocusFromTouch();
    }

    /**
     * Method to check set property name for the profile field generation.
     */
    private void getWidgetPropertyName() {
        switch (mCurrentSelectedOption) {
            case ConstantVariables.ADVANCED_EVENT_MENU_TITLE:
            case ConstantVariables.SITE_PAGE_MENU_TITLE:
                mWidgetProperty = "tags";
                break;

            case ConstantVariables.MLT_MENU_TITLE:
                mWidgetProperty = "auth_view";
                break;

            case ConstantVariables.ADV_GROUPS_MENU_TITLE:
                mWidgetProperty = "body";
                break;

            case ConstantVariables.STORE_MENU_TITLE:
            case ConstantVariables.PRODUCT_MENU_TITLE:
                mWidgetProperty = "minPrice";
                break;

            case ConstantVariables.ADV_VIDEO_MENU_TITLE:
            case ConstantVariables.ADV_VIDEO_CHANNEL_MENU_TITLE:
                mWidgetProperty = "description";
                break;

        }
    }

    /**
     * Method to check the selected video type and show the fields accordingly.
     * @param parent Adapter view parent.
     * @param position Position of the selected item in spinner.
     */
    private void checkForVideoOptions(AdapterView<?> parent, int position) {
        if (isCreateForm && parent.getTag().toString().equals("type")
                && ((Activity) mContext).findViewById(R.id.button) != null
                && ((Activity) mContext).findViewById(R.id.editTextUrl) != null) {

            if (_propmap.get(_adapter.getItem(position)).equals("0")) {
                ((Activity) mContext).findViewById(R.id.button).setVisibility(View.GONE);
                ((Activity) mContext).findViewById(R.id.editTextUrl).setVisibility(View.GONE);

            } else if (_propmap.get(_adapter.getItem(position)).equals("3")) {
                ((Activity) mContext).findViewById(R.id.button).setVisibility(View.VISIBLE);
                ((Activity) mContext).findViewById(R.id.editTextUrl).setVisibility(View.GONE);

            } else {
                ((Activity) mContext).findViewById(R.id.button).setVisibility(View.GONE);
                ((Activity) mContext).findViewById(R.id.editTextUrl).setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        String parentTag = parent.getTag().toString();
        String key = _propmap.get(_adapter.getItem(position));

        if (!mIsSpinnerWithIcon) {
            TextView textView = (TextView) parent.getChildAt(0);
            textView.setTextColor(ContextCompat.getColor(mContext, R.color.black));
        }

        mSelectedItem = position;
        _adapter.getCustomView(position, view, parent, mSelectedItem);

        switch (mCurrentSelectedOption) {

            case ConstantVariables.VIDEO_MENU_TITLE:
                checkForVideoOptions(parent, position);
                break;

            case ConstantVariables.HOME_MENU_TITLE:
            case "message":
                checkForVideoOptions(parent, position);
                break;

            case "core_main_siteevent":
               /* Create sub category fields in the form on the basis of Category selection from spinner
		            in advanced events module */

                if (parentTag.equals("category_id")) {
                    sIsEventRepeatTypeMonthly = false;
                }

                for (int  i = 0; i < _widgetsNew.size(); i++) {
                    if (_widgetsNew.get(i).getPropertyName().equals("category_id")) {
                        sposition = i;
                    } else if (_widgetsNew.get(i).getPropertyName().equals("eventrepeat_id")) {
                        repeat_s_position = i;
                    }
                }

                /* Monthly type repeat have event have two type, checked the monthly
                 type and show respective field in the form */

                checkMonthlyTypeRepeatEvent();

                repeat_s_position++;

                /* Create sub category field in the form on the selection of category */

                createSubCategoryField(key, parentTag);

                /* Create 3rd level category field in the form on the selection of sub category */

                createSubSubCategoryField(key, parentTag);

                /* Create profile  fields in the form on the basis of category selection */

                createProfileFieldsForCategory(key);

                repeat_e_position = repeat_s_position + size;

                /* Create repeat event fields in the form on the basis of
			                          repeat event type selected from spinner */

                createProfileFieldsForRepeatEvent(key);


                if (parentTag.equals("host_type_select")) {

                    if (FormActivity.mAddPeopleAdapter != null)
                        FormActivity.mAddPeopleAdapter.clear();

                    if (key.equals("siteevent_organizer")) {
                        addNewTextView.setVisibility(View.VISIBLE);
                    } else {
                        addNewTextView.setVisibility(View.GONE);
                    }
                    FormActivity.hostKey = key;
                }
                break;

            case "compose_message":
                for (int i = 0; i < _widgetsNew.size(); i++) {
                    if (_widgetsNew.get(i).getPropertyName().equals("toValues")) {
                        _widgetsNew.get(i).getView().setVisibility(key.equals("1") ? View.VISIBLE : View.GONE);
                    } else if (_widgetsNew.get(i).getPropertyName().equals("searchGuests")) {
                        _widgetsNew.get(i).getView().setVisibility(key.equals("4") ? View.VISIBLE : View.GONE);
                    }
                }
                break;

            case "core_main_user":
                try {
                    if (mAdvancedMemberWhatWhereWithinmile != null && !mAdvancedMemberWhatWhereWithinmile.isEmpty()) {
                        selectedType = mJsonObject.optString("name");
                        key = _propmap.get(_adapter.getItem(position));
                        hasSubCategory = hasSubSubCategory = false;

                        // Checking the selected type.
                        if (selectedType.equals("profile_type")) {
                            canChangeProfileFields = true;
                            subSPosition = subSubSPosition = 0;
                        }

                        for (int i = 0; i < _widgetsNew.size(); i++) {
                            if (_widgetsNew.get(i).getPropertyName().equals("profile_type")) {
                                sPosition = i;
                                break;
                            }
                        }

                        // When the profile fields exists for profile_type selection.
                        if (mCategoryFields != null && mCategoryFields.has(key) && selectedType.equals("profile_type")) {
                            // Inflating view for profile fields.
                            inflateProfileFieldView(key);
                        }

                        // When there are no profile fields for a profile_type then removing all profile fields
                        if ((mCategoryFields != null && !mCategoryFields.has(key) && selectedType.equals("profile_type")) ||
                                (key != null && selectedType.equals("profile_type") && key.equals("0"))) {
                            removeViewFromCategorySelection(key);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case ConstantVariables.ADV_VIDEO_MENU_TITLE:
                checkForVideoOptions(parent, position);
            case ConstantVariables.ADV_GROUPS_MENU_TITLE:
            case ConstantVariables.SITE_PAGE_MENU_TITLE:
            case ConstantVariables.STORE_MENU_TITLE:
            case ConstantVariables.PRODUCT_MENU_TITLE:
            case ConstantVariables.MLT_MENU_TITLE:
            case ConstantVariables.ADV_VIDEO_CHANNEL_MENU_TITLE:
                try {
                    selectedType = mJsonObject.optString("name");
                    hasSubCategory = hasSubSubCategory = false;

                    // When the form is edit form then getting the start position, end position
                    // and the type from which profile fields are generated.
                    if (!isCreateForm) {
                        if (mCurrentSelectedOption.equals(ConstantVariables.MLT_MENU_TITLE)
                                || mCurrentSelectedOption.equals(ConstantVariables.STORE_MENU_TITLE)
                                || mCurrentSelectedOption.equals(ConstantVariables.PRODUCT_MENU_TITLE)
                                || mCurrentSelectedOption.equals(ConstantVariables.ADV_VIDEO_MENU_TITLE)
                                || mCurrentSelectedOption.equals(ConstantVariables.ADV_VIDEO_CHANNEL_MENU_TITLE)) {
                            fieldCategoryLevel = mFormValuesJsonObject.optString("fieldCategoryLevel", "category_id");

                        }
                        if (isFirstTimeLoadingFromEdit) {
                            for (int i = 0; i < _widgetsNew.size(); i++) {
                                if (_widgetsNew.get(i).getPropertyName().equals("category_id")) {
                                    sPosition = i;
                                } else if (mCurrentSelectedOption.equals(ConstantVariables.MLT_MENU_TITLE)
                                        && _widgetsNew.get(i).getPropertyName().equals("auth_view")) {
                                    ePosition = i - 1;
                                } else if ((mCurrentSelectedOption.equals(ConstantVariables.ADV_VIDEO_MENU_TITLE)
                                        || mCurrentSelectedOption.equals(ConstantVariables.ADV_VIDEO_CHANNEL_MENU_TITLE))
                                        && _widgetsNew.get(i).getPropertyName().equals("description")) {
                                    ePosition = i - 1;

                                } else if (_widgetsNew.get(i).getPropertyName().equals(mWidgetProperty)) {
                                    ePosition = i - 1;
                                    fieldCategoryLevel = "category_id";
                                }
                            }

                            if (fieldCategoryLevel != null && !fieldCategoryLevel.isEmpty()) {
                                switch (fieldCategoryLevel) {
                                    case "category_id":
                                        isCategoryProfileFields = true;
                                        isSubCategoryProfileFields = false;
                                        isSubSubCategoryProfileFields = false;
                                        break;
                                    case "subcategory_id":
                                        isCategoryProfileFields = false;
                                        isSubCategoryProfileFields = true;
                                        isSubSubCategoryProfileFields = false;
                                        break;
                                    case "subsubcategory_id":
                                        isCategoryProfileFields = false;
                                        isSubCategoryProfileFields = false;
                                        isSubSubCategoryProfileFields = true;
                                        break;
                                }
                            } else {
                                ePosition = 0;
                                isCategoryProfileFields = false;
                                isSubCategoryProfileFields = false;
                                isSubSubCategoryProfileFields = false;
                                canChangeProfileFields = true;
                            }
                            isFirstTimeLoadingFromEdit = false;
                        }
                    }

                    // Checking the selected type.
                    if (selectedType.equals("category_id") && (isCreateForm || (!isCreateForm && !isFirstTimeSelectedTypeCategory))) {
                        isCategoryProfileFields = false;
                        isSubCategoryProfileFields = false;
                        isSubSubCategoryProfileFields = false;
                        canChangeProfileFields = true;
                        subSPosition = subSubSPosition = 0;
                        isFirstTimeSelectedTypeCategory = false;

                    }

                    for (int i = 0; i < _widgetsNew.size(); i++) {
                        if (_widgetsNew.get(i).getPropertyName().equals("category_id")) {
                            sPosition = i;
                        } else if (_widgetsNew.get(i).getPropertyName().equals("subcategory_id")) {
                            subSPosition = i;
                        } else if (_widgetsNew.get(i).getPropertyName().equals("subsubcategory_id")) {
                            subSubSPosition = i;
                        }
                    }

                    /**
                     * Subcategory, SubSubCategory and profile fields view generation code <---start--->
                     */
                    // When subcategory exists.
                    if (key != null && mSubCategory != null && mSubCategory.has(key) &&
                            (isCreateForm || !isFirstTimeSubCategorySelection)) {
                        // Inflating view from subcategory selection.
                        // but ignoring first time when the form is loaded for edit to show the form values.
                        inflateSubcategoryView(key);
                        isFirstTimeSubCategorySelection = false;
                    }

                    // When SubSubCategory exists.
                    if (key != null && mSubSubCategory != null && mSubSubCategory.has(key) &&
                            (isCreateForm || !isFirstTimeSubSubCategorySelection)) {
                        // Inflating view from SubSubCategory selection.
                        // but ignoring first time when the form is loaded for edit to show the form values.
                        inflateSubSubcategoryView(key);
                        isFirstTimeSubSubCategorySelection = false;

                    }

                    // When the profile fields exists for category/SubCategory/SubSubCategory selection.
                    if (mCategoryFields != null && mCategoryFields.has(key) && !isCategoryProfileFields &&
                            (selectedType.equals("category_id") || selectedType.equals("subcategory_id") || selectedType.equals("subsubcategory_id"))) {
                        // Inflating view for profile fields.
                        // but ignoring first time when the form is loaded for edit to show the form values.
                        if (isCreateForm || !isFirstTimeCateFieldSelection) {
                            inflateProfileFieldView(key);
                            isFirstTimeCateFieldSelection = false;
                        }
                    }
                    /**
                     * Subcategory, SubSubCategory and profile fields view generation code <---end--->
                     */



                    /**
                     * Remove field code <--start-->
                     */

                    // When there are no profile fields for a category then removing all view
                    // (i.e. SubCategory, SubSubCategory. and profile fields.).
                    if ((((mCategoryFields != null && !mCategoryFields.has(key))
                            || ((mCategoryFields == null || !mCategoryFields.has(key)) && mSubCategory != null && !mSubCategory.has(key)))
                            && selectedType.equals("category_id")) ||
                            (key != null && selectedType.equals("category_id") && key.equals("0"))) {
                        //ignoring first time when the form is loaded for edit to show the form values.
                        if (!isFirstTimeNoCateFieldSelection) {
                            removeViewFromCategorySelection(key);
                            isFirstTimeNoCateFieldSelection = false;
                        }
                    }
                    // When the selected type is SubSubCategory and previous fields are generated by SubSubCategory but the
                    //new SubSubCategory does not contain profile fields then removing all profile fields.
                    else if (mCategoryFields != null && !mCategoryFields.has(key) && selectedType.equals("subsubcategory_id") && isSubSubCategoryProfileFields) {
                        removeViewFromSubSubCategorySelection();

                    } else if ((key != null && selectedType.equals("subcategory_id") &&
                            (key.equals("0") || (mCategoryFields != null && !mCategoryFields.has(key) && isSubCategoryProfileFields)))) {
                        removeViewFromSubCategorySelection();
                    }

                    /**
                     * Remove field code <--end-->
                     */

                    isFirstTimeSubCategorySelection = false;
                    isFirstTimeSubSubCategorySelection = false;
                    isFirstTimeNoCateFieldSelection = false;
                    isFirstTimeCateFieldSelection = false;
                    isFirstTimeSelectedTypeCategory = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

    }

    private void checkMonthlyTypeRepeatEvent() {

        isMonthlyTypeCheck = (CheckBox) FormActivity._layout.findViewById(R.id.monthly_type);
        if (isMonthlyTypeCheck != null && !isRemoveRepeatEvent && !sIsChangedMonthlyField ) {
            sIsChangedMonthlyField = true;
            sIsEventRepeatTypeMonthly = true;
            sEditPageLoadingTime++;

            if (isMonthlyTypeCheck.getTag().equals("1")) {
                for (int i = 0; i < _widgetsNew.size(); i++) {
                    if (_widgetsNew.get(i).getPropertyName().equals("repeat_day")) {
                        _widgetsNew.get(i).getView().setVisibility(View.VISIBLE);
                    } else if (_widgetsNew.get(i).getPropertyName().equals("repeat_week") || _widgetsNew.get(i).getPropertyName().equals("repeat_weekday")) {
                        _widgetsNew.get(i).getView().setVisibility(View.GONE);
                    }
                }

            } else {
                for (int i = 0; i < _widgetsNew.size(); i++) {
                    if (_widgetsNew.get(i).getPropertyName().equals("repeat_week") || _widgetsNew.get(i).getPropertyName().equals("repeat_weekday")) {
                        _widgetsNew.get(i).getView().setVisibility(View.VISIBLE);
                    } else 	if (_widgetsNew.get(i).getPropertyName().equals("repeat_day")) {
                        _widgetsNew.get(i).getView().setVisibility(View.GONE);
                    }
                }
            }

            isMonthlyTypeCheck.setOnCheckedChangeListener(this);

        }

    }

    private void createProfileFieldsForRepeatEvent(String key) {

        if (key != null) {

            if(mRepeatOccurences != null && mRepeatOccurences.has(key)) {

                sEditPageLoadingTimeRepeatEvent++;

                JSONArray fieldsJsonArray = mRepeatOccurences.optJSONArray(key);
                if (fieldsJsonArray != null) {

                    if (sIsChangeOccurence) {
                        for (int k = repeat_s_position; k <= repeat_e_position; k++) {
                            _widgetsNew.remove(repeat_s_position);
                        }
                    }

                    if (!isRemoveRepeatEvent && sEditPageLoadingTimeRepeatEvent >= 2) {
                        int startRemove = 0, endRemove = 0;
                        for (int i = 0; i < _widgetsNew.size(); i++) {
                            if (_widgetsNew.get(i).getPropertyName().equals("eventrepeat_id")) {
                                startRemove = i;
                            } else if (_widgetsNew.get(i).getPropertyName().equals("body")) {
                                endRemove = i;
                            }
                        }

                        startRemove++;

                        for (int i = startRemove; i < endRemove; i++) {
                            _widgetsNew.remove(startRemove);
                        }

                        isRemoveRepeatEvent = true;

                    }

                    if (isCreateForm || sEditPageLoadingTimeRepeatEvent >= 2) {

                        try {

                            for (int l = 0; l < fieldsJsonArray.length(); l++) {

                                JSONObject fieldObject = fieldsJsonArray.getJSONObject(l);
                                String fieldName = fieldObject.getString("name");
                                String fieldLabel = fieldObject.optString("label");
                                String fieldDescription = fieldObject.optString("description");
                                boolean hasValidator = fieldObject.optBoolean("hasValidator");

                                widget = formActivity.getWidget(mContext, fieldName, fieldObject, fieldLabel, hasValidator, createForm,
                                        fieldDescription, _widgetsNew, _mapNew, mCurrentSelectedOption, null, mCategoryFields, mRepeatOccurences, null, null, null);

                                if (widget == null) continue;

                                if (fieldObject.has(FormActivity.SCHEMA_KEY_HINT))
                                    widget.setHint(fieldObject.getString(FormActivity.SCHEMA_KEY_HINT));

                                _widgetsNew.add(repeat_s_position + l, widget);
                                _mapNew.put(fieldName, widget);

                                repeat_e_position = repeat_s_position + l;

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        size = repeat_e_position - repeat_s_position;

                        FormActivity._layout.removeAllViews();

                        for (int i = 0; i < _widgetsNew.size(); i++) {
                            FormActivity._layout.addView(_widgetsNew.get(i).getView());
                        }

                    /*Change type and field of monthly type repeat event on change checkbox value */

                        if (key.equals("monthly") &&  !sIsChangedMonthlyField) {
                            CheckBox isMonthlyTypeCheck1 = (CheckBox) FormActivity._layout.findViewById(R.id.monthly_type);
                            for (int i = 0; i < _widgetsNew.size(); i++) {
                                if (_widgetsNew.get(i).getPropertyName().equals("repeat_week") || _widgetsNew.get(i).getPropertyName().equals("repeat_weekday")) {
                                    _widgetsNew.get(i).getView().setVisibility(View.VISIBLE);
                                } else 	if (_widgetsNew.get(i).getPropertyName().equals("repeat_day")) {
                                    _widgetsNew.get(i).getView().setVisibility(View.GONE);
                                }
                            }

                            isMonthlyTypeCheck1.setOnCheckedChangeListener(this);

                        }
                    }

                }

                sIsChangeOccurence = true;


            } else if (key.equals("never")) {
                sEditPageLoadingTimeRepeatEvent++;

                size = 0;
                FormActivity._layout.removeAllViews();
                if (sIsChangeOccurence) {
                    for (int k = repeat_s_position; k <= repeat_e_position; k++) {
                        _widgetsNew.remove(repeat_s_position);

                    }
                }

                sIsChangeOccurence = false;

                if (!isRemoveRepeatEvent) {
                    int startRemove = 0, endRemove = 0;
                    for (int  i = 0; i < _widgetsNew.size(); i++) {
                        if (_widgetsNew.get(i).getPropertyName().equals("eventrepeat_id")) {
                            startRemove = i;
                        } else if (_widgetsNew.get(i).getPropertyName().equals("body")) {
                            endRemove = i;
                        }
                    }

                    startRemove ++;

                    for (int  i = startRemove; i < endRemove; i++) {
                        _widgetsNew.remove(startRemove);
                    }

                    isRemoveRepeatEvent = true;

                }

                for (int i = 0; i < _widgetsNew.size(); i++) {
                    FormActivity._layout.addView(_widgetsNew.get(i).getView());
                }

            }
        }

    }

    private void createProfileFieldsForCategory(String key) {

        if (key != null && mSubCategory != null && mCategoryFields != null && mCategoryFields.has(key) &&  (sEditPageLoadingTime >= 2 || isCreateForm)) {
            JSONArray fieldsJsonArray = mCategoryFields.optJSONArray(key);

            if (fieldsJsonArray != null) {
                if (sIsChangeCategory) {

                    for (int i = 0; i < _widgetsNew.size(); i++) {
                        if (_widgetsNew.get(i).getPropertyName().equals("subsubcategory_id")) {
                            eposition++;
                            break;
                        }
                    }

                    if (isCreateForm) {
                        eposition++;
                    }

                    for (int k = sposition; k < eposition; k++) {
                        _widgetsNew.remove(sposition + 1);
                    }
                }

                if (!isRemoveFields || !isCreateForm ) {
                    int startRemove = 0, endRemove = 0;
                    for (int  i = 0; i < _widgetsNew.size(); i++) {
                        if (_widgetsNew.get(i).getPropertyName().equals("subcategory_id")) {
                            startRemove = i;
                        } else if (_widgetsNew.get(i).getPropertyName().equals("tags")) {
                            endRemove = i;
                        }
                    }

                    startRemove ++;

                    for (int  i = startRemove; i < endRemove; i++) {
                        _widgetsNew.remove(startRemove);
                    }

                    isRemoveFields = true;
                }


                try {

                    for (int j = 0; j < fieldsJsonArray.length(); j++) {

                        JSONObject fieldObject = fieldsJsonArray.getJSONObject(j);
                        String fieldName = fieldObject.getString("name");
                        String fieldLabel = fieldObject.optString("label");
                        String fieldDescription = fieldObject.optString("description");
                        boolean hasValidator = fieldObject.optBoolean("hasValidator");

                        widget = formActivity.getWidget(mContext, fieldName, fieldObject, fieldLabel, hasValidator, createForm,
                                fieldDescription, _widgetsNew, _mapNew, mCurrentSelectedOption, null, mCategoryFields,
                                mRepeatOccurences, null, null, null);
                        if (widget == null) continue;

                        if (fieldObject.has(FormActivity.SCHEMA_KEY_HINT))
                            widget.setHint(fieldObject.getString(FormActivity.SCHEMA_KEY_HINT));


                        _widgetsNew.add(sposition + j + 1, widget);
                        positionMap.put(String.valueOf(sposition + j + 1), String.valueOf(sposition + j + 1));
                        _mapNew.put(fieldName, widget);

                        eposition = sposition + j + 1;

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            FormActivity._layout.removeAllViews();

            for (int i = 0; i < _widgetsNew.size(); i++) {
                FormActivity._layout.addView(_widgetsNew.get(i).getView());

                if ( mCurrentSelectedOption.equals("core_main_siteevent") &&
                        _widgetsNew.get(i).getPropertyName().equals("eventrepeat_id")) {
                    repeat_s_position = i;
                }
            }
            repeat_s_position++;

            sIsChangeCategory = true;
        }
    }


    private void createSubCategoryField(String key, String tag) {

        if(key != null){

            if ( mSubCategory != null && mSubCategory.has(key) && !sIsEventRepeatTypeMonthly) {

                sEditPageLoadingTime++;
                if ((!isCreateForm && sEditPageLoadingTime >= 2 ) || (mCategoryFields == null && sIsChangeCategory)) {
                    for (int  i = 0; i < _widgetsNew.size(); i++) {
                        if (_widgetsNew.get(i).getPropertyName().equals("subcategory_id")) {
                            _widgetsNew.remove(i);
                            break;
                        }
                    }
                }

                if (isCreateForm || sEditPageLoadingTime >= 2) {
                    JSONObject jsonObject = mSubCategory.optJSONObject(key);
                    JSONObject formObject = jsonObject.optJSONObject("form");
                    FormActivity.subSubCategory = jsonObject.optJSONObject("subsubCategories");
                    sposition++;

                    String name = formObject.optString("name");
                    String label = formObject.optString("label");
                    widget = formActivity.getWidget(mContext, name, formObject, label, false, createForm,
                            null, _widgetsNew, _mapNew, mCurrentSelectedOption, null, mCategoryFields, mRepeatOccurences, null, null, null);

                     _widgetsNew.add(sposition, widget);
                    _mapNew.put(name, widget);

                    if (mCategoryFields == null) {
                        eposition = sposition;
                        FormActivity._layout.removeAllViews();
                        for (int i = 0; i < _widgetsNew.size(); i++) {
                            FormActivity._layout.addView(_widgetsNew.get(i).getView());
                        }
                        sIsChangeCategory = true;
                    }
                }

            } else if (key.equals("0") && tag.equals("category_id")) {

                int startRemove = 0, endRemove = 0;

                if (isCreateForm) {
                    startRemove = sposition;
                    endRemove = eposition;
                    eposition = sposition;
                } else {
                    for (int i = 0; i < _widgetsNew.size(); i++) {
                        if (_widgetsNew.get(i).getPropertyName().equals("subcategory_id")) {
                            startRemove = i;
                        } else if (_widgetsNew.get(i).getPropertyName().equals("tags")) {
                            endRemove = i;
                        }
                    }
                    startRemove--;
                    endRemove--;
                    eposition = sposition;
                }

                for (int  i = startRemove; i < endRemove; i++) {
                    _widgetsNew.remove( startRemove + 1);
                }

                FormActivity._layout.removeAllViews();
                for (int i = 0; i < _widgetsNew.size(); i++) {
                    FormActivity._layout.addView(_widgetsNew.get(i).getView());
                }
                sIsChangeCategory = true;
            }
        }

    }

    private void createSubSubCategoryField(String key, String tag) {

        if (FormActivity.subSubCategory != null && key != null && FormActivity.subSubCategory.has(key) && ( sEditPageLoadingTime >= 2 || isCreateForm)) {

            sposition = sposition + 2;

            if (sIsChangeSubCategory) {
                for (int k = 0; k < _widgetsNew.size(); k++) {
                    if (_widgetsNew.get(k).getPropertyName().equals("subsubcategory_id")) {
                        _widgetsNew.remove(k);
                        break;
                    }
                }
            }

            JSONObject object = FormActivity.subSubCategory.optJSONObject(key);

            String name = object.optString("name");
            String label = object.optString("label");
            widget = formActivity.getWidget(mContext, name, object, label, false, createForm,
                    null, _widgetsNew, _mapNew, mCurrentSelectedOption, null, mCategoryFields, mRepeatOccurences, null, null, null);

            _widgetsNew.add(sposition, widget);
            _mapNew.put(name, widget);

            FormActivity._layout.removeAllViews();

            for (int i = 0; i < _widgetsNew.size(); i++) {
                FormActivity._layout.addView(_widgetsNew.get(i).getView());
                if (_widgetsNew.get(i).getPropertyName().equals("eventrepeat_id")) {
                    repeat_s_position = i;
                }
            }
            repeat_s_position++;

            sIsChangeSubCategory = true;
            sIsRemoveCalled = true;

        } else if (tag.equals("subcategory_id") && sIsRemoveCalled) {
            for (int k = 0; k < _widgetsNew.size(); k++) {
                if (_widgetsNew.get(k).getPropertyName().equals("subsubcategory_id")) {
                    _widgetsNew.remove(k);
                    break;
                }
            }
            FormActivity._layout.removeAllViews();
            for (int i = 0; i < _widgetsNew.size(); i++) {
                FormActivity._layout.addView(_widgetsNew.get(i).getView());
            }
            sIsChangeSubCategory = false;

        }
    }

    public void inflateSubcategoryView(String key) {
        hasSubCategory = true;
        // When category is selected.
        if (selectedType.equals("category_id") && ePosition != 0) {
            if (subSPosition != 0 && subSubSPosition == ePosition)
                ePosition = subSubSPosition;
            int remove = 0;
            if (subSPosition != 0)
                remove = subSPosition;
            else if (!_widgetsNew.get(sPosition).getPropertyName().equals("category_id"))
                remove = sPosition;
            else
                remove = sPosition + 1;

            for (int k = ePosition; k >= remove; k--) {
                _widgetsNew.remove(k);
            }
            ePosition = 0;
            subSubSPosition = 0;
            isCategoryProfileFields = isSubCategoryProfileFields = isSubSubCategoryProfileFields = false;
            canChangeProfileFields = true;
        } else if (canChangeProfileFields && ePosition == 0 && subSPosition != 0 &&
                !isCategoryProfileFields && !isSubCategoryProfileFields && !isSubSubCategoryProfileFields) {
            _widgetsNew.remove(subSPosition);
        }

        JSONObject jsonObject = mSubCategory.optJSONObject(key);
        JSONObject formObject = jsonObject.optJSONObject("form");
        mSubSubCategory = jsonObject.optJSONObject("subsubcategories");

        sPosition++;
        String name = formObject.optString("name");
        String label = formObject.optString("label");

        widget = formActivity.getWidget(mContext, name, formObject, label, false, true, null, _widgetsNew,
                _mapNew, mCurrentSelectedOption, null, mCategoryFields, null, mSubSubCategory, null, null);

        _widgetsNew.add(sPosition, widget);
        _mapNew.put(name, widget);

        FormActivity._layout.removeAllViews();
        for (int i = 0; i < _widgetsNew.size(); i++) {
            FormActivity._layout.addView(_widgetsNew.get(i).getView());
        }
    }

    public void inflateSubSubcategoryView(String key) {
        hasSubSubCategory = true;

        // when the fields are generated by subcategory or SubSubCategory then removing profile fields.
        if (!canChangeProfileFields && (isSubCategoryProfileFields || isSubSubCategoryProfileFields) && ePosition != 0) {
            for (int k = ePosition; k >= (subSPosition + 1); k--) {
                _widgetsNew.remove(k);
            }
            ePosition = subSubSPosition;
            isSubCategoryProfileFields = isSubSubCategoryProfileFields = false;
            canChangeProfileFields = true;
        } else if (canChangeProfileFields && !isCategoryProfileFields && ePosition != 0 && subSubSPosition != 0) {
            for (int k = ePosition; k >= subSubSPosition; k--) {
                _widgetsNew.remove(k);
            }
            ePosition = 0;
            isSubCategoryProfileFields = isSubSubCategoryProfileFields = false;
        } else if (canChangeProfileFields && isCategoryProfileFields) {
            boolean isSubcateExists = true;
            for (int i = 0; i < _widgetsNew.size(); i++) {
                if (!_widgetsNew.get(i).getPropertyName().equals("subsubcategory_id")) {
                    isSubcateExists = false;
                }
            }
            if (!isSubcateExists) {
                if (subSubSPosition == 0) {
                    ePosition = ePosition + 1;
                } else if(subSubSPosition != 0 && ePosition != subSubSPosition) {
                    _widgetsNew.remove(subSubSPosition);
                }
            } else {
                subSubSPosition = subSPosition;
            }
        } else if (canChangeProfileFields && subSubSPosition != 0) {
            _widgetsNew.remove(subSubSPosition);
            ePosition = 0;
        }

        JSONObject jsonObject = mSubSubCategory.optJSONObject(key);

        subSPosition++;
        String name = jsonObject.optString("name");
        String label = jsonObject.optString("label");

        widget = formActivity.getWidget(mContext, name, jsonObject, label, false, true, null, _widgetsNew,
                _mapNew, mCurrentSelectedOption, null, mCategoryFields, null, null, null, null);

        _widgetsNew.add(subSPosition, widget);
        _mapNew.put(name, widget);

        FormActivity._layout.removeAllViews();
        for (int i = 0; i < _widgetsNew.size(); i++) {
            FormActivity._layout.addView(_widgetsNew.get(i).getView());
        }
        subSubSPosition = subSPosition;
    }

    public void inflateProfileFieldView(String key) {
        JSONArray fieldsJsonArray = mCategoryFields.optJSONArray(key);

        if (fieldsJsonArray != null) {
            if (canChangeProfileFields) {

                // When choosing subcategory and profile fields are generated from SubSubCategory.
                //then checking if new subcategory contains SubSubCategory or not
                if (selectedType.equals("subcategory_id") && !hasSubSubCategory && isSubSubCategoryProfileFields && ePosition != 0 && subSubSPosition != 0) {
                    _widgetsNew.remove(subSubSPosition);
                    ePosition = ePosition - 1;
                }
                if (selectedType.equals("subcategory_id") && !hasSubSubCategory && !isSubSubCategoryProfileFields && subSubSPosition != 0) {
                    _widgetsNew.remove(subSubSPosition);
                    if (ePosition != 0) {
                        ePosition = ePosition - 1;
                    } else {
                        ePosition = subSubSPosition - 1;
                    }
                }

                // Removing subcategory and subSubCategory view of the previous selected category.
                if (selectedType.equals("category_id")) {
                    if (!hasSubSubCategory && subSubSPosition != 0) {
                        _widgetsNew.remove(subSubSPosition);
                        subSubSPosition = 0;
                        ePosition = ePosition - 1;
                    }
                    if (!hasSubCategory && subSPosition != 0) {
                        _widgetsNew.remove(subSPosition);
                        subSPosition = 0;
                        ePosition = ePosition - 1;
                    }
                }

                // Setting condition true for which profile fields are generated.
                switch (selectedType) {
                    case "category_id":
                        isCategoryProfileFields = true;
                        isSubCategoryProfileFields = false;
                        isSubSubCategoryProfileFields = false;
                        canChangeProfileFields = false;
                        break;
                    case "subcategory_id":
                        isCategoryProfileFields = false;
                        isSubCategoryProfileFields = true;
                        isSubSubCategoryProfileFields = false;
                        canChangeProfileFields = false;
                        break;
                    case "subsubcategory_id":
                        isCategoryProfileFields = false;
                        isSubCategoryProfileFields = false;
                        isSubSubCategoryProfileFields = true;
                        canChangeProfileFields = false;
                        break;
                    default:
                        isCategoryProfileFields = false;
                        isSubCategoryProfileFields = false;
                        isSubSubCategoryProfileFields = false;
                        canChangeProfileFields = true;
                        break;
                }
                generateProfileFields(fieldsJsonArray);
            } else if ((selectedType.equals("subcategory_id") && (isSubCategoryProfileFields || isSubSubCategoryProfileFields)) ||
                    (selectedType.equals("subsubcategory_id") && isSubSubCategoryProfileFields)) {
                generateProfileFields(fieldsJsonArray);
            }
        }
    }

    public void removeViewFromCategorySelection(String key) {

        FormActivity._layout.removeAllViews();
        //removing subcategory and subsubcategory when the no category is chosen.
        if (key != null && key.equals("0") && (ePosition != 0 && subSPosition != 0 && ePosition == subSPosition ||
                ePosition != 0 && subSubSPosition != 0 && ePosition == subSubSPosition)) {
            ePosition = ePosition + 1;
        } else if (key != null && (key.equals("0") || mSubCategory != null && !mSubCategory.has(key))
                && ePosition == 0 && canChangeProfileFields) {
            if (subSubSPosition != 0) {
                _widgetsNew.remove(subSubSPosition);
            }
            if (subSPosition != 0) {
                _widgetsNew.remove(subSPosition);
            }
        }

        // If there are profile fields then remove it.

        if (ePosition != 0) {
            for (int k = ePosition; k > sPosition; k--) {
                _widgetsNew.remove(k);
            }
        }

        for (int i = 0; i < _widgetsNew.size(); i++) {
            FormActivity._layout.addView(_widgetsNew.get(i).getView());
        }
        sPosition = ePosition = subSPosition = subSubSPosition = 0;
        isCategoryProfileFields = isSubCategoryProfileFields = isSubSubCategoryProfileFields = false;
        canChangeProfileFields = true;
    }

    public void removeViewFromSubSubCategorySelection() {
        FormActivity._layout.removeAllViews();

        if (ePosition != 0) {
            for (int k = ePosition; k > subSubSPosition; k--) {
                _widgetsNew.remove(k);
            }
            ePosition = 0;
            isSubSubCategoryProfileFields = false;
            canChangeProfileFields = true;
        }
        for (int i = 0; i < _widgetsNew.size(); i++) {
            FormActivity._layout.addView(_widgetsNew.get(i).getView());
        }
    }

    public void removeViewFromSubCategorySelection() {

        FormActivity._layout.removeAllViews();

        if (isSubCategoryProfileFields || isSubSubCategoryProfileFields) {

            if (ePosition != 0) {
                for (int k = ePosition; k > subSPosition; k--) {
                    _widgetsNew.remove(k);
                }
            } else if (subSubSPosition != 0) {
                _widgetsNew.remove(subSubSPosition);
            }
            ePosition = 0;
            subSubSPosition = 0;
            isSubCategoryProfileFields = false;
            isSubSubCategoryProfileFields = false;
            canChangeProfileFields = true;

        } else if (isCategoryProfileFields && ePosition != 0 && subSubSPosition != 0) {
            _widgetsNew.remove(subSubSPosition);
            subSubSPosition = 0;
            ePosition = ePosition - 1;

        } else if (subSubSPosition != 0) {
            _widgetsNew.remove(subSubSPosition);
            subSubSPosition = 0;
            ePosition = 0;
        }
        for (int i = 0; i < _widgetsNew.size(); i++) {
            FormActivity._layout.addView(_widgetsNew.get(i).getView());
        }
    }

    /**
     * Method to generate profile fields according to selected type and key.
     * @param fieldsJsonArray array which contains the profile fields.
     */
    public void generateProfileFields(JSONArray fieldsJsonArray) {

        int startPosition = sPosition;

        if (subSPosition != 0 && !_widgetsNew.get(subSPosition).getPropertyName().equals(mWidgetProperty))
            startPosition = subSPosition;
        if (selectedType.equals("subcategory_id") && !hasSubSubCategory && subSubSPosition != 0) {
            startPosition = subSubSPosition - 1;
        } else if (subSubSPosition != 0 && !_widgetsNew.get(subSubSPosition).getPropertyName().equals(mWidgetProperty)) {
            startPosition = subSubSPosition;
        }

        if (ePosition != 0) {
            for (int k = ePosition; k > startPosition; k--) {
                _widgetsNew.remove(k);
            }
            ePosition = 0;
        }

        try {

            for (int j = 0; j < fieldsJsonArray.length(); j++) {

                JSONObject fieldObject = fieldsJsonArray.getJSONObject(j);
                String fieldName = fieldObject.getString("name");
                String fieldLabel = fieldObject.optString("label");
                String fieldDescription = fieldObject.optString("description");
                boolean hasValidator = fieldObject.optBoolean("hasValidator");

                widget = formActivity.getWidget(mContext, fieldName, fieldObject, fieldLabel, hasValidator, isCreateForm,
                        fieldDescription, _widgetsNew, _mapNew, mCurrentSelectedOption, null,
                        mCategoryFields, null, null, null, mAdvancedMemberWhatWhereWithinmile);

                if (widget == null) continue;

                if (fieldObject.has(FormActivity.SCHEMA_KEY_HINT))
                    widget.setHint(fieldObject.getString(FormActivity.SCHEMA_KEY_HINT));

                ePosition = startPosition + j + 1;
                _widgetsNew.add(startPosition + j + 1, widget);
                positionMap.put(String.valueOf(startPosition + j + 1), String.valueOf(startPosition + j + 1));
                _mapNew.put(fieldName, widget);


            }

            FormActivity._layout.removeAllViews();
            for (int i = 0; i < _widgetsNew.size(); i++) {
                FormActivity._layout.addView(_widgetsNew.get(i).getView());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if (isChecked) {
            for (int i = 0; i < _widgetsNew.size(); i++) {
                if (_widgetsNew.get(i).getPropertyName().equals("repeat_day")) {
                    _widgetsNew.get(i).getView().setVisibility(View.VISIBLE);
                } else if (_widgetsNew.get(i).getPropertyName().equals("repeat_week") || _widgetsNew.get(i).getPropertyName().equals("repeat_weekday")) {
                    _widgetsNew.get(i).getView().setVisibility(View.GONE);
                }
            }

        } else {
            for (int i = 0; i < _widgetsNew.size(); i++) {
                if (_widgetsNew.get(i).getPropertyName().equals("repeat_week") || _widgetsNew.get(i).getPropertyName().equals("repeat_weekday")) {
                    _widgetsNew.get(i).getView().setVisibility(View.VISIBLE);
                } else 	if (_widgetsNew.get(i).getPropertyName().equals("repeat_day")) {
                    _widgetsNew.get(i).getView().setVisibility(View.GONE);
                }
            }
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.change_host:
                FormHostChange.sIsAddNewHost = false;
                for (int k = 0; k < _widgetsNew.size(); k++) {
                    if (_widgetsNew.get(k).getPropertyName().equals(FormHostChange.sEventHost)) {
                        _widgetsNew.get(k).getView().setVisibility(View.VISIBLE);
                    } else if (_widgetsNew.get(k).getPropertyName().equals("host_type_select") ||
                            _widgetsNew.get(k).getPropertyName().equals("host_auto")) {
                        _widgetsNew.get(k).getView().setVisibility(View.GONE);
                    }
                }
                break;

            case R.id.add_new_host:
                FormHostChange.sIsAddNewHost = true;
                FormHostChange.sIsCreateNewHost = true;
                FormActivity.loadEditHostForm++;
                int host_position = 0;

                for (int j = 0; j < _widgetsNew.size(); j++) {

                    if (_widgetsNew.get(j).getPropertyName().equals("host_type_select")) {
                        host_position = j;
                    } else if ( !createForm && (_widgetsNew.get(j).getPropertyName().equals("host_facebook") ||
                            _widgetsNew.get(j).getPropertyName().equals("host_twitter") ||
                            _widgetsNew.get(j).getPropertyName().equals("host_website"))) {
                        _widgetsNew.remove(j);
                        j--;
                    }
                }

                for (int i = 0; i < FormActivity.mHostCreateForm.length(); i++) {

                    JSONObject jsonObject = FormActivity.mHostCreateForm.optJSONObject(i);
                    String fieldName = jsonObject.optString("name");
                    String fieldLabel = jsonObject.optString("label");

                    widget = formActivity.getWidget(mContext, fieldName, jsonObject, fieldLabel, false, isCreateForm,
                            null, _widgetsNew, _mapNew, mCurrentSelectedOption, null,
                            null, null, null, null, null);

                    if (widget == null) continue;

                    if (jsonObject.has(FormActivity.SCHEMA_KEY_HINT))
                        widget.setHint(jsonObject.optString(FormActivity.SCHEMA_KEY_HINT));

                    _widgetsNew.add(host_position + i, widget);
                    _mapNew.put(fieldName, widget);

                }

                int socialLinkPosition = host_position + FormActivity.mHostCreateForm.length();
                socialLinkPosition++;

                for (int i = 0; i < FormActivity.mHostCreateFormSocial.length(); i++) {

                    JSONObject jsonObject = FormActivity.mHostCreateFormSocial.optJSONObject(i);
                    String fieldName = jsonObject.optString("name");
                    String fieldLabel = jsonObject.optString("label");

                    widget = formActivity.getWidget(mContext, fieldName, jsonObject, fieldLabel, false, isCreateForm,
                            null, _widgetsNew, _mapNew, mCurrentSelectedOption, null,
                            null, null, null, null, null);

                    if (widget == null) continue;

                    if (jsonObject.has(FormActivity.SCHEMA_KEY_HINT))
                        widget.setHint(jsonObject.optString(FormActivity.SCHEMA_KEY_HINT));

                    _widgetsNew.add(socialLinkPosition + i, widget);
                    _mapNew.put(fieldName, widget);

                }

                FormActivity._layout.removeAllViews();
                for (int k = 0; k < _widgetsNew.size(); k++) {

                    if (_widgetsNew.get(k).getPropertyName().equals(FormHostChange.sEventHost) ||
                            _widgetsNew.get(k).getPropertyName().equals("host_type_select") ||
                            _widgetsNew.get(k).getPropertyName().equals("host_auto") ||
                            _widgetsNew.get(k).getPropertyName().equals("host_facebook") ||
                            _widgetsNew.get(k).getPropertyName().equals("host_twitter") ||
                            _widgetsNew.get(k).getPropertyName().equals("host_website") ) {
                        _widgetsNew.get(k).getView().setVisibility(View.GONE);
                    }

                    FormActivity._layout.addView(_widgetsNew.get(k).getView());

                }
                break;

            case R.id.create_channel:
                Intent intent = new Intent(mContext, CreateNewEntry.class);
                intent.putExtra(ConstantVariables.CREATE_URL, AppConstant.DEFAULT_URL + "advancedvideos/channel/create");
                intent.putExtra("add_to_new_channel", true);
                intent.putExtra(ConstantVariables.EXTRA_MODULE_TYPE, ConstantVariables.ADV_VIDEO_CHANNEL_MENU_TITLE);
                ((Activity)mContext).startActivityForResult(intent, ConstantVariables.UPDATE_REQUEST_CODE);
                ((Activity)mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
        }
    }

    class SelectionHandler implements AdapterView.OnItemSelectedListener {
        protected FormWidget _widget;

        public SelectionHandler(FormWidget widget) {
            _widget = widget;
        }

        public void onItemSelected(AdapterView<?> parent, View arg1, int pos, long id) {
            if (_handler != null) {
                _handler.toggle(_widget);
            }
        }

        public void onNothingSelected(AdapterView<?> arg0) {
            // TODO Auto-generated method stub
        }

    }

}
