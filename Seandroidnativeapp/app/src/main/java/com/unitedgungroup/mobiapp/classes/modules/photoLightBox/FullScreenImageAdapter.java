/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */

package com.unitedgungroup.mobiapp.classes.modules.photoLightBox;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.unitedgungroup.mobiapp.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.wunderlist.slidinglayer.SlidingLayer;


public class FullScreenImageAdapter extends PagerAdapter implements View.OnClickListener {

    private Activity _activity;
    private LayoutInflater inflater;
    private View viewLayout;
    private TouchImageView imgDisplay;
    private ImageView placeholder;
    private ArrayList<PhotoListDetails> mPhotoDetails;
    public PhotoLongClickListener mLongPressListener;


    public FullScreenImageAdapter(Activity activity, PhotoLongClickListener photoLongClickListener,
                                  ArrayList<PhotoListDetails> photoDetail) {
        this._activity = activity;
        this.mPhotoDetails = photoDetail;
        mLongPressListener = photoLongClickListener;
    }


    @Override
    public int getCount() {
        return mPhotoDetails.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {


        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container,
                false);
        imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.image);
        placeholder = (ImageView) viewLayout.findViewById(R.id.placeholder);
        PhotoListDetails photoListDetails = mPhotoDetails.get(position);

        if (photoListDetails.getImageUrl() != null && !photoListDetails.getImageUrl().isEmpty()) {
            Picasso.with(_activity)
                    .load(photoListDetails.getImageUrl())
                    .into(imgDisplay, new Callback() {
                        @Override
                        public void onSuccess() {
                            placeholder.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }

        imgDisplay.setOnClickListener(this);

        imgDisplay.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mLongPressListener.onLongPressed(v);
                return false;
            }
        });
        container.addView(viewLayout);

        return viewLayout;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.image:


                if (_activity.findViewById(R.id.bottomView).getVisibility() == View.VISIBLE) {
                    _activity.findViewById(R.id.bottomView).setVisibility(View.GONE);
                    _activity.findViewById(R.id.topView).setVisibility(View.GONE);
                    ((SlidingLayer) _activity.findViewById(R.id.slidingLayer)).closeLayer(true);
                } else {
                    _activity.findViewById(R.id.bottomView).setVisibility(View.VISIBLE);
                    _activity.findViewById(R.id.topView).setVisibility(View.VISIBLE);
                }

                break;
            default:
                break;
        }
    }
    public interface PhotoLongClickListener{
        void onLongPressed(View view);
    }

}