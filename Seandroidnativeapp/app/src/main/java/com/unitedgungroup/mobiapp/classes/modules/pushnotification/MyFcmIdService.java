/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */

package com.unitedgungroup.mobiapp.classes.modules.pushnotification;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnResponseListener;
import com.unitedgungroup.mobiapp.classes.common.utils.PreferencesUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.UrlUtil;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class MyFcmIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();
        registerRefreshedToken(token);
    }

    /**
     * Method to Update FCM token on server when the token is refreshed.
     *
     * @param token Updated FCM token.
     */
    public void registerRefreshedToken(final String token) {
        AppConstant appConst = new AppConstant(getApplicationContext(), false);
        if (!appConst.isLoggedOutUser() && PreferencesUtils.getUserDetail(getApplicationContext()) != null) {
            Map<String, String> postParams = new HashMap<>();
            try {
                JSONObject userDetail = new JSONObject(PreferencesUtils.getUserDetail(getApplicationContext()));
                postParams.put("user_id", userDetail.optString("user_id"));
                postParams.put("registration_id", token);
                postParams.put("device_uuid", appConst.getDeviceUUID());
                appConst.postJsonResponseForUrl(UrlUtil.UPDATE_FCM_TOKEN_URL, postParams, new OnResponseListener() {
                    @Override
                    public void onTaskCompleted(JSONObject jsonObject) throws JSONException {

                    }

                    @Override
                    public void onErrorInExecutingTask(String message, boolean isRetryOption) {

                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
