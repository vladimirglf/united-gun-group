package com.unitedgungroup.mobiapp.classes.common.formgenerator;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;


import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.ui.SelectableTextView;
import com.unitedgungroup.mobiapp.classes.common.utils.GlobalFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class FormRadioButtons extends FormWidget{

    protected RadioGroup  _radioGroup;
    protected JSONObject _options;
    protected SelectableTextView _textView, _descriptionTextView;
    Context mContext;

    protected Map<String, String> _propmap;
    protected ArrayAdapter<String> _adapter;
    private String mCurrentSelectedOption, mProperty;

    public FormRadioButtons(final Context context, String property, JSONObject options, String label,
                            boolean hasValidator, String description, JSONObject jsonObjectProperty,
                            String currentSelectedOption){
        super( context, property, hasValidator );

        mContext = context;
        _options = options;
        _textView = new SelectableTextView( context );

        mCurrentSelectedOption = currentSelectedOption;

        _textView.setText(label);

        _textView.setTypeface(Typeface.DEFAULT_BOLD);

        if(description != null && !description.isEmpty()){
            _descriptionTextView = new SelectableTextView(context);
            _descriptionTextView.setText(description);
        }

        _radioGroup = new RadioGroup( context );
        if (jsonObjectProperty.optString("name").equals("end_date_enable") ||
                (mCurrentSelectedOption.equals("payment_method") && property.equals("method"))) {
            _radioGroup.setId(R.id.form_radio_button);
        }

        JSONArray propertyNames = options.names();

        String name, p = null;

        _propmap = new HashMap<>();
        _adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item);

        try{
            for( int i = 0 ; i < options.length(); i++)
            {
                name = propertyNames.getString(i);

                /**
                 * The Subscription plans will come with currency code
                 * Need to change currency code to currency symbol
                 * and add with radio button label
                 */
                if(mCurrentSelectedOption.equals("subscription_account") || mCurrentSelectedOption.equals("settings_subscription")){

                    JSONObject subscriptionDetails = options.optJSONObject(name);
                    p = subscriptionDetails.getString("label");
                    double price = subscriptionDetails.optDouble("price", 0);
                    String currencyCode = subscriptionDetails.optString("currency");

                    if(price != 0){
                        String priceWithCurrency = GlobalFunctions.getFormattedCurrencyString(currencyCode, price);
                        p += "(" + priceWithCurrency + ")";
                    }
                }else{
                    p = options.getString(name);
                }


                AppCompatRadioButton newRadioButton = new AppCompatRadioButton(context);
                newRadioButton.setText(p);
                newRadioButton.setId(i);

                _adapter.add( p );
                _propmap.put( p, name );

                if(i == 0 &&  !mCurrentSelectedOption.equals("settings_subscription")) {
                    newRadioButton.setChecked(true);
                }
                _radioGroup.addView(newRadioButton);
            }
        } catch( JSONException e){
            e.printStackTrace();
        }

        if (mCurrentSelectedOption.equals("settings_subscription")) {

            LinearLayout linearLayout = new LinearLayout(mContext);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            linearLayout.setLayoutParams(layoutParams);
            linearLayout.setOrientation(LinearLayout.VERTICAL);

            TextView subscriptionPlanTextView = new TextView(mContext);
            TextView subscriptionPriceTextView = new TextView(mContext);

            subscriptionPlanTextView.setText(mContext.getResources().getString(R.string.subscription_plan_content_prefix)
                    + " " + FormActivity.subscriptionPlanDescription);
            subscriptionPriceTextView.setText(mContext.getResources().getString(R.string.subscription_plan_price_prefix)
                    + " " + FormActivity.subscriptionPriceDescription);

            subscriptionPlanTextView.setPadding(0,0,0,5);
            subscriptionPriceTextView.setPadding(0,0,0,5);

            linearLayout.addView(subscriptionPlanTextView);
            linearLayout.addView(subscriptionPriceTextView);

            _layout.addView(linearLayout);

        }

        _layout.addView(_textView);
        if(_descriptionTextView != null)
            _layout.addView(_descriptionTextView);

        _layout.addView( _radioGroup );
    }

    @Override
    public String getValue() {

        int pos = _radioGroup.getCheckedRadioButtonId();
        return _propmap.get(_adapter.getItem(pos));

    }

    @Override
    public void setValue(String value)
    {
        try{
            String name;
            JSONArray names = _options.names();
            for( int i = 0; i < names.length(); i++ )
            {
                name = names.getString(i);

                if( name.equals(value) )
                {
                    String item;
                    /**
                     * Work for subscription plans.
                     * to get the radio button label with currency symbol
                     */
                    if(mCurrentSelectedOption.equals("subscription_account")){

                        JSONObject subscriptionDetails = _options.optJSONObject(name);
                        item = subscriptionDetails.getString("label");
                        double price = subscriptionDetails.optDouble("price", 0);
                        String currencyCode = subscriptionDetails.optString("currency");

                        if(price != 0){
                            String priceWithCurrency = GlobalFunctions.getFormattedCurrencyString(currencyCode, price);
                            item += "(" + priceWithCurrency + ")";
                        }
                    }else{
                        item = _options.getString(name);
                    }
                    AppCompatRadioButton radioButton = (AppCompatRadioButton)_radioGroup
                            .getChildAt(_adapter.getPosition(item));
                    radioButton.setChecked(true);
                }
            }
        } catch( JSONException e ){
            e.printStackTrace();
        }
    }

    @Override
    public void setToggleHandler( FormActivity.FormWidgetToggleHandler handler ) {
        super.setToggleHandler(handler);
    }

}
