/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 *
 */

package com.unitedgungroup.mobiapp.classes.common.formgenerator;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.activities.CreateNewEntry;
import com.unitedgungroup.mobiapp.classes.common.adapters.AddPeopleAdapter;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.utils.PreferencesUtils;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;
import com.unitedgungroup.mobiapp.classes.core.ConstantVariables;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * FormActivity allows you to create dynamic form layouts based upon a json schema file.
 * This class should be sub-classed.
 *
 * @author Jeremy Brown
 */

public class FormActivity extends AppCompatActivity implements View.OnClickListener, CheckBox.OnCheckedChangeListener {
    public static final String SCHEMA_KEY_TYPE= "type";
    public static final String SCHEMA_KEY_SLIDER = "Slider";
    public static final String SCHEMA_KEY_BOOL_UPPER = "Checkbox";
    public static final String SCHEMA_KEY_BOOL = "checkbox";
    public static final String SCHEMA_KEY_SELECT_UPPER = "Select";
    public static final String SCHEMA_KEY_SELECT = "select";
    public static final String SCHEMA_KEY_STRING_UPPER = "Text";
    public static final String SCHEMA_KEY_STRING = "text";
    public static final String SCHEMA_KEY_PASSWORD = "Password";
    public static final String SCHEMA_KEY_TextArea = "Textarea";
    public static final String SCHEMA_KEY_TextArea_LOWER = "textarea";
    public static final String SCHEMA_KEY_TOGGLES = "toggles";
    public static final String SCHEMA_KEY_DEFAULT = "default";
    public static final String SCHEMA_KEY_OPTIONS = "multiOptions";
    public static final String SCHEMA_KEY_HINT = "label";
    public static final String SCHEMA_KEY_FILE = "File";
    public static final String SCHEMA_KEY_FILE1 = "file";
    public static final String SCHEMA_KEY_FLOAT = "Float";
    public static final String SCHEMA_KEY_INTEGER = "Integer";
    public static final String TAG = "FormActivity";
    public static final String SCHEMA_KEY_RADIO = "Radio";
    public static final String SCHEMA_KEY_MULTI_CHECKBOX = "Multicheckbox";
    public static final String SCHEMA_KEY_MULTI_CHECKBOX_UPPER = "MultiCheckbox";
    public static final String SCHEMA_KEY_MULTI_CHECKBOX_UNDERSCORE = "Multi_checkbox";
    public static final String SCHEMA_KEY_DATE = "Date";
    public static final String SCHEMA_KEY_HIDDEN = "Hidden";
    public static final String SCHEMA_KEY_RATING = "Rating";
    public static final String SCHEMA_KEY_BLOCKED_LIST = "blockList";
    public static final String SCHEMA_KEY_MULTI_SELECT= "Multiselect";
    public static final String SCHMEA_KEY_HOST = "host";
    public static final String SCHEMA_KEY_DUMMY = "Dummy";
    public static final String Poll_OPTIONS = "options";
    public static final String Poll_OPTIONS_NAME = "name";
    public List<String> pollOptionsList = new ArrayList<>();
    public List<String> pollOptionListCount = new ArrayList<>();

    public static final LayoutParams defaultLayoutParams = CustomViews.getFullWidthLayoutParams();

    // -- data
    protected Map<String, FormWidget> _map;
    public static ArrayList<FormWidget> _widgets;
    protected ArrayList<String> jsonObjects;
    FormMultiCheckBox formMultiCheckBox;

    // -- widgets
    protected LinearLayout _container;
    protected static LinearLayout _layout;
    protected ScrollView  _viewport;
    public String mCurrentSelectedOption = null;

    public JSONObject pollJsonObject;
    public String pollFieldName;
    public String pollFieldLabel;

    public boolean pollCreateForm, hasBody = false, pollHasValidator, mIsSpinnerWithIcon = false;
    int optionListSize;

    String addOptionButton, mAdvancedMemberWhatWhereWithinmile;

    FormWidget pollWidget;
    int temp = 0;
    int optionPosition = 4;
    public int flag = 3;

    JSONObject hostDetails, subCategory, categoryFields, repeatOccurences, formValues = null;
    public static Map<String, String> selectedGuest;

    public static String createDiaryDescription, addToDiaryDescription, hostType, hostKey, host_id,
            subscriptionPriceDescription, subscriptionPlanDescription, overviewText;

    public static JSONObject subSubCategory = null, mHostForm, buyerFormValues;
    public static JSONArray mHostSelectionForm, mHostCreateForm, mHostCreateFormSocial;
    public int mContentId;
    public static int reset = 0, loadEditHostForm = 0;
    public static Boolean sIsAddToDiaryDescription = false, sIsCreateDiaryDescription = false;
    public static AddPeopleAdapter mAddPeopleAdapter;
    public static JSONArray mTickerHolderForm, mBuyerForm;
    public String first_name, last_name;
    public Context mContext = this;
    JSONArray chequeForm;

    // -----------------------------------------------
    //
    // parse data and build view
    //
    // -----------------------------------------------

    /**
     * parses a supplied schema of raw json data and creates widgets
     * @param data - the raw json data as a String
     */
    public View generateForm(JSONObject data, boolean createForm, String currentSelectedOption, int contentId) {

        mContentId = contentId;
        return generateForm(data, createForm, currentSelectedOption);
    }

    /**
     * parses a supplied schema of raw json data and creates widgets
     * @param data - the raw json data as a String
     * @param firstName -to set first_name value in sign_up form
     * @param lastName - to set last_name value in sign_up form
     */
    public View generateForm(JSONObject data, boolean createForm, String currentSelectedOption,
                             String firstName, String lastName) {

        first_name = firstName;
        last_name = lastName;
        return generateForm(data, createForm, currentSelectedOption);
    }


    // -----------------------------------------------
    //
    // parse data and build view
    //
    // -----------------------------------------------

    /**
     * parses a supplied schema of raw json data and creates widgets
     * @param data - the raw json data as a String
     */
    public View generateForm(JSONObject data, boolean createForm, String currentSelectedOption) {

        int margin = (int) (getResources().getDimension(R.dimen.margin_5dp) /
                getResources().getDisplayMetrics().density);
        defaultLayoutParams.setMargins(margin, margin, margin, 0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            defaultLayoutParams.setMarginEnd(margin);
            defaultLayoutParams.setMarginStart(margin);
        }
        mCurrentSelectedOption = currentSelectedOption;
        _widgets = new ArrayList<>();
        _map = new HashMap<>();
        jsonObjects = new ArrayList<>();
        selectedGuest = new HashMap<>();
        FormSpinner.sIsChangeOccurence = false;
        sIsAddToDiaryDescription = false;
        sIsCreateDiaryDescription = false;
        FormSpinner.sIsChangedMonthlyField = false;
        FormSpinner.sEditPageLoadingTime = 0;
        FormSpinner.sEditPageLoadingTimeRepeatEvent = 0;
        FormSpinner.sIsEventRepeatTypeMonthly = false;
        FormHostChange.sIsCreateNewHost = false;
        loadEditHostForm = 0;

        try {
            FormWidget widget;
            JSONArray formArray;
            JSONArray names;
            JSONArray ratingArray = null;

            if (!currentSelectedOption.equals("signup_fields") && !currentSelectedOption.
                    equals("edit_member_profile")){
                formArray = data.optJSONArray("form");
                if (formArray == null) {
                    formArray = data.optJSONArray("response");
                    if (currentSelectedOption.equals("home") && formArray != null) {
                        mIsSpinnerWithIcon = true;
                    }
                }

                if (createForm) {
                    switch (currentSelectedOption) {
                        case "core_main_siteevent":
                            hostDetails = data.optJSONObject("host");
                            subCategory = data.optJSONObject("subcategories");
                            categoryFields = data.optJSONObject("fields");
                            repeatOccurences = data.optJSONObject("repeatOccurences");
                            mHostSelectionForm = data.optJSONArray("hostSelectionForm");
                            mHostCreateForm = data.optJSONArray("hostCreateForm");
                            mHostCreateFormSocial = data.optJSONArray("hostCreateFormSocial");
                            break;
                        case "create_review":
                            ratingArray = data.optJSONArray("ratingParams");
                            break;
                        case ConstantVariables.STORE_MENU_TITLE:
                        case ConstantVariables.PRODUCT_MENU_TITLE:
                        case ConstantVariables.ADV_VIDEO_MENU_TITLE:
                        case ConstantVariables.ADV_VIDEO_CHANNEL_MENU_TITLE:
                        case "sitereview_listing":
                        case "sitereview_video":
                            ratingArray = data.optJSONArray("ratingParams");
                            subCategory = data.optJSONObject("subcategories");
                            if (subCategory == null)
                                subCategory = data.optJSONObject("categoriesForm");
                            categoryFields = data.optJSONObject("fields");
                            break;
                        case "add_wishlist":
                            createDiaryDescription = data.optString("create_wishlist_descriptions");
                            addToDiaryDescription = data.optString("add_wishlist_description");
                            break;
                        case "add_to_playlist":
                            createDiaryDescription = data.optString("create_playlist_description");
                            addToDiaryDescription = data.optString("add_playlist_description");
                            break;
                        case "add_to_diary":
                            createDiaryDescription = data.optString("create_diary_descriptions");
                            addToDiaryDescription = data.optString("add_diary_description");
                            break;
                        case "sitepage":
                        case "core_main_sitegroup":
                            subCategory = data.optJSONObject("subCategories");
                            categoryFields = data.optJSONObject("fields");
                            break;
                        case "core_main_user":
                            mAdvancedMemberWhatWhereWithinmile = data.optString("whatWhereWithinmile");
                            categoryFields = data.optJSONObject("fields");
                            break;
                        case "add_to_friend_list":
                            createDiaryDescription = getResources().getString(R.string.create_list_description);
                            addToDiaryDescription = getResources().getString(R.string.add_to_list_description);
                            break;
                        case "settings_subscription":
                            formArray = data.optJSONArray("subscription_form");
                            subscriptionPlanDescription = data.optString("current_subsciption_plan");
                            subscriptionPriceDescription = data.optString("current_subsciption_price");
                            break;
                        case "payment_method":
                            formArray = data.optJSONArray("paymentForm");
                            chequeForm = data.optJSONArray("chequeForm");
                            break;
                    }
                } else {
                    switch (currentSelectedOption) {
                        case "signup_account":
                            formArray = data.optJSONArray("account");
                            break;
                        case "subscription_account":
                            formArray = data.optJSONArray("subscription");
                            break;
                        case "core_main_siteevent":
                            hostDetails = data.optJSONObject("host");
                            subCategory = data.optJSONObject("subcategories");
                            categoryFields = data.optJSONObject("fields");
                            repeatOccurences = data.optJSONObject("repeatOccurences");
                            mHostSelectionForm = data.optJSONArray("hostSelectionForm");
                            mHostCreateForm = data.optJSONArray("hostCreateForm");
                            mHostCreateFormSocial = data.optJSONArray("hostCreateFormSocial");
                            break;
                        case ConstantVariables.STORE_MENU_TITLE:
                        case ConstantVariables.PRODUCT_MENU_TITLE:
                        case ConstantVariables.ADV_VIDEO_MENU_TITLE:
                        case ConstantVariables.ADV_VIDEO_CHANNEL_MENU_TITLE:
                        case "sitereview_listing":
                            ratingArray = data.optJSONArray("ratingParams");
                            subCategory = data.optJSONObject("subcategories");
                            categoryFields = data.optJSONObject("fields");
                            formValues = data.optJSONObject("formValues");
                            break;
                        case "update_review":
                            ratingArray = data.optJSONArray("ratingParams");
                            break;
                        case "sitepage":
                        case "core_main_sitegroup":
                            subCategory = data.optJSONObject("subCategories");
                            categoryFields = data.optJSONObject("fields");
                            break;
                        case "buyer_form":
                            formArray = data.optJSONArray("buyerForm");
                            mTickerHolderForm = data.optJSONArray("tickerHolderForm");
                            break;
                    }
                }

                if (formArray != null ) {

                    // If rating params exists then showing them on top of the form.
                    if (ratingArray != null) {

                        getFormWidgets(ratingArray, createForm);

                    }

                    if (currentSelectedOption.equals("settings_notifications")) {

                        // Show Notification types with the category as heading on Notifications Setting page
                        for (int j = 0; j < formArray.length(); j++) {
                            JSONObject jsonObject = formArray.getJSONObject(j);

                            // Added description at the top.
                            if (j == 0) {
                                widget = getTextViewWidget(this, SCHEMA_KEY_STRING,
                                        mContext.getResources().getString(R.string.notification_description));
                                _widgets.add(widget);
                            }

                            String category = jsonObject.optString("category");
                            if (category != null) {
                                widget = getTextViewWidget(this, SCHEMA_KEY_STRING, category);
                                _widgets.add(widget);
                            }
                            JSONArray typeJsonArray = jsonObject.optJSONArray("types");
                            getFormWidgets(typeJsonArray, createForm);
                        }

                    } else {
                        getFormWidgets(formArray, createForm);
                    }
                }

                if (currentSelectedOption.equals("buyer_form") && mTickerHolderForm != null) {

                    getFormWidgets(mTickerHolderForm, createForm);

                } else if (mCurrentSelectedOption.equals("payment_method") && chequeForm != null)  {

                    getFormWidgets(chequeForm, createForm);
                }

            } else {
                if(data != null){

                    if(currentSelectedOption.equals("edit_member_profile")){
                        data = data.optJSONObject("form");
                    }
                    names = data.names();

                    for(int i = 0; i < data.length(); i++){
                        String jsonArrayKey = names.optString(i);

                        if(!jsonArrayKey.equals("") && !jsonArrayKey.isEmpty()){
                            widget = getTextViewWidget(this, SCHEMA_KEY_STRING, jsonArrayKey);
                            _widgets.add(widget);
                        }

                        getFormWidgets(data.getJSONArray(jsonArrayKey), createForm);

                    }
                }
            }

        } catch( JSONException e ) {
            e.printStackTrace();
        }


        // -- create the layout

        _container = new LinearLayout( this );
        _container.setOrientation(LinearLayout.VERTICAL);
        _container.setLayoutParams( FormActivity.defaultLayoutParams );

        _viewport  = new ScrollView( this );
        _viewport.setLayoutParams(FormActivity.defaultLayoutParams);

        _layout = new LinearLayout( this );
        _layout.setOrientation(LinearLayout.VERTICAL);
        _layout.setLayoutParams(FormActivity.defaultLayoutParams);


        initToggles();

        for( int i = 0; i < _widgets.size(); i++ ) {

            if (mCurrentSelectedOption.equals("core_main_siteevent") && !createForm) {
                switch(_widgets.get(i).getPropertyName()){
                    case "host_title":
                    case "host_description":
                    case "host_photo":
                    case "host_link":
                    case "host_facebook":
                    case "host_twitter":
                    case "host_website":
                        _widgets.get(i).getView().setVisibility(View.GONE);
                        break;
                }
            }

            _layout.addView(_widgets.get(i).getView());

        }

        switch(mCurrentSelectedOption) {
            case "core_main_poll":
                Button addOptions = (Button) _layout.findViewById(R.id.label);
                optionListSize = pollOptionListCount.size();
                if (addOptions != null) {
                    addOptions.setOnClickListener(this);
                }
                break;

            case "core_main_event":
            case "core_main_group":
            case "core_main_siteevent":
                CheckBox checkBox = (CheckBox) _layout.findViewById(R.id.form_checkbox);
                if (checkBox != null)
                    checkBox.setOnCheckedChangeListener(this);
                break;
            case "core_main_forum":
            case "core_main_classified":
            case "core_main_blog":
                Button continueButton = new Button(this);
                if (createForm) {
                    continueButton.setText(getResources().getString(R.string.create_desc_button_title));
                } else {
                    continueButton.setText(getResources().getString(R.string.edit_desc_button_title));
                }
                continueButton.setLayoutParams(defaultLayoutParams);
                continueButton.setId(R.id.add_description);
                if(hasBody) {
                    continueButton.setVisibility(View.VISIBLE);
                }else {
                    continueButton.setVisibility(View.GONE);
                }
                _layout.addView(continueButton);

                break;
            case "sitereview_listing":
                RadioGroup radioGroup = (RadioGroup) _layout.findViewById(R.id.form_radio_button);
                if (radioGroup != null) {
                    for (int i = 0; i < _widgets.size(); i++) {
                        if ((formValues == null && _widgets.get(i).getPropertyName().equals("end_date") &&
                                radioGroup.getCheckedRadioButtonId() == 0) ||
                                (formValues != null && formValues.optString("end_date_enable").equals("0") &&
                                        _widgets.get(i).getPropertyName().equals("end_date"))) {
                            _widgets.get(i).getView().setVisibility(View.GONE);
                        }
                    }
                    radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            for (int i = 0; i<_widgets.size(); i++) {
                                if (_widgets.get(i).getPropertyName().equals("end_date")) {
                                    _widgets.get(i).getView().setVisibility(checkedId == 0 ? View.GONE : View.VISIBLE);
                                }
                            }
                        }
                    });
                }
                break;

            case "home":
            case "message":
                LinearLayout uploadVideoLayout = new LinearLayout(this);
                uploadVideoLayout.setLayoutParams(defaultLayoutParams);
                uploadVideoLayout.setOrientation(LinearLayout.VERTICAL);
                TextView uploadVideoText = new TextView(this);
                uploadVideoText.setText(getResources().getString(R.string.my_device_video_msg));
                uploadVideoText.setLayoutParams(defaultLayoutParams);
                uploadVideoLayout.addView(uploadVideoText);
                Button uploadVideo = new Button(this);
                uploadVideo.setText(getResources().getString(R.string.upload));
                uploadVideo.setLayoutParams(defaultLayoutParams);
                uploadVideoLayout.addView(uploadVideo);
                uploadVideoLayout.setVisibility(View.GONE);
                uploadVideoLayout.setId(R.id.button);
                _layout.addView(uploadVideoLayout);

                uploadVideo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent createEntry = new Intent(FormActivity.this, CreateNewEntry.class);
                        List<String> enabledModuleList = null;
                        if (PreferencesUtils.getEnabledModuleList(mContext) != null) {
                            enabledModuleList = new ArrayList<>(Arrays.asList(PreferencesUtils.getEnabledModuleList(mContext).split("\",\"")));
                        }
                        String url;
                        if (enabledModuleList != null && enabledModuleList.contains("sitevideo")
                                && !Arrays.asList(ConstantVariables.DELETED_MODULES).contains("sitevideo")) {
                            url = AppConstant.DEFAULT_URL + "advancedvideos/create";
                            createEntry.putExtra(ConstantVariables.EXTRA_MODULE_TYPE, ConstantVariables.ADV_VIDEO_MENU_TITLE);
                        } else {
                            url = AppConstant.DEFAULT_URL + "videos/create";
                            createEntry.putExtra(ConstantVariables.EXTRA_MODULE_TYPE, ConstantVariables.VIDEO_MENU_TITLE);
                        }
                        createEntry.putExtra(ConstantVariables.CREATE_URL, url);
                        createEntry.putExtra(ConstantVariables.AAF_VIDEO, true);
                        startActivityForResult(createEntry, ConstantVariables.CREATE_REQUEST_CODE);
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                });

                break;
            case "payment_method":
                RadioGroup paymentMethod = (RadioGroup) _layout.findViewById(R.id.form_radio_button);
                if (paymentMethod != null) {

                    setChequeFormVisibility();

                    paymentMethod.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {

                            setChequeFormVisibility();

                        }
                    });
                }
                break;

        }

        _viewport.addView(_layout);
        _container.addView(_viewport);
        _container.setId(R.id.form_layout);

        return _container;
    }


    //Set visiblity of cheque form field on payment method selections

    private void setChequeFormVisibility() {

        String id = _widgets.get(0).getValue();
        for (int i = 0; i < _widgets.size(); i++) {
            if (_widgets.get(i).getPropertyName().equals("cheque_no") ||
                    _widgets.get(i).getPropertyName().equals("signature") ||
                    _widgets.get(i).getPropertyName().equals("account_no") ||
                    _widgets.get(i).getPropertyName().equals("routing_no")) {

                if (id.equals("3")) {
                    _widgets.get(i).getView().setVisibility(View.VISIBLE);
                } else {
                    _widgets.get(i).getView().setVisibility(View.GONE);
                }
            }

        }
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if (formMultiCheckBox != null) {
            for (int i = 0; i < formMultiCheckBox._options.length(); i++) {
                AppCompatCheckBox compatCheckBox  = formMultiCheckBox.checkBoxArray.get(i);
                if (compatCheckBox != null) {
                    compatCheckBox.setChecked(isChecked);
                }
            }
        }

        /**
         * Hide Venue name and Location fields if Online Event checkbox is checked in Adv Events
         * creation form.
         */
        if (mCurrentSelectedOption.equals("core_main_siteevent")) {
            for (int i = 0; i < _widgets.size(); i++) {
                if (_widgets.get(i).getPropertyName().equals("venue_name") ||
                        _widgets.get(i).getPropertyName().equals("location")) {
                    _widgets.get(i).getView().setVisibility(isChecked ? View.GONE : View.VISIBLE);
                }
            }
        }
    }


    //Add Poll Options, click on add another options button
    @Override
    public void onClick(View view) {

        if (optionListSize > 0) {

            int size = _widgets.size();
            String fieldName = "options_" + flag;
            String optionsLabel = "Possible answer";
            temp = 1;

            pollWidget = getWidget(this, fieldName, pollJsonObject, optionsLabel, pollHasValidator, pollCreateForm, null,
                    null, null, mCurrentSelectedOption, null, null, null, null, null, null);

            _widgets.add(optionPosition, pollWidget);

            size++;
            optionPosition++;

            _map.put(fieldName, pollWidget);


            _container.removeAllViews();
            _viewport.removeAllViews();
            _layout.removeAllViews();


            _layout = new LinearLayout(this);
            _layout.setOrientation(LinearLayout.VERTICAL);
            _layout.setLayoutParams(FormActivity.defaultLayoutParams);

            initToggles();

            for (int i = 0; i < size; i++) {
                _layout.addView((_widgets.get(i).getView()));
            }


            _viewport.addView(_layout);
            _container.addView(_viewport);

            optionListSize--;
            flag++;

        }
    }


    public void getFormWidgets(JSONArray jsonArray, boolean createForm) {
        FormWidget widget;
        for (int j = 0; j < jsonArray.length(); j++) {

            JSONObject jsonObject = jsonArray.optJSONObject(j);

            if (mCurrentSelectedOption.equals( "core_main_poll"))
            {
                pollCreateForm = createForm;
                pollFieldName = jsonObject.optString("name");
                pollFieldLabel = jsonObject.optString("label");
                pollHasValidator = jsonObject.optBoolean("hasValidator");
            }

            String fieldName = jsonObject.optString("name");
            String fieldLabel = jsonObject.optString("label");
            String fieldDescription = jsonObject.optString("description");

            if (jsonObject.optString("type").equals("Checkbox") && fieldName.equals("terms"))
                fieldLabel = jsonObject.optString("description");

            boolean hasValidator = jsonObject.optBoolean("hasValidator");

            boolean toggles = hasToggles(jsonObject);

            widget = getWidget(this, fieldName, jsonObject, fieldLabel, hasValidator, createForm,
                    fieldDescription, _widgets, _map, mCurrentSelectedOption, subCategory, categoryFields, repeatOccurences, null, formValues, mAdvancedMemberWhatWhereWithinmile);

            if (widget == null) continue;

            if (toggles) {
                widget.setToggles(processToggles(jsonObject));
                widget.setToggleHandler(new FormWidgetToggleHandler());
            }

            if (jsonObject.has(FormActivity.SCHEMA_KEY_HINT))
                widget.setHint(jsonObject.optString(FormActivity.SCHEMA_KEY_HINT));

            _widgets.add(widget);
            _map.put(fieldName, widget);

        }

    }

    // -----------------------------------------------
    ///
    // populate and save
    //
    // -----------------------------------------------

    /**
     * this method fills the form with existing data
     * get the json string stored in the record we are editing
     * create a json object ( if this fails then we know there is now existing record )
     * create a list of property names from the json object
     * loop through the map returned by the Form class that maps widgets to property names
     * if the map contains the property name as a key that means there is a widget to populate w/ a value
     */
    public View populate( JSONObject jsonString, String currentSelectedOption) {
        mCurrentSelectedOption = currentSelectedOption;
        View view =  generateForm(jsonString, false,currentSelectedOption);

        try {
            FormWidget widget;
            switch (currentSelectedOption) {
                case "settings_notifications": {
                    JSONArray formValues = jsonString.optJSONArray("formValues");
                    for (int i = 0; i < formValues.length(); i++) {
                        String name = formValues.optString(i);
                        if ((!name.equals("photo")) && _map.containsKey(name)) {
                            widget = _map.get(name);
                            widget.setValue("1");
                        }
                    }
                    break;
                }

                case "signup_account":{

                    JSONArray formFields = jsonString.optJSONArray("account");
                    JSONObject formValues = jsonString.optJSONObject("formValues");
                    if (formFields != null && formFields.length() != 0) {
                        for (int j = 0; j < formFields.length(); j++) {
                            JSONObject jsonObject = formFields.optJSONObject(j);
                            String name = jsonObject.optString("name");
                            if ((!name.equals("photo")) && _map.containsKey(name)) {
                                widget = _map.get(name);
                                widget.setValue(formValues.optString(name));
                            }
                        }
                    }
                    break;
                }

                case "subscription_account": {

                    JSONArray formFields = jsonString.optJSONArray("subscription");
                    JSONObject formValues = jsonString.optJSONObject("formValues");
                    if (formFields != null && formFields.length() != 0) {
                        for (int j = 0; j < formFields.length(); j++) {
                            JSONObject jsonObject = formFields.optJSONObject(j);
                            String name = jsonObject.optString("name");
                            if ((!name.equals("photo")) && _map.containsKey(name)) {
                                widget = _map.get(name);
                                widget.setValue(formValues.optString(name));
                            }
                        }
                    }
                    break;
                }

                case "update_review": {
                    JSONArray ratingFields = jsonString.optJSONArray("ratingParams");
                    JSONObject formValues = jsonString.optJSONObject("formValues");
                    JSONArray formFields = jsonString.optJSONArray("form");

                    if (formFields != null && formFields.length() != 0) {
                        for (int j = 0; j < formFields.length(); j++) {
                            JSONObject jsonObject = formFields.optJSONObject(j);
                            String name = jsonObject.optString("name");
                            if (_map.containsKey(name)) {
                                widget = _map.get(name);
                                widget.setValue(formValues.optString(name));

                            }
                        }
                    }

                    if (ratingFields != null && ratingFields.length() != 0) {
                        for (int j = 0; j < ratingFields.length(); j++) {
                            JSONObject jsonObject = ratingFields.optJSONObject(j);
                            String name = jsonObject.optString("name");
                            if (_map.containsKey(name)) {
                                widget = _map.get(name);
                                widget.setValue(formValues.optString(name));

                            }
                        }
                    }


                    break;
                }

                case "settings_privacy":
                case "notification_settings": {
                    JSONArray formFields = jsonString.optJSONArray("form");
                    JSONObject formValues = jsonString.optJSONObject("formValues");
                    if (formFields != null && formFields.length() != 0) {
                        for (int j = 0; j < formFields.length(); j++) {
                            JSONObject jsonObject = formFields.optJSONObject(j);
                            String name = jsonObject.optString("name");
                            if ((!name.equals("photo")) && _map.containsKey(name)) {
                                widget = _map.get(name);
                                widget.setValue(formValues.optString(name));
                            }
                        }
                    }
                    break;

                }

                case "buyer_form": {
                    JSONArray formFields = jsonString.optJSONArray("buyerForm");
                    buyerFormValues = jsonString.optJSONObject("buyerFormValues");
                    if (formFields != null && formFields.length() != 0) {
                        for (int j = 0; j < formFields.length(); j++) {
                            JSONObject jsonObject = formFields.optJSONObject(j);
                            String name = jsonObject.optString("name");
                            if ((!name.equals("photo")) && _map.containsKey(name)) {
                                widget = _map.get(name);
                                widget.setValue(buyerFormValues.optString(name));
                            }
                        }
                    }
                    break;
                }

                default: {

                    JSONArray formFields = jsonString.optJSONArray("form");
                    JSONObject formValues = jsonString.optJSONObject("formValues");
                    if (formFields != null && formFields.length() != 0) {
                        for (int j = 0; j < formFields.length(); j++) {
                            JSONObject jsonObject = formFields.optJSONObject(j);
                            String name = jsonObject.optString("name");
                            if ((!name.equals("photo")) && _map.containsKey(name)) {
                                widget = _map.get(name);
                                widget.setValue(formValues.optString(name));
                            }
                        }
                    } else {
                        JSONArray mFieldNames = formValues.names();
                        for (int j = 0; j < formValues.length(); j++) {
                            String fieldName = mFieldNames.optString(j);
                            JSONObject fieldJsonObject = formValues.optJSONObject(fieldName);
                            if ((!fieldName.equals("photo")) && _map.containsKey(fieldName)) {
                                widget = _map.get(fieldName);
                                widget.setValue(fieldJsonObject.getString("value"));
                            }
                        }
                    }

                    JSONObject profileFieldsObject = formValues.optJSONObject("profile_fields");
                    if (profileFieldsObject == null) {
                        JSONArray profileFieldsArray = formValues.optJSONArray("profile_fields");
                        profileFieldsObject = convertToJsonObject(profileFieldsArray);
                    }

                    if (formFields != null) {
                        for (int j = 0; j < formFields.length(); j++) {
                            JSONObject jsonObject = formFields.optJSONObject(j);
                            String name = jsonObject.optString("name");
                            if ((!name.equals("photo")) && _map.containsKey(name) && profileFieldsObject != null) {
                                widget = _map.get(name);
                                if (profileFieldsObject.has(name)) {
                                    widget.setValue(profileFieldsObject.optString(name));
                                } else {
                                    widget.setValue(formValues.optString(name));
                                }
                            } else {
                                widget = _map.get(name);
                                if (formValues.has(name))
                                    if (widget != null) {
                                        widget.setValue(formValues.optString(name));
                                    }
                            }
                        }
                    }

                    break;
                }
            }
        } catch ( JSONException | NullPointerException e ) {
            e.printStackTrace();
        }

        return view;
    }

    /**
     * this method preps the data and saves it
     * if there is a problem w/ creating the json string, the method fails
     * loop through each widget and set a property on a json object to the value of the widget's getValue() method
     */
    public HashMap<String, String> save() {
        FormWidget widget;
        HashMap <String, String> params = new HashMap<>();

        boolean success = true;
        if(_widgets != null) {
            for (int i = 0; i < _widgets.size(); i++) {
                widget = _widgets.get(i);

                if (mCurrentSelectedOption != null && mCurrentSelectedOption.equals("core_main_siteevent") &&
                        widget.getPropertyName().equals(FormHostChange.sEventHost)) {

                    if (hostType != null) {
                        params.put("host_type", hostType);
                    }
                    if (!FormHostChange.sIsAddNewHost) {
                        params.put("host_id", host_id);
                    }
                } else {
                    if (!widget.getPropertyName().equals("searchGuests")
                            && !widget.getPropertyName().equals("toValues")) {
                        if (!widget.getPropertyName().equals("text")) {
                            if (widget.is_hasValidator() && widget.getValue().isEmpty() &&
                                    widget.getView().getVisibility() == View.VISIBLE) {

                                success = false;
                                widget.setErrorMessage(getResources().getString(R.string.widget_error_msg));

                            }

                                else if (widget.getPropertyName().equals("overview") && (mCurrentSelectedOption.equals("sitereview_listing")
                                    || mCurrentSelectedOption.equals("core_main_siteevent")) && overviewText != null) {
                                params.put("overview", overviewText);

                            } else {
                                params.put(widget.getPropertyName(), widget.getValue());
                            }
                        }

                        if(widget.getPropertyName().equals("category_id"))
                        {
                            if (widget.getValue().equals("0") &&
                                    widget.getView().getVisibility() == View.VISIBLE) {

                                success = false;
                                widget.setErrorMessage(getResources().getString(R.string.widget_error_msg));

                            }
                        }


                    }
                    else
                    {
                        Set<String> keySet = selectedGuest.keySet();

                        String res = "";
                        for (Iterator<String> iterator = keySet.iterator(); iterator.hasNext(); ) {
                            res += iterator.next() + (iterator.hasNext() ? "," : "");
                        }

                        if(widget.getPropertyName().equals("searchGuests")){
                            params.put("user_ids", res);
                        }else{
                            params.put("toValues", res);
                        }


                    }
                }
            }
        }

        if( success ) {
            return params;
        }

        return null;
    }

    public boolean showValidations(JSONObject validationMessages) {

        FormWidget widget;
        boolean error = false;
        for( int i = 0; i < _widgets.size(); i++ ) {
            widget = _widgets.get(i);
            String fieldName = widget.getPropertyName();

            if(validationMessages.has(fieldName)) {

                try {
                    String message = validationMessages.getString(fieldName);
                    widget.setErrorMessage(message);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                error = true;
            }
        }
        return error;
    }

    // -----------------------------------------------
    //
    // toggles
    //
    // -----------------------------------------------

    /**
     * creates the map a map of values for visibility and references to the widgets the value affects
     */
    protected HashMap<String, ArrayList<String>> processToggles( JSONObject property ) {
        try{
            ArrayList<String> toggled;
            HashMap<String, ArrayList<String>> toggleMap = new HashMap<>();

            JSONObject toggleList = property.getJSONObject( FormActivity.SCHEMA_KEY_TOGGLES );
            JSONArray toggleNames = toggleList.names();

            for( int j = 0; j < toggleNames.length(); j++ )
            {
                String toggleName = toggleNames.getString(j);
                JSONArray toggleValues = toggleList.getJSONArray( toggleName );
                toggled = new ArrayList<>();
                toggleMap.put( toggleName, toggled );
                for( int k = 0; k < toggleValues.length(); k++ ) {
                    toggled.add( toggleValues.getString(k) );
                }
            }

            return toggleMap;

        } catch( JSONException e ){
            return null;
        }
    }

    /**
     * returns a boolean indicating that the supplied json object contains a property for toggles
     */
    protected boolean hasToggles( JSONObject obj ){
        try{
            obj.getJSONObject( FormActivity.SCHEMA_KEY_TOGGLES );
            return true;
        } catch ( JSONException e ){
            return false;
        }
    }

    /**
     * initializes the visibility of widgets that are togglable
     */
    protected void initToggles() {
        int i;
        FormWidget widget;

        for( i = 0; i < _widgets.size(); i++ )  {
            widget = _widgets.get(i);
            updateToggles( widget );
        }
    }

    /**
     * updates any widgets that need to be toggled on or off
     * @param widget
     */
    protected void updateToggles( FormWidget widget ) {
        int i;
        String name;
        ArrayList<String> toggles;
        ArrayList<FormWidget> ignore = new ArrayList<>();

        toggles = widget.getToggledOn();
        for( i = 0; i < toggles.size(); i++ ) {
            name = toggles.get(i);
            if( _map.get(name) != null )
            {
                FormWidget toggle = _map.get(name);
                ignore.add( toggle );
                toggle.setVisibility( View.VISIBLE );
            }
        }

        toggles = widget.getToggledOff();
        for( i = 0; i < toggles.size(); i++ ) {
            name = toggles.get(i);
            if( _map.get(name) != null )
            {
                FormWidget toggle = _map.get(name);
                if( ignore.contains(toggle) ) continue;
                toggle.setVisibility( View.GONE );
            }
        }
    }


    /**
     * simple callbacks for widgets to use when their values have changed
     */
    class FormWidgetToggleHandler {
        public void toggle( FormWidget widget ) {
            updateToggles( widget );
        }
    }


    /**
     * factory method for actually instantiating widgets
     */

    protected FormWidget getWidget(Context context, String name, JSONObject property , String label, boolean hasValidator,
                                   boolean createForm, String description, ArrayList<FormWidget> _widgets,
                                   Map<String, FormWidget> _map, String currentSelectedOption, JSONObject subCategory,
                                   JSONObject categoryFields, JSONObject repeatOccurences, JSONObject subSubCategory, JSONObject formValues, String mAdvancedMemberWhatWhereWithinmile) {
        JSONArray jsonArray;
        JSONObject jsonObject;
        String value;
        mCurrentSelectedOption = currentSelectedOption;
        if (property != null) {
            String type = property.optString(FormActivity.SCHEMA_KEY_TYPE);

            switch (type) {

                case FormActivity.SCHEMA_KEY_PASSWORD:
                case FormActivity.SCHEMA_KEY_SLIDER:
                case FormActivity.SCHEMA_KEY_FLOAT:
                case FormActivity.SCHEMA_KEY_INTEGER:
                case FormActivity.SCHEMA_KEY_STRING:
                case FormActivity.SCHEMA_KEY_STRING_UPPER:
                    if (mCurrentSelectedOption.equals(ConstantVariables.POLL_MENU_TITLE)) {
                        String optionName = property.optString(FormActivity.Poll_OPTIONS_NAME);
                        Boolean b = optionName.startsWith(FormActivity.Poll_OPTIONS);

                        if (b) {
                            pollOptionsList.add(optionName);

                            if (pollOptionsList.size() < 3) {
                                return new FormEditText(this, name, description, hasValidator, type,
                                        _widgets, _map, mContentId, createForm, null, mCurrentSelectedOption);
                            } else if (pollOptionsList.size() == 3) {
                                pollOptionListCount.add(optionName);
                                pollJsonObject = property;
                                label = getResources().getString(R.string.add_another_poll_option);
                                addOptionButton = "button";
                                return new FormButton(this, addOptionButton, label, hasValidator);
                            } else {
                                pollOptionListCount.add(optionName);
                                if (temp == 1) {
                                    temp = 0;
                                    return new FormEditText(this, name, description, hasValidator, type,
                                            _widgets, _map, mContentId, createForm, null, mCurrentSelectedOption);
                                }
                            }

                        } else {
                            return new FormEditText(this, name, description, hasValidator, type, _widgets,
                                    _map, mContentId, createForm, null, mCurrentSelectedOption);

                        }
                    } else {
                        if (name.contains("first_name")) {
                            value = first_name;
                        } else if (name.contains("last_name")) {
                            value = last_name;
                        } else {
                            value = property.optString("value");
                        }
                        return new FormEditText(context, name, description, hasValidator, type, _widgets,
                                _map, mContentId, createForm, value, mCurrentSelectedOption);
                    }
                    break;

                case FormActivity.SCHEMA_KEY_BOOL:
                case FormActivity.SCHEMA_KEY_BOOL_UPPER:
                    return new FormCheckBox(context, name, label, hasValidator, property.optInt("value"), _widgets, mCurrentSelectedOption);

                case FormActivity.SCHEMA_KEY_FILE:
                case FormActivity.SCHEMA_KEY_FILE1:
                    return new FormSelectFile(context, name, createForm, mCurrentSelectedOption);

                case FormActivity.SCHEMA_KEY_RADIO:
                    jsonObject = property.optJSONObject(FormActivity.SCHEMA_KEY_OPTIONS);
                    if (jsonObject == null) {
                        jsonArray = property.optJSONArray(FormActivity.SCHEMA_KEY_OPTIONS);
                        jsonObject = convertToJsonObject(jsonArray);
                    }
                    if (jsonObject != null && jsonObject.length() != 0) {
                        return new FormRadioButtons(context, name, jsonObject, label, hasValidator,
                                description, property, mCurrentSelectedOption);
                    }

                case FormActivity.SCHEMA_KEY_MULTI_CHECKBOX_UPPER:
                case FormActivity.SCHEMA_KEY_MULTI_CHECKBOX:
                case FormActivity.SCHEMA_KEY_MULTI_CHECKBOX_UNDERSCORE:
                case FormActivity.SCHEMA_KEY_MULTI_SELECT:

                    jsonObject = property.optJSONObject(FormActivity.SCHEMA_KEY_OPTIONS);
                    if (jsonObject == null) {
                        jsonArray = property.optJSONArray(FormActivity.SCHEMA_KEY_OPTIONS);
                        jsonObject = convertToJsonObject(jsonArray);
                    }
                    if (jsonObject != null && jsonObject.length() != 0) {
                        formMultiCheckBox = new FormMultiCheckBox(context, name, jsonObject, label, hasValidator, description);
                        formMultiCheckBox.setValue(property.optString("value"));
                        return formMultiCheckBox;
                    }

                case FormActivity.SCHEMA_KEY_SELECT:
                case FormActivity.SCHEMA_KEY_SELECT_UPPER:
                    if (property.has(FormActivity.SCHEMA_KEY_OPTIONS)) {
                        jsonObject = property.optJSONObject(FormActivity.SCHEMA_KEY_OPTIONS);
                        String defaultValue = property.optString("value");
                        if (jsonObject == null) {
                            jsonArray = property.optJSONArray(FormActivity.SCHEMA_KEY_OPTIONS);
                            jsonObject = convertToJsonObject(jsonArray);
                        }
                        if (jsonObject != null && jsonObject.length() != 0) {

                            switch (mCurrentSelectedOption) {
                                case ConstantVariables.ADV_VIDEO_MENU_TITLE:
                                    if (createForm && name.equals("type")
                                            && CreateNewEntry.getInstance().mIsAAFVideoUpload) {
                                        defaultValue = "3";
                                    }
                                case "core_main_siteevent":
                                case "sitereview_listing":
                                case "sitepage":
                                case "core_main_sitegroup":
                                case "core_main_user":
                                case ConstantVariables.ADV_VIDEO_CHANNEL_MENU_TITLE:
                                    return new FormSpinner(context, name, defaultValue, jsonObject, subCategory, categoryFields, repeatOccurences, label,
                                            hasValidator, mCurrentSelectedOption, createForm, _widgets, _map, subSubCategory, property, formValues, mAdvancedMemberWhatWhereWithinmile, mIsSpinnerWithIcon);
                                case "compose_message":
                                    return new FormSpinner(context, name, defaultValue, jsonObject, null, null, null, label,
                                            hasValidator, mCurrentSelectedOption, createForm, _widgets, null, null, property, null, null, mIsSpinnerWithIcon);

                                case ConstantVariables.VIDEO_MENU_TITLE:
                                    if (createForm && name.equals("type")
                                            && CreateNewEntry.getInstance().mIsAAFVideoUpload) {
                                        defaultValue = "3";
                                    }
                                    return new FormSpinner(context, name, defaultValue, jsonObject, null, null, null, label,
                                            hasValidator, mCurrentSelectedOption, createForm, null, null, null, property, null, null, mIsSpinnerWithIcon);

                                default:
                                    return new FormSpinner(context, name, defaultValue, jsonObject, null, null, null, label,
                                            hasValidator, mCurrentSelectedOption, createForm, null, null, null, property, null, null, mIsSpinnerWithIcon);
                            }
                        }
                    }
                    break;

                case FormActivity.SCHEMA_KEY_DATE:
                    return new FormDateTimePickers(context, name, hasValidator, property.optString("format"));

                case FormActivity.SCHEMA_KEY_RATING:
                    return new FormRatingBar(context, name, label, hasValidator);

                case FormActivity.SCHEMA_KEY_BLOCKED_LIST:
                    return new FormBlockedUsers(this, name, label, hasValidator, description);

                case FormActivity.SCHEMA_KEY_TextArea:
                    if (mCurrentSelectedOption.equals("core_main_blog") ||
                            mCurrentSelectedOption.equals("core_main_classified")
                            || mCurrentSelectedOption.equals("core_main_forum")) {
                                /* We will not return any view in this case because editor
                                 will be used for adding description */
                        hasBody = true;
                        break;
                    } else {
                        value = property.optString("value");
                        return new FormEditText(context, name, description, hasValidator, type, _widgets, _map, mContentId, createForm, value, mCurrentSelectedOption);
                    }

                case FormActivity.SCHMEA_KEY_HOST:
                    name = FormHostChange.sEventHost;
                    mHostForm = property;
                    return new FormHostChange(context, name, property, createForm, _widgets, _map, mCurrentSelectedOption);

                case FormActivity.SCHEMA_KEY_DUMMY:
                    return new FormTextView(context, name, hasValidator, label, property);

            }
        } else {
            return new FormTextView(context, name, hasValidator, label, null);
        }
        return null;
    }

    protected FormWidget getTextViewWidget(Context context, String name, String label){
        return new FormTextView(context, name, false, label, null);
    }

    /**
     * Method to convert jsonArray in JsonObject
     * @param jsonArray JsonArray to convert in JsonObject
     * @return Converted JsonObject
     */
    public JSONObject convertToJsonObject(JSONArray jsonArray) {
        JSONObject jsonObject = new JSONObject();
        if(jsonArray != null) {
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    String value = jsonArray.optString(i);
                    jsonObject.put("" + i, value);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return jsonObject;
        }else {
            return null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FormSpinner.sPosition = FormSpinner.ePosition = FormSpinner.subSPosition = FormSpinner.subSubSPosition = 0 ;
        FormSpinner.hasSubCategory = FormSpinner.hasSubSubCategory = FormSpinner.isCategoryProfileFields = FormSpinner.isSubCategoryProfileFields = FormSpinner.isSubSubCategoryProfileFields = false;
    }

}
