package com.unitedgungroup.mobiapp.classes.common.formgenerator;

import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.TextViewCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.activities.CreateNewEntry;
import com.unitedgungroup.mobiapp.classes.common.activities.EditEntry;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.utils.GlobalFunctions;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FormDateTimePickers extends FormWidget{

    protected TextView dateButton, _input;
    protected Context mContext;
    protected RelativeLayout relativeLayout;
    CreateNewEntry createNewEntry;
    EditEntry editEntry;
    String propertyName;
    int editTextId;
    private static long mMinDate = 0L;

    public FormDateTimePickers(final Context context, final String name, boolean hasValidator, final String type) {

        super(context, name, hasValidator);

        mContext = context;
        createNewEntry = new CreateNewEntry();
        editEntry = new EditEntry();
        propertyName = name;


        RelativeLayout.LayoutParams relativeLayoutParams = CustomViews.getFullWidthRelativeLayoutParams();

        int marginMin = (int) (context.getResources().getDimension(R.dimen.margin_5dp) /
                context.getResources().getDisplayMetrics().density);

        int marginMax = (int) (context.getResources().getDimension(R.dimen.margin_10dp) /
                context.getResources().getDisplayMetrics().density);

        int dateIconMarginRight  = (int) (context.getResources().getDimension(R.dimen.date_icon_margin) /
                context.getResources().getDisplayMetrics().density);

        _input = new TextView(context);
        _input.setLayoutParams(relativeLayoutParams);

        int padding = (int) mContext.getResources().getDimension(R.dimen.padding_10dp);

        _input.setPadding(padding, padding, 0, padding);
        _input.setSingleLine(true);
        _input.setFocusable(false);
        _input.setFocusableInTouchMode(true);
        _input.setFocusable(true);
        _input.setPadding(marginMin, marginMax, marginMin, marginMin);
        _input.setTag("");

        switch (name) {
            case "starttime":
                editTextId = R.id.starttime;
                break;
            case "endtime":
                editTextId = R.id.endtime;
                break;
            case "start_date":
                editTextId = R.id.start_date;
                break;
            case "end_date":
                editTextId = R.id.end_date;
                break;
            case "date":
                editTextId = R.id.date;
            default:
                editTextId = R.id.birth_date;

        }

        _input.setId(editTextId);

        RelativeLayout.LayoutParams dateIconParams = CustomViews.getWrapRelativeLayoutParams();
        dateIconParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        dateIconParams.addRule(RelativeLayout.CENTER_VERTICAL);
        dateIconParams.setMargins(0, 0, dateIconMarginRight, 0);
        dateButton = new TextView(context);
        dateButton.setLayoutParams(dateIconParams);
        dateButton.setPadding(0, marginMin, marginMax, marginMin);
        dateButton.setTextColor(ContextCompat.getColor(context, R.color.body_text_2));
        TextViewCompat.setTextAppearance(dateButton, R.style.CaptionView);

        dateButton.setTypeface(GlobalFunctions.getFontIconTypeFace(context));
        dateButton.setText("\uf073");

        relativeLayoutParams.setMargins(marginMin, marginMin, marginMin, 0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            relativeLayoutParams.setMarginEnd(marginMin);
            relativeLayoutParams.setMarginStart(marginMin);
        }

        relativeLayout = new RelativeLayout(context);
        relativeLayout.setPadding(marginMin, marginMax, marginMin, marginMin);
        relativeLayout.setBackgroundResource(R.drawable.gradient_blackborder);
        relativeLayout.setLayoutParams(relativeLayoutParams);

        relativeLayout.addView(_input);
        relativeLayout.addView(dateButton);
        _layout.addView( relativeLayout );

        if (name.equals("starttime")) {
            String currentDateString = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
            if (currentDateString != null && !currentDateString.isEmpty()) {
                _input.setText(currentDateString);
                setMinDate(currentDateString);
            }
        }

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _input.setError(null);

                if (name.equals("starttime")) {
                    createNewEntry.showDateTimeDialogue(context, editTextId, type, true, 0L);
                    createNewEntry.setOnDateSelectedListener(new CreateNewEntry.OnDateSelectedListener() {
                        @Override
                        public void onDateSelected(String date) {
                            _input.setText(date);
                            _input.setTag(date);
                            setMinDate(date);
                        }
                    });
                } else if (name.equals("endtime")) {
                    createNewEntry.showDateTimeDialogue(context, editTextId, type, true, mMinDate);
                    createNewEntry.setOnDateSelectedListener(new CreateNewEntry.OnDateSelectedListener() {
                        @Override
                        public void onDateSelected(String date) {
                            _input.setText(date);
                            _input.setTag(date);
                        }
                    });
                } else {
                    createNewEntry.showDateTimeDialogue(context, editTextId, type, false, 0L);
                }
            }
        });

    }

    private void setMinDate(String startDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Date date = sdf.parse(startDate);
            mMinDate = date.getTime();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setHint( String value ){
        _input.setHint( value );
    }

    @Override
    public String getValue(){

        return (_input.getTag() != null ? _input.getTag().toString() : _input.getTag().toString());
    }

    @Override
    public void setValue( String value ) {
        _input.setText( value );
        _input.setTag( value );
    }

    public void setErrorMessage(String errorMessage) {
        _input.setError(errorMessage);

        if (FormActivity.reset == 0) {
            _input.requestFocus();
            _input.setFocusable(true);
            FormActivity.reset++;
        }
    }
}
