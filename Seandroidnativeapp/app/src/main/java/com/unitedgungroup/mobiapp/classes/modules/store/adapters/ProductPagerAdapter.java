package com.unitedgungroup.mobiapp.classes.modules.store.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnItemClickListener;
import com.unitedgungroup.mobiapp.classes.modules.store.utils.ProductInfoModel;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

public class ProductPagerAdapter extends PagerAdapter {
    private List<ProductInfoModel> mProductImageList;
    private Activity mActivity;
    LayoutInflater inflater;
    View viewLayout;
    ImageView imageView;
    private OnItemClickListener mItemClickListener;
    public ProductPagerAdapter(Activity activity,List<ProductInfoModel> imageList,
                               OnItemClickListener itemListener) {
        mActivity = activity;
        mProductImageList = imageList;
        mItemClickListener = itemListener;
    }


    @Override public int getCount() {
        return mProductImageList.size();
    }

    @Override public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView((View) object);
    }

    @Override public Object instantiateItem(ViewGroup view, final int position) {
        inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, view, false);
        imageView = (ImageView) viewLayout.findViewById(R.id.image);
        ProductInfoModel productInfoModel = mProductImageList.get(position);
        if (productInfoModel.getProductImage() != null && !productInfoModel.getProductImage().isEmpty()) {
            Picasso.with(mActivity)
                    .load(productInfoModel.getProductImage())
                    .into(imageView);
        }
        viewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemClickListener.onItemClick(view, position);
            }
        });
        view.addView(viewLayout);
        return viewLayout;
    }
    public void addItem() {
        notifyDataSetChanged();
    }

    public void removeItem() {
        notifyDataSetChanged();
    }
}