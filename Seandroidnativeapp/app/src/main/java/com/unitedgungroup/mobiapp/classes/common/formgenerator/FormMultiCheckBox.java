package com.unitedgungroup.mobiapp.classes.common.formgenerator;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatCheckBox;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;

import com.unitedgungroup.mobiapp.classes.common.ui.SelectableTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;

public class FormMultiCheckBox extends FormWidget {

    protected JSONObject _options;
    protected SelectableTextView _textView, _descriptionTextView;
    Context mContext;

    protected ArrayAdapter<String> _nameAdapter;
    ArrayList<AppCompatCheckBox> checkBoxArray = new ArrayList<>();

    public FormMultiCheckBox( final Context context, String property, JSONObject options, String label,
                              boolean hasValidator, String description){
        super( context, property, hasValidator );

        mContext = context;
        _options = options;
        _textView = new SelectableTextView( context );

        _textView.setText(label);

        _textView.setTypeface(Typeface.DEFAULT_BOLD);

        _layout.addView(_textView);

        if(description != null && !description.isEmpty()){
            _descriptionTextView = new SelectableTextView(context);
            _descriptionTextView.setText(description);

            _layout.addView(_descriptionTextView);
        }



        JSONArray propertyNames = options.names();

        String name, p;

        _nameAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item);

        try{
            for( int i = 0; i < options.length(); i++ ) {
                name = propertyNames.getString(i);
                p = options.getString(name);

                AppCompatCheckBox checkBox = new AppCompatCheckBox(context);
                checkBox.setText(p);
                checkBox.setId(i);
                checkBoxArray.add(checkBox);

                _nameAdapter.add( name );
                _layout.addView(checkBox);

            }
        } catch( JSONException e){
                e.printStackTrace();
        }


        if (checkBoxArray != null && checkBoxArray.size() > 0) {
            for (int i = 0; i< checkBoxArray.size(); i++) {
                checkBoxArray.get(i).setOnCheckedChangeListener(new ChangeHandler(this));
            }
        }
        //_layout.addView( _radioGroup );
    }

    @Override
    public String getValue() {

        String returnValues = "";

        for(int i = 0; i < checkBoxArray.size(); i++){

            int arrayLength = (checkBoxArray.size())-1;

            if(checkBoxArray.get(i).isChecked()){
                AppCompatCheckBox checkBox = checkBoxArray.get(i);
                String value = _nameAdapter.getItem(checkBox.getId());

                if(i < arrayLength){
                    returnValues += value + ",";
                } else {
                    returnValues += value;
                }
            }
        }

        return returnValues;
    }

    @Override
    public void setValue(String value) {
        try{
            if(value != null && !value.isEmpty()){

                Object json = new JSONTokener(value).nextValue();
                /* If Values are coming in form of JsonObject */
                if (json instanceof JSONObject){
                    JSONObject valuesObject = (JSONObject) json;
                    if(valuesObject != null && valuesObject.length() != 0){
                        JSONArray valueIds = valuesObject.names();

                        for( int i = 0; i < valueIds.length(); i++ ){

                            String checkBoxId = valueIds.optString(i);
                            String checkBoxName = valuesObject.optString(checkBoxId);
                            String item = _options.optString(checkBoxName);

                            if(item != null && !item.isEmpty()){
                                AppCompatCheckBox checkBox = checkBoxArray.get(_nameAdapter.getPosition(checkBoxName));
                                checkBox.setChecked(true);
                            }
                        }
                    }
                }
                /* If Values are coming in form of JsonArray */
                else if (json instanceof JSONArray){
                    JSONArray valuesArray = (JSONArray) json;
                    for(int i = 0; i < valuesArray.length(); i++){

                        String checkBoxName = valuesArray.optString(i);
                        String item = _options.optString(checkBoxName);

                        if(item != null && !item.isEmpty()){
                            AppCompatCheckBox checkBox = checkBoxArray.get(_nameAdapter.getPosition(checkBoxName));
                            checkBox.setChecked(true);
                        }
                    }
                }
            }
        } catch( JSONException e ){
            e.printStackTrace();
        }
    }

    @Override
    public void setErrorMessage(String errorMessage) {
        _textView.setError(errorMessage);
        _textView.setFocusable(true);
        _textView.requestFocus();
    }

    @Override
    public void setToggleHandler( FormActivity.FormWidgetToggleHandler handler ) {
        super.setToggleHandler(handler);
    }

    class ChangeHandler implements CompoundButton.OnCheckedChangeListener {

        protected FormWidget _widget;

        public ChangeHandler( FormWidget widget ) {
            _widget = widget;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            _textView.setError(null);
        }

    }

}
