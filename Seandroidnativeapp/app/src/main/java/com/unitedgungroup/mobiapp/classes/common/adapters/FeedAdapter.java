/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */

package com.unitedgungroup.mobiapp.classes.common.adapters;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import android.widget.Toast;

import com.facebook.ads.NativeAd;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.activities.MapActivity;
import com.unitedgungroup.mobiapp.classes.common.activities.SearchActivity;
import com.unitedgungroup.mobiapp.classes.common.activities.WebViewActivity;
import com.unitedgungroup.mobiapp.classes.common.ads.admob.AdMobViewHolder;
import com.unitedgungroup.mobiapp.classes.common.ads.FacebookAdViewHolder;
import com.unitedgungroup.mobiapp.classes.common.ads.communityAds.CommunityAdsHolder;
import com.unitedgungroup.mobiapp.classes.common.ads.communityAds.RemoveAdHolder;
import com.unitedgungroup.mobiapp.classes.common.ads.sponsoredStories.SponsoredStoriesHolder;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnFeedDisableCommentListener;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnFilterSelectedListener;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnItemClickListener;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnMenuClickResponseListener;
import com.unitedgungroup.mobiapp.classes.common.ui.ActionIconButton;
import com.unitedgungroup.mobiapp.classes.common.ui.ActionIconThemedTextView;
import com.unitedgungroup.mobiapp.classes.common.ui.BaseButton;
import com.unitedgungroup.mobiapp.classes.common.ui.BezelImageView;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomGridLayoutManager;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.ui.NameView;
import com.unitedgungroup.mobiapp.classes.common.ui.ThemedTextView;
import com.unitedgungroup.mobiapp.classes.common.ui.viewholder.ProgressViewHolder;
import com.unitedgungroup.mobiapp.classes.common.utils.AddPeopleList;
import com.unitedgungroup.mobiapp.classes.common.utils.CommunityAdsList;
import com.unitedgungroup.mobiapp.classes.common.utils.CustomTabUtil;
import com.unitedgungroup.mobiapp.classes.common.utils.EmojiUtil;
import com.unitedgungroup.mobiapp.classes.common.ui.hashtag.OnTagClickListener;
import com.unitedgungroup.mobiapp.classes.common.ui.hashtag.TagSelectingTextView;
import com.unitedgungroup.mobiapp.classes.common.utils.GlobalFunctions;
import com.unitedgungroup.mobiapp.classes.common.utils.GutterMenuUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.LogUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.PreferencesUtils;
import com.unitedgungroup.mobiapp.classes.common.ui.SelectableTextView;
import com.unitedgungroup.mobiapp.classes.common.utils.Smileys;
import com.unitedgungroup.mobiapp.classes.common.utils.SnackbarUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.SocialShareUtil;
import com.unitedgungroup.mobiapp.classes.common.utils.SoundUtil;
import com.unitedgungroup.mobiapp.classes.common.utils.SponsoredStoriesList;
import com.unitedgungroup.mobiapp.classes.common.utils.UrlUtil;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnResponseListener;
import com.unitedgungroup.mobiapp.classes.modules.advancedActivityFeeds.FeedsFragment;
import com.unitedgungroup.mobiapp.classes.modules.advancedActivityFeeds.SingleFeedPage;
import com.unitedgungroup.mobiapp.classes.common.activities.ReportEntry;
import com.unitedgungroup.mobiapp.classes.modules.advancedActivityFeeds.Status;
import com.unitedgungroup.mobiapp.classes.modules.advancedVideos.AdvVideoUtil;
import com.unitedgungroup.mobiapp.classes.modules.likeNComment.Likes;
import com.unitedgungroup.mobiapp.classes.modules.photoLightBox.PhotoLightBoxActivity;
import com.unitedgungroup.mobiapp.classes.modules.photoLightBox.PhotoListDetails;
import com.unitedgungroup.mobiapp.classes.modules.user.profile.userProfile;
import com.unitedgungroup.mobiapp.classes.core.ConstantVariables;
import com.unitedgungroup.mobiapp.classes.common.utils.FeedList;
import com.unitedgungroup.mobiapp.classes.common.utils.ImageViewList;
import com.unitedgungroup.mobiapp.classes.modules.likeNComment.Comment;
import com.unitedgungroup.mobiapp.classes.modules.video.VideoUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import github.ankushsachdeva.emojicon.EmojiconEditText;
import github.ankushsachdeva.emojicon.EmojiconTextView;

public class FeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements
        View.OnClickListener, AdapterView.OnItemSelectedListener, OnTagClickListener,
        OnMenuClickResponseListener, AdapterView.OnItemClickListener {

    private Context mContext;
    private List<Object> mFeedItemList;
    private FeedList mSelectedFeedList;
    private FeedList mFeedItem;
    private int mLayoutResID;
    private AppConstant mAppConst;
    private SocialShareUtil socialShareUtil;
    private LayoutInflater inflater;
    private Typeface fontIcon;
    public final String TAG = "FeedAdapter";
    private String mProfileIconImage;
    private int mLoggedInUserId;
    private ArrayList<Integer> mHiddenFeeds = new ArrayList<>();
    private ArrayList<Integer> mAllHiddenFeeds = new ArrayList<>();
    private ArrayList<Integer> mEditFeeds = new ArrayList<>();
    private ArrayList<Integer> mRemoveAds;

    private Map<String, Integer> mSubjectPositionList = new HashMap<>();
    private final int FEED_TYPE = 0, HIDE_ALL_TYPE = 1, HIDDEN_TYPE = 2, FOOTER_TYPE = 3, HEADER_TYPE = 4,
            AD_FB = 5, EDIT_TYPE = 6, AD_ADMOB = 7, PEOPLE_SUGGESTION = 8, COMMUNITY_ADS = 9, SPONSORED_STORIES = 10,
    REMOVE_AD_TYPE = 11;
    boolean mIsSingleFeed, isPostMenusSet = false, isPhotoFeed = false, isAdapterSet = false;
    public Map<String, String> mPostParams;
    private List<ImageViewList> mPhotoUrls, reactionsImages;
    private ArrayList<PhotoListDetails> mPhotoDetails;
    private ImageAdapter adapter, reactionsAdapter;
    int columnWidth, itemPosition, mPhotoPosition;
    private OnFilterSelectedListener mFilterSelectedListener;
    private OnFeedDisableCommentListener mFeedDisableCommentListener;
    private JSONObject mFeedPostMenus;
    private JSONArray mFeedFiltersArray;
    protected ArrayAdapter<String> _adapter;
    protected Map<String, String> mFilterKeysMap;
    private int mFilterPosition = 0;
    private List mIncludedModulesList, mDeletedModulesList;
    private EditText mCommentEditText;
    private int mFeedSubjectId, mClickedFeedPosition;
    private String mSubjectType, mModuleName, mEditBody;
    TagSelectingTextView mTagSelectingTextView;
    private FeedsFragment mFeedsFragment;
    private int mReactionsEnabled;
    private JSONObject mReactions;
    private GutterMenuUtils mGutterMenuUtils;
    private ArrayList<JSONObject> mReactionsArray;
    private List<AddPeopleList> mAddPeopleList;
    private AddPeopleAdapter mAddPeopleAdapter;
    private Dialog mDialog;

    //senal
   // private int mFeedItemActionIdActual;


    public FeedAdapter(Context context, int layoutResourceID, List<Object> listItem, boolean isSinglePageFeed,
                       EditText commentEditText, String subjectType, int subjectId, String moduleName,
                       int feedPosition,boolean hidePhotos,FeedsFragment feedsFragment) {


        mContext = context;
        mLayoutResID = layoutResourceID;
        mFeedItemList = listItem;

        fontIcon = GlobalFunctions.getFontIconTypeFace(mContext);
        mAppConst = new AppConstant(context);
        socialShareUtil = new SocialShareUtil(context);
        mGutterMenuUtils = new GutterMenuUtils(context);

        mPostParams = new HashMap<>();
        mAddPeopleList = new ArrayList<>();

        this.mIsSingleFeed = isSinglePageFeed;

        inflater = LayoutInflater.from(context);

        mSubjectType = subjectType;
        mFeedSubjectId = subjectId;
        mModuleName = moduleName;
        mClickedFeedPosition = feedPosition;

        _adapter = new ArrayAdapter<>(mContext, R.layout.simple_text_view);

        isPhotoFeed = hidePhotos;

        mFilterKeysMap = new HashMap<>();

        mIncludedModulesList = Arrays.asList(ConstantVariables.INCLUDED_MODULES_ARRAY);
        mDeletedModulesList = Arrays.asList(ConstantVariables.DELETED_MODULES);

        mCommentEditText = commentEditText;

        mFeedsFragment = feedsFragment;
        mTagSelectingTextView = new TagSelectingTextView();

        mRemoveAds = new ArrayList<>();

    }

    public void setOnFeedDisableCommentListener(OnFeedDisableCommentListener onFeedDisableCommentListener) {
        this.mFeedDisableCommentListener = onFeedDisableCommentListener;
    }

    /**
     * Return the View Type according to the position of the feed
     */

    @Override
    public int getItemViewType(int position) {
        if (mFeedItemList.get(position) instanceof FeedList) {
            // Header on 0th Position
            if (position == 0) {
                return HEADER_TYPE;
            } else {
                if ((mHiddenFeeds.size() == 0 || !mHiddenFeeds.contains(position)) &&
                        (mAllHiddenFeeds.size() == 0 || !mAllHiddenFeeds.contains(position)) &&
                        (mEditFeeds.size() == 0 || !mEditFeeds.contains(position))) {
                    return FEED_TYPE;
                } else if ((mEditFeeds.size() != 0 || mEditFeeds.contains(position))) {
                    return EDIT_TYPE;
                } else if (mAllHiddenFeeds.size() == 0 || !mAllHiddenFeeds.contains(position)) {
                    return HIDDEN_TYPE;
                } else {
                    return HIDE_ALL_TYPE;
                }
            }
        } else if (mFeedItemList.get(position) != null && mFeedItemList.get(position) instanceof ArrayList) {
            return PEOPLE_SUGGESTION;
        } else if (mFeedItemList.get(position) != null && !mFeedItemList.get(position).equals(ConstantVariables.FOOTER_TYPE)) {

            if(mRemoveAds.size() != 0 && mRemoveAds.contains(position)){
                return REMOVE_AD_TYPE;
            } else {
                // To show Ads
                switch (ConstantVariables.FEED_ADS_TYPE){
                    case ConstantVariables.TYPE_FACEBOOK_ADS:
                        return AD_FB;
                    case ConstantVariables.TYPE_GOOGLE_ADS:
                        return AD_ADMOB;
                    case ConstantVariables.TYPE_COMMUNITY_ADS:
                        return COMMUNITY_ADS;
                    case ConstantVariables.TYPE_SPONSORED_STORIES:
                        return SPONSORED_STORIES;
                }
            }
        } else {
            // Footer to show Loading bar on scroll
            return FOOTER_TYPE;
        }

        return 0;
    }

    /**
     * Return View Holder according to View Type
     * @param viewType The type of view of a position in RecyclerView
     *
     * FEED_TYPE:  If the view is for showing the feed content.
     * HIDE_ALL_TYPE or HIDDEN_TYPE:  View when a user clicks on option Hide this feed or
     *                 Hide All for this user.
     * HEADER_TYPE: to show the header of the feeds which contain Status Menu and Filters
     * FOOTER_TYPE: to show the loading bar when we scroll for loading more results in recyclerview.
     */

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        switch (viewType) {
            case FEED_TYPE:
            case EDIT_TYPE:
                viewHolder =  new ListItemHolder(inflater.inflate(mLayoutResID, viewGroup, false));
                break;

            case HIDE_ALL_TYPE:
            case HIDDEN_TYPE:
                viewHolder =  new HiddenItemHolder(inflater.inflate(R.layout.hidden_feeds, viewGroup, false));
                break;

            case HEADER_TYPE:
                viewHolder =  new HeaderViewHolder(inflater.inflate(R.layout.feeds_header, viewGroup, false));
                break;

            case AD_FB:
                viewHolder = new FacebookAdViewHolder(inflater.inflate(R.layout.feeds_ad_item_card, viewGroup, false));

                break;
            case AD_ADMOB:
                viewHolder = new AdMobViewHolder(inflater.inflate(R.layout.admob_feeds, viewGroup, false));
                break;

            case COMMUNITY_ADS:
                viewHolder = new CommunityAdsHolder(this, inflater.inflate(R.layout.list_community_ads,
                        viewGroup, false), ConstantVariables.FEED_ADS_POSITION,
                        ConstantVariables.FEED_ADS_TYPE, mRemoveAds);
                break;

            case SPONSORED_STORIES:
                viewHolder = new SponsoredStoriesHolder(this, inflater.inflate(R.layout.list_sponsored_stories_feed,
                        viewGroup, false), ConstantVariables.FEED_ADS_POSITION,
                        ConstantVariables.FEED_ADS_TYPE, mRemoveAds);
                break;

            case PEOPLE_SUGGESTION:
                viewHolder = new PeopleSuggestionViewHolder(inflater.inflate(R.layout.people_suggestion_recycler_view, viewGroup, false));
                break;

            case REMOVE_AD_TYPE:
                viewHolder =  new RemoveAdHolder(this, inflater.inflate(R.layout.remove_ads_layout,
                        viewGroup, false), mRemoveAds, mFeedItemList);
                break;

            default:
                viewHolder =  new ProgressViewHolder(inflater.inflate(R.layout.progress_item, viewGroup, false));
                break;
        }

        return viewHolder;
    }

    /**
     * Set Data in views according to view type and holders.
     */

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {

        int type = getItemViewType(position);


        /* End */

        /* Start : To set data in feeds */

        switch (type) {
            case FEED_TYPE:
            case EDIT_TYPE:
                mFeedItem = (FeedList) this.mFeedItemList.get(position);
               // this.mFeedItemActionIdActual = mFeedItem.getmActionId();

                /* Start: Put Subject_id and position of the feed in
                 * HashMap for hide all by a user functionality
                 */

                if (mFeedItem != null) {
                    mSubjectPositionList.put(mFeedItem.getmSubjectId() + "-" + position, position);
                }

                final ListItemHolder listItemHolder = (ListItemHolder) viewHolder;

                listItemHolder.mPhotoAttachmentCount = mFeedItem.getmPhotoAttachmentCount();

            /* Start: Set Horizontal Scrolling in Images RecyclerView */
                CustomGridLayoutManager gridLayoutManager = new CustomGridLayoutManager(listItemHolder.mImagesGallery,
                        2, LinearLayoutManager.VERTICAL, false);
                listItemHolder.mImagesGallery.setLayoutManager(gridLayoutManager);

                gridLayoutManager.setSpanSizeLookup(new CustomGridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        if (listItemHolder.mPhotoAttachmentCount == 2) {
                            return 1;
                        } else {
                            if (position == 0) {
                                return 2;
                            } else {
                                return 1;
                            }
                        }
                    }
                });


                listItemHolder.mImagesGallery.setTag(listItemHolder);
            /* End */

                listItemHolder.mCounterView.setTag(position);
                listItemHolder.mWebUrl = mFeedItem.getmWebUrl();

            /* Start: Show Feed Icon Image */

                if (mFeedItem.getmFeedIcon() != null && !mFeedItem.getmFeedIcon().isEmpty()) {
                    Picasso.with(mContext)
                            .load(mFeedItem.getmFeedIcon())
                            .placeholder(R.drawable.default_user_profile)
                            .error(R.drawable.default_user_profile)
                            .into(listItemHolder.mFeedProfileImage);
                } else {
                    Picasso.with(mContext)
                            .load(R.drawable.default_user_profile)
                            .into(listItemHolder.mFeedProfileImage);
                }

                listItemHolder.mFeedProfileImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FeedList feedList = (FeedList) mFeedItemList.get(
                                listItemHolder.getAdapterPosition());
                        int subjectId;
                        String subjectType;

                        if (feedList.getmOwnerFeedType() == 0) {
                            subjectType = "user";
                            subjectId = feedList.getmSubjectId();
                        } else {
                            subjectType = feedList.getmObjectType();
                            subjectId = feedList.getmObjectId();
                        }
                        attachmentClicked(null, subjectType, subjectId, listItemHolder, null);
//                        attachmentClicked(null, subjectType, subjectId, null, null, null, null, listItemHolder.getAdapterPosition());

                    }
                });
            /* End Feed Icon work */


            /* Start: Show Feed Title with Clickable Links */
                listItemHolder.mFeedTitle.setMovementMethod(LinkMovementMethod.getInstance());

                HashMap<String, String> clickableParts = mFeedItem.getmClickableStringsList();
                final HashMap<Integer, String> videoInfo = mFeedItem.getmVideoInfo();
                listItemHolder.mFeedObject = mFeedItem.getmFeedObject();
                listItemHolder.mPopularReactionsArray = mFeedItem.getmFeedReactions();

                // Show Clickable Parts and Apply Click Listener to redirect
                if (clickableParts != null && clickableParts.size() != 0) {

                    CharSequence title = Html.fromHtml(mFeedItem.getmFeedTitle());
                    SpannableString text = new SpannableString(title);
                    SortedSet<String> keys = new TreeSet<>(clickableParts.keySet());

                    int lastIndex = 0;
                    for (String key : keys) {

                        final String slug;
                        String[] keyParts = key.split("-");
                        final String attachment_type = keyParts[1];
                        final int attachment_id = Integer.parseInt(keyParts[2]);
                        if (keyParts.length >= 4) {
                            slug = keyParts[3];
                        } else {
                            slug = null;
                        }

                        final String value = clickableParts.get(key);

                        if (value != null && !value.isEmpty()) {
                            int i1 = title.toString().indexOf(value, lastIndex);
                            if (i1 != -1) {
                                int i2 = i1 + value.length();
                                if (lastIndex != -1) {
                                    lastIndex += value.length();
                                }
                                ClickableSpan myClickableSpan = new ClickableSpan() {
                                    @Override
                                    public void onClick(View widget) {
                                        redirectToActivity(value, slug, attachment_type, attachment_id,
                                                listItemHolder, videoInfo);
                                    }

                                    @Override
                                    public void updateDrawState(TextPaint ds) {
                                        super.updateDrawState(ds);
                                        ds.setUnderlineText(false);
                                        ds.setColor(ContextCompat.getColor(mContext, R.color.black));
                                    }
                                };
                                text.setSpan(myClickableSpan, i1, i2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            }
                        }
                    }
                    listItemHolder.mFeedTitle.setText(text);
                } else {
                    listItemHolder.mFeedTitle.setText(Html.fromHtml(mFeedItem.getmFeedTitle()));
                }

            /* End Feed Title work */

            /* Start: Show body of the feed */
                listItemHolder.mFeedTitleBodyText = mFeedItem.getmActionTypeBody();
                listItemHolder.mFeedTitleBody.setMovementMethod(LinkMovementMethod.getInstance());

                if (listItemHolder.mFeedTitleBodyText != null && !listItemHolder.mFeedTitleBodyText.isEmpty()) {
                    listItemHolder.mFeedTitleBodyText = Smileys.getEmojiFromString(Html.fromHtml(listItemHolder.mFeedTitleBodyText).toString());
                    listItemHolder.mFeedTitleBody.setVisibility(View.VISIBLE);

                     /* Show Edit feed body if type is Edit feed else show normal feed body
                           */
                    if (type == EDIT_TYPE) {
                        listItemHolder.mFeedBodyEditLayout.setVisibility(View.VISIBLE);
                        listItemHolder.mFeedTitleBody.setVisibility(View.GONE);

                        listItemHolder.mFeedTitleBodyEditText.setText(listItemHolder.mFeedTitleBodyText.
                                replaceAll("<img.+?>|<IMG.+?>", ""));
                        listItemHolder.mFeedTitleBodyEditText.setSelection(listItemHolder.mFeedTitleBodyEditText.getText().length());
                        listItemHolder.mFeedTitleBodyEditText.requestFocus();
                        mAppConst.showKeyboard();
                        listItemHolder.mFeedTitleBodyEditText.setError(null);
                        listItemHolder.mFeedEditSubmit.setTag(position);
                        listItemHolder.mFeedEditCancel.setTag(position);
                        listItemHolder.mFeedEditSubmit.setText(mContext.getResources().getString(R.string.feed_edit_button_text));
                        listItemHolder.mEmojiButton.setTypeface(fontIcon);
                        listItemHolder.mEmojiButton.setText("\uf118");

                            /* To create a Emoji popup with all emoticons of keyboard height */
                        EmojiUtil.createEmojiPopup(mContext, listItemHolder.itemView, listItemHolder.mFeedTitleBodyEditText);

                    } else {

                        listItemHolder.mFeedBodyEditLayout.setVisibility(View.GONE);
                        listItemHolder.mFeedTitleBody.setMinWidth(AppConstant.getDisplayMetricsWidth(mContext));
                        //Checking feed is single feed or activity feed
                        // if feed is single then showing post with large text else showing a "See More" option(if text too large)
                        //Checking if post length is grater than 150 then showing "More" option else showing full post
                        if (!mIsSingleFeed && listItemHolder.mFeedTitleBodyText.trim().length() >
                                ConstantVariables.FEED_TITLE_BODY_LENGTH) {
                            seeMoreOption(listItemHolder.mFeedTitleBody, listItemHolder.mFeedTitleBodyText,
                                    listItemHolder, null);
                        } else {

                            HashMap<String, String> clickablePartsNew = mFeedItem.getmClickableStringsListNew();

                            // Show Clickable Parts and Apply Click Listener to redirect
                            if (clickablePartsNew != null && clickablePartsNew.size() != 0) {

                                CharSequence title = mTagSelectingTextView.addClickablePart(listItemHolder.mFeedTitleBodyText.
                                        replaceAll("<img.+?>|<IMG.+?>", ""), this, 0, ConstantVariables.DEFAULT_HASHTAG_COLOR);

                                SpannableString text = new SpannableString(title);
                                SortedSet<String> keys = new TreeSet<>(clickablePartsNew.keySet());

                                int lastIndex = 0;
                                for (String key : keys) {

                                    String[] keyParts = key.split("-");
                                    final String attachment_type = keyParts[1];
                                    final int attachment_id = Integer.parseInt(keyParts[2]);

                                    final String value = clickablePartsNew.get(key);

                                    if (value != null && !value.isEmpty()) {
                                        int i1 = title.toString().indexOf(value, lastIndex);
                                        if (i1 != -1) {
                                            int i2 = i1 + value.length();
                                            if (lastIndex != -1) {
                                                lastIndex += value.length();
                                            }
                                            ClickableSpan myClickableSpan = new ClickableSpan() {
                                                @Override
                                                public void onClick(View widget) {
                                                    attachmentClicked(value, attachment_type, attachment_id, listItemHolder, null);
                                                }

                                                @Override
                                                public void updateDrawState(TextPaint ds) {
                                                    super.updateDrawState(ds);
                                                    ds.setUnderlineText(false);
                                                    ds.setColor(ContextCompat.getColor(mContext, R.color.black));
                                                    ds.setFakeBoldText(true);
                                                }
                                            };
                                            text.setSpan(myClickableSpan, i1, i2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                        }
                                    }
                                }
                                listItemHolder.mFeedTitleBody.setText(text);
                            } else {
                                listItemHolder.mFeedTitleBody.setText(mTagSelectingTextView.addClickablePart(listItemHolder.mFeedTitleBodyText.
                                            replaceAll("<img.+?>|<IMG.+?>", ""), this, 0, ConstantVariables.DEFAULT_HASHTAG_COLOR),
                                    BufferType.SPANNABLE);
                            }

                        }
                    }


                } else {
                    listItemHolder.mFeedTitleBody.setVisibility(View.GONE);
                }

            /* End Feed Body Work */

                if (mFeedItem.getHashTagString() == null || mFeedItem.getHashTagString().isEmpty()) {
                    listItemHolder.hashTagView.setVisibility(View.GONE);
                } else {
                    listItemHolder.hashTagView.setVisibility(View.VISIBLE);
                    listItemHolder.hashTagView.setText(mTagSelectingTextView.
                            addClickablePart(mFeedItem.getHashTagString(), this, 0,
                                    ConstantVariables.DEFAULT_HASHTAG_COLOR), BufferType.SPANNABLE);
                }


            /* Show Feed Post Time */
                listItemHolder.mFeedPostedTime.setText(AppConstant.convertDateFormat(mContext.getResources()
                        , mFeedItem.getmFeedPostTime()));
            /* End Feed Post Time work */

            /* Show Feed Menus Icon with options Delete Feed, Hide feed etc. */

                if (mFeedItem.getmFeedMenusArray() != null && mFeedItem.getmFeedMenusArray().length() != 0) {
                    mGutterMenuUtils.setOnMenuClickResponseListener(this);
                    listItemHolder.mFeedMenusIcon.setVisibility(View.VISIBLE);
                    listItemHolder.mFeedMenusIcon.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View view) {

                            FeedList selectedFeedList = (FeedList) mFeedItemList.get(listItemHolder.getAdapterPosition());

                            mGutterMenuUtils.showPopup(view, selectedFeedList.getmFeedMenusArray(),
                                    listItemHolder.getAdapterPosition(), mFeedItemList, "home");
                        }
                    });
                } else {
                    listItemHolder.mFeedMenusIcon.setVisibility(View.GONE);
                }
            /* End Show Feed Menus Icon */


            /* Start: Show Attachment Info */
                if (isPhotoFeed) {
                    listItemHolder.mAttachmentView.setVisibility(View.GONE);
                    listItemHolder.mMusicAttachmentView.setVisibility(View.GONE);
                    listItemHolder.mImagesGallery.setVisibility(View.GONE);
                    listItemHolder.mSingleAttachmentImage.setVisibility(View.GONE);
                    listItemHolder.mMapView.setVisibility(View.GONE);
                } else {

                    listItemHolder.mFeedType = mFeedItem.getmFeedType();

                    if (mFeedItem.getmAttachmentCount() != 0) {
                        listItemHolder.mMapView.setVisibility(View.GONE);

                        listItemHolder.mFeedAttachmentArray = mFeedItem.getmFeedAttachmentArray();
                        listItemHolder.mFeedAttachmentType = mFeedItem.getmFeedAttachmentType();

                        if (listItemHolder.mFeedAttachmentArray != null
                                && listItemHolder.mFeedAttachmentArray.length() != 0) {
                            try {

                                listItemHolder.mAttachmentView.setVisibility(View.VISIBLE);
                                listItemHolder.mPhotoAttachmentCount = mFeedItem.getmPhotoAttachmentCount();

                                mPhotoUrls = new ArrayList<>();
                                mPhotoDetails = new ArrayList<>();
                                mPhotoPosition = 0;

                                for (int i = 0; i < listItemHolder.mFeedAttachmentArray.length(); i++) {
                                    columnWidth = 0;

                                    final JSONObject singleAttachmentObject = listItemHolder.mFeedAttachmentArray.getJSONObject(i);
                                    final String attachmentType = singleAttachmentObject.optString("attachment_type");
                                    final int attachmentId = singleAttachmentObject.optInt("attachment_id");
                                    String mainImage = singleAttachmentObject.optString("image_main");
                                    JSONObject imageMainObj = singleAttachmentObject.optJSONObject("image_main");
                                    if (imageMainObj != null && imageMainObj.length() > 0) {
                                        mainImage = imageMainObj.optString("src");
                                    }
                                    final int playlistId = singleAttachmentObject.optInt("playlist_id");
                                    final String attachmentTitle = singleAttachmentObject.optString("title");
                                    int mode = singleAttachmentObject.optInt("mode");
                                    String attachmentUri = singleAttachmentObject.optString("uri");
                                    if (attachmentUri != null && !attachmentUri.isEmpty()) {
                                        listItemHolder.mWebUrl = attachmentUri;
                                    }

                                    listItemHolder.mMusicAttachmentView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            attachmentClicked(attachmentTitle,
                                                    listItemHolder.mFeedAttachmentType, playlistId,
                                                    listItemHolder, null);
                                        }
                                    });

                                    //senal
                                    listItemHolder.mAttachmentView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            redirectToAttachmentClickedActivity(attachmentType, attachmentId,
                                                    singleAttachmentObject, listItemHolder);
                                        }
                                    });

                                    if (mode == 1 || mode == 2) {
                                        if (listItemHolder.mPhotoAttachmentCount > 0) {
                                            if (attachmentType.contains("_photo") || (listItemHolder.mFeedAttachmentType != null
                                                    && listItemHolder.mFeedAttachmentType.contains("_photo")
                                                    && listItemHolder.mFeedType.equals("share"))) {

                                                // Add PhotoDetails in list to show in PhotoLightBox
                                                final String mediumImage = singleAttachmentObject.optString("image_medium");
                                                int photo_id = singleAttachmentObject.optInt("photo_id");
                                                int likes_count = singleAttachmentObject.optInt("likes_count");
                                                int comment_count = singleAttachmentObject.optInt("comment_count");
                                                int is_like = singleAttachmentObject.optInt("is_like");
                                                boolean likeStatus = is_like != 0;
                                                String reactions = singleAttachmentObject.optString("reactions");
                                                String mUserTagArray = singleAttachmentObject.optString("tags");

                                                mPhotoDetails.add(new PhotoListDetails(photo_id, mainImage,
                                                        likes_count, comment_count, mUserTagArray, likeStatus, reactions));
                                                mFeedItem.setmPhotoDetails(mPhotoDetails);

                                                listItemHolder.mAttachmentView.setVisibility(View.GONE);
                                                listItemHolder.mMusicAttachmentView.setVisibility(View.GONE);

                                                int columnHeight = (int) (mAppConst.getScreenWidth() / ConstantVariables.
                                                        IMAGE_SCALE_FACTOR);

                                                if (listItemHolder.mPhotoAttachmentCount == 1) {

                                                    LinearLayout.LayoutParams singleImageParam;

                                                    // Show Single image in ImageView.
                                                    if (imageMainObj != null && imageMainObj.length() > 0) {
                                                        singleImageParam = getSingleImageParamFromWidthHeight(imageMainObj);
                                                        singleImageParam.setMargins(0,
                                                                mContext.getResources().getDimensionPixelSize(R.dimen.element_spacing_small), 0, 0);
                                                        listItemHolder.mSingleAttachmentImage.setLayoutParams(
                                                                singleImageParam);

                                                        if (mainImage != null && !mainImage.isEmpty()) {
                                                            Picasso.with(mContext)
                                                                    .load(mainImage)
                                                                    .placeholder(R.color.grey_light)
                                                                    .into(listItemHolder.mSingleAttachmentImage);
                                                        }

                                                        final String imageSrc = mainImage;

                                                        listItemHolder.mSingleAttachmentImage.setVisibility(View.VISIBLE);
                                                        listItemHolder.mImagesGallery.setVisibility(View.GONE);
                                                        listItemHolder.mSingleAttachmentImage.setOnClickListener(
                                                                new View.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(View v) {
                                                                        mPhotoDetails.clear();
                                                                        mFeedItem = (FeedList) mFeedItemList.get(listItemHolder.getAdapterPosition());
                                                                        JSONObject singleAttachmentObject = mFeedItem.getmFeedAttachmentArray().optJSONObject(0);
                                                                        int photo_id = singleAttachmentObject.optInt("photo_id");
                                                                        int album_id = singleAttachmentObject.optInt("album_id");
                                                                        int likes_count = singleAttachmentObject.optInt("likes_count");
                                                                        int comment_count = singleAttachmentObject.optInt("comment_count");
                                                                        int is_like = singleAttachmentObject.optInt("is_like");
                                                                        String reactions = singleAttachmentObject.optString("reactions");
                                                                        String mUserTagArray = singleAttachmentObject.optString("tags");

                                                                        boolean likeStatus = is_like != 0;
                                                                        String albumViewUrl;

                                                                        if (listItemHolder.mFeedAttachmentType != null &&
                                                                                listItemHolder.mFeedAttachmentType.equals("album_photo")) {
                                                                            albumViewUrl = UrlUtil.ALBUM_VIEW_PAGE + album_id + "?gutter_menu=1";

                                                                        } else {
                                                                            albumViewUrl = UrlUtil.ALBUM_VIEW_URL + album_id;
                                                                        }

                                                                        String postBody = null;
                                                                        if (mFeedItem.getmActionTypeBody() != null && !mFeedItem.getmActionTypeBody().isEmpty()) {
                                                                            postBody = Smileys.getEmojiFromString(Html.fromHtml(mFeedItem.getmActionTypeBody()).toString());
                                                                            postBody = postBody.replaceAll("\n", "<br/>");
                                                                        }

                                                                        mPhotoDetails.add(new PhotoListDetails(postBody, photo_id, imageSrc,
                                                                                likes_count, comment_count, likeStatus, reactions, mUserTagArray));
                                                                        mFeedItem.setmPhotoDetails(mPhotoDetails);

                                                                        openPhotoLightBox(listItemHolder.getAdapterPosition(),
                                                                                albumViewUrl, listItemHolder.mPhotoAttachmentCount,
                                                                                listItemHolder.mFeedAttachmentType, album_id);


                                                                    }
                                                                });

                                                    }

                                                } else {
                                                    listItemHolder.mSingleAttachmentImage.setVisibility(View.GONE);
                                                    listItemHolder.mImagesGallery.setVisibility(View.VISIBLE);
                                                    switch (listItemHolder.mPhotoAttachmentCount) {
                                                        case 2:
                                                            columnWidth = mAppConst.getScreenWidth() / 2;
                                                            if (mediumImage != null && !mediumImage.isEmpty()) {
                                                                mPhotoUrls.add(new ImageViewList(mainImage, columnWidth,
                                                                        columnHeight));
                                                            }
                                                            break;
                                                        case 3:
                                                            if (mPhotoPosition == 0) {
                                                                columnWidth = mAppConst.getScreenWidth();
                                                            } else {
                                                                columnWidth = mAppConst.getScreenWidth() / 2;
                                                            }
                                                            if (mediumImage != null && !mediumImage.isEmpty()) {
                                                                mPhotoUrls.add(new ImageViewList(mainImage, columnWidth,
                                                                        columnHeight));
                                                            }
                                                            ++mPhotoPosition;
                                                            break;
                                                        default:
                                                            if (mPhotoPosition < 2) {
                                                                if (mPhotoPosition == 0) {
                                                                    columnWidth = mAppConst.getScreenWidth();
                                                                } else {
                                                                    columnWidth = mAppConst.getScreenWidth() / 2;
                                                                }
                                                                if (mediumImage != null && !mediumImage.isEmpty()) {
                                                                    mPhotoUrls.add(new ImageViewList(mediumImage, columnWidth,
                                                                            columnHeight));
                                                                }
                                                            } else if (mPhotoPosition == 2) {
                                                                columnWidth = mAppConst.getScreenWidth() / 2;
                                                                if (mediumImage != null && !mediumImage.isEmpty()) {
                                                                    mPhotoUrls.add(new ImageViewList(mediumImage, columnWidth,
                                                                            columnHeight, listItemHolder.mPhotoAttachmentCount - mPhotoPosition));
                                                                }
                                                            }
                                                            ++mPhotoPosition;
                                                            break;
                                                    }
                                                }
                                            } else {
                                                // Show Photo as attachment in card view
                                                listItemHolder.mImagesGallery.setVisibility(View.GONE);
                                                listItemHolder.mSingleAttachmentImage.setVisibility(View.GONE);
                                                if (!listItemHolder.mFeedAttachmentType.equals("music_playlist_song")) {
                                                    listItemHolder.mAttachmentView.setVisibility(View.VISIBLE);
                                                    if (mainImage != null && !mainImage.isEmpty()) {
                                                        listItemHolder.mAttachmentPreviewBlock.setVisibility(View.VISIBLE);
                                                        if (imageMainObj != null && imageMainObj.length() > 0) {

                                                            LinearLayout.LayoutParams singleImageParam;
                                                            RelativeLayout.LayoutParams layoutParams;
                                                            JSONObject size = imageMainObj.optJSONObject("size");

                                                            if (size.optInt("width") != 0 && size.optInt("height") != 0) {
                                                                if (size.optInt("width") == size.optInt("height")) {
                                                                    layoutParams = CustomViews.getCustomWidthHeightRelativeLayoutParams
                                                                            (mAppConst.getScreenWidth(), mAppConst.getScreenWidth());
                                                                } else {
                                                                    float ratio = ((float) size.optInt("height") / (float)size.optInt("width")) *
                                                                            (mAppConst.getScreenWidth() - size.optInt("width"));

                                                                    layoutParams = CustomViews.getCustomWidthHeightRelativeLayoutParams
                                                                            (mAppConst.getScreenWidth(), size.optInt("height") + Math.round(ratio));
                                                                }
                                                                listItemHolder.mAttachmentImage.setLayoutParams(layoutParams);
                                                                singleImageParam = CustomViews.getFullWidthLayoutParams();
                                                            } else {
                                                                singleImageParam = CustomViews.getCustomWidthHeightLayoutParams(mAppConst.getScreenWidth(),
                                                                        (int) mContext.getResources().getDimension(R.dimen.feed_attachment_image_height));
                                                            }
                                                            listItemHolder.mAttachmentPreviewBlock.setLayoutParams(singleImageParam);
                                                        }

                                                        listItemHolder.mAttachmentImage.setVisibility(View.VISIBLE);
                                                        Picasso.with(mContext)
                                                                .load(mainImage)
                                                                .into(listItemHolder.mAttachmentImage);
                                                    }
                                                } else {
                                                    listItemHolder.mAttachmentPreviewBlock.setVisibility(View.GONE);
                                                    listItemHolder.mAttachmentImage.setVisibility(View.GONE);
                                                    listItemHolder.mPlayIcon.setVisibility(View.GONE);
                                                }

                                                if((attachmentType.contains("video") && !attachmentType.equals("sitevideo_channel")
                                                        && !attachmentType.equals("sitevideo_playlist"))
                                                        || ((listItemHolder.mFeedAttachmentType != null
                                                        && (listItemHolder.mFeedAttachmentType.contains("video")
                                                        && !listItemHolder.mFeedAttachmentType.equals("sitevideo_channel")
                                                        && !listItemHolder.mFeedAttachmentType.equals("sitevideo_playlist"))
                                                        && listItemHolder.mFeedType.equals("share")))) {
                                                    listItemHolder.mPlayIcon.setVisibility(View.VISIBLE);
                                                } else {
                                                    listItemHolder.mPlayIcon.setVisibility(View.GONE);
                                                }
                                            }
                                        } else {
                                            // Hide Photo Fields if there are no photos
                                            listItemHolder.mSingleAttachmentImage.setVisibility(View.GONE);
                                            listItemHolder.mImagesGallery.setVisibility(View.GONE);
                                            listItemHolder.mAttachmentImage.setVisibility(View.GONE);
                                            listItemHolder.mAttachmentPreviewBlock.setVisibility(View.GONE);
                                            listItemHolder.mPlayIcon.setVisibility(View.GONE);
                                        }

                                        // Set Title and description for mode 1.
                                        if (mode == 1) {
                                            String attachmentBody = singleAttachmentObject.optString("body");

                                        /*
                                         Show Attachment Title in card View
                                         Do not show title and description in case of photo attachment feeds
                                          */
                                            if (listItemHolder.mFeedAttachmentType != null &&
                                                    listItemHolder.mFeedAttachmentType.equals("music_playlist_song")) {

                                                listItemHolder.mMusicAttachmentView.setVisibility(View.VISIBLE);
                                                listItemHolder.mAttachmentView.setVisibility(View.GONE);
                                                listItemHolder.mMusicAndLinkAttachmentImage.setVisibility(View.VISIBLE);
                                                listItemHolder.mMusicAndLinkAttachmentImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_empty_music2));

                                                if (attachmentTitle != null && !attachmentTitle.isEmpty()
                                                        && !attachmentTitle.equals("null")) {

                                                    listItemHolder.mMusicAttachmentTitle.setVisibility(View.VISIBLE);
                                                    listItemHolder.mMusicAttachmentTitle.setText(attachmentTitle);
                                                    listItemHolder.mMusicAttachmentTitle.setMovementMethod
                                                            (LinkMovementMethod.getInstance());

                                                } else {
                                                    listItemHolder.mMusicAttachmentTitle.setVisibility(View.GONE);
                                                }

                                                if (attachmentBody != null && !attachmentBody.isEmpty()) {
                                                    listItemHolder.mMusicAttachmentBody.setVisibility(View.VISIBLE);
                                                    listItemHolder.mMusicAttachmentBody.setText(
                                                            Html.fromHtml(attachmentBody.replaceAll("<img.+?>|<IMG.+?>", "")));
                                                } else {
                                                    listItemHolder.mMusicAttachmentBody.setVisibility(View.GONE);
                                                }


                                            } else {
                                                listItemHolder.mMusicAttachmentView.setVisibility(View.GONE);
                                                if (!(attachmentType.contains("_photo") || (listItemHolder.mFeedAttachmentType != null
                                                        && listItemHolder.mFeedAttachmentType.contains("_photo")
                                                        && listItemHolder.mFeedType.equals("share")))) {

                                                    if (attachmentTitle != null && !attachmentTitle.isEmpty()
                                                            && !attachmentTitle.equals("null")) {

                                                        listItemHolder.mAttachmentTitle.setVisibility(View.VISIBLE);
                                                        listItemHolder.mAttachmentTitle.setText(attachmentTitle);
                                                        listItemHolder.mAttachmentTitle.setMovementMethod
                                                                (LinkMovementMethod.getInstance());
                                                        // Showing attachment url in card view.
                                                        if (listItemHolder.mFeedType.equals("share") &&
                                                                listItemHolder.mWebUrl != null &&
                                                                !listItemHolder.mWebUrl.isEmpty()) {
                                                            listItemHolder.mAttachmentUrlView.setText(Html.fromHtml(listItemHolder.mWebUrl).toString().trim());
                                                            listItemHolder.mAttachmentUrlView.setVisibility(View.VISIBLE);
                                                        } else {
                                                            listItemHolder.mAttachmentUrlView.setVisibility(View.GONE);
                                                        }

                                                    } else {
                                                        listItemHolder.mAttachmentTitle.setVisibility(View.GONE);
                                                    }

                                                    if (attachmentBody != null && !attachmentBody.isEmpty()) {
                                                        listItemHolder.mAttachmentView.setVisibility(View.VISIBLE);
                                                        listItemHolder.mAttachmentBody.setVisibility(View.VISIBLE);

                                                        //Checking feed is singlefeed or activity feed
                                                        // if feed is single then showing post with large text else showing a "See More" option(if text too large)
                                                        //Checking if post length is grater than 150 then showing "See More" option else showing full post
                                                        if (!mIsSingleFeed && Html.fromHtml(attachmentBody).toString().trim().length() >
                                                                ConstantVariables.FEED_TITLE_BODY_LENGTH) {

                                                            String contentAttachment = attachmentType;
                                                            if (listItemHolder.mFeedAttachmentType != null
                                                                    && listItemHolder.mFeedType.equals("share")) {
                                                                contentAttachment = listItemHolder.mFeedAttachmentType;
                                                            }
                                                            seeMoreOption(listItemHolder.mAttachmentBody,
                                                                    Html.fromHtml(attachmentBody).toString(),
                                                                    listItemHolder, contentAttachment);
                                                        } else {
                                                            listItemHolder.mAttachmentBody.setText(
                                                                    Html.fromHtml(attachmentBody.replaceAll("<img.+?>|<IMG.+?>", "")));
                                                        }
                                                    } else {
                                                        listItemHolder.mAttachmentBody.setVisibility(View.GONE);
                                                    }
                                                } else {
                                                    listItemHolder.mAttachmentUrlView.setVisibility(View.GONE);
                                                }
                                            }


                                        } else {
                                            listItemHolder.mMusicAttachmentView.setVisibility(View.GONE);
                                            listItemHolder.mAttachmentTitle.setVisibility(View.GONE);
                                            listItemHolder.mAttachmentBody.setVisibility(View.GONE);
                                        }

                                    } else if (mode == 3) {

                                        listItemHolder.mMusicAttachmentView.setVisibility(View.GONE);
                                        // Show Attachment Body in mode 3
                                        String attachmentBody = singleAttachmentObject.optString("body");
                                        if (attachmentBody != null && !attachmentBody.isEmpty()) {
                                            listItemHolder.mAttachmentView.setVisibility(View.VISIBLE);
                                            listItemHolder.mAttachmentBody.setVisibility(View.VISIBLE);
                                            listItemHolder.mAttachmentBody.setText(Html.fromHtml(attachmentBody).toString().trim());

                                            // Showing attachment url in card view.
                                            if (listItemHolder.mFeedType.equals("share") &&
                                                    listItemHolder.mWebUrl != null &&
                                                    !listItemHolder.mWebUrl.isEmpty()) {
                                                listItemHolder.mAttachmentUrlView.setText(Html.fromHtml(listItemHolder.mWebUrl).toString().trim());
                                                listItemHolder.mAttachmentUrlView.setVisibility(View.VISIBLE);
                                            } else {
                                                listItemHolder.mAttachmentUrlView.setVisibility(View.GONE);
                                            }
                                        } else {
                                            listItemHolder.mAttachmentBody.setVisibility(View.GONE);
                                        }
                                        listItemHolder.mImagesGallery.setVisibility(View.GONE);
                                        listItemHolder.mSingleAttachmentImage.setVisibility(View.GONE);
                                    }
                                }

                                if (mPhotoUrls != null && mPhotoUrls.size() != 0) {
                                    if (mIsSingleFeed) {
                                        listItemHolder.mImagesGallery.setNestedScrollingEnabled(false);
                                    }
                                    adapter = new ImageAdapter((Activity) mContext, mPhotoUrls,
                                            new OnItemClickListener() {

                                                @Override
                                                public void onItemClick(View view, int position) {

                                                    mSelectedFeedList = (FeedList) mFeedItemList.get(listItemHolder.getAdapterPosition());
                                                    Intent intent = new Intent(mContext, SingleFeedPage.class);
                                                    intent.putExtra(ConstantVariables.ITEM_POSITION, listItemHolder.getAdapterPosition());
                                                    intent.putExtra(ConstantVariables.ACTION_ID, mSelectedFeedList.getmActionId());
                                                    intent.putExtra(ConstantVariables.FEED_LIST, mSelectedFeedList);
                                                    intent.putExtra("reactionEnabled", mReactionsEnabled);
                                                    if(mReactions != null){
                                                        intent.putExtra("reactions", mReactions.toString());
                                                    }
                                                    if (mFeedsFragment != null) {
                                                        mFeedsFragment.startActivityForResult(intent, ConstantVariables.VIEW_SINGLE_FEED_PAGE);
                                                    } else {
                                                        (mContext).startActivity(intent);
                                                    }
                                                }
                                            });
                                    listItemHolder.mImagesGallery.setAdapter(adapter);

                                } else {
                                    listItemHolder.mImagesGallery.setVisibility(View.GONE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            listItemHolder.mAttachmentView.setVisibility(View.GONE);
                            listItemHolder.mMusicAttachmentView.setVisibility(View.GONE);
                            listItemHolder.mSingleAttachmentImage.setVisibility(View.GONE);
                            listItemHolder.mImagesGallery.setVisibility(View.GONE);
                        }

                    } else if (listItemHolder.mFeedType.equals("sitetagcheckin_status") ||
                            listItemHolder.mFeedType.equals("sitetagcheckin_checkin")) {
                        listItemHolder.mMapView.setVisibility(View.VISIBLE);
                        listItemHolder.mMapView.onResume();
                        listItemHolder.mLatitude = mFeedItem.getmLatitude();
                        listItemHolder.mLongitude = mFeedItem.getmLongitude();
                        listItemHolder.mLocationLabel = mFeedItem.getmLocationLabel();

                        listItemHolder.mMapView.getMapAsync(new OnMapReadyCallback() {
                            @Override
                            public void onMapReady(GoogleMap googleMap) {
                                MapsInitializer.initialize(mContext);
                                LatLng latLng = new LatLng(listItemHolder.mLatitude, listItemHolder.mLongitude);
                                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                                googleMap.addMarker(new MarkerOptions().position(latLng)).setTitle(listItemHolder.mLocationLabel);
                            }
                        });

                        listItemHolder.mAttachmentView.setVisibility(View.GONE);
                        listItemHolder.mMusicAttachmentView.setVisibility(View.GONE);
                        listItemHolder.mSingleAttachmentImage.setVisibility(View.GONE);
                        listItemHolder.mImagesGallery.setVisibility(View.GONE);

                    } else {
                        listItemHolder.mAttachmentView.setVisibility(View.GONE);
                        listItemHolder.mMusicAttachmentView.setVisibility(View.GONE);
                        listItemHolder.mSingleAttachmentImage.setVisibility(View.GONE);
                        listItemHolder.mImagesGallery.setVisibility(View.GONE);
                        listItemHolder.mMapView.setVisibility(View.GONE);
                    }
                }


            /* End: Show Attachment Info */


            /* Start: Set Like and Comment Count Info */

                if (mFeedItem.ismCanComment() != 0) {

                /* Show Like Count */
                    if (mFeedItem.getmLikeCount() > 0) {
                        listItemHolder.mCounterView.setVisibility(View.VISIBLE);
                        listItemHolder.mLikeCount.setVisibility(View.VISIBLE);

                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) listItemHolder.mCommentCount.getLayoutParams();
                        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE);
                        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
                        listItemHolder.mCommentCount.setLayoutParams(layoutParams);

                        /**
                         * Show 3 popular reaction icons with the reaction count
                         */
                        if (mReactionsEnabled == 1 && listItemHolder.mPopularReactionsArray != null &&
                                listItemHolder.mPopularReactionsArray.length() != 0) {

                            listItemHolder.mCountSaperator.setVisibility(View.VISIBLE);
                            listItemHolder.mPopularReactionsView.setVisibility(View.VISIBLE);
                            listItemHolder.mPopularReactionsView.removeAllViews();

                            JSONArray reactionIds = listItemHolder.mPopularReactionsArray.names();

                            for (int i = 0; i < listItemHolder.mPopularReactionsArray.length() && i < 3; i++) {
                                String imageUrl = listItemHolder.mPopularReactionsArray.optJSONObject(reactionIds.optString(i)).
                                        optString("reaction_image_icon");
                                int reactionId = listItemHolder.mPopularReactionsArray.optJSONObject(reactionIds.optString(i)).
                                        optInt("reactionicon_id");
                                listItemHolder.mPopularReactionsView.addView(CustomViews
                                        .generateReactionImageView(mContext, reactionId, imageUrl));
                            }
                        } else {
                            listItemHolder.mCountSaperator.setVisibility(View.GONE);
                            listItemHolder.mPopularReactionsView.setVisibility(View.GONE);
                        }

                        setLikeCount(mFeedItem.getmIsLike(), mFeedItem.getmLikeCount(), listItemHolder);

                    } else {
                        listItemHolder.mLikeCount.setVisibility(View.GONE);
                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) listItemHolder.mCommentCount.getLayoutParams();
                        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_END, 0);
                        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
                        listItemHolder.mCommentCount.setLayoutParams(layoutParams);
                        listItemHolder.mCountSaperator.setVisibility(View.GONE);
                        listItemHolder.mPopularReactionsView.setVisibility(View.GONE);
                    }

                    /* Show Comment Count */
                    if (mFeedItem.getmCommentCount() != 0 && (!mIsSingleFeed || isPhotoFeed)) {
                        listItemHolder.mCounterView.setVisibility(View.VISIBLE);
                        listItemHolder.mCommentCount.setVisibility(View.VISIBLE);
                        String commentText = mContext.getResources().getQuantityString(R.plurals.profile_page_comment,
                                mFeedItem.getmCommentCount());
                        listItemHolder.mCommentCount.setText(Html.fromHtml(String.format(
                                mContext.getResources().getString(R.string.comment_count_text),
                                mFeedItem.getmCommentCount(), commentText
                        )));
                    } else {
                        listItemHolder.mCommentCount.setVisibility(View.GONE);
                    }
                } else {
                    listItemHolder.mLikeCount.setVisibility(View.GONE);
                    listItemHolder.mCommentCount.setVisibility(View.GONE);
                    listItemHolder.mPopularReactionsView.setVisibility(View.GONE);
                    listItemHolder.mCountSaperator.setVisibility(View.GONE);
                }
            /* End: Set Like and Comment Count Info */


            /* Start: Show FooterMenus Like/Comment/Share */
                if (mFeedItem.getmFeedFooterMenus() != null &&
                        mFeedItem.getmFeedFooterMenus().length() != 0) {
                    listItemHolder.mFeedFooterMenusBlock.setVisibility(View.VISIBLE);

                    try {
                        if (mFeedItem.ismCanComment() != 0) {
                            // Show Like Option
                            // Create New TextView for Like Option and add this in mFeedFooterMenusBlock.
                            if (mFeedItem.getmIsLike() == 0) {
                                listItemHolder.mLikeButton.setCompoundDrawablesWithIntrinsicBounds(
                                        ContextCompat.getDrawable(mContext, R.drawable.ic_thumb_up_white_18dp),
                                        null, null, null);
                                int drawablePadding = mContext.getResources().getDimensionPixelSize(R.dimen.element_spacing_normal);
                                listItemHolder.mLikeButton.setCompoundDrawablePadding(drawablePadding);
                                listItemHolder.mLikeButton.setActivated(false);
                                listItemHolder.mLikeButton.setTextColor(
                                        ContextCompat.getColor(mContext, R.color.grey_dark));
                                listItemHolder.mLikeButton.setText(mContext.getResources().
                                        getString(R.string.like_text));
                                listItemHolder.mReactionImage.setVisibility(View.GONE);
                            } else {
                                listItemHolder.mLikeButton.setActivated(true);
                                listItemHolder.mLikeButton.setTextColor(
                                        ContextCompat.getColor(mContext, R.color.colorPrimary));
                                if(mReactionsEnabled == 1 && mFeedItem.getmMyFeedReactions() != null){
                                    String reactionImage = mFeedItem.getmMyFeedReactions().optString("reaction_image_icon");

                                    listItemHolder.mReactionImage.setVisibility(View.VISIBLE);
                                    Picasso.with(mContext)
                                            .load(reactionImage)
                                            .into(listItemHolder.mReactionImage);
                                    listItemHolder.mLikeButton.setCompoundDrawables(null, null, null, null);
                                    listItemHolder.mLikeButton.setText(mFeedItem.getmMyFeedReactions()
                                            .optString("caption"));
                                } else{
                                    listItemHolder.mReactionImage.setVisibility(View.GONE);
                                    listItemHolder.mLikeButton.setCompoundDrawablesWithIntrinsicBounds(
                                            ContextCompat.getDrawable(mContext, R.drawable.ic_thumb_up_white_18dp),
                                            null, null, null);
                                    listItemHolder.mLikeButton.setText(mContext.getResources().
                                            getString(R.string.like_text));
                                }
                            }
                            listItemHolder.mLikeButton.setVisibility(View.VISIBLE);
                            listItemHolder.mLikeButton.setTag(position);
                            listItemHolder.mCommentButton.setVisibility(View.VISIBLE);
                            listItemHolder.mCommentButton.setTag(position);

                        } else {
                            // Hide Like and Comment if comments are not allowed.
                            listItemHolder.mLikeButton.setVisibility(View.GONE);
                            listItemHolder.mCommentButton.setVisibility(View.GONE);
                            listItemHolder.mReactionImage.setVisibility(View.GONE);

                        }

                        // Show Share Option
                        if (mFeedItem.getmShareAble() != 0
                                && mFeedItem.getmFeedFooterMenus().optJSONObject("share") != null) {
                            listItemHolder.mShareButton.setVisibility(View.VISIBLE);
                            listItemHolder.mShareButton.setText(mFeedItem.getmFeedFooterMenus().
                                    optJSONObject("share").getString("label").trim());
                            listItemHolder.mShareButton.setTag(position);


                        } else {
                            listItemHolder.mShareButton.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    listItemHolder.mFeedFooterMenusBlock.setVisibility(View.GONE);
                }
            /* End: Show FooterMenus Like/Comment/Share */

            /* Start: Click Listener on Comment Option */

                listItemHolder.mCommentButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        itemPosition = (int) view.getTag();
                        // Show Keyboard for Single Feed page when Click on Comments.
                        if ((!mIsSingleFeed || isPhotoFeed))
                            showComments();
                        else {
                            mAppConst.showKeyboard();
                            if (mCommentEditText != null)
                                mCommentEditText.requestFocus();
                        }
                    }
                });

            /* End: Click Listener on Comment Option */

            /* Start: Click Listener on Like Comment Info */

                if (listItemHolder.mCounterView != null) {
                    listItemHolder.mCounterView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            itemPosition = (int) view.getTag();
                            showComments();
                        }
                    });

                    listItemHolder.mCounterView.setClickable((mFeedItem.ismCanComment() != 0));
                }

            /* End: Click Listener on Like Comment Info */

            /* Start: Click Listener on Like Menu */

                if(mReactionsEnabled == 1){
                    listItemHolder.mLikeButton.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {

                            itemPosition = (int) v.getTag();
                            final FeedList feedInfoList = (FeedList) mFeedItemList.get(itemPosition);

                            int[] location = new int[2];
                            listItemHolder.mCounterView.getLocationOnScreen(location);
                            RecyclerView reactionsRecyclerView = new RecyclerView(mContext);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
                            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                            reactionsRecyclerView.setHasFixedSize(true);
                            reactionsRecyclerView.setLayoutManager(linearLayoutManager);
                            reactionsRecyclerView.setItemAnimator(new DefaultItemAnimator());

                            final PopupWindow popUp = new PopupWindow(reactionsRecyclerView, LinearLayout.LayoutParams.WRAP_CONTENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT);
                            popUp.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.shape));
                            popUp.setTouchable(true);
                            popUp.setFocusable(true);
                            popUp.setOutsideTouchable(true);
                            popUp.setAnimationStyle(R.style.customDialogAnimation);

                            // Playing popup effect when user long presses on like button of a feed.
                            if (PreferencesUtils.isSoundEffectEnabled(mContext)) {
                                SoundUtil.playSoundEffectOnReactionsPopup(mContext);
                            }
                            popUp.showAtLocation(reactionsRecyclerView, Gravity.TOP, location[0], location[1]);


                            if(mReactions != null && mReactionsArray != null) {

                                reactionsImages = new ArrayList<>();

                                for(int i = 0; i< mReactionsArray.size(); i++){
                                    JSONObject reactionObject = mReactionsArray.get(i);
                                    String reaction_image_url = reactionObject.optJSONObject("icon").
                                            optString("reaction_image_icon");
                                    String caption = reactionObject.optString("caption");
                                    String reaction = reactionObject.optString("reaction");
                                    int reactionId = reactionObject.optInt("reactionicon_id");
                                    String reactionIconUrl = reactionObject.optJSONObject("icon").
                                            optString("reaction_image_icon");
                                    reactionsImages.add(new ImageViewList(reaction_image_url, caption,
                                            reaction, reactionId, reactionIconUrl));
                                }

                                reactionsAdapter = new ImageAdapter((Activity) mContext, reactionsImages, true,
                                        new OnItemClickListener() {

                                            @Override
                                            public void onItemClick(View view, int position) {


                                                ImageViewList imageViewList = reactionsImages.get(position);
                                                String reaction = imageViewList.getmReaction();
                                                String caption = imageViewList.getmCaption();
                                                String reactionIcon = imageViewList.getmReactionIcon();
                                                int reactionId = imageViewList.getmReactionId();
                                                popUp.dismiss();

                                                /**
                                                 * If the user Presses the same reaction again then don't do anything
                                                 */
                                                if (feedInfoList.getmMyFeedReactions() != null) {
                                                    if (feedInfoList.getmMyFeedReactions().optInt("reactionicon_id") != reactionId) {
                                                        boolean isLike = feedInfoList.getmIsLike() == 1;
                                                        instantLike(listItemHolder, caption, reactionId, reactionIcon, isLike);
                                                        doLikeUnlike(listItemHolder.getAdapterPosition(), reaction, isLike);
                                                    }
                                                } else {
                                                    instantLike(listItemHolder, caption, reactionId, reactionIcon, false);
                                                    doLikeUnlike(listItemHolder.getAdapterPosition(), reaction, false);
                                                }
                                            }
                                        });

                                reactionsRecyclerView.setAdapter(reactionsAdapter);
                            }
                            return true;
                        }
                    });
                }

                listItemHolder.mLikeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {

                        /**
                         * Apply animation on like button
                         */
                        listItemHolder.mLikeButton.startAnimation(AppConstant.getZoomInAnimation(mContext));

                        itemPosition = (int) view.getTag();

                        int reactionId = 0;
                        String reactionIcon = null;
                        if(mReactions != null ){
                            reactionId = mReactions.optJSONObject("like").optInt("reactionicon_id");
                            reactionIcon = mReactions.optJSONObject("like").optJSONObject("icon").
                                    optString("reaction_image_icon");
                        }
                        instantLike(listItemHolder, mContext.getResources().getString(R.string.like_text),
                                reactionId, reactionIcon, false);
                        doLikeUnlike(itemPosition, null, false);
                    }

                });
            /* End: Click Listener on Like Menu */

            /* Start: Click Listener on Share Menu */

                listItemHolder.mShareButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String attachmentType = null, contentUrl = null;

                        itemPosition = (int) view.getTag();
                        final FeedList feedInfoList = (FeedList) mFeedItemList.get(itemPosition);
                        final JSONArray feedAttachmentArray = feedInfoList.getmFeedAttachmentArray();
                        final JSONObject shareJsonObject = feedInfoList.getmFeedFooterMenus().optJSONObject("share");
                        JSONObject feedObject = feedInfoList.getmFeedObject();
                        String title = null, image = null, url = null;

                        try {
                            url = AppConstant.DEFAULT_URL + shareJsonObject.getString("url");
                            JSONObject urlParams = shareJsonObject.optJSONObject("urlParams");
                            HashMap<String, String> shareParams = new HashMap<>();
                            if (urlParams != null && urlParams.length() != 0) {
                                JSONArray urlParamsKeys = urlParams.names();
                                for (int i = 0; i < urlParams.length(); i++) {
                                    String key = urlParamsKeys.getString(i);
                                    String value = urlParams.getString(key);
                                    shareParams.put(key, value);
                                }
                            }
                            if (shareParams.size() != 0) {
                                url = mAppConst.buildQueryString(url, shareParams);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (feedAttachmentArray != null && feedAttachmentArray.length() != 0) {
                            JSONObject feedAttachmentObject = feedAttachmentArray.optJSONObject(0);

                            if (feedAttachmentObject != null && feedAttachmentObject.length() != 0) {
                                title = feedAttachmentObject.optString("title");
                                JSONObject imgObject = feedAttachmentObject.optJSONObject("image_main");
                                if (imgObject != null)
                                    image = imgObject.optString("src");
                                attachmentType = feedAttachmentObject.optString("attachment_type");
                                contentUrl = feedAttachmentObject.optString("content_url");
                            }
                        }

                        if(attachmentType != null && (attachmentType.contains("_photo")
                                || attachmentType.equals("image")) && mFeedsFragment != null){
                            mFeedsFragment.checkWriteAccessPermission(view, title, image, url,
                                    attachmentType, feedObject.optString("url"));
                        }else {

                            socialShareUtil.sharePost(view, title, image, url,
                                    attachmentType,
                                    (contentUrl != null && !contentUrl.isEmpty() ? contentUrl : feedObject.optString("url")));
                        }


                    }
                });
           /* End: Click Listener on Share Menu */

            /* Start click listener on edit feed Post button*/

                listItemHolder.mFeedEditSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final int removePosition = (int) v.getTag();
                        mAppConst.hideKeyboard();

                        String body = listItemHolder.mFeedTitleBodyEditText.getText().toString();

                        if ( body != null && !body.isEmpty()) {

                            listItemHolder.mFeedEditCancel.setClickable(false);
                            listItemHolder.mFeedTitleBodyEditText.setError(null);
                            listItemHolder.mFeedEditSubmit.setText(mContext.getResources().getString(R.string.comment_posting) + "...");

                            try {
                                byte[] bytes = body.trim().getBytes("UTF-8");
                                body = new String(bytes, Charset.forName("UTF-8"));
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }

                            body = Smileys.getEmojiFromString(body);

                            mEditBody = body;

                            final FeedList selectedFeedList = (FeedList) mFeedItemList.get(removePosition);

                            String actionUrl = selectedFeedList.getmMenuUrl();
                            int action_id = selectedFeedList.getmActionId();
                            mPostParams.clear();

                            mPostParams.put("action_id", String.valueOf(action_id));
                            mPostParams.put("body", mEditBody);

                            actionUrl = mAppConst.buildQueryString(actionUrl, mPostParams);
                            mAppConst.putResponseForUrl(actionUrl, mPostParams, new
                                    OnResponseListener() {
                                        @Override
                                        public void onTaskCompleted(JSONObject jsonObject) {

                                            if (mEditFeeds.contains(removePosition)) {
                                                mEditFeeds.remove(mEditFeeds.indexOf(removePosition));
                                            }
                                            mEditBody = mEditBody.replaceAll("\n", "<br/>");
                                            selectedFeedList.setmActionTypeBody(mEditBody);
                                            notifyDataSetChanged();
                                            SnackbarUtils.displaySnackbar(listItemHolder.itemView,
                                                    mContext.getResources().getString(R.string.feed_edit_success_message));
                                        }

                                        @Override
                                        public void onErrorInExecutingTask(String message, boolean isRetryOption) {

                                            listItemHolder.mFeedTitleBodyEditText.setError(message);
                                            listItemHolder.mFeedEditSubmit.setText(mContext.getResources().
                                                    getString(R.string.feed_edit_button_text));
                                            listItemHolder.mFeedEditCancel.setClickable(true);
                                        }
                                    });


                        } else {
                            listItemHolder.mFeedTitleBodyEditText.setError(mContext.getResources().
                                    getString(R.string.feed_edit_empty_field_error_message));
                        }

                    }
                });

                /* End edit feed post click listener*/

                /* Start click listener on Cancel edit feed button */

                listItemHolder.mFeedEditCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int removePosition = (int) v.getTag();
                        mAppConst.hideKeyboard();

                        listItemHolder.mFeedBodyEditLayout.setVisibility(View.GONE);
                        listItemHolder.mFeedTitleBody.setVisibility(View.VISIBLE);
                        listItemHolder.mFeedTitleBodyEditText.setError(null);

                        if (mEditFeeds.contains(removePosition)) {
                            mEditFeeds.remove(mEditFeeds.indexOf(removePosition));
                        }
                    }
                });

                 /* End edit feed cancel click listener */

                /* To show Emoji Keybaord in edit feed body on-click emoji button*/

                listItemHolder.mEmojiButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EmojiUtil.showEmojiKeyboard(mContext, listItemHolder.mFeedTitleBodyEditText);
                    }
                });


                break;

            case HIDE_ALL_TYPE:
            case HIDDEN_TYPE:
                mFeedItem = (FeedList) this.mFeedItemList.get(position);

                /* Start: Put Subject_id and position of the feed in
                 * HashMap for hide all by a user functionality
                 */

                if (mFeedItem != null) {
                    mSubjectPositionList.put(mFeedItem.getmSubjectId() + "-" + position, position);
                }
                // Show Hidden Type Feed
                final HiddenItemHolder hiddenItemHolder = (HiddenItemHolder) viewHolder;
                if (type == HIDDEN_TYPE && mFeedItem != null) {
                    String hideBodyText = mFeedItem.getmHiddenBodyText();
                    final String undoURl = mFeedItem.getmUndoHiddenFeedURl();
                    String undoText = mContext.getResources().getString(R.string.undo_text);
                    hiddenItemHolder.mHiddenFeedBody.setMovementMethod(LinkMovementMethod.getInstance());
                    hiddenItemHolder.mHiddenFeedBody.setText(hideBodyText + " " +undoText, BufferType.SPANNABLE);

                    // Make Undo Clickable and Show the Feed again
                    Spannable mySpannable = (Spannable) hiddenItemHolder.mHiddenFeedBody.getText();
                    int start = hideBodyText.length() + 1;
                    int end = start + undoText.length();
                    ClickableSpan myClickableSpan = new ClickableSpan() {

                        @Override
                        public void onClick(View widget) {

                            mAppConst.showProgressDialog();
                            mAppConst.postJsonResponseForUrl(undoURl, mPostParams,
                                    new OnResponseListener() {
                                        @Override
                                        public void onTaskCompleted(JSONObject jsonObject) {
                                            mAppConst.hideProgressDialog();
                                            if (mHiddenFeeds.contains(viewHolder.getAdapterPosition())) {
                                                mHiddenFeeds.remove(mHiddenFeeds.indexOf(viewHolder.getAdapterPosition()));
                                            }
                                            mAllHiddenFeeds.clear();
                                            notifyDataSetChanged();
                                        }

                                        @Override
                                        public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                                            mAppConst.hideProgressDialog();

                                        }
                                    });
                        }

                        @Override
                        public void updateDrawState(TextPaint ds) {
                            super.updateDrawState(ds);
                            ds.setUnderlineText(false);
                            ds.setColor(ContextCompat.getColor(mContext, R.color.black));
                        }
                    };
                    mySpannable.setSpan(myClickableSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    // Set File a Report Option and Redirect to Report Activity
                    String hideAllName = mFeedItem.getmHideAllName();
                    if (hideAllName!= null && hideAllName.equals("report")) {
                        hiddenItemHolder.mHiddenFeedOptions.setText(mFeedItem.getmHideAllText());
                        hiddenItemHolder.mHiddenFeedOptions.setClickable(true);
                        final String hideAllUrl = mFeedItem.getmHideAllUrl();
                        hiddenItemHolder.mHiddenFeedOptions.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent reportIntent = new Intent(mContext, ReportEntry.class);
                                reportIntent.putExtra(ConstantVariables.URL_STRING, hideAllUrl);
                                mContext.startActivity(reportIntent);
                                ((Activity) mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            }
                        });
                    }
                } else {
                    hiddenItemHolder.mHiddenFeedBody.setVisibility(View.GONE);
                    hiddenItemHolder.mHiddenFeedOptions.setVisibility(View.GONE);
                }

                break;
            case HEADER_TYPE:
                mFeedItem = (FeedList) this.mFeedItemList.get(position);
             //   this.mFeedItemActionIdActual = mFeedItem.getmActionId();

                /* Start: Put Subject_id and position of the feed in
                 * HashMap for hide all by a user functionality
                 */

                if (mFeedItem != null) {
                    mSubjectPositionList.put(mFeedItem.getmSubjectId() + "-" + position, position);
                }
                // Show Header Block with Status Menus and Filters
                HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;

                assert mFeedItem != null;
                mFeedPostMenus = mFeedItem.getmFeedPostMenus();
                mFeedFiltersArray = mFeedItem.getmFeedFilterArray();
                boolean isNoFeed = mFeedItem.isNoFeed();
                mReactionsEnabled = mFeedItem.getmReactionsEnabled();
                mReactions = mFeedItem.getmReactions();
                if(mReactions != null){
                    mReactionsArray = GlobalFunctions.sortReactionsObjectWithOrder(mReactions);
                }

                if(PreferencesUtils.getUserDetail(mContext) != null) {
                    try {
                        JSONObject userDetail = new JSONObject(PreferencesUtils.getUserDetail(mContext));
                        mProfileIconImage = userDetail.getString("image_profile");
                        mLoggedInUserId = userDetail.getInt("user_id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (mFeedPostMenus != null && mFeedPostMenus.length() != 0) {

                    if (mProfileIconImage != null && !mProfileIconImage.isEmpty()) {
                        Picasso.with(mContext)
                                .load(mProfileIconImage)
                                .into(headerViewHolder.mUserProfileImage);

                        headerViewHolder.mUserProfileImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent userProfileIntent = new Intent(mContext, userProfile.class);
                                userProfileIntent.putExtra(ConstantVariables.USER_ID, mLoggedInUserId);
                                ((Activity) mContext).startActivityForResult(userProfileIntent, ConstantVariables.
                                        USER_PROFILE_CODE);
                                ((Activity)mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            }
                        });
                    }

                    if(!isPostMenusSet){
                        try {
                            int status = 0, photo = 0, checkIn = 0;

                            if (mFeedPostMenus.has("status")) {
                                status = mFeedPostMenus.getInt("status");
                            }

                            if (mFeedPostMenus.has("photo")) {
                                photo = mFeedPostMenus.getInt("photo");
                            }

                            if (mFeedPostMenus.has("checkin")) {
                                checkIn = mFeedPostMenus.getInt("checkin");
                            }

                            // Show Status
                            if (status != 0) {
                                headerViewHolder.mStatusMenuIcon.setTypeface(fontIcon,Typeface.BOLD);
                                headerViewHolder.mStatusMenuIcon.setText("\uF044");
                                headerViewHolder.mStatusMenuText.setText(mContext.getResources().getString(R.string.status));
                            } else {
                                headerViewHolder.mStatusMenu.setVisibility(View.GONE);
                            }

                            // Show Photo
                            if (photo != 0) {
                                headerViewHolder.mPhotoMenuIcon.setTypeface(fontIcon,Typeface.BOLD);
                                headerViewHolder.mPhotoMenuIcon.setText("\uf030 ");
                                headerViewHolder.mPhotoMenuText.setText(
                                        mContext.getResources().getString(R.string.photos));
                            } else {
                                headerViewHolder.mPhotoMenu.setVisibility(View.GONE);
                            }

                            // Show CheckIn Option

                            if(checkIn != 0 && ! mContext.getResources().getString(R.string.places_api_key).isEmpty()){
                                headerViewHolder.mCheckInMenuIcon.setTypeface(fontIcon,Typeface.BOLD);
                                headerViewHolder.mCheckInMenuIcon.setText("\uF041");
                                headerViewHolder.mCheckInMenuText.setText(mContext.getResources().getString(R.string.checkIn));
                            }else{
                                headerViewHolder.mCheckInMenu.setVisibility(View.GONE);
                            }

                            headerViewHolder.mStatusMenu.setOnClickListener(this);
                            headerViewHolder.mPhotoMenu.setOnClickListener(this);
                            headerViewHolder.mCheckInMenu.setOnClickListener(this);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        headerViewHolder.mStatusUpdateText.setText(mContext.getResources().getString
                                (R.string.status_box_default_text) + "...");
                        headerViewHolder.mStatusTextLayout.setOnClickListener(this);

                        headerViewHolder.mPostFeedOptions.setVisibility(View.VISIBLE);
                        isPostMenusSet = true;
                    }
                }

                // Set Filter DropDown
                if (mFeedFiltersArray != null && mFeedFiltersArray.length() != 0 &&
                        headerViewHolder.mFiltersSpinner != null) {
                    _adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    headerViewHolder.mFiltersSpinner.setAdapter(_adapter);
                    headerViewHolder.mFiltersSpinner.setSelection(mFilterPosition);

                    mFilterKeysMap = new HashMap<>();

                    try {
                        _adapter.clear();
                        for (int i = 0; i < mFeedFiltersArray.length(); i++) {
                            JSONObject singleTabObject = mFeedFiltersArray.getJSONObject(i);
                            String tabTitle = singleTabObject.getString("tab_title").trim();
                            JSONObject urlParams = singleTabObject.optJSONObject("urlParams");
                            String filter_type = urlParams.getString("filter_type");
                            _adapter.add(tabTitle);
                            mFilterKeysMap.put(tabTitle, filter_type);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    headerViewHolder.mFiltersSpinner.setVisibility(View.VISIBLE);
                    headerViewHolder.mFiltersSpinner.setGravity(Gravity.CENTER);
                    headerViewHolder.mFiltersSpinner.setOnItemSelectedListener(this);

                    //isFilterSet = true;
                }else {
                    CardView spinnerView = (CardView) headerViewHolder.mFiltersSpinner.getParent();
                    spinnerView.setVisibility(View.GONE);
                }

                // Set a No feeds Available Message if there are no feeds
                if (isNoFeed) {
                    headerViewHolder.mNoFeedMessage.setVisibility(View.VISIBLE);
                    headerViewHolder.mNoFeedMessage.setText(mContext.getResources().getString
                            (R.string.no_activity_feed_message));
                } else {
                    headerViewHolder.mNoFeedMessage.setVisibility(View.GONE);
                }
                break;

            case AD_FB:
                FacebookAdViewHolder adMob = (FacebookAdViewHolder) viewHolder ;
                if(adMob.mNativeAd !=null){
                    adMob.mNativeAd.unregisterView();
                }
                adMob.mNativeAd = (NativeAd) mFeedItemList.get(position);

                FacebookAdViewHolder.inflateAd(adMob.mNativeAd, adMob.adView, mContext,false);
                break;
            case AD_ADMOB:
                AdMobViewHolder adMobViewHolder = (AdMobViewHolder) viewHolder;

                AdMobViewHolder.inflateAd(mContext,
                        (NativeAppInstallAd) mFeedItemList.get(position),adMobViewHolder.mAdView);
                break;

            case COMMUNITY_ADS:
                CommunityAdsHolder communityAdsHolder = (CommunityAdsHolder) viewHolder;
                communityAdsHolder.mCommunityAd = (CommunityAdsList) mFeedItemList.get(position);
                CommunityAdsHolder.inflateAd(communityAdsHolder.mCommunityAd,
                        communityAdsHolder.adView, mContext, position);

                break;

            case SPONSORED_STORIES:
                SponsoredStoriesHolder sponsoredStoriesHolder = (SponsoredStoriesHolder) viewHolder;
                sponsoredStoriesHolder.mSponsoredStory = (SponsoredStoriesList) mFeedItemList.get(position);
                SponsoredStoriesHolder.inflateAd(sponsoredStoriesHolder.mSponsoredStory,
                        sponsoredStoriesHolder.adView, mContext, position);

                break;

            case PEOPLE_SUGGESTION:

                // Showing people suggestion recycler view at the specified position.
                final PeopleSuggestionViewHolder peopleSuggestionViewHolder = (PeopleSuggestionViewHolder) viewHolder;
                final List<Object> browseList = (List<Object>) mFeedItemList.get(position);
                final PeopleSuggestionAdapter peopleSuggestionAdapter = new PeopleSuggestionAdapter(mContext,
                        browseList, "feed_suggestion", new PeopleSuggestionAdapter.OnItemDeleteListener() {
                    @Override
                    public void onItemDelete() {
                        // When all the suggestion removed the sent the request to reload the data.
                        browseList.clear();
                        peopleSuggestionViewHolder.mProgressBar.setVisibility(View.VISIBLE);
                        mFeedsFragment.addPeopleSuggestionList(false);
                    }
                }, new PeopleSuggestionAdapter.OnAddRequestSuccessListener() {
                    @Override
                    public void onScrollPosition(int position) {
                        peopleSuggestionViewHolder.mSuggestionRecyclerView.smoothScrollToPosition(position);
                    }
                });

                // Setting adapter only once until no new data is added.
                if (!isAdapterSet) {
                    peopleSuggestionViewHolder.mSuggestionRecyclerView.setHasFixedSize(true);
                    ((SimpleItemAnimator) peopleSuggestionViewHolder.mSuggestionRecyclerView.getItemAnimator()).
                            setSupportsChangeAnimations(false);
                    peopleSuggestionViewHolder.mSuggestionRecyclerView.setLayoutManager(
                            new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                    peopleSuggestionViewHolder.mSuggestionRecyclerView.setAdapter(peopleSuggestionAdapter);
                    peopleSuggestionViewHolder.mProgressBar.setVisibility(View.GONE);
                    isAdapterSet = true;
                }
                peopleSuggestionViewHolder.tvSeeAll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        peopleSuggestionAdapter.showAllSuggestionActivity();
                    }
                });

                break;

            case REMOVE_AD_TYPE:

                // Show Hidden Type Feed
                final RemoveAdHolder removeAdHolder = (RemoveAdHolder) viewHolder;
                if(ConstantVariables.FEED_ADS_TYPE == ConstantVariables.TYPE_COMMUNITY_ADS){
                    removeAdHolder.mCommunityAd = (CommunityAdsList) mFeedItemList.get(position);
                    removeAdHolder.removeAd(removeAdHolder.mCommunityAd, removeAdHolder.adView, mContext, position);
                } else {
                    removeAdHolder.mSponsoredStory = (SponsoredStoriesList) mFeedItemList.get(position);
                    removeAdHolder.removeAd(removeAdHolder.mSponsoredStory, removeAdHolder.adView, mContext, position);
                }
                break;

            default:
                ProgressViewHolder.inflateProgressView(mContext, ((ProgressViewHolder) viewHolder).progressView,
                        mFeedItemList.get(position));
                break;
        }
    }

    private LinearLayout.LayoutParams getSingleImageParamFromWidthHeight(JSONObject imageMainObj) {
        LinearLayout.LayoutParams singleImageParam;
        JSONObject size = imageMainObj.optJSONObject("size");

        if (size.optInt("width") != 0 && size.optInt("height") != 0) {
            if (size.optInt("width") == size.optInt("height")) {
                singleImageParam = CustomViews.getCustomWidthHeightLayoutParams(
                        mAppConst.getScreenWidth(),  mAppConst.getScreenWidth());
            } else {
                float ratio = ((float) size.optInt("height") / (float)size.optInt("width")) *
                        (mAppConst.getScreenWidth() - size.optInt("width"));
                singleImageParam = CustomViews.getCustomWidthHeightLayoutParams(
                        mAppConst.getScreenWidth(), size.optInt("height") + Math.round(ratio));
            }
            
        } else {
            singleImageParam = CustomViews.getCustomWidthHeightLayoutParams(
                    mAppConst.getScreenWidth(),
                    LinearLayout.LayoutParams.WRAP_CONTENT);
        }

        return singleImageParam;
    }

    @Override
    public int getItemCount() {
        return mFeedItemList.size();
    }

    @Override
    public void onClick(View view) {

        int id = view.getId();
        Bundle feedPostMenus = new Bundle();
        switch (id){
            case R.id.statusMenu:
            case R.id.status_text_layout:
                feedPostMenus.putBoolean("showPhotoBlock", false);
                feedPostMenus.putBoolean("openCheckIn", false);
                break;
            case R.id.photoMenu:
                feedPostMenus.putBoolean("showPhotoBlock", true);
                feedPostMenus.putBoolean("openCheckIn", false);
                break;
            case R.id.checkInMenu:
                feedPostMenus.putBoolean("showPhotoBlock", false);
                feedPostMenus.putBoolean("openCheckIn", true);
                break;
        }
        if(mFeedPostMenus != null && mFeedPostMenus.length() != 0){
            feedPostMenus.putString("feedPostMenus", mFeedPostMenus.toString());
        }
        Intent statusIntent = new Intent(mContext, Status.class);
        statusIntent.putExtra(ConstantVariables.SUBJECT_TYPE, mSubjectType);
        statusIntent.putExtra(ConstantVariables.SUBJECT_ID, mFeedSubjectId);
        statusIntent.putExtras(feedPostMenus);

        if(id == R.id.statusMenu || id == R.id.status_text_layout){
            ((Activity)mContext).startActivityForResult(statusIntent, ConstantVariables.FEED_REQUEST_CODE);
            ((Activity)mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }else if(mFeedsFragment != null){
            if(id == R.id.photoMenu){
                mFeedsFragment.checkManifestPermissions(statusIntent, Manifest.permission.READ_EXTERNAL_STORAGE,
                        ConstantVariables.READ_EXTERNAL_STORAGE, true);
            }else{
                mFeedsFragment.checkManifestPermissions(statusIntent, Manifest.permission.ACCESS_FINE_LOCATION,
                        ConstantVariables.ACCESS_FINE_LOCATION, true);
            }
        }
    }

    /**
     * When Filter Will be selected from Filter DropDown
     */

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

        if (mFilterPosition != position) {
            mFilterPosition = position;
            String mFilterType = mFilterKeysMap.get(_adapter.getItem(position));

            if(this.mFilterSelectedListener != null){
                mFilterSelectedListener.setFilterType(mFilterType);
            }
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    /**
     * Method to reset people suggestion adapter.
     * @param isAdapterSet true if need to reset.
     */
    public void isPeopleSuggestionAdapterSet(boolean isAdapterSet) {
        this.isAdapterSet = isAdapterSet;
    }

    /**
     * Function Called to change Like/Unlike Option in footer Menus
     */

    public void doLikeUnlike(final int position, String reaction, boolean isReactionChanged){

        final Map<String, String> likeParams = new HashMap<>();
        final FeedList feedInfoList = (FeedList) mFeedItemList.get(position);
        final int action_id = feedInfoList.getmActionId();
        likeParams.put("action_id", String.valueOf(action_id));
        if(reaction != null){
            likeParams.put("reaction", reaction);
        }
        final JSONObject likeJsonObject = feedInfoList.getmFeedFooterMenus().optJSONObject("like");

        String likeUnlikeUrl = AppConstant.DEFAULT_URL;
        final String sendLikeNotificationUrl = AppConstant.DEFAULT_URL + "advancedactivity/send-like-notitfication";
        if(feedInfoList.getmIsLike() == 0) {
            feedInfoList.setmIsLike(1);
            notifyItemChanged(position);
            likeUnlikeUrl += "/advancedactivity/like?sendNotification=0";
            mAppConst.postJsonResponseForUrl(likeUnlikeUrl, likeParams, new OnResponseListener() {
                @Override
                public void onTaskCompleted(JSONObject jsonObject) {

                    try {
                        likeJsonObject.put("label", "Unlike");
                        likeJsonObject.put("name", "unlike");
                        feedInfoList.setmFeedFooterMenus(feedInfoList.getmFeedFooterMenus().put("like",
                                likeJsonObject));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (mIsSingleFeed && mClickedFeedPosition != -1) {
                        if(mReactionsEnabled == 1){
                            PreferencesUtils.updateFeedReactionsPref(mContext, PreferencesUtils.FEED_REACTIONS,
                                    feedInfoList.getmFeedReactions());
                            PreferencesUtils.updateFeedReactionsPref(mContext, PreferencesUtils.MY_FEED_REACTIONS,
                                    feedInfoList.getmMyFeedReactions());
                        }
                    }
                    updatePhotoLikeCommentCount(position);

                    /* Calling to send notifications after like action */
                    mAppConst.postJsonRequest(sendLikeNotificationUrl, likeParams);
                }

                @Override
                public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                    try {
                        likeJsonObject.put("label", "Like");
                        likeJsonObject.put("name", "like");
                        feedInfoList.setmFeedFooterMenus(feedInfoList.getmFeedFooterMenus().put("like",
                                likeJsonObject));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }else{
            if(isReactionChanged){
                likeUnlikeUrl += "/advancedactivity/like?sendNotification=1";
                feedInfoList.setmIsLike(1);
            } else {
                likeUnlikeUrl += "/advancedactivity/unlike";
                feedInfoList.setmIsLike(0);
            }
            notifyItemChanged(position);
            mAppConst.postJsonResponseForUrl(likeUnlikeUrl, likeParams, new OnResponseListener() {
                @Override
                public void onTaskCompleted(JSONObject jsonObject) {

                    try {
                        likeJsonObject.put("label", "Like");
                        likeJsonObject.put("name", "like");
                        feedInfoList.setmFeedFooterMenus(feedInfoList.getmFeedFooterMenus().put("like",
                                likeJsonObject));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (mIsSingleFeed && mClickedFeedPosition != -1) {
                        if(mReactionsEnabled == 1){
                            PreferencesUtils.updateFeedReactionsPref(mContext, PreferencesUtils.FEED_REACTIONS,
                                    feedInfoList.getmFeedReactions());
                            PreferencesUtils.updateFeedReactionsPref(mContext, PreferencesUtils.MY_FEED_REACTIONS,
                                    feedInfoList.getmMyFeedReactions());
                        }
                    }
                    updatePhotoLikeCommentCount(position);
                }

                @Override
                public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                    try {
                        likeJsonObject.put("label", "Unlike");
                        likeJsonObject.put("name", "unlike");
                        feedInfoList.setmFeedFooterMenus(feedInfoList.getmFeedFooterMenus().put("like",
                                likeJsonObject));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    /**
     * Method to update photo like and comment count.
     * @param position Position of item on which count is to be update.
     */
    public void updatePhotoLikeCommentCount(int position) {
        final FeedList feedInfoList = (FeedList) mFeedItemList.get(position);
        if (feedInfoList.getmPhotoAttachmentCount() == 1) {
            JSONArray feedAttachmentArray = feedInfoList.getmFeedAttachmentArray();
            JSONObject singleAttachmentObject = feedAttachmentArray.optJSONObject(0);
            try {
                singleAttachmentObject.put("likes_count", feedInfoList.getmLikeCount());
                singleAttachmentObject.put("comment_count", feedInfoList.getmCommentCount());
                singleAttachmentObject.put("is_like", feedInfoList.getmIsLike());
                if(mReactionsEnabled == 1 && PreferencesUtils.isNestedCommentEnabled(mContext)){
                    JSONObject reactionsObject = new JSONObject();
                    reactionsObject.put("feed_reactions", feedInfoList.getmFeedReactions());
                    reactionsObject.put("my_feed_reaction", feedInfoList.getmMyFeedReactions());
                    singleAttachmentObject.put("reactions", reactionsObject);
                }
                feedAttachmentArray.put(0, singleAttachmentObject);
                feedInfoList.setmFeedAttachmentArray(feedAttachmentArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            notifyItemChanged(position);
        }
    }


    /**
     * Function Called to Comment on Feeds.
     */

    public void showComments(){

        FeedList feedInfoList = (FeedList) mFeedItemList.get(itemPosition);
        Intent commentIntent;

        String mLikeCommentsUrl = AppConstant.DEFAULT_URL + "advancedactivity/feeds/likes-comments" ;

        /* In case of Single feed page we will redirect it to direct Like Page*/
        if(!(!mIsSingleFeed || isPhotoFeed)){
            mLikeCommentsUrl += "?viewAllLikes=1&action_id=" + feedInfoList.getmActionId();
            commentIntent = new Intent(mContext, Likes.class);
            commentIntent.putExtra("ViewAllLikesUrl", mLikeCommentsUrl);
        }else{
            mLikeCommentsUrl += "?subject_type=activity_action" + "&subject_id=" + feedInfoList.getmActionId() + "&viewAllComments=1&page=1&limit=20";
            commentIntent = new Intent(mContext, Comment.class);
            commentIntent.putExtra(ConstantVariables.ITEM_POSITION, itemPosition);
            commentIntent.putExtra(ConstantVariables.LIKE_COMMENT_URL, mLikeCommentsUrl);
            commentIntent.putExtra("reactionsEnabled", mReactionsEnabled);
            commentIntent.putExtra(ConstantVariables.SUBJECT_TYPE, ConstantVariables.AAF_MENU_TITLE);
            commentIntent.putExtra(ConstantVariables.SUBJECT_ID, feedInfoList.getmActionId());
            commentIntent.putExtra(ConstantVariables.ACTION_ID, feedInfoList.getmActionId());
            if(feedInfoList.getmFeedReactions() != null){
                commentIntent.putExtra("popularReactions", feedInfoList.getmFeedReactions().toString());
            }
        }
        if (mFeedsFragment != null) {
            mFeedsFragment.startActivityForResult(commentIntent, ConstantVariables.VIEW_COMMENT_PAGE_CODE);
        } else {
            ((Activity)mContext).startActivityForResult(commentIntent, ConstantVariables.VIEW_COMMENT_PAGE_CODE);
        }
        ((Activity) mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    /**
     * Method to check for parameter before transfer to respective activity.
     * @param title Title of the feed.
     * @param slug Slug in case of forum.
     * @param attachmentType Type of attachment.
     * @param attachmentId Id of attachment.
     * @param listItemHolder ListItemHolder for current selected item.
     * @param videoInfo video info.
     */
    public void redirectToActivity(String title, String slug, String attachmentType, int attachmentId,
                                   ListItemHolder listItemHolder, HashMap<Integer, String> videoInfo) {

        FeedList feedList = (FeedList) mFeedItemList.get(listItemHolder.getAdapterPosition());

        if((attachmentType.contains("video") && !attachmentType.equals("sitevideo_channel")
                && !attachmentType.equals("sitevideo_playlist"))
                || ((listItemHolder.mFeedAttachmentType != null
                && (listItemHolder.mFeedAttachmentType.contains("video")
                && !listItemHolder.mFeedAttachmentType.equals("sitevideo_channel")
                && !listItemHolder.mFeedAttachmentType.equals("sitevideo_playlist"))
                && listItemHolder.mFeedType.equals("share")))) {
            if (videoInfo != null && videoInfo.size() != 0) {
                String videoParams = videoInfo.get(attachmentId);
                String[] videoParts = videoParams.split("-");
                String videoType = videoParts[0];
                String videoUrl = videoParts[1];

                videoAttachmentClicked(listItemHolder.mFeedType, attachmentType,
                        listItemHolder.mFeedAttachmentType, videoType, videoUrl,
                        attachmentId, listItemHolder.mFeedObject,
                        listItemHolder.mFeedAttachmentArray.optJSONObject(0));
            }
        } else {
            int id;
            switch (attachmentType) {
                case "checkIn":
                    openMapActivity(title, listItemHolder.mLatitude,
                            listItemHolder.mLongitude, listItemHolder.mPlaceId);
                    break;

                case "sitereview_review":
                    id = listItemHolder.mFeedObject.optInt("listing_id",
                            listItemHolder.mFeedObject.optInt("resource_id"));
                    if (id == 0 && feedList.getmFeedAttachmentArray() != null) {
                        id = feedList.getmFeedAttachmentArray().optJSONObject(0).optInt("listing_id");
                    }
                    attachmentClicked(title, attachmentType, id, listItemHolder, slug);
                    break;

                case "sitepage_review":
                case "sitepagereview_review":
                    id = listItemHolder.mFeedObject.optInt("page_id",
                            listItemHolder.mFeedObject.optInt("resource_id"));
                    if (id == 0 && feedList.getmFeedAttachmentArray() != null) {
                        id = feedList.getmFeedAttachmentArray().optJSONObject(0).optInt("page_id");
                    }
                    attachmentClicked(title, attachmentType, id, listItemHolder, slug);
                    break;

                case "sitegroup_review":
                case "sitegroupreview_review":
                    id = listItemHolder.mFeedObject.optInt("group_id",
                            listItemHolder.mFeedObject.optInt("resource_id"));
                    if (id == 0 && feedList.getmFeedAttachmentArray() != null) {
                        id = feedList.getmFeedAttachmentArray().optJSONObject(0).optInt("group_id");
                    }
                    attachmentClicked(title, attachmentType, id, listItemHolder, slug);
                    break;

                default:
                    attachmentClicked(title, attachmentType, attachmentId, listItemHolder, slug);
                    break;
            }
        }
    }

    /**
     * Method to check for parameters before transfer to respective activity.
     * @param attachmentType Type of attachment.
     * @param attachmentId Id of attachment.
     * @param singleAttachmentObject Single attachment object.
     * @param listItemHolder ListItemHolder for current selected item.
     */
    public void redirectToAttachmentClickedActivity(String attachmentType, int attachmentId,
                                                    JSONObject singleAttachmentObject,
                                                    ListItemHolder listItemHolder) {
        int id;
        String attachmentTitle = singleAttachmentObject.optString("title");

        if((attachmentType.contains("video") && !attachmentType.equals("sitevideo_channel")
                && !attachmentType.equals("sitevideo_playlist"))
                || ((listItemHolder.mFeedAttachmentType != null
                && (listItemHolder.mFeedAttachmentType.contains("video")
                && !listItemHolder.mFeedAttachmentType.equals("sitevideo_channel")
                && !listItemHolder.mFeedAttachmentType.equals("sitevideo_playlist"))
                && listItemHolder.mFeedType.equals("share")))) {

            videoAttachmentClicked(listItemHolder.mFeedType, listItemHolder.mFeedAttachmentType,
                    attachmentType, singleAttachmentObject.optString("attachment_video_type"),
                    singleAttachmentObject.optString("attachment_video_url"), attachmentId,
                    listItemHolder.mFeedObject, singleAttachmentObject);

        } else {
            switch (attachmentType) {

                case "music_playlist_song":
                    id = singleAttachmentObject.optInt("playlist_id");
                    attachmentClicked(attachmentTitle, attachmentType, id, listItemHolder, null);
                    break;

                case "sitereview_review":
                    id = listItemHolder.mFeedObject.optInt("listing_id",
                            listItemHolder.mFeedObject.optInt("resource_id"));
                    attachmentClicked(attachmentTitle, attachmentType, id, listItemHolder, null);
                    break;

                case "siteevent_review":
                    id = singleAttachmentObject.optInt("event_id");
                    attachmentClicked(attachmentTitle, attachmentType, id, listItemHolder, null);
                    break;

                case "sitepage_review":
                case "sitepagereview_review":
                    id = listItemHolder.mFeedObject.optInt("page_id",
                            listItemHolder.mFeedObject.optInt("resource_id"));
                    if (id == 0 && mFeedItem.getmFeedAttachmentArray() != null) {
                        id = mFeedItem.getmFeedAttachmentArray().optJSONObject(0).optInt("page_id");
                    }
                    attachmentClicked(attachmentTitle, attachmentType, id, listItemHolder, null);
                    break;

                case "sitegroup_review":
                case "sitegroupreview_review":
                    id = listItemHolder.mFeedObject.optInt("group_id",
                            listItemHolder.mFeedObject.optInt("resource_id"));
                    if (id == 0 && mFeedItem.getmFeedAttachmentArray() != null) {
                        id = mFeedItem.getmFeedAttachmentArray().optJSONObject(0).optInt("group_id");
                    }
                    attachmentClicked(attachmentTitle, attachmentType, id, listItemHolder, null);
                    break;

                default:
                    if (listItemHolder.mFeedAttachmentType != null
                            && listItemHolder.mFeedType.equals("share")) {

                        id = attachmentId;
                        switch (listItemHolder.mFeedAttachmentType) {
                            case "sitereview_review":
                                id = singleAttachmentObject.optInt("listing_id");
                                break;

                            case "siteevent_review":
                                id = singleAttachmentObject.optInt("event_id");
                                break;

                            case "sitegroup_review":
                            case "sitegroupreview_review":
                                id = singleAttachmentObject.optInt("group_id");
                                break;

                            case "sitepage_review":
                            case "sitepagereview_review":
                                id = singleAttachmentObject.optInt("page_id");
                                break;
                        }
                        attachmentClicked(attachmentTitle, listItemHolder.mFeedAttachmentType, id,
                                listItemHolder, null);

                    } else {
                        //senal
                        attachmentClicked(attachmentTitle, attachmentType, attachmentId, listItemHolder, null);
                    }
                    break;
            }
        }
    }

    /**
     * Method to redirect attachments on click.
     * @param title Attachment Title.
     * @param attachmentType Which decide where to redirect the attachment.
     * @param attachmentId The id of attachment.
     * @param listItemHolder ListItemHolder for current selected item.
     * @param slug Slug in case of forum.
     */
    public void attachmentClicked(String title, String attachmentType, int attachmentId,
                                  ListItemHolder listItemHolder, String slug) {

        Intent viewIntent;
        int listingTypeId = 0;

        if (attachmentType.equals("user")) {
            if (mModuleName != null && !mModuleName.isEmpty() && mModuleName.equals("userProfile")) {
                if (mFeedSubjectId != attachmentId) {
                    viewIntent = new Intent(mContext, userProfile.class);
                    viewIntent.putExtra(ConstantVariables.USER_ID, attachmentId);
                    viewIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    ((Activity) mContext).startActivityForResult(viewIntent, ConstantVariables.USER_PROFILE_CODE);
                    ((Activity) mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            } else {
                viewIntent = new Intent(mContext, userProfile.class);
                viewIntent.putExtra(ConstantVariables.USER_ID, attachmentId);
                viewIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                ((Activity) mContext).startActivityForResult(viewIntent, ConstantVariables.USER_PROFILE_CODE);
                ((Activity) mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }

        } else if (attachmentType.equals("activity_action")) {
            FeedList feedList = (FeedList) mFeedItemList.get(listItemHolder.getAdapterPosition());
            Intent intent = new Intent(mContext, SingleFeedPage.class);
            intent.putExtra(ConstantVariables.ITEM_POSITION, listItemHolder.getAdapterPosition());
            intent.putExtra(ConstantVariables.ACTION_ID, attachmentId);
            if (feedList.getmPhotoAttachmentCount() > 1) {
                intent.putExtra(ConstantVariables.FEED_LIST, feedList);
            }
            if (mFeedsFragment != null) {
                mFeedsFragment.startActivityForResult(intent, ConstantVariables.VIEW_SINGLE_FEED_PAGE);
            } else {
                (mContext).startActivity(intent);
            }
            ((Activity) mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        } else if (mIncludedModulesList.contains(attachmentType)
                && !mDeletedModulesList.contains(attachmentType)) {

            /*
              Do not make Groups/Events name clickable if the feeds are loading on group profile page.
             */
            if (!((attachmentType.equals("group") || attachmentType.equals("event")) &&
                    mModuleName != null && !mModuleName.isEmpty() && mModuleName.equals("groupEventProfile")
                    && (mFeedSubjectId == attachmentId))) {

                if (attachmentType.equals("sitestoreproduct_review")) {
                    viewIntent = GlobalFunctions.getIntentForModule(mContext,
                            listItemHolder.mFeedObject.optInt("product_id"), attachmentType, slug);
                }else {
                    //senal
                    viewIntent = GlobalFunctions.getIntentForModule(mContext, attachmentId, attachmentType, slug);
            //        viewIntent.putExtra("mFeedItemActionIdActual",this.mFeedItemActionIdActual);
                }

                switch (attachmentType) {
                    case ConstantVariables.FORUM_TITLE:
                    case "form_topic":
                        // Add Slug in Forum Urls
                        viewIntent.putExtra(ConstantVariables.CONTENT_TITLE, title);
                        break;

                    case ConstantVariables.MLT_MENU_TITLE:
                        if (listItemHolder.mFeedObject.optInt("listingtype_id") != 0) {
                            listingTypeId = listItemHolder.mFeedObject.optInt("listingtype_id");
                        } else if (listItemHolder.mFeedAttachmentArray != null) {
                            listingTypeId = listItemHolder.mFeedAttachmentArray.optJSONObject(0).optInt("listingtype_id");
                        }
                        viewIntent.putExtra(ConstantVariables.LISTING_TYPE_ID, listingTypeId);
                        break;

                    case ConstantVariables.MLT_REVIEW_MENU_TITLE:
                        if (listItemHolder.mFeedObject.optInt("listingtype_id") != 0) {
                            listingTypeId = listItemHolder.mFeedObject.optInt("listingtype_id");
                        } else if (listItemHolder.mFeedAttachmentArray != null) {
                            listingTypeId = listItemHolder.mFeedAttachmentArray.optJSONObject(0).optInt("listingtype_id");
                        }
                        viewIntent.putExtra(ConstantVariables.LISTING_TYPE_ID, listingTypeId);
                        break;

                    case ConstantVariables.MLT_WISHLIST_MENU_TITLE:
                        if (listItemHolder.mFeedObject.has("title")) {
                            viewIntent.putExtra(ConstantVariables.CONTENT_TITLE,
                                    listItemHolder.mFeedObject.optString("title"));
                        } else if (listItemHolder.mFeedAttachmentArray != null) {
                            viewIntent.putExtra(ConstantVariables.CONTENT_TITLE,
                                    listItemHolder.mFeedAttachmentArray.optJSONObject(0).optString("title"));
                        }
                        break;
                }

                if (viewIntent != null) {
                    ((Activity) mContext).startActivityForResult(viewIntent, ConstantVariables.VIEW_PAGE_CODE);
                    ((Activity) mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            }
        } else if (attachmentType.equals("tagged_users_list")) {

            mAddPeopleList.clear();

            FeedList feedList = (FeedList) mFeedItemList.get(listItemHolder.getAdapterPosition());
            JSONArray jsonArray = feedList.getmUserTagArray();

            // Create listview for tagged user's
            ListView mUserListView = new ListView(mContext);
            mUserListView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));

            mAddPeopleAdapter = new AddPeopleAdapter(mContext, R.layout.list_friends, mAddPeopleList);
            mUserListView.setAdapter(mAddPeopleAdapter);
            mUserListView.setOnItemClickListener(this);

            // Create dialog for show listview inside it
            mDialog = new Dialog(mContext);
            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mDialog.setContentView(mUserListView);
            mDialog.setCancelable(true);
            mDialog.setCanceledOnTouchOutside(true);
            mDialog.show();

            // Get user's info and add into the list
            for (int i = 1; i < jsonArray.length(); i++) {
                JSONObject userObject = jsonArray.optJSONObject(i);
                JSONObject tagObject = userObject.optJSONObject("tag_obj");
                String username = tagObject.optString("displayname");
                int userId = tagObject.optInt("user_id");
                String image = tagObject.optString("image_icon");

                mAddPeopleList.add(new AddPeopleList(userId, username, image));

            }
            mAddPeopleAdapter.notifyDataSetChanged();

        } else if (ConstantVariables.WEBVIEW_ENABLE == 0) {
            CustomTabUtil.launchCustomTab((Activity) mContext, GlobalFunctions.getWebViewUrl(listItemHolder.mWebUrl, mContext));
        } else {
            Intent webViewActivity = new Intent(mContext, WebViewActivity.class);
            webViewActivity.putExtra("url", listItemHolder.mWebUrl);
            ((Activity) mContext).startActivityForResult(webViewActivity, ConstantVariables.WEB_VIEW_ACTIVITY_CODE);
            ((Activity) mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }

    }

    /**
     *  When Video Attachment in feeds will be clicked
     */
    public void videoAttachmentClicked(String feedType, String feedAttachmentType, String attachment_type, String video_type, String video_url, int attachment_id,
                                       JSONObject mFeedObject, JSONObject singleAttachmentObject){

        Intent mainIntent = null;
        List<String> mEnabledModuleList = null;
        if (PreferencesUtils.getEnabledModuleList(mContext) != null) {
            mEnabledModuleList = new ArrayList<String>(Arrays.asList(PreferencesUtils.getEnabledModuleList(mContext).split("\",\"")));
        }

        ///TODO, recheck and confirm with api team that if can replace the check with only "feedAttachmentType".
        // Checking if the video is present in enabled module list
        // and also checked for the activity_action videos (Videos which are shared)
        if (mEnabledModuleList != null
                && ((mEnabledModuleList.contains("video") || mEnabledModuleList.contains("sitevideo"))
                || ((feedAttachmentType.contains("video") || attachment_type.contains("video"))
                && attachment_type.equals("activity_action")))) {

            if (mEnabledModuleList.contains("sitevideo") && !mDeletedModulesList.contains("core_main_sitevideo")
                    && ((feedAttachmentType.equals("video")) || attachment_type.equals("video"))) {
                mainIntent = AdvVideoUtil.getViewPageIntent(mContext, attachment_id, video_url, new Bundle());
                mainIntent.putExtra(ConstantVariables.VIDEO_TYPE, Integer.parseInt(video_type));
                ((Activity) mContext).startActivityForResult(mainIntent, ConstantVariables.VIEW_PAGE_CODE);
                ((Activity) mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            } else if (mEnabledModuleList.contains("video") && !mDeletedModulesList.contains("video")
                    && (video_type.equals("1") || video_type.equals("2") || video_type.equals("3"))) {
                mainIntent = VideoUtil.getViewPageIntent(mContext, attachment_id, video_url, new Bundle());
                if (mainIntent != null) {
                    if (!feedAttachmentType.equals("video")) {
                        if (!mFeedObject.optString("name").equals("user") && !feedType.equals("share")) {
                            mainIntent = GlobalFunctions.setIntentParamForVideo(feedAttachmentType, mFeedObject,
                                    attachment_id, mainIntent);
                        } else if (singleAttachmentObject != null) {
                            mainIntent = GlobalFunctions.setIntentParamForVideo(feedAttachmentType,
                                    singleAttachmentObject, attachment_id, mainIntent);
                        }
                    }
                    mainIntent.putExtra(ConstantVariables.VIDEO_TYPE, Integer.parseInt(video_type));
                    ((Activity) mContext).startActivityForResult(mainIntent, ConstantVariables.VIEW_PAGE_CODE);
                    ((Activity) mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            } else {
                mainIntent = new Intent(mContext, WebViewActivity.class);
                if (feedType != null && feedType.equals("share") && singleAttachmentObject != null) {
                    String url = singleAttachmentObject.optString("uri");
                    if (url == null || url.isEmpty()) {
                        url = singleAttachmentObject.optString("content_url");
                    }
                    mainIntent.putExtra("url", url);
                }else {
                    mainIntent.putExtra("url", mFeedObject.optString("url"));
                }
                if (singleAttachmentObject != null) {
                    mainIntent.putExtra("headerText", singleAttachmentObject.optString("title"));
                }
                ((Activity)mContext).startActivityForResult(mainIntent, ConstantVariables.WEB_VIEW_ACTIVITY_CODE);
                ((Activity)mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        } else {
            mainIntent = new Intent(mContext, WebViewActivity.class);
            if (feedType != null && feedType.equals("share") && singleAttachmentObject != null) {
                String url = singleAttachmentObject.optString("uri");
                if (url == null || url.isEmpty()) {
                    url = singleAttachmentObject.optString("content_url");
                }
                mainIntent.putExtra("url", url);
            }else {
                mainIntent.putExtra("url", mFeedObject.optString("url"));
            }
            if (singleAttachmentObject != null) {
                mainIntent.putExtra("headerText", singleAttachmentObject.optString("title"));
            }
            ((Activity)mContext).startActivityForResult(mainIntent, ConstantVariables.WEB_VIEW_ACTIVITY_CODE);
            ((Activity)mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    /**
     * When Location text get clicked in checkIn feeds
     */

    public void openMapActivity(String locationLabel, double latitude, double longitude, String place_id){

        Intent viewIntent = new Intent(mContext, MapActivity.class);
        viewIntent.putExtra("location", locationLabel);
        viewIntent.putExtra("latitude", latitude);
        viewIntent.putExtra("longitude", longitude);
        viewIntent.putExtra("place_id", place_id);

        if(mFeedsFragment != null){
            mFeedsFragment.checkManifestPermissions(viewIntent, Manifest.permission.ACCESS_FINE_LOCATION,
                    ConstantVariables.ACCESS_FINE_LOCATION, false);
        }else{
            mContext.startActivity(viewIntent);
            ((Activity) mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    @Override
    public void clickedTag(CharSequence tag) {
        LogUtils.LOGD("FeedAdapter",tag.toString());
        Intent searchActivity = new Intent(mContext, SearchActivity.class);
        searchActivity.putExtra(ConstantVariables.HASTAG_SEARCH,true);
        searchActivity.putExtra(ConstantVariables.EXTRA_MODULE_TYPE,"home");
        searchActivity.putExtra(SearchManager.QUERY,tag.toString());
        mContext.startActivity(searchActivity);
    }

    @Override
    public void onItemDelete(int position) {

        Toast.makeText(mContext, mContext.getResources().getString(R.string.feed_delete_success),
                Toast.LENGTH_SHORT).show();
        if (mIsSingleFeed) {
            ((Activity) mContext).finish();
        } else {
            mFeedItemList.remove(position);
            notifyItemRemoved(position);
        }
    }

    @Override
    public void onItemActionSuccess(int position, Object itemList, String menuName) {

        switch (menuName) {

            case "hide":
            case "report_feed":
                mHiddenFeeds.add(position);
                break;

            case "disable_comment":
                // When single feed, so hide comment bar when disabled comment is performed.
                if (mIsSingleFeed && mFeedDisableCommentListener != null) {
                    FeedList feedList = (FeedList) itemList;
                    mFeedDisableCommentListener.onFeedDisableComment(feedList.ismCanComment() == 1);
                } else {
                    mFeedItemList.set(position, itemList);
                }
                break;

            case "edit_feed":
                if (!mEditFeeds.contains(position)) {
                    mEditFeeds.add(position);
                }
                break;
        }
        notifyDataSetChanged();

    }

    // Set The Selected Filters
    public void setmFilterSelectedListener(OnFilterSelectedListener mFilterSelectedListener) {
        this.mFilterSelectedListener = mFilterSelectedListener;
    }

    /**
     * Method to show showMore option when post is large.
     * @param postTextView text view containing long post.
     * @param postBodyText long which which needs to be truncated.
     * @param attachmentType if see more option is clicked for attachment.
     */
    public void seeMoreOption(TextView postTextView, String postBodyText,
                              final ListItemHolder listItemHolder, final String attachmentType) {

        try {
            String mTitleBody = postBodyText.trim().substring(0, 150);

            mTitleBody = mTitleBody.concat("..." + mContext.getResources().getString(R.string.readMore)).replaceAll("<img.+?>|<IMG.+?>", "");
            int index1 = mTitleBody.trim().length() -
                    mContext.getResources().getString(R.string.readMore).length();
            int index2 = mTitleBody.trim().length();

            postTextView.setMovementMethod(LinkMovementMethod.getInstance());
            postTextView.setText(mTitleBody, TextView.BufferType.SPANNABLE);
            Spannable mySpannable = (Spannable) postTextView.getText();

            ClickableSpan myClickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View view) {

                    FeedList feedList = (FeedList) mFeedItemList.get(listItemHolder.getAdapterPosition());
                    if (attachmentType != null) {
                        JSONArray attachmentArray = feedList.getmFeedAttachmentArray();
                        JSONObject singleAttachmentObject = attachmentArray.optJSONObject(0);
                        final int attachmentId = singleAttachmentObject.optInt("attachment_id");
                        final String attachmentTitle = singleAttachmentObject.optString("title");
                        final String attachmentVideoType = singleAttachmentObject.optString("attachment_video_type");
                        final String attachment_video_url = singleAttachmentObject.optString("attachment_video_url");
                        /*
                        Redirect to view pages when see more get clicked on body of attachments
                        */
                        switch (attachmentType) {
                            case ConstantVariables.VIDEO_TITLE:
                            case ConstantVariables.MLT_VIDEO_MENU_TITLE:
                            case ConstantVariables.ADV_GROUPS_VIDEO_MENU_TITLE:
                            case ConstantVariables.SITE_STORE_VIDEO_MENU_TITLE:
                            case ConstantVariables.PRODUCT_VIDEO_MENU_TITLE:
                            case ConstantVariables.ADV_EVENT_VIDEO_MENU_TITLE:
                                videoAttachmentClicked(feedList.getmFeedType(), "", attachmentType,
                                        attachmentVideoType, attachment_video_url, attachmentId,
                                        feedList.getmFeedObject(), attachmentArray.optJSONObject(0));
                                break;

                            case ConstantVariables.AAF_MENU_TITLE:
                                attachmentClicked(attachmentTitle, attachmentType, feedList.getmActionId(),
                                        listItemHolder, null);
                                break;

                            default:
                                attachmentClicked(attachmentTitle, attachmentType, attachmentId,
                                        listItemHolder, null);
                                break;
                        }

                    }else if (feedList.getmActionId() != 0) {
                        Intent intent = new Intent(mContext, SingleFeedPage.class);
                        intent.putExtra(ConstantVariables.ITEM_POSITION, listItemHolder.getAdapterPosition());
                        intent.putExtra(ConstantVariables.ACTION_ID, feedList.getmActionId());

                        // Showing all photos in single feed when seeMore option is clicked.
                        if (feedList.getmPhotoAttachmentCount() > 1) {
                            intent.putExtra(ConstantVariables.FEED_LIST, feedList);
                        }
                        // Put Reactions params if Reactions are enabled
                        intent.putExtra("reactionEnabled", mReactionsEnabled);
                        if(mReactions != null){
                            intent.putExtra("reactions", mReactions.toString());
                        }
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        if (mFeedsFragment != null) {
                            mFeedsFragment.startActivityForResult(intent, ConstantVariables.VIEW_SINGLE_FEED_PAGE);
                        } else {
                            (mContext).startActivity(intent);
                        }
                        ((Activity) mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                    ds.setColor(ContextCompat.getColor(mContext, R.color.body_text_3));
                }
            };
            mySpannable.setSpan(myClickableSpan, index1, index2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

        if (mDialog != null)
            mDialog.dismiss();

        AddPeopleList addPeopleList = mAddPeopleList.get(position);
        int userId = addPeopleList.getmUserId();

        Intent intent = new Intent(mContext, userProfile.class);
        intent.putExtra(ConstantVariables.USER_ID, userId);
        ((Activity) mContext).startActivityForResult(intent, ConstantVariables.USER_PROFILE_CODE);
        ((Activity) mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        if(mAddPeopleAdapter != null){
            mAddPeopleAdapter.clear();
        }
    }

    /**
     * Holder for Feed View
     */
    public static class ListItemHolder extends RecyclerView.ViewHolder {

        ActionIconThemedTextView mLikeButton,mCommentButton,mShareButton;
        ThemedTextView mLikeCount,mCommentCount;
        BezelImageView mFeedProfileImage;
        RelativeLayout mAttachmentPreviewBlock;
        ImageView mAttachmentImage, mSingleAttachmentImage, mMusicAndLinkAttachmentImage, mPlayIcon;
        EmojiconTextView mFeedTitleBody;
        TextView mFeedTitle, mAttachmentTitle, mEmojiButton, hashTagView, mMusicAttachmentTitle,
                mMusicAttachmentBody;
        SelectableTextView mAttachmentBody,mFeedPostedTime, mAttachmentUrlView;
        ActionIconButton mFeedMenusIcon;
        JSONArray mFeedAttachmentArray;
        JSONObject mFeedObject, mPopularReactionsArray;
        int mPhotoAttachmentCount;
        String mWebUrl, mFeedTitleBodyText,mFeedType, mFeedAttachmentType;
        LinearLayout mFeedFooterMenusBlock, mAttachmentView, mMusicAttachmentView, mPopularReactionsView;
        RelativeLayout mCounterView;
        RecyclerView mImagesGallery;
        EmojiconEditText mFeedTitleBodyEditText;
        RelativeLayout mFeedBodyEditLayout;
        BaseButton mFeedEditSubmit, mFeedEditCancel;
        View mCountSaperator;
        ImageView mReactionImage;
        MapView mMapView;
        Double mLatitude, mLongitude;
        String mLocationLabel, mPlaceId;

        public ListItemHolder(View itemView) {
            super(itemView);


            /* Feed Body Info Fields */
            mFeedProfileImage = (BezelImageView) itemView.findViewById(R.id.profile_image);
            mFeedTitle = (TextView) itemView.findViewById(R.id.feed_title);
            mFeedTitleBody = (EmojiconTextView) itemView.findViewById(R.id.feed_body);
            mFeedMenusIcon = (ActionIconButton) itemView.findViewById(R.id.feed_menu);
            mFeedPostedTime = (SelectableTextView) itemView.findViewById(R.id.feed_time);
            mCounterView = (RelativeLayout) itemView.findViewById(R.id.counts_container);
            mCountSaperator = itemView.findViewById(R.id.counts_saperator);
            mPopularReactionsView = (LinearLayout) itemView.findViewById(R.id.popularReactionIcons);
            mReactionImage = (ImageView) itemView.findViewById(R.id.reactionIcon);

            /*
            Feed Attachment Fields
             */
            // Music and link attachment fields.
            mMusicAndLinkAttachmentImage = (ImageView) itemView.findViewById(R.id.attachment_preview_link_music);
            mMusicAttachmentView = (LinearLayout) itemView.findViewById(R.id.attachment_view_link_music);
            mMusicAttachmentTitle = (NameView) itemView.findViewById(R.id.attachment_title_link_music);
            mMusicAttachmentBody = (TextView) itemView.findViewById(R.id.attachment_body_link_music);

            mAttachmentPreviewBlock = (RelativeLayout) itemView.findViewById(R.id.attachment_preview_layout);
            mAttachmentImage = (ImageView) itemView.findViewById(R.id.attachment_preview);
            mPlayIcon = (ImageView) itemView.findViewById(R.id.play_button);
            mAttachmentView = (LinearLayout) itemView.findViewById(R.id.attachment_view);
            mAttachmentTitle = (NameView) itemView.findViewById(R.id.attachment_title);
            mAttachmentBody = (SelectableTextView) itemView.findViewById(R.id.attachment_body);
            mAttachmentUrlView = (SelectableTextView) itemView.findViewById(R.id.attachment_url_view);
            mSingleAttachmentImage = (ImageView) itemView.findViewById(R.id.singleAlbumPhoto);
            mImagesGallery = (RecyclerView) itemView.findViewById(R.id.image_preview);
            hashTagView = (TextView) itemView.findViewById(R.id.hashTag_view);

            //Map view field
            mMapView = (MapView) itemView.findViewById(R.id.map_view);
            mMapView.onCreate(null);

            mImagesGallery.setHasFixedSize(true);
            mImagesGallery.setItemAnimator(new DefaultItemAnimator());
            mImagesGallery.addItemDecoration(new SpaceItemDecoration(5));

            mAttachmentBody.setMovementMethod(LinkMovementMethod.getInstance());
            hashTagView.setMovementMethod(LinkMovementMethod.getInstance());
            /*
            Like and Comment Fields
             */
            mLikeCount = (ThemedTextView) itemView.findViewById(R.id.like_count);
            mCommentCount = (ThemedTextView) itemView.findViewById(R.id.comment_count);

            mShareButton = (ActionIconThemedTextView) itemView.findViewById(R.id.share_button);
            mLikeButton = (ActionIconThemedTextView) itemView.findViewById(R.id.like_button);
            mCommentButton = (ActionIconThemedTextView) itemView.findViewById(R.id.comment_button);

            /*
            Feed Footer Menu Fields
             */
            mFeedFooterMenusBlock = (LinearLayout) itemView.findViewById(R.id.feedFooterMenusBlock);

            /*
                Edit Feed Layout Fileds
             */
            mFeedBodyEditLayout = (RelativeLayout) itemView.findViewById(R.id.feed_edit_layout);
            mFeedTitleBodyEditText = (EmojiconEditText) itemView.findViewById(R.id.feed_body_edit_text);
            mEmojiButton = (TextView) itemView.findViewById(R.id.emojiIcon);
            mFeedEditSubmit = (BaseButton) itemView.findViewById(R.id.edit_feed_submit);
            mFeedEditCancel = (BaseButton) itemView.findViewById(R.id.edit_feed_cancel);

        }
    }

    /**
     * Holder for Hidden Feeds
     */

    public static class HiddenItemHolder extends RecyclerView.ViewHolder{
        TextView mHiddenFeedOptions;
        SelectableTextView mHiddenFeedBody;
        public HiddenItemHolder(View itemView) {
            super(itemView);
            mHiddenFeedBody = (SelectableTextView) itemView.findViewById(R.id.hiddenFeedBody);
            mHiddenFeedOptions = (TextView) itemView.findViewById(R.id.hiddenFeedOptions);
        }
    }

    /**
     * Holder for Header with Status Menu and Filters
     */

    public static class HeaderViewHolder extends RecyclerView.ViewHolder{
        public ImageView mUserProfileImage;
        public TextView mCheckInMenuIcon, mCheckInMenuText, mStatusUpdateText, mPhotoMenuIcon,
                mPhotoMenuText, mStatusMenuIcon, mStatusMenuText;
        public SelectableTextView mNoFeedMessage;
        public LinearLayout mPhotoMenu, mStatusMenu, mCheckInMenu;
        Spinner mFiltersSpinner;
        LinearLayout mPostFeedOptions, mStatusTextLayout;

        public HeaderViewHolder(View v) {
            super(v);
            mUserProfileImage = (ImageView)v.findViewById(R.id.userProfileImage);
            mStatusMenu = (LinearLayout) v.findViewById(R.id.statusMenu);
            mStatusMenuIcon = (TextView) v.findViewById(R.id.statusMenuIcon);
            mStatusMenuText = (TextView) v.findViewById(R.id.statusMenuText);
            mPhotoMenuIcon = (TextView) v.findViewById(R.id.photoMenuIcon);
            mPhotoMenuText = (TextView) v.findViewById(R.id.photoMenuText);
            mPhotoMenu = (LinearLayout) v.findViewById(R.id.photoMenu);
            mCheckInMenu = (LinearLayout) v.findViewById(R.id.checkInMenu);
            mCheckInMenuIcon = (TextView) v.findViewById(R.id.checkInMenuIcon);
            mCheckInMenuText = (TextView) v.findViewById(R.id.checkInMenuText);
            mFiltersSpinner = (Spinner) v.findViewById(R.id.filter_view);
            mPostFeedOptions = (LinearLayout) v.findViewById(R.id.postFeedLayout);
            mStatusTextLayout = (LinearLayout) v.findViewById(R.id.status_text_layout);
            mStatusUpdateText = (TextView) v.findViewById(R.id.status_update_text);
            mNoFeedMessage = (SelectableTextView) v.findViewById(R.id.noFeedMessage);

        }
    }

    public static class PeopleSuggestionViewHolder extends RecyclerView.ViewHolder {

        public RecyclerView mSuggestionRecyclerView;
        public TextView tvSeeAll;
        public ProgressBar mProgressBar;
        public PeopleSuggestionViewHolder(View view) {
            super(view);
            mSuggestionRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_list);
            tvSeeAll = (TextView) view.findViewById(R.id.tv_see_all);
            mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        }
    }

    public static class SpaceItemDecoration extends RecyclerView.ItemDecoration {

        private final int mVerticalSpaceHeight;

        public SpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
            outRect.right = mVerticalSpaceHeight;
        }
    }


    private void openPhotoLightBox(int feedPosition, String album_url, int photoCount, String mFeedAttachmentType, int albumId){

        mSelectedFeedList = (FeedList) mFeedItemList.get(feedPosition);
        Bundle bundle = new Bundle();
        ArrayList<PhotoListDetails> mFeedPhotoDetails = mSelectedFeedList.getmPhotoDetails();
        bundle.putSerializable(PhotoLightBoxActivity.EXTRA_IMAGE_URL_LIST, mFeedPhotoDetails);
        Intent i = new Intent(mContext, PhotoLightBoxActivity.class);
        i.putExtra(ConstantVariables.ITEM_POSITION, feedPosition);
        i.putExtra(ConstantVariables.SUBJECT_TYPE, mFeedAttachmentType);
        i.putExtra(ConstantVariables.TOTAL_ITEM_COUNT, photoCount);
        i.putExtra(ConstantVariables.SHOW_ALBUM_BUTTON,true);
        i.putExtra(ConstantVariables.PHOTO_REQUEST_URL, album_url);
        i.putExtra(ConstantVariables.ALBUM_ID, albumId);
        i.putExtras(bundle);
        if (mFeedsFragment != null) {
            mFeedsFragment.startActivityForResult(i, ConstantVariables.VIEW_LIGHT_BOX);
        } else {
            ((Activity) mContext).startActivityForResult(i, ConstantVariables.VIEW_LIGHT_BOX);
        }
    }

    private void setLikeCount(int isLike, int likeCount, ListItemHolder listItemHolder){

        if(mReactionsEnabled ==  1){
            // You and count others
            if(isLike == 1){
                if(likeCount == 1){
                    listItemHolder.mLikeCount.setText(mContext.getResources().
                            getString(R.string.reaction_string));
                } else {
                    String likeText = mContext. getResources().getQuantityString(R.plurals.others,
                            likeCount - 1, likeCount - 1);
                    listItemHolder.mLikeCount.setText(String.format(mContext.getResources().
                                    getString(R.string.reaction_text_format),
                            mContext.getResources().getString(R.string.you_and_text), likeText
                    ));
                }
            } else {
                // Only count
                listItemHolder.mLikeCount.setText(Integer.toString(mFeedItem.getmLikeCount()));
            }
        } else{
            String likeText = mContext.getResources().getQuantityString(R.plurals.profile_page_like,
                    likeCount);
            listItemHolder.mLikeCount.setText(String.format(
                    mContext.getResources().getString(R.string.like_count_text),
                    likeCount, likeText));
            listItemHolder.mCountSaperator.setVisibility(View.GONE);
            listItemHolder.mPopularReactionsView.setVisibility(View.GONE);
        }
    }

    private void instantLike(ListItemHolder listItemHolder, String likeButtonText, int reactionId,
                             String reactionIcon, boolean isReactionChanged){
        FeedList feedInfoList = (FeedList) mFeedItemList.get(itemPosition);

        if (feedInfoList.getmIsLike() == 0) {

            /**
             * Increase Like count and set Visibility visible to show Like count
             */

            // Playing likeSound effect when user liked a post.
            if (PreferencesUtils.isSoundEffectEnabled(mContext)) {
                SoundUtil.playSoundEffectOnLike(mContext);
            }

            feedInfoList.setmLikeCount(feedInfoList.getmLikeCount() + 1);

            setLikeCount(1, feedInfoList.getmLikeCount(), listItemHolder);

            listItemHolder.mCounterView.setVisibility(View.VISIBLE);
            listItemHolder.mLikeCount.setVisibility(View.VISIBLE);

            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) listItemHolder.
                    mCommentCount.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
            listItemHolder.mCommentCount.setLayoutParams(layoutParams);

            listItemHolder.mLikeButton.setActivated(true);
            listItemHolder.mLikeButton.setTextColor(
                    ContextCompat.getColor(mContext, R.color.colorPrimary));
            listItemHolder.mLikeButton.setText(likeButtonText);

            if(mReactionsEnabled == 1){
                JSONObject feedReactions =  feedInfoList.getmFeedReactions();
                setFeedReactions(likeButtonText, reactionIcon, reactionId, feedReactions, feedInfoList);
            } else{
                listItemHolder.mCountSaperator.setVisibility(View.VISIBLE);
                listItemHolder.mPopularReactionsView.setVisibility(View.VISIBLE);
            }


        } else {
            /**
             * Decrease like count, hide Like count option visibility if there was only
             * one like, also check if comment count is zero, then hide mFeedLikeCommentInfo
             * view.
             * Else decrease the like count and set in textview.
             */
            if(!isReactionChanged){
                if (feedInfoList.getmLikeCount() == 1) {
                    feedInfoList.setmLikeCount(feedInfoList.getmLikeCount() - 1);
                    listItemHolder.mLikeCount.setVisibility(View.GONE);
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) listItemHolder.
                            mCommentCount.getLayoutParams();
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_END, 0);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
                    listItemHolder.mCommentCount.setLayoutParams(layoutParams);
                    if (feedInfoList.getmCommentCount() == 0)
                        listItemHolder.mCounterView.setVisibility(View.INVISIBLE);
                } else {
                    feedInfoList.setmLikeCount(feedInfoList.getmLikeCount() - 1);
                    setLikeCount(0, feedInfoList.getmLikeCount(), listItemHolder);
                    listItemHolder.mLikeCount.setVisibility(View.VISIBLE);
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) listItemHolder.
                            mCommentCount.getLayoutParams();
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
                    listItemHolder.mCommentCount.setLayoutParams(layoutParams);
                }
                listItemHolder.mLikeButton.setActivated(false);
                listItemHolder.mLikeButton.setTextColor(
                        ContextCompat.getColor(mContext, R.color.grey_dark));
            }

            /**
             * Check the reaction count of previous reaction
             * If the reaction count is 1 then remove that object from feedReactions
             * Else Decrease the reaction count and update feedReactions.
             * If the new reaction does not exit in FeedReactions object then put it in the object
             * with Reaction count 1 and update myFeedReactions
             */
            if(mReactionsEnabled == 1) {
                try {
                    JSONObject feedReactions = feedInfoList.getmFeedReactions();
                    JSONObject myFeedReactions = feedInfoList.getmMyFeedReactions();

                    if(myFeedReactions != null && feedReactions != null){
                        int myReactionId = myFeedReactions.optInt("reactionicon_id");
                        if(feedReactions.optJSONObject(String.valueOf(myReactionId)) != null){
                            int myReactionCount = feedReactions.optJSONObject(String.valueOf(myReactionId)).
                                    optInt("reaction_count");
                            if((myReactionCount - 1) <= 0){
                                feedReactions.remove(String.valueOf(myReactionId));
                            } else {
                                feedReactions.optJSONObject(String.valueOf(myReactionId)).put("reaction_count",
                                        myReactionCount-1);
                            }
                            feedInfoList.setmFeedReactions(feedReactions);
                        }
                    }

                    if (isReactionChanged) {
                        setFeedReactions(likeButtonText, reactionIcon, reactionId, feedReactions, feedInfoList);
                    } else {
                        feedInfoList.setmMyFeedReactions(null);
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            listItemHolder.mLikeButton.setText(likeButtonText);

        }
    }

    private void setFeedReactions(String likeButtonText, String reactionIcon, int reactionId, JSONObject feedReactions,
                                  FeedList feedInfoList){
        try{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("caption", likeButtonText);
            jsonObject.put("reaction_image_icon", reactionIcon);
            jsonObject.put("reactionicon_id", reactionId);

            feedInfoList.setmMyFeedReactions(jsonObject);

            if (feedReactions != null) {
                if (feedReactions.optJSONObject(String.valueOf(reactionId)) != null) {
                    int reactionCount = feedReactions.optJSONObject(String.valueOf(reactionId)).optInt("reaction_count");
                    feedReactions.optJSONObject(String.valueOf(reactionId)).putOpt("reaction_count", reactionCount + 1);
                } else {
                    jsonObject.put("reaction_count", 1);
                    feedReactions.put(String.valueOf(reactionId), jsonObject);
                }
            } else {
                feedReactions = new JSONObject();
                jsonObject.put("reaction_count", 1);
                feedReactions.put(String.valueOf(reactionId), jsonObject);
            }
            feedInfoList.setmFeedReactions(feedReactions);
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

}
