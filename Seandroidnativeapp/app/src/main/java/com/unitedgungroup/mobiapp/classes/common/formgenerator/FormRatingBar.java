/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 *
 */

package com.unitedgungroup.mobiapp.classes.common.formgenerator;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.widget.RatingBar;
import android.widget.TextView;

import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.utils.LogUtils;


public class FormRatingBar extends FormWidget implements RatingBar.OnRatingBarChangeListener {

    protected RatingBar _label;
    Context mContext;
    TextView _textView;
    String textLabel;

    public FormRatingBar(Context context, String property, String label,  boolean hasValidator) {
        super( context, property, hasValidator );

        mContext  = context;
        textLabel = label;

        _label = new RatingBar(context, null, android.R.attr.ratingBarStyle);
        _label.setLayoutParams(CustomViews.getWrapLayoutParams());
        _label.setScaleX(0.7f);
        _label.setScaleY(0.7f);
        _label.setStepSize(1);

        LayerDrawable stars = (LayerDrawable) _label.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(ContextCompat.getColor(mContext, R.color.dark_yellow), PorterDuff.Mode.SRC_ATOP);

        _label.setOnRatingBarChangeListener(this);

        _textView = new TextView(mContext);
        _textView.setLayoutParams(FormActivity.defaultLayoutParams);
        _textView.setText(textLabel);
        _textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimension(R.dimen.body_medium_font_size));

        _layout.addView(_textView);
        _layout.addView(_label);

    }

    @Override
    public String getValue() {
        return String.valueOf(_label.getRating());
    }

    @Override
    public void setValue(String value) {
        try {
            _label.setRating(Float.valueOf(value));
        } catch (Exception e) {
           e.printStackTrace();
        }
    }

    @Override
    public void setToggleHandler( FormActivity.FormWidgetToggleHandler handler ) {
        super.setToggleHandler(handler);
        _label.setOnRatingBarChangeListener(new RatingHandler(this));
    }



    @Override
    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

    }

    class RatingHandler implements RatingBar.OnRatingBarChangeListener {
        protected FormWidget _widget;

        public RatingHandler( FormWidget widget ){
            _widget = widget;
        }

        @Override
        public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

            if( _handler != null ){

                _handler.toggle( _widget );
            }

        }
    }

}
