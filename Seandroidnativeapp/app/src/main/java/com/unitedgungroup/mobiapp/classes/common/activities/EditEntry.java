/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 *
 */

package com.unitedgungroup.mobiapp.classes.common.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.adapters.GridViewAdapter;
import com.unitedgungroup.mobiapp.classes.common.dialogs.AlertDialogWithAction;
import com.unitedgungroup.mobiapp.classes.common.formgenerator.FormActivity;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnUploadResponseListener;
import com.unitedgungroup.mobiapp.classes.common.multiimageselector.MultiImageSelectorActivity;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.utils.BitmapUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.GlobalFunctions;
import com.unitedgungroup.mobiapp.classes.common.utils.ImageViewList;
import com.unitedgungroup.mobiapp.classes.common.utils.PreferencesUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.SnackbarUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.SoundUtil;
import com.unitedgungroup.mobiapp.classes.common.utils.UploadFileToServerUtils;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;
import com.unitedgungroup.mobiapp.classes.core.ConstantVariables;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnResponseListener;
import com.unitedgungroup.mobiapp.classes.modules.editor.NewEditorActivity;
import com.unitedgungroup.mobiapp.classes.modules.forum.ForumUtil;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EditEntry extends FormActivity implements OnUploadResponseListener {

    private String editFormUrl,currentSelectedOption, mFormType, mToolBarTitle = "", mSuccessMessage;
    static EditEntry editEntry;
    private AppConstant mAppConst;
    private HashMap<String, String> postParams;
    private static ArrayList<String> mSelectPath,mSelectedMusicFiles;
    private GridView mResultView;
    private TextView mSelectedFileCount;
    private GridViewAdapter mGridViewAdapter;
    private List<ImageViewList> mPhotoUrls;
    private int columnWidth, editForumPosition, mSelectMode, mRequestCode;
    private Intent intent;
    private Toolbar mToolbar;
    private RelativeLayout editFormView;
    private String mBodyString=null;
    private Context mFormActivityContext;
    private boolean isRequestCompleted = false;
    private boolean mShowCamera, editForm = false;
    private String mUploadingOption, property, mSelectedFilePath;
    private HashMap<String, ArrayList> mHashMap;
    private AlertDialogWithAction mAlertDialogWithAction;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_creation_view);


        /* Create Back Button On Action Bar **/
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mSelectPath = new ArrayList<>();
        mSelectedMusicFiles = new ArrayList<>();
        mHashMap = new HashMap<>();

        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        editFormView = (RelativeLayout)findViewById(R.id.form_view);
        mFormType = getIntent().getStringExtra(ConstantVariables.FORM_TYPE);
        mRequestCode = getIntent().getIntExtra(ConstantVariables.REQUEST_CODE, ConstantVariables.PAGE_EDIT_CODE);

        //Fetch Current Selected Module
        currentSelectedOption = getIntent().getStringExtra(ConstantVariables.EXTRA_MODULE_TYPE);
        if (currentSelectedOption == null || currentSelectedOption.isEmpty()) {
            currentSelectedOption = PreferencesUtils.getCurrentSelectedModule(this);
        }

        mAppConst = new AppConstant(this);
        editEntry = this;
        mAlertDialogWithAction = new AlertDialogWithAction(EditEntry.this);
        editFormUrl  = getIntent().getStringExtra(ConstantVariables.URL_STRING);

        switch (currentSelectedOption) {
            case ConstantVariables.FORUM_MENU_TITLE:
            case ConstantVariables.MLT_MENU_TITLE:
            case ConstantVariables.MLT_WISHLIST_MENU_TITLE:
            case ConstantVariables.ADV_GROUPS_MENU_TITLE:
            case ConstantVariables.SITE_PAGE_MENU_TITLE:
            case ConstantVariables.ADVANCED_EVENT_MENU_TITLE:
            case ConstantVariables.PRODUCT_MENU_TITLE:
            case ConstantVariables.STORE_MENU_TITLE:
            case ConstantVariables.DIARY_MENU_TITLE:

                if (mFormType != null && !mFormType.isEmpty()) {
                    switch (mFormType) {
                        case "rename_topic":
                            mToolBarTitle = getResources().getString(R.string.title_rename_topic);
                            mSuccessMessage = getResources().getString(R.string.successful_edit);
                            break;
                        case "edit_post":
                            mToolBarTitle = getResources().getString(R.string.title_edit_post);
                            mSuccessMessage = getResources().getString(R.string.successful_edit);
                            editForumPosition = getIntent().getIntExtra(ConstantVariables.ITEM_POSITION, 0);
                            break;
                        case "update_review":
                            mToolBarTitle = getResources().getString(R.string.update_review_title);
                            mSuccessMessage = getResources().getString(R.string.review_update_success_message);
                            break;

                        case "apply_now":
                            mToolBarTitle = getIntent().getStringExtra(ConstantVariables.CONTENT_TITLE);
                            mSuccessMessage = getResources().getString(R.string.apply_success_message);
                            break;
                        case "claim_listing":
                            mToolBarTitle = getIntent().getStringExtra(ConstantVariables.CONTENT_TITLE);
                            mSuccessMessage = getResources().getString(R.string.claim_listing_success_message);
                            break;
                        case "tellafriend":
                            mSuccessMessage = getResources().getString(R.string.tell_friend_success_message);
                            mToolBarTitle = getIntent().getStringExtra(ConstantVariables.CONTENT_TITLE);
                            break;
                        case "edit_wishlist":
                        case "edit_listing":
                            mToolBarTitle = getIntent().getStringExtra(ConstantVariables.CONTENT_TITLE);
                            break;
                        case "notification_settings":
                            mToolBarTitle = getResources().getString(R.string.title_notification_and_email_settings);
                            mSuccessMessage = getResources().getString(R.string.notification_setting_success_message);
                            break;
                        case "edit_diary":
                            mToolBarTitle = getIntent().getStringExtra(ConstantVariables.CONTENT_TITLE);
                            break;
                        case "edit_event":
                            mToolBarTitle = getResources().getString(R.string.edit_event_title);
                            break;
                        case "buyer_form":
                            mToolBarTitle = getResources().getString(R.string.action_bar_title_buyer_info);
                            break;
                    }
                } else {
                    mToolBarTitle = getResources().getString(R.string.edit);
                }
                break;

            default:
                mToolBarTitle = getResources().getString(R.string.edit);
                break;
        }

        getSupportActionBar().setTitle(mToolBarTitle);
        CustomViews.createMarqueeTitle(this, mToolbar);

        /*
        Code to Send Request for Edit Form
         */
        mAppConst.getJsonResponseFromUrl(editFormUrl, new OnResponseListener() {
            @Override
            public void onTaskCompleted(JSONObject jsonObject) {
                findViewById(R.id.progressBar).setVisibility(View.GONE);
                switch (currentSelectedOption) {

                    case ConstantVariables.FORUM_MENU_TITLE:
                    case ConstantVariables.BLOG_MENU_TITLE:
                    case ConstantVariables.CLASSIFIED_MENU_TITLE:
                        mBodyString = jsonObject.optJSONObject("formValues").optString("body");
                        editFormView.addView(populate(jsonObject, currentSelectedOption));
                        editFormView.findViewById(R.id.add_description).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                openEditor();
                            }
                        });
                        break;

                    case ConstantVariables.MLT_MENU_TITLE:
                    case ConstantVariables.PRODUCT_MENU_TITLE:
                    case ConstantVariables.STORE_MENU_TITLE:
                    case ConstantVariables.SITE_PAGE_MENU_TITLE:
                    case ConstantVariables.ADV_GROUPS_MENU_TITLE:
                    case ConstantVariables.ADVANCED_EVENT_MENU_TITLE:
                        if (mFormType !=  null && !mFormType.isEmpty() && (mFormType.equals("update_review")
                                || mFormType.equals("notification_settings") || mFormType.equals("buyer_form"))) {
                            editFormView.addView(populate(jsonObject, mFormType));

                        } else {
                            editFormView.addView(populate(jsonObject, currentSelectedOption));
                        }
                        break;

                    default:
                        editFormView.addView(populate(jsonObject, currentSelectedOption));
                        break;
                }
            }

            @Override
            public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                findViewById(R.id.progressBar).setVisibility(View.GONE);
                SnackbarUtils.displaySnackbarLongWithListener(editFormView, message,
                        new SnackbarUtils.OnSnackbarDismissListener() {
                            @Override
                            public void onSnackbarDismissed() {
                                finish();
                            }
                        });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_with_action_icon, menu);
        menu.findItem(R.id.submit).setTitle(getResources().getString(R.string.save_menu_title));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.submit:
                editEntry();
                break;
            case android.R.id.home:
                onBackPressed();
                // Playing backSound effect when user tapped on back button from tool bar.
                if (PreferencesUtils.isSoundEffectEnabled(EditEntry.this)) {
                    SoundUtil.playSoundEffectOnBackPressed(EditEntry.this);
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void editEntry() {
        mAppConst.hideKeyboard();
        postParams = new HashMap<>();
        postParams = save();

        if (postParams != null && !isRequestCompleted) {
            // uploading the file to server
            new UploadFileToServerUtils(EditEntry.this, editFormUrl, currentSelectedOption,
                    (mFormType != null && mFormType.equals("edit_event")), mBodyString,
                    mSelectedFilePath, mSelectPath, mSelectedMusicFiles, postParams, mHashMap).execute();
        }
    }

    public void openEditor(){

        postParams = save();

        if(postParams != null){

            Intent intent = new Intent(EditEntry.this, NewEditorActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(NewEditorActivity.POST_URL, editFormUrl);
            bundle.putStringArrayList(NewEditorActivity.SELECTED_PATHS, mSelectPath);
            bundle.putString(NewEditorActivity.TITLE_PARAM, "");
            bundle.putString(NewEditorActivity.CONTENT_PARAM, mBodyString);
            bundle.putSerializable(NewEditorActivity.POST_PARAM, postParams);
            bundle.putString(NewEditorActivity.TITLE_PLACEHOLDER_PARAM,
                    getString(R.string.example_post_title_placeholder));
            bundle.putString(NewEditorActivity.CONTENT_PLACEHOLDER_PARAM,
                    getString(R.string.post_content_placeholder) + "…");
            bundle.putInt(NewEditorActivity.EDITOR_PARAM, NewEditorActivity.USE_NEW_EDITOR);
            bundle.putString(ConstantVariables.EXTRA_MODULE_TYPE, currentSelectedOption);
            bundle.putInt(NewEditorActivity.PAGE_DETAIL, NewEditorActivity.EDIT_PAGE);
            bundle.putString("forumType", mFormType);
            intent.putExtras(bundle);
            startActivityForResult(intent, ConstantVariables.EDITOR_REQUEST_CODE);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }

    }

    public void checkPermission(Context context,int selectedMode,boolean showCamera, String uploadingOption, String name){

        mFormActivityContext = context;
        mSelectMode = selectedMode;
        mShowCamera = showCamera;
        mUploadingOption = uploadingOption;
        property = name;

        /* Check if permission is granted or not */
        if(!mAppConst.checkManifestPermission(Manifest.permission.READ_EXTERNAL_STORAGE)){
            mAppConst.requestForManifestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                    ConstantVariables.READ_EXTERNAL_STORAGE);
        }else{
            switch(mUploadingOption){
                case "photo":
                    startImageUploadActivity();
                    break;
                case "music":
                    startMusicUploading();
                    break;
                case "file":
                    GlobalFunctions.openDocumentUploadingIntent(EditEntry.this);
                    break;
            }
        }
    }

    public void startImageUploadActivity() {

        intent = new Intent((mFormActivityContext),MultiImageSelectorActivity.class);
        // Whether photoShoot
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA, mShowCamera);
        // The maximum number of selectable image
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_COUNT, ConstantVariables.FILE_UPLOAD_LIMIT);
        // Select mode
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE, mSelectMode);
        // The default selection
        if(mSelectPath != null && mSelectPath.size() > 0){
            intent.putExtra(MultiImageSelectorActivity.EXTRA_DEFAULT_SELECTED_LIST, mSelectPath);
        }
        ((Activity)mFormActivityContext).startActivityForResult(intent, ConstantVariables.REQUEST_IMAGE);
    }

    //Method for uploading music files
    public void startMusicUploading(){
        intent = new Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        ((Activity) mFormActivityContext).startActivityForResult(intent, ConstantVariables.REQUEST_MUSIC);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case ConstantVariables.REQUEST_IMAGE :
                if (resultCode == RESULT_OK) {
                    mSelectPath = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);

                    if (property != null && property.equals("host_photo")) {
                        mResultView = (GridView)findViewById(R.id.host_image);
                        mHashMap.put("host_photo", mSelectPath);
                    } else {
                        mResultView = (GridView)findViewById(R.id.result);
                        mHashMap.put("photo", mSelectPath);
                    }

                    InitializeGridLayout();
                    mPhotoUrls = new ArrayList<>();
                    mGridViewAdapter = new GridViewAdapter(this, columnWidth, mPhotoUrls,false,true);
                    mResultView.setAdapter(mGridViewAdapter);
                    for(int i=0;i<mSelectPath.size();i++) {
                        mPhotoUrls.add(new ImageViewList(BitmapUtils.decodeSampledBitmapFromFile(EditEntry.this,
                                mSelectPath.get(i), AppConstant.getDisplayMetricsWidth(EditEntry.this),
                                (int) getResources().getDimension(R.dimen.feed_attachment_image_height), false)));
                        mGridViewAdapter.notifyDataSetChanged();
                    }
                }
                break;
            case ConstantVariables.REQUEST_MUSIC:

                if(resultCode == RESULT_OK && data != null){

                    Uri selectedMusicUri = data.getData();
                    if (selectedMusicUri != null) {
                        if(mSelectedMusicFiles.size() < 5) {
                            mSelectedMusicFiles.add(GlobalFunctions.getMusicFilePathFromURI(this, selectedMusicUri));
                            mSelectedFileCount = (TextView) findViewById(R.id.pathTextView);
                            mSelectedFileCount.setVisibility(View.VISIBLE);
                            //Getting file name
                            mSelectedFileCount.setText(getResources().
                                    getQuantityString(R.plurals.music_uploading_message_custom,
                                            mSelectedMusicFiles.size(), mSelectedMusicFiles.size()));
                        }else {
                            SnackbarUtils.displaySnackbar(editFormView,
                                    getResources().getString(R.string.music_upload_limit_msg));
                        }
                    }

                }else if (resultCode != RESULT_CANCELED) {
                    // failed to capture image
                    SnackbarUtils.displaySnackbar(editFormView,
                            getResources().getString(R.string.music_capturing_failed));

                }
                break;

            case ConstantVariables.INPUT_FILE_REQUEST_CODE:
                if(resultCode == RESULT_OK && data != null){
                    Uri selectedFileUri = data.getData();
                    if (selectedFileUri != null) {
                        mSelectedFilePath = GlobalFunctions.getFileRealPathFromUri(EditEntry.this, selectedFileUri);
                        mSelectedFileCount=(TextView)findViewById(R.id.pathTextView);
                        mSelectedFileCount.setVisibility(View.VISIBLE);
                        mSelectedFileCount.setText(getResources().getString(R.string.file_uploading_message));
                    }

                } else if (resultCode != RESULT_CANCELED) {
                    SnackbarUtils.displaySnackbar(editFormView,getResources().getString(R.string.file_uploading_failed));

                }
                break;

            case ConstantVariables.EDITOR_REQUEST_CODE:
                if (resultCode == ConstantVariables.EDIT_ENTRY_RETURN_CODE)
                {
                    mBodyString = data.getStringExtra(ConstantVariables.EXTRA_CREATE_RESPONSE);
                }

        }

    }

    private void InitializeGridLayout() {
        Resources r = getResources();
        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                AppConstant.GRID_PADDING, r.getDisplayMetrics());

        // Column width
        columnWidth = (int) ((mAppConst.getScreenWidth() - ((10 + 1) * padding)) /
                10);

        // Setting number of grid columns
        mResultView.setNumColumns(AppConstant.NUM_OF_COLUMNS_FOR_VIEW_PAGE);
        mResultView.setColumnWidth(columnWidth);
        mResultView.setStretchMode(GridView.NO_STRETCH);
        mResultView.setPadding((int) padding, (int) padding, (int) padding,
                (int) padding);

        // Setting horizontal and vertical padding
        mResultView.setHorizontalSpacing((int) padding);
        mResultView.setVerticalSpacing((int) padding);
    }

    @Override
    public void onUploadResponse(final JSONObject jsonObject, boolean isRequestSuccessful) {

        if (isRequestSuccessful) {
            isRequestCompleted = true;
            switch (currentSelectedOption){

                case ConstantVariables.FORUM_MENU_TITLE:
                    SnackbarUtils.displaySnackbarShortWithListener(editFormView,
                            getResources().getString(R.string.successful_edit),
                            new SnackbarUtils.OnSnackbarDismissListener() {
                                @Override
                                public void onSnackbarDismissed() {
                                    ForumUtil.increaseViewTopicPageCounter();
                                    if (mFormType.equals("rename_topic")) {
                                        ForumUtil.increaseProfilePageCounter();
                                        startActivity(ForumUtil.getViewTopicPageIntent(EditEntry.this,
                                                postParams.get("title"),
                                                getIntent().getStringExtra(ConstantVariables.VIEW_PAGE_URL)));
                                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                        finish();
                                        //When successfully updated name of topic then finish parent activity(i.e. ForumView)
                                        ForumUtil.finishViewTopicActivity();
                                    } else {
                                        ForumUtil.editForumPosition(editForumPosition);
                                        finish();
                                    }
                                }
                            });
                    break;

                case ConstantVariables.ADVANCED_EVENT_MENU_TITLE:
                case ConstantVariables.SITE_PAGE_MENU_TITLE:
                case ConstantVariables.ADV_GROUPS_MENU_TITLE:
                case ConstantVariables.MLT_MENU_TITLE:
                case ConstantVariables.MLT_WISHLIST_MENU_TITLE:
                case ConstantVariables.PRODUCT_MENU_TITLE:
                case ConstantVariables.STORE_MENU_TITLE:
                    if (mFormType == null || mFormType.isEmpty() || mSuccessMessage == null) {
                        mSuccessMessage = getResources().getString(R.string.successful_edit);
                    }
                    SnackbarUtils.displaySnackbarShortWithListener(editFormView,
                            mSuccessMessage, new SnackbarUtils.OnSnackbarDismissListener() {
                                @Override
                                public void onSnackbarDismissed() {
                                    Bundle bundle = new Bundle();
                                    bundle.putString(ConstantVariables.EXTRA_MODULE_TYPE, currentSelectedOption);
                                    Intent intent = new Intent();
                                    intent.putExtras(bundle);
                                    setResult(ConstantVariables.PAGE_EDIT_CODE, intent);
                                    finish();
                                }
                            });
                    break;

                default:
                    SnackbarUtils.displaySnackbarShortWithListener(editFormView,
                            getResources().getString(R.string.successful_edit),
                            new SnackbarUtils.OnSnackbarDismissListener() {
                                @Override
                                public void onSnackbarDismissed() {
                                    Bundle bundle = new Bundle();
                                    bundle.putString(ConstantVariables.EXTRA_MODULE_TYPE, currentSelectedOption);
                                    Intent intent = new Intent();
                                    intent.putExtras(bundle);
                                    setResult(mRequestCode, intent);
                                    finish();
                                }
                            });
                    break;
            }

        } else if (jsonObject.has("showValidation")) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject validationMessages = jsonObject.optJSONObject("message");
                    if (validationMessages != null) {
                        showValidations(validationMessages);
                    } else {
                        SnackbarUtils.displaySnackbar(editFormView, jsonObject.optString("message"));
                    }
                }
            });

        } else {
            isRequestCompleted = true;
            SnackbarUtils.displaySnackbarLongWithListener(editFormView, jsonObject.optString("message"),
                    new SnackbarUtils.OnSnackbarDismissListener() {
                        @Override
                        public void onSnackbarDismissed() {
                            finish();
                        }
                    });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mAppConst.hideKeyboard();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public static EditEntry getInstance(){
        return editEntry;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case ConstantVariables.READ_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, proceed to the normal flow
                    switch(mUploadingOption){
                        case "photo":
                            startImageUploadActivity();
                            break;
                        case "music":
                            startMusicUploading();
                            break;
                        case "file":
                            GlobalFunctions.openDocumentUploadingIntent(EditEntry.this);
                            break;
                    }
                } else {
                    // If user deny the permission popup
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {

                        // Show an explanation to the user, After the user
                        // sees the explanation, try again to request the permission.

                        mAlertDialogWithAction.showDialogForAccessPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                                ConstantVariables.READ_EXTERNAL_STORAGE);

                    }else{
                        // If user pressed never ask again on permission popup
                        // show snackbar with open app info button
                        // user can revoke the permission from Permission section of App Info.
                        SnackbarUtils.displaySnackbarOnPermissionResult( EditEntry.this, editFormView,
                                ConstantVariables.READ_EXTERNAL_STORAGE);

                    }
                }
                break;

        }
    }

}
