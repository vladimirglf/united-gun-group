/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */
package com.unitedgungroup.mobiapp.classes.core;
import org.json.JSONObject;
public class ConstantVariables {
    public static final String[] INCLUDED_MODULES_ARRAY = {"group", "blog", "classified", "event",
            "user", "music", "music_playlist", "music_playlist_song", "video", "album", "poll", "core",
            "activity", "advancedactivity", "forum", "forum_topic", "siteevent_event", "sitereview_review",
            "sitereview_listing", "sitereview_wishlist", "sitereview_video", "siteevent_review" ,"sitepage_page",
            "sitepagereview_review", "sitegroup_group", "sitegroupreview_review", "album_photo",
            "sitestoreproduct_wishlist","sitestoreproduct_product", "sitestore_store","sitestoreproduct_order",
            "sitestoreproduct_review","sitestorereview_review", "core_main_diaries", "core_main_sitevideo",
            "core_main_sitevideochannel", "core_main_sitevideoplaylist", "sitevideo_channel", "sitevideo_playlist"};
    public static final String[] ADV_SEARCH_MODULE_ARRAY = { "group", "blog", "classified", "event",
            "user", "music","music_playlist","music_playlist_song", "video", "album", "poll", "forum",
            "siteevent_event", "sitereview_review", "sitereview_listing","sitepage_page", "sitegroup_group"};
public static final String[] DELETED_MODULES = {"blog","forum","music"};
public static final int TOTAL_SLIDESHOW_IMAGES = 1;
    public static final int INPUT_FILE_REQUEST_CODE = 1;
    public static final int COMMENT_LIMIT = 10;
    // MLT View type for view page
    public static final int BLOG_VIEW = 1, CLASSIFIED_VIEW_WITHOUT_CAROUSEL = 2,
            CLASSIFIED_VIEW_WITH_CAROUSEL = 3;
    // MLT View type for browse page.
    public static final int LIST_VIEW = 1, GRID_VIEW = 2, MATRIX_VIEW = 3, SLIDER_VIEW = 4;
    // Advanced Event View page gutter menus option.
    public static final int REQUEST_INVITE = 1, CANCEL_INVITE = 2, JOIN_EVENT = 3, LEAVE_EVENT = 4,
            EVENT_WAIT_LIST = 5, BOOK_EVENT_TICKETS = 6;
    //For Music Module
    public static final String ACTION_SONG_COMPLETED = "com.socialengine.music.songCompleted";
    public static final String ACTION_SONG_PREPARED = "com.socialengine.music.songPrepared";
    public static final String ACTION_UPDATE_SONG = "com.socialengine.music.updateProgress";
    //For Home Page
    public static final String ACTION_VIEW_ALL_MESSAGES = "com.socialengine.messages.viewAll";
    public static final String ACTION_VIEW_ALL_NOTIFICATIONS = "com.socialengine.notification.viewAll";
    //For feed notifications
    public static final String ACTION_FEED_NOTIFICATIONS = "com.socialengine.home.action.feeds.notifications";
    public static final String DEFAULT_HASHTAG_COLOR = "#000000";
    //Request codes for starting activities
    public static final int CREATE_REQUEST_CODE = 2;
    public static final int PAGE_EDIT_CODE = 3;
    public static final int VIEW_PAGE_CODE = 5;
    public static final int VIEW_LIGHT_BOX = 8;
    public static final int LIGHT_BOX_DELETE = 9;
    public static final int LIGHT_BOX_EDIT = 10;
    public static final int MESSAGE_DELETED = 11;
    public static final int MESSAGE_CREATED = 12;
    public static final int FEED_REQUEST_CODE = 13;
    public static final int MESSAGE_VIEW_PAGE = 14;
    public static final int ADD_PEOPLE_CODE = 15;
    public static final int CHECK_IN_CODE = 16;
    public static final int ALBUM_VIEW_PAGE = 17;
    public static final int EDITOR_REQUEST_CODE = 18;
    public static final int EDIT_ENTRY_RETURN_CODE = 19;
    public static final int CHECK_IN_RESULT_CODE = 20;
    public static final int VERTICAL_ITEM_SPACE = 21;
    public static final int VIEW_POLL_REQUEST_CODE = 22;
    public static final int VIEW_PAGE_EDIT_CODE = 23;
    public static final int WAITING_MEMBERS_CODE = 24;
    public static final int OUTSIDE_SHARING_CODE = 25;
    public static final int SIGN_UP_CODE = 20;
    public static final int SIGN_UP_WEBVIEW_CODE = 21;
    public static final int PACKAGE_WEBVIEW_CODE = 22;
    public static final int LOCATION_UPDATE_CODE = 33;
    public static final int VIEW_COMMENT_PAGE_CODE = 35;
    public static final int VIEW_SINGLE_FEED_PAGE = 36;
    public static final int OVERVIEW_REQUEST_CODE = 38;
    // PERMISSION RESULT REQUEST CONSTANTS
    public static final int PERMISSION_CAMERA = 27;
    public static final int READ_EXTERNAL_STORAGE = 28;
    public static final int WRITE_EXTERNAL_STORAGE = 29;
    public static final int ACCESS_FINE_LOCATION = 30;
    public static final int ACCESS_COARSE_LOCATION = 31;
    public static final int READ_CONTACTS = 40;
    public static final int PERMISSION_WAKE_LOCK = 331;
    public static final int REQUEST_IMAGE = 300;
    public static final int REQUEST_VIDEO = 400;
    public static final int REQUEST_MUSIC = 500;
    public static final int FRAGMENT_LOAD_CODE = 34;
    public static final int UPDATE_REQUEST_CODE = 32;
    public static final int ADV_GROUPS_INVITE_REQUEST = 33;
    public static final int STICKER_STORE_REQUEST = 34;
    public static final int FILTER_PAGE_CODE = 35;
    public static final int ORDER_PAGE_CODE = 36;
    public static final int WISHLIST_CREATE_CODE = 37;
public static final int WEBVIEW_ENABLE = 1;
    public static final int REQUEST_CANCLED = 416;
    public static final String IS_CONTENT_EDITED = "ContentEdited";
    public static final String USER_ID = "user_id";
    public static final String CATEGORY_ID = "category_id";
    public static final String CATEGORY_VALUE = "category_value";
    public static final String CATEGORY_FORUM_TOPIC = "category_forum_topic";
    public static final String EXTRA_CREATE_RESPONSE = "CreateViewDetails";
    public static final String VIDEO_TYPE = "VideoType";
    public static final String VIDEO_URL = "VideoUrl";
    public static final String VIEW_ID = "ViewId";
    public static final String EXTRA_MODULE_TYPE = "ModuleName";
    public static final String IS_SEARCHED_FROM_DASHBOARD = "dashboardSearch";
    public static final String FOOTER_TYPE = "footer";
    public static final String HEADER_TYPE = "header";
    //For push notifications
    // Google Project Number
    public final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    // Request Time
    public final static int REFRESH_NOTIFICATION_TIME = 80000;
    public final static int FIRST_COUNT_REQUEST_DELAY = 40000;
    //Swipe Refresh Layout disable request time
    public final static int REFRESH_DELAY_TIME = 1000;
    //Request code for fragment loading in twitter
    public static final int TWITTER_REQUEST_CODE = 140;
    public static float IMAGE_SCALE_FACTOR = 1.5f;
    public static final int FILE_UPLOAD_LIMIT = 10;
    // View Page Parameters
    public static final String VIEW_PAGE_URL = "viewUrl";
    public static final String VIEW_PAGE_ID = "content_id";
    public static final String ALBUM_ID = "album_id";
    /**
     *     Modules Title
     */
    public static final String HOME_MENU_TITLE = "home";
    public static final String NOTIFICATION_MENU_TITLE = "core_mini_notification";
    public static final String FRIEND_REQUEST_MENU_TITLE = "core_mini_friend_request";
    public static final String GLOBAL_SEARCH_MENU_TITLE = "core_main_global_search";
    public static final String USER_SETTINGS_MENU_TITLE = "user_settings";
    public static final String CONTACT_US_MENU_TITLE = "contact_us";
    public static final String PRIVACY_POLICY_MENU_TITLE = "privacy_policy";
    public static final String TERMS_OF_SERVICE_MENU_TITLE = "terms_of_service";
    public static final String MULTI_LANGUAGES_MENU_TITLE = "core_multi_languages";
    public static final String LOCATION_MENU_TITLE = "seaocore_location";
    public static final String SPREAD_THE_WORD_MENU_TITLE = "spread_the_word";
    public static final String WISHLIST_MENU_TITLE = "core_main_wishlist";
    public static final String COMET_CHAT_MENU_TITLE = "core_main_cometchat";
    public static final String AAF_MENU_TITLE = "activity_action";
    //Message titles
    public static final String MESSAGE_MENU_TITLE = "core_mini_messages";
    public static final String MESSAGE_CONVERSATION_MENU_TITLE = "messages_conversation";
    //Blog titles
    public static final String BLOG_MENU_TITLE = "core_main_blog";
    public static final String BLOG_TITLE = "blog";
    // Classified titles
    public static final String CLASSIFIED_MENU_TITLE = "core_main_classified";
    public static final String CLASSIFIED_TITLE = "classified";
    // Group titles
    public static final String GROUP_MENU_TITLE = "core_main_group";
    public static final String GROUP_TITLE = "group";
    // Event titles
    public static final String EVENT_MENU_TITLE = "core_main_event";
    public static final String EVENT_TITLE = "event";
    // Music titles
    public static final String MUSIC_MENU_TITLE = "core_main_music";
    public static final String MUSIC_TITLE = "music";
    public static final String MUSIC_PLAYLIST_TITLE = "music_playlist";
    public static final String MUSIC_PLAYLIST_SONG_TITLE = "music_playlist_song";
    // Album titles
    public static final String ALBUM_MENU_TITLE = "core_main_album";
    public static final String ALBUM_TITLE = "album";
    public static final String ALBUM_PHOTO_MENU_TITLE = "album_photo";
    // Poll titles
    public static final String POLL_MENU_TITLE = "core_main_poll";
    public static final String POLL_TITLE = "poll";
    // Video titles
    public static final String VIDEO_MENU_TITLE = "core_main_video";
    public static final String VIDEO_TITLE = "video";
    // Forum titles
    public static final String FORUM_MENU_TITLE = "core_main_forum";
    public static final String FORUM_TITLE = "forum";
    public static final String FORUM_TOPIC_MENU_TITLE = "forum_topic";
    public static final String FORUM_POST_MENU_TITLE = "forum_post";
    // MLT titles
    public static final String MLT_MENU_TITLE = "sitereview_listing";
    public static final String MLT_TITLE = "sitereview";
    public static final String MLT_VIDEO_MENU_TITLE = "sitereview_video";
    public static final String MLT_PHOTO_MENU_TITLE = "sitereview_photo";
    public static final String MLT_REVIEW_MENU_TITLE = "sitereview_review";
    public static final String MLT_WISHLIST_MENU_TITLE = "sitereview_wishlist";
    // Advanced event titles
    public static final String ADVANCED_EVENT_MENU_TITLE = "core_main_siteevent";
    public static final String ADVANCED_EVENT_TITLE = "siteevent";
    public static final String ADV_EVENT_REVIEW_MENU_TITLE = "siteevent_review";
    public static final String ADV_EVENT_VIDEO_MENU_TITLE = "siteevent_video";
    public static final String ADV_EVENT_MENU_TITLE = "siteevent_event";
    public static final String ADV_EVENT_DIARY_MENU_TITLE = "adv_event_diary";
    public static final String DIARY_MENU_TITLE = "core_main_diaries";
    // Site page titles
    public static final String SITE_PAGE_MENU_TITLE = "sitepage";
    public static final String SITE_PAGE_TITLE = "sitepage_page";
    public static final String SITE_PAGE_TITLE_MENU = "core_main_sitepage";
    public static final String SITE_PAGE_REVIEW_MENU_TITLE = "sitepagereview_review";
    public static final String SITE_PAGE_REVIEW_TITLE = "sitepage_review";
    public static final String SITE_PAGE_PHOTO_MENU_TITLE = "sitepage_photo";
    public static final String SITE_PAGE_ALBUM_MENU_TITLE = "sitepage_album";
    // Advanced group titles
    public static final String ADV_GROUPS_MENU_TITLE = "core_main_sitegroup";
    public static final String ADV_GROUPS_TITLE = "sitegroup";
    public static final String ADVANCED_GROUPS_MENU_TITLE = "sitegroup_group";
    public static final String ADV_GROUPS_REVIEW_MENU_TITLE = "sitegroupreview_review";
    public static final String ADV_GROUPS_REVIEW_TITLE = "sitegroup_review";
    public static final String ADV_GROUPS_VIDEO_MENU_TITLE = "sitegroupvideo_video";
    public static final String ADV_GROUPS_ALBUM_MENU_TITLE = "sitegroup_album";
    // Advanced Video titles
    public static final String ADV_VIDEO_MENU_TITLE = "core_main_sitevideo";
    public static final String ADV_VIDEO_TITLE = "sitevideo";
    public static final String ADV_VIDEO_CHANNEL_MENU_TITLE = "core_main_sitevideochannel";
    public static final String SITE_VIDEO_CHANNEL_MENU_TITLE = "sitevideo_channel";
    public static final String ADV_VIDEO_PLAYLIST_MENU_TITLE = "core_main_sitevideoplaylist";
    public static final String SITE_VIDEO_PLAYLIST_MENU_TITLE = "sitevideo_playlist";
    // User titles
    public static final String USER_MENU_TITLE = "core_main_user";
    public static final String USER_TITLE = "user";
    // Store titles
    public static final String STORE_MENU_TITLE = "core_main_sitestore";
    public static final String STORE_TITLE = "sitestore";
    public static final String SITE_STORE_MENU_TITLE = "sitestore_store";
    public static final String STORE_REVIEW_MENU_TITLE = "sitestorereview_review";
    public static final String SITE_STORE_VIDEO_MENU_TITLE = "sitestorevideo_video";
    public static final String STORE_ORDER_MENU_TITLE = "sitestore_orders";
    public static final String STORE_OFFER_MENU_TITLE = "core_main_sitestoreoffer";
    // Store product titles
    public static final String PRODUCT_MENU_TITLE = "core_main_sitestoreproduct";
    public static final String PRODUCT_TITLE = "sitestore_product";
    public static final String PRODUCT_CART_MENU_TITLE = "core_main_sitestoreproduct_cart";
    public static final String SITE_PRODUCT_MENU_TITLE = "sitestoreproduct_product";
    public static final String SITE_PRODUCT_REVIEW_MENU_TITLE = "sitestoreproduct_review";
    public static final String PRODUCT_WISHLIST_MENU_TITLE = "sitestoreproduct_wishlist";
    public static final String PRODUCT_ORDER_MENU_TITLE = "sitestoreproduct_order";
    public static final String PRODUCT_VIDEO_MENU_TITLE = "sitestoreproduct_video";
    public static final String SITE_PRODUCT_ORDER_MENU_TITLE = "core_main_sitestoreproduct_orders";
    public static final String SITE_PRODUCT_WISHLIST_MENU_TITLE = "core_main_sitestoreproduct_wishlists";
    // Create Url Parameter
    public static final String CREATE_URL = "createUrl";
    public static final String URL_STRING = "URL";
    // Intent Parameters
    public static final String AAF_VIDEO = "aaf_video";
    public static final String HASTAG_SEARCH = "hashtagsearch";
    public static final String SUBJECT_TYPE = "subject_type";
    public static final String SUBJECT_ID = "subject_id";
    public static final String VIDEO_SUBJECT_TYPE = "video_subject_type";
    public static final String ACTION_ID = "action_id";
    public static final String FRAGMENT_NAME = "fragmentName";
    public static final String TOTAL_ITEM_COUNT = "totalItemCount";
    public static final String IS_WAITING = "isWaiting";
    public static final String INVITE_GUEST = "inviteGuestUrl";
    public static final String PROFILE_RSVP_VALUE = "profile_rsvp_value";
    public static final String CAN_UPLOAD = "canUpload";
    public static final String CAN_UPLOAD_VIDEO = "canUploadVideo";
    public static final String RESPONSE_OBJECT = "responseObject";
    public static final String OVERVIEW = "OverView";
    public static final String CONTENT_TITLE = "content_title";
    public static final String TITLE = "title";
    public static final String MODULE_NAME = "moduleName";
    public static final String ITEM_POSITION = "position";
    public static final String ATTACH_VIDEO = "attach_video";
    public static final String DESCRIPTION = "description";
    public static final String IMAGE = "image";
    public static final String CONTENT_ID = "content_id";
    public static final String LISTING_ID = "listing_id";
    public static final String LISTING_TYPE_ID = "listingtype_id";
    public static final String TAB_LABEL = "tab_label";
    public static final String MLT_VIEW_TYPE = "mlt_view_type";
    public static final String FORM_TYPE = "form_type";
    public static final String MENU_ARRAY = "menu_array";
    public static final String PROFILE_TYPE = "profile_type";
    public static final String PHOTO_LIST = "photo_list";
    public static final String IS_PHOTO_UPLOADED = "is_photo_upload_request";
    public static final String USER_PROFILE_COVER_BUNDLE = "user_profile_cover_bundle";
    public static final String CAN_EDIT = "canEdit";
    public static final String PHOTO_REQUEST_URL = "photoRequestUrl";
    public static final String SHOW_OPTIONS = "optionvisibility";
    public static final String ENABLE_COMMENT_CACHE = "commentcaching";
    public static final String SHOW_ALBUM_BUTTON = "ViewAlbum";
    public static final String CHANGE_DEFAULT_LOCATION = "default_location";
    public static final String IS_PROFILE_PAGE_REQUEST = "profile_page_request";
    public static final String IS_CATEGORY_BASED_RESULTS = "categoryBasedResults";
    public static final String PHOTO_COMMENT_COUNT = "photoCommentCount";
    public static final String IS_PHOTO_COMMENT = "is_photo_comment";
    public static final String FEED_LIST = "feed_list";
    public static final String LIKE_COMMENT_URL = "like_comment_url";
    public static final String PHOTO_POSITION = "photo_position";
    public static final String IS_LIKED = "isLike";
    public static final String PHOTO_LIKE_COUNT = "photoLikeCount";
    public static final String IS_ALBUM_PHOTO_REQUEST = "is_album_photo_request";
    //Activity feed post length
    public static final int FEED_TITLE_BODY_LENGTH = 150;
    // User Profile Code
    public static final int USER_PROFILE_CODE = 100;
    //Content Cover Edit Code
    public static final int CONTENT_COVER_EDIT_CODE = 171;
    // WebView Activity Request Code
    public static final int WEB_VIEW_ACTIVITY_CODE = 600;
    //Payment failed Request Code
    public static final int PAYMENT_FAILED_ACTIVITY_CODE = 601;
    //Payment success Request code
    public static final int PAYMENT_SUCCESS_ACTIVITY_CODE = 602;
    /**
     * People suggestion settings
     */
    public static final int ENABLE_PEOPLE_SUGGESTION = 0;
    public static final int PEOPLE_SUGGESTION_POSITION = 5;
    public static final int PEOPLE_SUGGESTION_LIMIT = 10;
    /*
    * Ad mob implementation settings
    */
    /**
     * Check which Ads are enabled on site
     * ENABLE_ADMOB ? 0 => Facebook Ads
     * ENABLE_ADMOB ? 1 => Google Ads.
     *
     * Check for each plugin which type of Ad is enabled
     * 0 => Facebook Ads, 1 => Google Ads, 2 => Community/Ads
     * 3 => Ad Campaigns, 4 => Sponsored stories
     */
public static final int ENABLE_ADMOB = 1;
    public static final int ENABLE_FEED_ADS = 0;
    public static final int ENABLE_CLASSIFIED_ADS = 0;
    public static final int ENABLE_MUSIC_ADS = 0;
    public static final int ENABLE_MLT_ADS = 0;
    public static final int ENABLE_BLOG_ADS = 0;
    public static final int ENABLE_ALBUM_ADS = 0;
    public static final int ENABLE_EVENT_ADS = 0;
    public static final int ENABLE_GROUPS_ADS = 0;
    public static final int ENABLE_POLL_ADS = 0;
    public static final int ENABLE_FORUM_ADS = 0;
    public static final int ENABLE_VIDEO_ADS = 0;
    public static final int ENABLE_ADV_EVENT_ADS = 0;
    public static final int ENABLE_SITE_PAGE_ADS = 0;
    public static final int ENABLE_ADV_GROUPS_ADS = 0;
    public static final int ENABLE_ADV_VIDEO_ADS = 0;
    public static final int FEED_ADS_TYPE = ENABLE_ADMOB;
    public static final int CLASSIFIED_ADS_TYPE = ENABLE_ADMOB;
    public static final int MUSIC_ADS_TYPE = ENABLE_ADMOB;
    public static final int MLT_ADS_TYPE = ENABLE_ADMOB;
    public static final int BLOG_ADS_TYPE = ENABLE_ADMOB;
    public static final int ALBUM_ADS_TYPE = ENABLE_ADMOB;
    public static final int EVENT_ADS_TYPE = ENABLE_ADMOB;
    public static final int GROUPS_ADS_TYPE = ENABLE_ADMOB;
    public static final int POLL_ADS_TYPE = ENABLE_ADMOB;
    public static final int FORUM_ADS_TYPE = ENABLE_ADMOB;
    public static final int VIDEO_ADS_TYPE = ENABLE_ADMOB;
    public static final int ADV_EVENT_ADS_TYPE = ENABLE_ADMOB;
    public static final int SITE_PAGE_ADS_TYPE = ENABLE_ADMOB;
    public static final int ADV_GROUPS_ADS_TYPE = ENABLE_ADMOB;
    public static final int ADV_VIDEO_ADS_TYPE = ENABLE_ADMOB;
    public static final int FEED_ADS_POSITION = 10;
    public static final int CLASSIFIED_ADS_POSITION = 7;
    public static final int MUSIC_ADS_POSITION = 7;
    public static final int MLT_ADS_POSITION = 7;
    public static final int BLOG_ADS_POSITION = 7;
    public static final int ALBUM_ADS_POSITION = 7;
    public static final int EVENT_ADS_POSITION = 7;
    public static final int GROUPS_ADS_POSITION = 7;
    public static final int POLL_ADS_POSITION = 7;
    public static final int FORUM_ADS_POSITION = 7;
    public static final int VIDEO_ADS_POSITION = 7;
    public static final int ADV_GROUPS_ADS_POSITION = 7;
    public static final int ADV_EVENT_ADS_POSITION = 7;
    public static final int SITE_PAGE_ADS_POSITION = 7;
    public static final int ADV_VIDEO_ADS_POSITION = 7;
    public static final int DEFAULT_AD_COUNT = 15;
    public static final int DEFAULT_TICKETS_COUNT = 10;
    public static final int TYPE_FACEBOOK_ADS = 0;
    public static final int TYPE_GOOGLE_ADS = 1;
    public static final int TYPE_COMMUNITY_ADS = 2;
    public static final int TYPE_SPONSORED_STORIES = 4;
public static final String COMETCHAT_PACKAGE_NAME = "";
    public static final String REQUEST_CODE = "requestCode";
    public static final String REACTION_NAME = "reaction";
    public static final String REACTION_RESPONSE = "reaction_response";
    public static JSONObject STICKERS_STORE_ARRAY = new JSONObject();
    public static final String ADV_VIDEO_INTEGRATED = "is_adv_video_integrated";
    public static final String ADV_VIDEOS_COUNT = "adv_videos_count";
    public static final String MY_PHOTO_REACTIONS = "my_photo_reactions";
    public static final String PHOTO_POPULAR_REACTIONS = "photo_popular_reactions";
    public static final boolean SHOW_CART_ICON = true;

    public static int ACTION_ID_VALUE = 0;
}
