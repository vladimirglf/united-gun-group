/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */

package com.unitedgungroup.mobiapp.classes.modules.advancedActivityFeeds;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.unitedgungroup.mobiapp.classes.common.dialogs.AlertDialogWithAction;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnFeedDisableCommentListener;
import com.unitedgungroup.mobiapp.classes.common.adapters.ImageAdapter;
import com.unitedgungroup.mobiapp.classes.common.dialogs.AlertDialogWithAction;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnAsyncResponseListener;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnFeedDisableCommentListener;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnItemClickListener;
import com.unitedgungroup.mobiapp.classes.common.multiimageselector.MultiImageSelectorActivity;
import com.unitedgungroup.mobiapp.classes.common.ui.ActionIconThemedTextView;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.ui.ThemedTextView;
import com.unitedgungroup.mobiapp.classes.common.utils.BitmapUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.ImageViewList;
import com.unitedgungroup.mobiapp.classes.common.utils.PreferencesUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.Smileys;
import com.unitedgungroup.mobiapp.classes.common.utils.SnackbarUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.SocialShareUtil;
import com.unitedgungroup.mobiapp.classes.common.utils.SoundUtil;
import com.unitedgungroup.mobiapp.classes.common.utils.UploadAttachmentUtil;
import com.unitedgungroup.mobiapp.classes.common.utils.UrlUtil;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;
import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.utils.GlobalFunctions;
import com.unitedgungroup.mobiapp.classes.common.adapters.FeedAdapter;
import com.unitedgungroup.mobiapp.classes.common.ui.NestedListView;
import com.unitedgungroup.mobiapp.classes.common.utils.FeedList;
import com.unitedgungroup.mobiapp.classes.core.ConstantVariables;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnResponseListener;
import com.unitedgungroup.mobiapp.classes.modules.likeNComment.Comment;
import com.unitedgungroup.mobiapp.classes.modules.likeNComment.CommentAdapter;
import com.unitedgungroup.mobiapp.classes.modules.likeNComment.CommentList;
import com.unitedgungroup.mobiapp.classes.modules.photoLightBox.PhotoLightBoxActivity;
import com.unitedgungroup.mobiapp.classes.modules.photoLightBox.PhotoListDetails;
import com.unitedgungroup.mobiapp.classes.modules.stickers.StickersClickListener;
import com.unitedgungroup.mobiapp.classes.modules.stickers.StickersGridView;
import com.unitedgungroup.mobiapp.classes.modules.stickers.StickersPopup;
import com.unitedgungroup.mobiapp.classes.modules.stickers.StickersUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SingleFeedPage extends AppCompatActivity implements View.OnClickListener, TextWatcher,
        AbsListView.OnScrollListener, OnFeedDisableCommentListener, OnAsyncResponseListener,
        View.OnLongClickListener {

    Toolbar mToolBar;
    int mActionId, mGetTotalComments, pageNumber = 1, mFeedPosition = -1;
    NestedListView mCommentsListView;
    CommentAdapter mCommentAdapter;
    String mCommentListUrl, mSubjectType, mFeedsUrl, mCommentPostUrl, mCommentNotificationUrl,
            mActionTypeBody = null, mAlbumViewUrl;
    AppConstant mAppConst;
    List<CommentList> mCommentListItems;
    CommentList mCommentList;
    RecyclerView mFeedsRecyclerView;
    FeedAdapter mFeedAdapter;
    List<Object> mFeedItemsList;
    JSONArray mFilterTabsArray, mDataJsonArray, mAllCommentsArray, mHashTagArray;
    JSONObject mFeedPostMenu;
    HashMap<String, String> mClickableParts, mClickablePartsNew;
    HashMap<Integer, String> mVideoInformation;
    LinearLayout mCommentPostBlock;
    TextView mCommentPostButton;
    EditText mCommentBox;
    private View footerView;
    private static final int COMMENT_LIMIT = 10;
    boolean isLoading = false, isLoadingFeed = true, isCommentsLoading = false;
    private FeedList mFeedList;
    private LinearLayout mPhotosBlock, mLikeCommentBlock;
    private ArrayList<PhotoListDetails> mPhotoDetails;
    private Context mContext;
    private int mPhotoPosition = 0;
    private ActionIconThemedTextView mShareButton, mLikeButton, mCommentButton;
    private ThemedTextView mLikeCount, mCommentCount;
    private RelativeLayout mCounterView;
    boolean likeUnlikeAction = true;
    private Map<String, String> params;
    private double mLatitude, mLongitude;
    private String mPlaceId = null, mHashTagString = "", mLocationLabel;
    private SocialShareUtil socialShareUtil;
    private JSONObject commentsObject;
    private int mReactionsEnabled, mStickersEnabled;
    private JSONObject mReactions;
    private StickersPopup mStickersPopup;
    public RelativeLayout mStickersParentView;
    private ArrayList<String> mSelectPath = new ArrayList<>();
    private String mAttachmentType;
    private int width;
    private RelativeLayout mSelectedImageBlock;
    private ImageView mSelectedImageView, mCancelImageView;
    private TextView mPhotoUploadingButton;
    private List<ImageViewList> reactionsImages;
    private String mLikeUnlikeUrl;
    private AlertDialogWithAction mAlertDialogWithAction;
    private ArrayList<JSONObject> mReactionsArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_feed_page);

        mContext = this;

        /* Set Toolbar */

        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolBar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        CustomViews.createMarqueeTitle(this, mToolBar);

        mFeedsRecyclerView = (RecyclerView) findViewById(R.id.feedsGrid);
        mFeedsRecyclerView.setNestedScrollingEnabled(false);
        mFeedsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mFeedsRecyclerView.setItemAnimator(new DefaultItemAnimator());

        ((SimpleItemAnimator) mFeedsRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);

        mCommentPostBlock = (LinearLayout) findViewById(R.id.commentPostBlock);
        mCommentBox = (EditText) findViewById(R.id.commentBox);
        mCommentBox.addTextChangedListener(this);

        mCommentPostButton = (TextView) findViewById(R.id.commentPostButton);
        mCommentPostButton.setTypeface(GlobalFunctions.getFontIconTypeFace(this));
        mCommentPostButton.setTextColor(ContextCompat.getColor(this, R.color.gray_stroke_color));
        mCommentPostButton.setOnClickListener(this);

        mCommentsListView = (NestedListView) findViewById(R.id.commentList);
        mCommentsListView.setOnScrollListener(this);

        mPhotoUploadingButton = (TextView) findViewById(R.id.photoUploadingButton);
        mPhotoUploadingButton.setTypeface(GlobalFunctions.getFontIconTypeFace(this));
        mPhotoUploadingButton.setText("\uf030");
        mPhotoUploadingButton.setOnClickListener(this);

        footerView = CustomViews.getFooterView(getLayoutInflater());

        mPhotosBlock = (LinearLayout) findViewById(R.id.addPhotoBlock);

        socialShareUtil = new SocialShareUtil(this);

        mCommentPostUrl = AppConstant.DEFAULT_URL + "advancedactivity/comment";
        mCommentNotificationUrl = AppConstant.DEFAULT_URL + "advancedactivity/add-comment-notifications";

        /* Get Values from intent */
        if (getIntent().getExtras() != null) {
            mActionId = getIntent().getIntExtra(ConstantVariables.ACTION_ID, 0);
            mFeedPosition = getIntent().getIntExtra(ConstantVariables.ITEM_POSITION, 0);
            mFeedList = getIntent().getParcelableExtra(ConstantVariables.FEED_LIST);
            mReactionsEnabled = getIntent().getIntExtra("reactionEnabled", 0);
            mStickersEnabled = getIntent().getIntExtra("stickersEnabled",
                    PreferencesUtils.getStickersEnabled(mContext));

            if (getIntent().getStringExtra("reactions") != null) {
                try {
                    mReactions = new JSONObject(getIntent().getStringExtra("reactions"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        mStickersParentView = (RelativeLayout) findViewById(R.id.stickersMainLayout);
        if (mStickersEnabled == 1) {
            mCommentPostButton.setText("\uf118");
        } else {
            mCommentPostButton.setText("\uf1d8");
        }

        mAppConst = new AppConstant(this);
        mAlertDialogWithAction = new AlertDialogWithAction(mContext);
        mFeedItemsList = new ArrayList<>();
        mCommentListItems = new ArrayList<>();
        mCommentList = new CommentList();

        width = AppConstant.getDisplayMetricsWidth(mContext);
        mSubjectType = "activity_action";

        if (mFeedList != null) {
            mCommentsListView.setVisibility(View.GONE);
            mCommentPostBlock.setVisibility(View.GONE);
            checkImagePostingOption();
            mFeedAdapter = new FeedAdapter(this, R.layout.list_feeds, mFeedItemsList, true, null,
                    mSubjectType, mActionId, null, mFeedPosition, true, null);

            mFeedsRecyclerView.setAdapter(mFeedAdapter);

            mFeedItemsList.add(0, new FeedList(null, null, false, mReactionsEnabled, mReactions));
            mFeedItemsList.add(mFeedList);
            mFeedAdapter.notifyDataSetChanged();

            showPhotosBlock(mFeedList);
            mPhotosBlock.setVisibility(View.VISIBLE);

        } else {
            mFeedAdapter = new FeedAdapter(this, R.layout.list_feeds, mFeedItemsList, true, mCommentBox,
                    mSubjectType, mActionId, null, mFeedPosition, false, null);

            mFeedsRecyclerView.setAdapter(mFeedAdapter);
            mFeedAdapter.setOnFeedDisableCommentListener(SingleFeedPage.this);

            mFeedsUrl = AppConstant.DEFAULT_URL + "advancedactivity/feeds?limit=" + AppConstant.LIMIT +
                    "&object_info=1&getAttachedImageDimention=0&action_id=" + mActionId
                    + "&feed_filter=0&post_elements=0&post_menus=0";

            mCommentListUrl = UrlUtil.AAF_LIKE_COMMNET_URL + "&action_id=" + mActionId + "&page=" + pageNumber;

            mCommentAdapter = new CommentAdapter(this, R.layout.list_comment, mCommentListItems,
                    mCommentList, true, mSubjectType, mActionId, mActionId);
            mCommentsListView.setAdapter(mCommentAdapter);

            getFeeds(mFeedsUrl);

            sendRequestToServer(mCommentListUrl);

        }

        mSelectedImageBlock = (RelativeLayout) findViewById(R.id.selectedImageBlock);
        mSelectedImageView = (ImageView) findViewById(R.id.imageView);
        mCancelImageView = (ImageView) findViewById(R.id.removeImageButton);

        PreferencesUtils.updateReactionsEnabledPref(mContext, mReactionsEnabled);
        PreferencesUtils.updateStickersEnabledPref(mContext, mStickersEnabled);

        if(mReactions != null){
            mReactionsArray = GlobalFunctions.sortReactionsObjectWithOrder(mReactions);
        }
    }

    /**
     * Method to check image posting option according to emojiEnabled value.
     */
    public void checkImagePostingOption() {
        if (PreferencesUtils.getEmojiEnabled(mContext) == 1) {
            mPhotoUploadingButton.setVisibility(View.VISIBLE);
        } else {
            mPhotoUploadingButton.setVisibility(View.GONE);
            LinearLayout.LayoutParams commentBoxParams = (LinearLayout.LayoutParams) mCommentBox.getLayoutParams();
            commentBoxParams.weight = 0.9f;
            mCommentBox.setLayoutParams(commentBoxParams);
        }
    }

    public void getFeeds(final String url) {

        isLoadingFeed = true;

        findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
        mAppConst.getJsonResponseFromUrl(url, new OnResponseListener() {
            @Override
            public void onTaskCompleted(JSONObject jsonObject) {

                if (mFeedItemsList != null) {
                    mFeedItemsList.clear();
                }

                findViewById(R.id.progressBar).setVisibility(View.GONE);

                if (jsonObject != null && jsonObject.length() != 0) {

                    // Send Stickers Request
                    if (mStickersEnabled == 1) {
                        mAppConst.getJsonResponseFromUrl(UrlUtil.AAF_VIEW_STICKERS_URL, new OnResponseListener() {

                            @Override
                            public void onTaskCompleted(JSONObject jsonObject) throws JSONException {

                                if (jsonObject != null) {
                                    mStickersPopup = StickersUtil.createStickersPopup(mContext, findViewById(R.id.main_content),
                                            mStickersParentView, mCommentBox,
                                            jsonObject, mPhotoUploadingButton, mCommentPostButton);
                                    mStickersPopup.setOnStickerClickedListener(new StickersClickListener() {
                                        @Override
                                        public void onStickerClicked(ImageViewList stickerItem) {
                                            params = new HashMap<>();
                                            postComment(null, stickerItem.getmStickerGuid(),
                                                    stickerItem.getmGridViewImageUrl());
                                        }
                                    });
                                }
                            }

                            @Override
                            public void onErrorInExecutingTask(String message, boolean isRetryOption) {

                            }
                        });
                    }

                    try {
                        mFilterTabsArray = jsonObject.optJSONArray("filterTabs");
                        mDataJsonArray = jsonObject.optJSONArray("data");
                        mFeedPostMenu = jsonObject.optJSONObject("feed_post_menu");
                        mReactionsEnabled = jsonObject.optInt("reactionsEnabled");
                        mStickersEnabled = jsonObject.optInt("reactionsEnabled");
                        mReactions = jsonObject.optJSONObject("reactions");
                        PreferencesUtils.setEmojiEnablePref(mContext, jsonObject.optInt("emojiEnabled"));
                        checkImagePostingOption();

                        if (mDataJsonArray != null && mDataJsonArray.length() != 0) {

                            // Add Header
                            if ((mFeedPostMenu != null && mFeedPostMenu.length() != 0) ||
                                    (mFilterTabsArray != null && mFilterTabsArray.length() != 0)) {
                                if (mDataJsonArray != null && mDataJsonArray.length() != 0)
                                    mFeedItemsList.add(0, new FeedList(mFeedPostMenu, mFilterTabsArray,
                                            false, mReactionsEnabled, mReactions));
                                else
                                    mFeedItemsList.add(0, new FeedList(mFeedPostMenu, mFilterTabsArray,
                                            true, mReactionsEnabled, mReactions));
                            } else {
                                mFeedItemsList.add(0, new FeedList(null, null, false, mReactionsEnabled, mReactions));
                            }

                            for (int i = 0; i < mDataJsonArray.length(); i++) {
                                String url = null;
                                JSONObject singleFeedJsonObject = mDataJsonArray.getJSONObject(i);

                                // Get Feed Info Object

                                JSONObject feedInfo = singleFeedJsonObject.optJSONObject("feed");
                                JSONObject userObject = feedInfo.optJSONObject("object");
                                if (userObject != null && userObject.length() != 0) {
                                    url = userObject.optString("url");
                                }
                                mHashTagArray = singleFeedJsonObject.optJSONArray("hashtags");
                                if (mHashTagArray != null) {
                                    mHashTagString = "";
                                    for (int j = 0; j < mHashTagArray.length(); j++) {
                                        mHashTagString += mHashTagArray.get(j) + " ";
                                    }
                                } else {
                                    mHashTagString = "";
                                }
                                JSONArray feedMenus = singleFeedJsonObject.optJSONArray("feed_menus");
                                JSONObject feedFooterMenus = singleFeedJsonObject.optJSONObject("feed_footer_menus");
                                int canComment = singleFeedJsonObject.optInt("can_comment");

                                if(canComment  != 0){
                                    mCommentPostBlock.setVisibility(View.VISIBLE);
//                                    mAppConst.showKeyboard();
                                }

                                int isLike = singleFeedJsonObject.optInt("is_like");

                                int actionId = feedInfo.optInt("action_id");
                                int subjectId = feedInfo.optInt(ConstantVariables.SUBJECT_ID);
                                int feedType = feedInfo.optInt("feed_type");
                                String objectType = feedInfo.optString("object_type");
                                int objectId = feedInfo.optInt("object_id");
                                String body = feedInfo.optString("body");
                                int commentCount = feedInfo.optInt("comment_count");
                                int attachmentCount = feedInfo.optInt("attachment_count");
                                int likeCount = feedInfo.optInt("like_count");
                                int commentAble = feedInfo.optInt("commentable");
                                int shareAble = feedInfo.optInt("shareable");
                                int isSaveFeedOption = singleFeedJsonObject.optInt("isSaveFeedOption");
                                String date = feedInfo.optString("date");
                                JSONObject feedObject = feedInfo.optJSONObject("object");

                                String type = feedInfo.optString("type");
                                String feedAttachmentType = feedInfo.optString("attachment_content_type");

                                /* CODE FOR FETCHING FEED ATTACHMENT */

                                JSONArray attachmentArray = feedInfo.optJSONArray("attachment");
                                int photoAttachmentCount = feedInfo.optInt("photo_attachment_count");

                                /* CODE STARTS FOR  PREPARING ACTION BODY */
                                String actionTypeBody = feedInfo.optString("action_type_body");
                                JSONArray actionTypeBodyParams = feedInfo.optJSONArray("action_type_body_params");
                                JSONArray tagsJsonArray = feedInfo.optJSONArray("tags");
                                JSONObject paramsJsonObject = feedInfo.optJSONObject("params");

                                JSONArray userTagJsonArray = feedInfo.optJSONArray("userTag");

                                JSONObject feedReactions = singleFeedJsonObject.optJSONObject("feed_reactions");
                                JSONObject myFeedReaction = singleFeedJsonObject.optJSONObject("my_feed_reaction");

                                String feedActionTitle;
                                mActionTypeBody = null;

                                if (actionTypeBodyParams != null && actionTypeBodyParams.length() != 0) {
                                    feedActionTitle = getActionBody(actionTypeBody, actionTypeBodyParams,
                                            tagsJsonArray, paramsJsonObject, body, attachmentArray, userTagJsonArray);
                                } else {
                                    feedActionTitle = feedInfo.optString("feed_title");
                                }

                                /* END ACTION BODY CODE */

                                String feedIcon = feedInfo.optString("feed_icon");

                                mFeedItemsList.add(new FeedList(actionId, subjectId, feedType, objectType,
                                        objectId, feedActionTitle, feedIcon, feedMenus, date, attachmentCount,
                                        likeCount, commentCount, canComment, isLike, feedObject,
                                        attachmentArray, photoAttachmentCount, feedFooterMenus, commentAble,
                                        shareAble, isSaveFeedOption, mClickableParts, mClickablePartsNew, mActionTypeBody,
                                        mVideoInformation, url, feedAttachmentType, type, mLocationLabel, mLatitude,
                                        mLongitude, mPlaceId, mHashTagString, feedReactions, myFeedReaction, userTagJsonArray));


                                // }
                            }

                        } else {
                            String message = getResources().getString(R.string.deleted_feed_message);
                            SnackbarUtils.displaySnackbarLongWithListener(findViewById(R.id.main_content),
                                    message, new SnackbarUtils.OnSnackbarDismissListener() {
                                        @Override
                                        public void onSnackbarDismissed() {
                                            if (!isFinishing()) {
                                                finish();
                                            }
                                        }
                                    });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                mFeedAdapter.notifyDataSetChanged();
                isLoadingFeed = false;

                if (!isCommentsLoading && commentsObject != null) {
                    try {

                        mGetTotalComments = commentsObject.getInt("getTotalComments");

                        if (mGetTotalComments != 0) {
                            mCommentList.setmTotalCommmentCount(mGetTotalComments);
                            mAllCommentsArray = commentsObject.optJSONArray("viewAllComments");
                            addCommentsToList(mAllCommentsArray);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mCommentAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                findViewById(R.id.progressBar).setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            // Playing backSound effect when user tapped on back button from tool bar.
            if (PreferencesUtils.isSoundEffectEnabled(mContext)) {
                SoundUtil.playSoundEffectOnBackPressed(mContext);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (mStickersParentView != null && mStickersParentView.getVisibility() == View.VISIBLE) {
            mStickersParentView.setVisibility(View.GONE);
        } else {

            int totalComments = mCommentList.getmTotalCommmentCount();
            if (mFeedItemsList.size() > 0) {
                FeedList feedList = (FeedList) mFeedItemsList.get(1);
                Intent intent = new Intent();

                // Update comment count from comment list when the comments are loaded on single feed.
                if (mFeedList == null && commentsObject != null) {
                    feedList.setmCommentCount(totalComments);
                }
                feedList.setmFeedAttachmentArray(updateAttachmentArrayOnBackPressed(feedList));
                intent.putExtra(ConstantVariables.ITEM_POSITION, mFeedPosition);
                intent.putExtra(ConstantVariables.FEED_LIST, feedList);
                setResult(ConstantVariables.VIEW_SINGLE_FEED_PAGE, intent);
            }

            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            super.onBackPressed();
        }
    }

    public void sendRequestToServer(String commentListUrl){
        mAppConst.getJsonResponseFromUrl(commentListUrl, new OnResponseListener() {
            @Override
            public void onTaskCompleted(JSONObject jsonObject) {
                findViewById(R.id.progressBar).setVisibility(View.GONE);
                mCommentListItems.clear();
                commentsObject = jsonObject;

                if (!isLoadingFeed && jsonObject.length() != 0) {
                    try {

                        mGetTotalComments = jsonObject.getInt("getTotalComments");
                        if (mGetTotalComments != 0) {
                            mCommentList.setmTotalCommmentCount(mGetTotalComments);
                            mAllCommentsArray = jsonObject.optJSONArray("viewAllComments");
                            addCommentsToList(mAllCommentsArray);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mCommentAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onErrorInExecutingTask(String message, boolean isRetryOption) {

            }
        });
    }

    /*
     Function to Prepare Action Body of Activity Feed
        */

    public String getActionBody(String actionTypeBody, JSONArray actionTypeBodyParams,
                                JSONArray tagsJsonArray, JSONObject paramsJsonObject,
                                String body, JSONArray attachmentArray, JSONArray userTagJsonArray) {

        mClickableParts = new HashMap<>();
        mVideoInformation = new HashMap<>();

        int order = 1, id;
        String type, keyForClick, url;

        try {

            for (int j = 0; j < actionTypeBodyParams.length(); j++) {

                JSONObject actionBodyObject = actionTypeBodyParams.getJSONObject(j);
                String search = actionBodyObject.optString("search");
                String label = actionBodyObject.optString("label");
                id = actionBodyObject.optInt("id");
                type = actionBodyObject.optString("type");
                url = actionBodyObject.optString("url");
                if (actionTypeBody.contains(search)) {
                    switch (search) {
                        case "{item:$subject}":
                        case "{item:$object}":
                        case "{item:$owner}":

                            keyForClick = order + "-" + type + "-" + id;

                            if (mClickableParts.containsKey(keyForClick))
                                keyForClick += "-" + label;

                            mClickableParts.put(keyForClick, label);
                            if (type.equals("video")) {
                                if (attachmentArray != null && attachmentArray.length() != 0) {
                                    for (int k = 0; k < attachmentArray.length(); k++) {
                                        JSONObject singleAttachmentObject = attachmentArray.getJSONObject(k);
                                        String attachmentType = singleAttachmentObject.getString("attachment_type");
                                        if (attachmentType.equals("video")) {
                                            int attachmentId = singleAttachmentObject.getInt("attachment_id");
                                            String attachment_video_type = singleAttachmentObject.
                                                    optString("attachment_video_type");
                                            String attachment_video_url = singleAttachmentObject.
                                                    optString("attachment_video_url");
                                            String videoInfo = attachment_video_type + "-" + attachment_video_url;

                                            mVideoInformation.put(attachmentId, videoInfo);
                                        }

                                    }
                                }
                            }
                            ++order;
                            actionTypeBody = actionTypeBody.replace(search, "<b>" + label + "</b>");
                            break;
                        case "{body:$body}":
                            //Replacing "\n" because in HTML there is no such format for line breaking. (Html uses <br/>)
                            mActionTypeBody = label.replaceAll("\n", "<br/>");

                            if (userTagJsonArray != null && userTagJsonArray.length() > 0) {

                                mClickablePartsNew = new HashMap<>();
                                int serial = 1;

                                for (int k = 0; k < userTagJsonArray.length(); k++) {
                                    JSONObject singleTagJsonObject = userTagJsonArray.optJSONObject(k);
                                    String tagType = singleTagJsonObject.optString("type");
                                    int user_id = singleTagJsonObject.optInt("resource_id");
                                    String taggedFriendName = singleTagJsonObject.optString("resource_name");

                                    if (mActionTypeBody.contains(taggedFriendName)) {
                                        keyForClick = serial + "-" + tagType + "-" + user_id;
                                        label = label.replaceAll("\\s+", " ").trim();
                                        if (mClickablePartsNew.containsKey(keyForClick)) {
                                            keyForClick += "-" + taggedFriendName;
                                        }
                                        mClickablePartsNew.put(keyForClick, taggedFriendName);

                                        ++serial;
                                        mActionTypeBody = mActionTypeBody.replace(taggedFriendName, "<b>" + taggedFriendName + "</b>");
                                    }
                                }
                            }

                            actionTypeBody = actionTypeBody.replace(search, "");

                            // Removing all line breaking from the action type body,
                            // because we are not showing body in feed title,
                            // so if there is any line break in body it will show in feed body.
                            actionTypeBody = actionTypeBody.replaceAll("<br />", "");
                            break;
                        case "{var:$type}":
                            actionTypeBody = actionTypeBody.replace(search, label);
                            break;

                        case "{item:$object:topic}":
                            String slug = actionBodyObject.optString("slug");
                            keyForClick = order + "-" + type + "-" + id + "-" + slug;

                            mClickableParts.put(keyForClick, label);
                            actionTypeBody = actionTypeBody.replace(search, "<font color=\"#000000\">"
                                    + label + "</font>");
                            ++order;
                            break;

                        case "{itemParent:$object:forum}":
                            String forumSlug = actionBodyObject.optString("slug");
                            keyForClick = order + "-" + type + "-" + id + "-" + forumSlug;

                            mClickableParts.put(keyForClick, label);
                            actionTypeBody = actionTypeBody.replace(search, "<font color=\"#000000\">"
                                    + label + "</font>");
                            ++order;
                            break;

                        case "{actors:$subject:$object}":
                            keyForClick = order + "-" + type + "-" + id;
                            mClickableParts.put(keyForClick, label);
                            ++order;

                            JSONObject objectDetails = actionBodyObject.optJSONObject("object");
                            if (objectDetails != null) {
                                String object_type = objectDetails.getString("type");
                                int object_id = objectDetails.getInt("id");
                                String object_label = objectDetails.getString("label");

                                mClickableParts.put(order + "-" + object_type + "-" + object_id, object_label);
                                ++order;
                                actionTypeBody = actionTypeBody.replace(search, "<font color=\"#000000\"><b>"
                                        + label + " → " + object_label + "</font></b>");
                            }
                            break;

                        default:

                            // Making a part is clickable when it contains the url,
                            // so that if its not integrated then it can be opened in WebView.
                            if (url != null && !url.isEmpty()) {
                                keyForClick = order + "-" + type + "-" + id;
                                mClickableParts.put(keyForClick, label);
                                ++order;
                                actionTypeBody = actionTypeBody.replace(search, "<b>" + label + "</b>");
                            } else {
                                actionTypeBody = actionTypeBody.replace(search, label);
                            }
                            break;
                    }
                }
            }

            if (tagsJsonArray != null && tagsJsonArray.length() != 0) {
                actionTypeBody += " -  <font color=\"#a9a9a9\">" + getResources().getString(R.string.location_with)
                        + " </font>";
                for (int k = 0; k < tagsJsonArray.length(); k++) {

                    JSONObject singleTagJsonObject = tagsJsonArray.getJSONObject(k);
                    String tagType = singleTagJsonObject.getString("tag_type");
                    JSONObject tagObject = singleTagJsonObject.getJSONObject("tag_obj");
                    int user_id = tagObject.getInt("user_id");
                    String taggedFriendName = tagObject.getString("displayname");

                    if (k == 0) {
                        if (taggedFriendName != null && !taggedFriendName.isEmpty()) {
                            actionTypeBody += "<b>" + taggedFriendName + "</b>";
                            mClickableParts.put(order + "-" + tagType + "-" + user_id, taggedFriendName);
                            ++order;
                        }
                    } else if (k > 0 && tagsJsonArray.length() == 2) {
                        actionTypeBody += " " + getResources().getString(R.string.and) + "  " + "<b>" +
                                taggedFriendName + "<b>";
                        mClickableParts.put(order + "-" + tagType + "-" + user_id, taggedFriendName);
                        ++order;
                    } else if (tagsJsonArray.length() > 2) {
                        if (k < tagsJsonArray.length() - 1) {
                            actionTypeBody += "<font color=\"#a9a9a9\">" + ","
                                    + " </font> " + "<b>" + taggedFriendName + "<b>";
                            mClickableParts.put(order + "-" + tagType + "-" + user_id, taggedFriendName);
                            ++order;
                        } else {
                            actionTypeBody += " " + getResources().getString(R.string.and) + "  " + "<b>"
                                    + taggedFriendName + "<b>";
                            mClickableParts.put(order + "-" + tagType + "-" + user_id, taggedFriendName);
                            ++order;
                        }

                    }
                }
            }

            if (paramsJsonObject != null && paramsJsonObject.length() != 0) {
                JSONObject checkInJsonObject = paramsJsonObject.optJSONObject("checkin");
                if (checkInJsonObject != null && checkInJsonObject.length() != 0) {
                    String checkIn_type = checkInJsonObject.optString("type");
                    if (checkIn_type != null && !checkIn_type.isEmpty() && checkIn_type.equals("place")) {
                        mLocationLabel = checkInJsonObject.optString("label");
                        String locationPrefix = checkInJsonObject.optString("prefixadd");
                        String locationId = checkInJsonObject.optString("id");
                        mLatitude = checkInJsonObject.optDouble("latitude");
                        mLongitude = checkInJsonObject.optDouble("longitude");
                        mPlaceId = checkInJsonObject.optString("place_id");

                        String[] locationIdParts = locationId.split("_");
                        if (locationIdParts.length == 2) {
                            int location_id = Integer.parseInt(locationIdParts[1]);
                            String locationKey = "checkIn" + "-" + location_id;
                            mClickableParts.put(order + "-" + locationKey, mLocationLabel);
                            ++order;

                            if (tagsJsonArray != null && tagsJsonArray.length() != 0)
                                actionTypeBody += "  <font color=\"#a9a9a9\">" + locationPrefix + " </font>"
                                        + " " + "<b>" + mLocationLabel + "</b>";
                            else
                                actionTypeBody += " -  <font color=\"#a9a9a9\">" + locationPrefix + " </font>"
                                        + " " + "<b>" + mLocationLabel + "</b>";
                        }

                    }

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return actionTypeBody;
    }

    @Override
    public void onClick(View view) {

        int id = view.getId();

        switch (id) {
            case R.id.commentPostButton:
                String commentBody = mCommentBox.getText().toString().trim();
                if (mStickersEnabled == 1 && (commentBody.isEmpty() && mSelectPath.isEmpty())) {
                    if (mStickersPopup != null) {
                        StickersUtil.showStickersKeyboard();
                    }
                } else {
                    try {

                        byte[] bytes = mCommentBox.getText().toString().trim().getBytes("UTF-8");
                        commentBody = new String(bytes, Charset.forName("UTF-8"));

                        if ((commentBody.length() == 0 || commentBody.trim().isEmpty()) && mSelectPath.isEmpty()) {
                            Toast.makeText(this, getResources().getString(R.string.comment_empty_msg),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            params = new HashMap<>();

                            // Convert text smileys to emoticons in comments
                            commentBody = Smileys.getEmojiFromString(commentBody);
                            params.put("body", commentBody);

                            mCommentBox.setText("");
                            mCommentBox.setHint(getResources().getString(R.string.write_comment_text) + "…");

                            postComment(commentBody, null, null);
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                break;

            case R.id.comment_button:
            case R.id.counts_container:
                int photoPosition = (int) view.getTag();
                PhotoListDetails photoDetails = mPhotoDetails.get(photoPosition);
                int photoId = photoDetails.getPhotoId();

                String mLikeCommentsUrl = AppConstant.DEFAULT_URL + "likes-comments?subject_type="
                        + "album_photo" + "&subject_id=" + photoId + "&viewAllComments=1";
                Intent commentIntent = new Intent(this, Comment.class);
                commentIntent.putExtra("photoComment", true);
                commentIntent.putExtra("photoPosition", photoPosition);
                commentIntent.putExtra("LikeCommentUrl", mLikeCommentsUrl);
                commentIntent.putExtra(ConstantVariables.SUBJECT_TYPE, "album_photo");
                commentIntent.putExtra(ConstantVariables.SUBJECT_ID, photoId);
                commentIntent.putExtra("commentCount", mGetTotalComments);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                startActivityForResult(commentIntent, ConstantVariables.VIEW_COMMENT_PAGE_CODE);
                break;

            case R.id.photoUploadingButton:
                if (!mAppConst.checkManifestPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    mAppConst.requestForManifestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                            ConstantVariables.READ_EXTERNAL_STORAGE);
                } else {
                    startImageUploadActivity(mContext, MultiImageSelectorActivity.MODE_SINGLE, true, 1);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        int limit = firstVisibleItem + visibleItemCount;

        if (limit == totalItemCount && !isLoading) {
            if (limit >= COMMENT_LIMIT && (COMMENT_LIMIT * pageNumber) <
                    mCommentList.getmTotalCommmentCount()) {
                CustomViews.addFooterView(mCommentsListView, footerView);
                pageNumber += 1;
                String likeCommentsUrl = UrlUtil.AAF_LIKE_COMMNET_URL + "&action_id=" + mActionId + "&page=" + pageNumber;
                isLoading = true;
                loadMoreComments(likeCommentsUrl);

            }
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

        if ((charSequence != null && charSequence.length() > 0 &&
                !charSequence.toString().trim().isEmpty()) || (mAttachmentType != null && !mAttachmentType.isEmpty())) {
            mCommentPostButton.setText("\uf1d8");
            mCommentPostButton.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            ;
        } else if (mStickersEnabled == 1) {
            mCommentPostButton.setText("\uf118");
            mCommentPostButton.setTextColor(ContextCompat.getColor(this, R.color.gray_stroke_color));
        } else {
            mCommentPostButton.setTextColor(ContextCompat.getColor(this, R.color.gray_stroke_color));
        }

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    /**
     * Method to start ImageUploadingActivity (MultiImageSelector)
     *
     * @param context      Context of Class.
     * @param selectedMode Selected mode i.e. multi images or single image.
     * @param showCamera   Whether to display the camera.
     * @param maxNum       Max number of images allowed to pick in case of MODE_MULTI.
     */
    public void startImageUploadActivity(Context context, int selectedMode, boolean showCamera, int maxNum) {

        Intent intent;

        intent = new Intent(context, MultiImageSelectorActivity.class);
        // Whether photoshoot
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA, showCamera);
        // The maximum number of selectable image
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_COUNT, maxNum);
        // Select mode
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE, selectedMode);
        // The default selection
        if (mSelectPath != null && mSelectPath.size() > 0) {
            intent.putExtra(MultiImageSelectorActivity.EXTRA_DEFAULT_SELECTED_LIST, mSelectPath);
        }
        ((Activity) context).startActivityForResult(intent, ConstantVariables.REQUEST_IMAGE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ConstantVariables.READ_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission granted, proceed to the normal flow
                    startImageUploadActivity(mContext, MultiImageSelectorActivity.MODE_SINGLE, true, 1);
                } else {
                    // If user press deny in the permission popup
                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) mContext,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {

                        // Show an expanation to the user After the user
                        // sees the explanation, try again to request the permission.
                        mAlertDialogWithAction.showDialogForAccessPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                                ConstantVariables.READ_EXTERNAL_STORAGE);
                    }else{
                        // If user pressed never ask again on permission popup
                        // show snackbar with open app info button
                        // user can revoke the permission from Permission section of App Info.
                        SnackbarUtils.displaySnackbarOnPermissionResult(mContext, findViewById(R.id.rootView),
                                ConstantVariables.READ_EXTERNAL_STORAGE);
                    }
                }
                break;
        }
    }

    public void loadMoreComments(String commentListUrl) {

        mAppConst.getJsonResponseFromUrl(commentListUrl, new OnResponseListener() {
            @Override
            public void onTaskCompleted(JSONObject jsonObject) {
                CustomViews.removeFooterView(mCommentsListView, footerView);

                if (jsonObject.length() != 0) {
                    try {
                        mGetTotalComments = jsonObject.getInt("getTotalComments");
                        mCommentList.setmTotalCommmentCount(mGetTotalComments);
                        mAllCommentsArray = jsonObject.optJSONArray("viewAllComments");
                        addCommentsToList(mAllCommentsArray);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mCommentAdapter.notifyDataSetChanged();
                    isLoading = false;
                }
            }

            public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                SnackbarUtils.displaySnackbar(findViewById(R.id.main_content),message);
            }
        });
    }

    private void addCommentsToList(JSONArray commentsArray) {

        isCommentsLoading = true;

        try{
            if(commentsArray != null && commentsArray.length() != 0){
                for (int i = 0; i < commentsArray.length(); i++){
                    JSONObject commentInfoObject = commentsArray.getJSONObject(i);
                    int userId = commentInfoObject.optInt("user_id");
                    int commentId = commentInfoObject.optInt("comment_id");
                    String authorPhoto = commentInfoObject.optString("author_image_profile");
                    String authorTitle = commentInfoObject.optString("author_title");
                    String commentDate = commentInfoObject.optString("comment_date");
                    int likeCount = commentInfoObject.optInt("like_count");
                    JSONObject deleteJsonObject = commentInfoObject.optJSONObject("delete");
                    JSONObject likeJsonObject = commentInfoObject.optJSONObject("like");
                    JSONObject attachmentObject = commentInfoObject.optJSONObject("attachment");

                    String stickerImage = null;
                    if(attachmentObject != null){
                        stickerImage = attachmentObject.optString("image_profile");
                    }

                    /* CODE STARTS FOR  PREPARING Tags and comments body */
                    String commentBody = commentInfoObject.getString("comment_body");
                    JSONArray tagsJsonArray = commentInfoObject.optJSONArray("userTag");
                    JSONObject paramsJsonObject = commentInfoObject.optJSONObject("params");

                    if (tagsJsonArray != null && tagsJsonArray.length() != 0) {
                        commentBody = getCommentsBody(commentBody, tagsJsonArray, paramsJsonObject);
                    }

                    int isLike = 0;
                    if (likeJsonObject != null)
                        isLike = likeJsonObject.getInt("isLike");

                    mCommentListItems.add(new CommentList(userId, commentId, likeCount,
                            isLike, authorPhoto, authorTitle, commentBody, mClickableParts, commentDate,
                            deleteJsonObject, likeJsonObject, stickerImage));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getCommentsBody(String commentBody, JSONArray tagsJsonArray, JSONObject paramsJsonObject) {
        mClickableParts = new HashMap<>();

        int order = 1;

        // Make Tagged Friends Name Clickable
        if (tagsJsonArray != null && tagsJsonArray.length() != 0) {

            commentBody += " - " + mContext.getResources().getString(R.string.location_with);

            for (int k = 0; k < tagsJsonArray.length(); k++) {

                JSONObject singleTagJsonObject = tagsJsonArray.optJSONObject(k);
                String tagType = singleTagJsonObject.optString("type");
                int user_id = singleTagJsonObject.optInt("resource_id");
                String taggedFriendName = singleTagJsonObject.optString("resource_name");

                if (k == 0) {
                    if (taggedFriendName != null && !taggedFriendName.isEmpty()) {
                        commentBody += "<b>" + taggedFriendName + "</b>" ;
                        mClickableParts.put(order + "-" +  tagType + "-" + user_id, taggedFriendName);
                        ++order;
                    }
                } else if (k > 0 && tagsJsonArray.length() == 2) {
                    commentBody += " " + mContext.getResources().getString(R.string.and)
                            + " " + "<b>" + taggedFriendName + "<b>";
                    mClickableParts.put(order + "-" +  tagType + "-" + user_id, taggedFriendName);
                    ++order;
                } else if (tagsJsonArray.length() > 2) {
                    if(k < tagsJsonArray.length() -1){
                        commentBody += "," + "<b>" + taggedFriendName + "<b>";
                        mClickableParts.put(order + "-" +  tagType + "-" + user_id, taggedFriendName);
                        ++order;
                    }else{
                        commentBody += " " + mContext.getResources().getString(R.string.and) + "  "
                                + "<b>" + taggedFriendName + "<b>";
                        mClickableParts.put(order + "-" +  tagType + "-" + user_id, taggedFriendName);
                        ++order;
                    }
                }
            }
        }

        return commentBody;
    }

    private void showPhotosBlock(FeedList feedList) {

        if (feedList.getmAttachmentCount() != 0) {
            JSONArray mFeedAttachmentArray = feedList.getmFeedAttachmentArray();

            final String mFeedAttachmentType = feedList.getmFeedAttachmentType();
            String mFeedType = feedList.getmFeedType();

            if (mFeedAttachmentArray != null && mFeedAttachmentArray.length() != 0) {
                try {

                    final int mPhotoAttachmentCount = feedList.getmPhotoAttachmentCount();

                    mPhotoDetails = new ArrayList<>();
                    mPhotoPosition = 0;

                    for (int i = 0; i < mFeedAttachmentArray.length(); i++) {

                        JSONObject singleAttachmentObject = mFeedAttachmentArray.getJSONObject(i);
                        final String attachmentType = singleAttachmentObject.optString("attachment_type");
                        int mode = singleAttachmentObject.optInt("mode");

                        if (mode == 1 || mode == 2) {
                            if (mPhotoAttachmentCount > 0) {
                                // Show Photos in RecyclerView with horizontal scrolling
                                if (attachmentType.contains("_photo") || (mFeedAttachmentType != null
                                        && mFeedAttachmentType.contains("_photo")
                                        && mFeedType.equals("share"))) {

                                    // Add PhotoDetails in list to show in PhotoLightBox
                                    String mainImage = singleAttachmentObject.optString("image_main");
                                    JSONObject imageMainObj = singleAttachmentObject.optJSONObject("image_main");
                                    if (imageMainObj != null && imageMainObj.length() > 0) {
                                        mainImage = imageMainObj.optString("src");
                                    }
                                    final int album_id = singleAttachmentObject.optInt("album_id");
                                    final int photo_id = singleAttachmentObject.optInt("photo_id");
                                    final int likes_count = singleAttachmentObject.optInt("likes_count");
                                    final int comment_count = singleAttachmentObject.optInt("comment_count");
                                    final int is_like = singleAttachmentObject.optInt("is_like");
                                    String reactions = singleAttachmentObject.optString("reactions");
                                    boolean likeStatus = is_like != 0;
                                    if (mFeedAttachmentType != null && mFeedAttachmentType.equals("album_photo")) {
                                        mAlbumViewUrl = UrlUtil.ALBUM_VIEW_PAGE
                                                + album_id + "?gutter_menu=1";
                                    } else {
                                        mAlbumViewUrl = UrlUtil.ALBUM_VIEW_URL + album_id;
                                    }


                                    mPhotoDetails.add(new PhotoListDetails(photo_id, mainImage, likes_count,
                                            comment_count, likeStatus, attachmentType, reactions));

                                    View likeCommentView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).
                                            inflate(R.layout.photos_like_comment, mPhotosBlock, false);

                                    ImageView imageView = (ImageView) likeCommentView.findViewById(R.id.image);
                                    mLikeCommentBlock = (LinearLayout) likeCommentView.findViewById(R.id.feedFooterMenusBlock);

                                    if (mainImage != null && !mainImage.isEmpty()) {
                                        Picasso.with(mContext)
                                                .load(mainImage)
                                                .placeholder(R.color.white)
                                                .into(imageView);
                                    }
                                    mPhotosBlock.setVisibility(View.VISIBLE);
                                    imageView.setTag(mPhotoPosition);

                                    /* Open PhotoLightBox */
                                    imageView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            int position = (int) v.getTag();
                                            Bundle bundle = new Bundle();
                                            bundle.putSerializable(PhotoLightBoxActivity.EXTRA_IMAGE_URL_LIST, mPhotoDetails);
                                            Intent i = new Intent(mContext, PhotoLightBoxActivity.class);
                                            i.putExtra(ConstantVariables.ITEM_POSITION, position);
                                            i.putExtra(ConstantVariables.ENABLE_COMMENT_CACHE, true);
                                            i.putExtra(ConstantVariables.TOTAL_ITEM_COUNT, mPhotoAttachmentCount);
                                            i.putExtra(ConstantVariables.PHOTO_REQUEST_URL, mAlbumViewUrl);
                                            i.putExtra(ConstantVariables.SHOW_ALBUM_BUTTON, true);
                                            i.putExtra(ConstantVariables.SUBJECT_TYPE, mFeedAttachmentType);
                                            i.putExtra(ConstantVariables.IS_ALBUM_PHOTO_REQUEST, true);
                                            i.putExtra(ConstantVariables.TOTAL_ITEM_COUNT, mPhotoAttachmentCount);
                                            i.putExtra(ConstantVariables.ALBUM_ID, album_id);
                                            i.putExtras(bundle);
                                            ((Activity) mContext).startActivityForResult(i, ConstantVariables.VIEW_PAGE_CODE);
                                        }
                                    });

                                    mShareButton = (ActionIconThemedTextView) likeCommentView.findViewById(R.id.share_button);
                                    mLikeButton = (ActionIconThemedTextView) likeCommentView.findViewById(R.id.like_button);
                                    mCommentButton = (ActionIconThemedTextView) likeCommentView.findViewById(R.id.comment_button);

                                    mCounterView = (RelativeLayout) likeCommentView.findViewById(R.id.counts_container);
                                    mLikeCount = (ThemedTextView) likeCommentView.findViewById(R.id.like_count);
                                    mCommentCount = (ThemedTextView) likeCommentView.findViewById(R.id.comment_count);

                                    mCommentButton.setTag(mPhotoPosition);
                                    mCounterView.setTag(mPhotoPosition);
                                    mLikeButton.setTag(mPhotoPosition);
                                    mShareButton.setTag(mPhotoPosition);
                                    likeCommentView.setId(mPhotoPosition);

                                    // Hiding the like/comment/share button when user is logged out.
                                    if (mAppConst.isLoggedOutUser()) {
                                        mLikeCommentBlock.setVisibility(View.GONE);
                                    } else {
                                        mLikeCommentBlock.setVisibility(View.VISIBLE);
                                    }

                                    mPhotosBlock.addView(likeCommentView, i);

                                    setSinglePhotoLikeAndCommentCount(mPhotoDetails.get(i), i);

                                    mCommentButton.setOnClickListener(this);
                                    mCounterView.setOnClickListener(this);

                                    mShareButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            int clickedPosition = (int) view.getTag();
                                            PhotoListDetails photoDetails = mPhotoDetails.get(clickedPosition);
                                            String url = AppConstant.DEFAULT_URL + "activity/share";
                                            url += "?type=album_photo" + "&id=" + photoDetails.getPhotoId();

                                            socialShareUtil.sharePost(view, null, photoDetails.getImageUrl(),
                                                    url, "image", photoDetails.getImageUrl());
                                        }
                                    });

                                    mLikeButton.setOnLongClickListener(this);

                                    mLikeButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            int clickedPosition = (int) view.getTag();
                                            PhotoListDetails photoDetails = mPhotoDetails.get(clickedPosition);

                                            int reactionId = 0;
                                            String reactionIcon = null, caption = null;

                                            if (mReactionsEnabled == 1 && PreferencesUtils.isNestedCommentEnabled(mContext)) {
                                                reactionId = mReactions.optJSONObject("like").optInt("reactionicon_id");
                                                reactionIcon = mReactions.optJSONObject("like").optJSONObject("icon").
                                                        optString("reaction_image_icon");
                                                caption = getResources().getString(R.string.like_text);
                                            }
                                            doLikeUnlike(photoDetails, null, false, reactionId, reactionIcon,
                                                    caption, clickedPosition);

                                        }
                                    });
                                }

                            } else {
                                mPhotosBlock.setVisibility(View.GONE);
                            }
                        }
                        ++mPhotoPosition;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    /**
     * Funtion to perform Like/Unlike Option on a photo
     *
     * @param photoListDetails
     * @param reaction
     * @param isReactionChanged
     * @param reactionId
     * @param reactionIcon
     * @param caption
     */
    private void doLikeUnlike(PhotoListDetails photoListDetails, String reaction, final boolean isReactionChanged, final int reactionId,
                              final String reactionIcon, final String caption, int photoPosition) {
//        try {

        final String sendLikeNotificationUrl = AppConstant.DEFAULT_URL + "advancedcomments/send-like-notitfication";
        final Map<String, String> likeParams = new HashMap<>();
        likeParams.put(ConstantVariables.SUBJECT_TYPE, mFeedList.getmFeedAttachmentType());
        likeParams.put(ConstantVariables.SUBJECT_ID, String.valueOf(photoListDetails.getPhotoId()));

        if (reaction != null) {
            likeParams.put("reaction", reaction);
        }

        /**
         * If Photo is not already liked...
         *  -- Increase the Like count and change color of Like Button
         *  -- Remove the left drawable from Like Button and show the reaction image and reaction text on Like Button
         *  -- if reactions and nestedcomment is enabled and show the popular reactions
         *
         *  Else If photo is already Like and Reaction is changed on that photo
         *   -- Just change the myReaction object and change the reaction icon and text on like Button
         *   -- Show the updated 3 popular reactions.
         *
         */
        if (!photoListDetails.isLiked() || isReactionChanged) {
            if (mReactionsEnabled == 1 && PreferencesUtils.isNestedCommentEnabled(mContext)) {
                mLikeUnlikeUrl = AppConstant.DEFAULT_URL + "advancedcomments/like?sendNotification=0";
                updateSinglePhotoReactions(reactionId, reactionIcon, caption, photoPosition, isReactionChanged);
            } else {
                mLikeUnlikeUrl = AppConstant.DEFAULT_URL + "like?sendNotification=0";
            }
            if (!isReactionChanged) {
                photoListDetails.setImageLikeCount(photoListDetails.getImageLikeCount() + 1);
            }
            photoListDetails.setIsLiked(true);
            setSinglePhotoLikeAndCommentCount(photoListDetails, photoPosition);
            updateClickedFeedData(photoPosition);
        } else {
            mLikeUnlikeUrl = AppConstant.DEFAULT_URL + "unlike";
            if (mReactionsEnabled == 1 && PreferencesUtils.isNestedCommentEnabled(mContext)) {
                updateSinglePhotoReactions(reactionId, reactionIcon, caption, photoPosition, false);
            }
            photoListDetails.setImageLikeCount(photoListDetails.getImageLikeCount() - 1);
            photoListDetails.setIsLiked(false);
            setSinglePhotoLikeAndCommentCount(photoListDetails, photoPosition);
            updateClickedFeedData(photoPosition);
        }

        mAppConst.postJsonResponseForUrl(mLikeUnlikeUrl, likeParams, new OnResponseListener() {
            @Override
            public void onTaskCompleted(JSONObject jsonObject) {
                if (mReactionsEnabled == 1 && PreferencesUtils.isNestedCommentEnabled(mContext)) {

                    /* Calling to send notifications after like action */
                    mAppConst.postJsonResponseForUrl(sendLikeNotificationUrl, likeParams, new OnResponseListener() {
                        @Override
                        public void onTaskCompleted(JSONObject jsonObject) throws JSONException {

                        }

                        @Override
                        public void onErrorInExecutingTask(String message, boolean isRetryOption) {

                        }
                    });
                }
            }

            @Override
            public void onErrorInExecutingTask(String message, boolean isRetryOption) {

            }
        });
    }


    private void updateSinglePhotoReactions(int reactionId, String reactionIcon, String caption,
                                            int photoPosition, boolean isReactionChanged) {

        try {

            JSONObject mReactionsObject = null, mPhotoReactions = null, myReactions = null;

            PhotoListDetails photoDetails = mPhotoDetails.get(photoPosition);
            boolean isLiked = photoDetails.isLiked();

            if (photoDetails.getmReactionsObject() != null && !photoDetails.getmReactionsObject().isEmpty()) {
                try {
                    mReactionsObject = new JSONObject(photoDetails.getmReactionsObject());
                    mPhotoReactions = mReactionsObject.optJSONObject("feed_reactions");
                    myReactions = mReactionsObject.optJSONObject("my_feed_reaction");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                mReactionsObject = new JSONObject();
            }


            if (mReactionsObject != null) {
                // Update the count of previous reaction in reactions object and remove the my_feed_reactions
                if (isLiked) {
                    if (myReactions != null && mPhotoReactions != null) {
                        int myReactionId = myReactions.optInt("reactionicon_id");
                        if (mPhotoReactions.optJSONObject(String.valueOf(myReactionId)) != null) {
                            int myReactionCount = mPhotoReactions.optJSONObject(String.valueOf(myReactionId)).
                                    optInt("reaction_count");
                            if ((myReactionCount - 1) <= 0) {
                                mPhotoReactions.remove(String.valueOf(myReactionId));
                            } else {
                                mPhotoReactions.optJSONObject(String.valueOf(myReactionId)).put("reaction_count",
                                        myReactionCount - 1);
                            }
                            mReactionsObject.put("feed_reactions", mPhotoReactions);
                        }
                    }
                    mReactionsObject.put("my_feed_reaction", null);
                }

                // Update the count of current reaction in reactions object and set new my_feed_reactions object.
                if (!isLiked || isReactionChanged) {
                    // Set the updated my Reactions
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.putOpt("reactionicon_id", reactionId);
                    jsonObject.putOpt("reaction_image_icon", reactionIcon);
                    jsonObject.putOpt("caption", caption);
                    mReactionsObject.put("my_feed_reaction", jsonObject);

                    if (mPhotoReactions != null) {
                        if (mPhotoReactions.optJSONObject(String.valueOf(reactionId)) != null) {
                            int reactionCount = mPhotoReactions.optJSONObject(String.valueOf(reactionId)).optInt("reaction_count");
                            mPhotoReactions.optJSONObject(String.valueOf(reactionId)).putOpt("reaction_count", reactionCount + 1);
                        } else {
                            jsonObject.put("reaction_count", 1);
                            mPhotoReactions.put(String.valueOf(reactionId), jsonObject);
                        }
                    } else {
                        mPhotoReactions = new JSONObject();
                        jsonObject.put("reaction_count", 1);
                        mPhotoReactions.put(String.valueOf(reactionId), jsonObject);
                    }
                    mReactionsObject.put("feed_reactions", mPhotoReactions);
                }

                photoDetails.setmReactionsObject(mReactionsObject.toString());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setSinglePhotoLikeAndCommentCount(PhotoListDetails photoDetails, int photoPosition) {

        JSONObject mContentReactionsObject, mPhotoReactions = null, myReactions = null;
        boolean isLiked = photoDetails.isLiked();
        int likeCount = photoDetails.getImageLikeCount();
        int commentCount = photoDetails.getImageCommentCount();

        if (photoDetails.getmReactionsObject() != null && !photoDetails.getmReactionsObject().isEmpty()) {
            try {
                mContentReactionsObject = new JSONObject(photoDetails.getmReactionsObject());
                mPhotoReactions = mContentReactionsObject.optJSONObject("feed_reactions");
                myReactions = mContentReactionsObject.optJSONObject("my_feed_reaction");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        // Getting view for each photo.
        View view = mPhotosBlock.getChildAt(photoPosition);

        RelativeLayout counterView = (RelativeLayout) view.findViewById(R.id.counts_container);
        ActionIconThemedTextView likeButton = (ActionIconThemedTextView) view.findViewById(R.id.like_button);
        ThemedTextView likeCountView = (ThemedTextView) view.findViewById(R.id.like_count);
        ThemedTextView commentCountView = (ThemedTextView) view.findViewById(R.id.comment_count);
        LinearLayout popularReactionsView = (LinearLayout) view.findViewById(R.id.popularReactionIcons);
        ImageView reactionIcon = (ImageView) view.findViewById(R.id.reactionIcon);

        // Setting the Like status on LikeButton.
        likeButton.setActivated(isLiked);

        if (!isLiked) {
            reactionIcon.setVisibility(View.GONE);
            likeButton.setText(getResources().getString(R.string.like_text));
            likeButton.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(this, R.drawable.ic_thumb_up_white_18dp),
                    null, null, null);
            likeButton.setTextColor(
                    ContextCompat.getColor(this, R.color.grey_dark));
        } else {
    /*
        If Reactions and Nestedcomments are enabled
            -- Show Reaction Icon and Reactions Text On Like Button
        else
            -- Show Only Like Icon
     */
            likeButton.setTextColor(
                    ContextCompat.getColor(this, R.color.colorPrimary));

            if (mReactionsEnabled == 1 && PreferencesUtils.isNestedCommentEnabled(mContext)) {
                if (myReactions != null && myReactions.length() != 0) {
                    String reactionImage = myReactions.optString("reaction_image_icon");

                    likeButton.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    reactionIcon.setVisibility(View.VISIBLE);
                    Picasso.with(this)
                            .load(reactionImage)
                            .into(reactionIcon);
                    likeButton.setText(myReactions.optString("caption"));
                } else {
                    reactionIcon.setVisibility(View.GONE);
                    likeButton.setCompoundDrawablesWithIntrinsicBounds(
                            ContextCompat.getDrawable(this, R.drawable.ic_thumb_up_white_18dp),
                            null, null, null);
                    likeButton.setText(this.getResources().
                            getString(R.string.like_text));
                }
            } else {
                likeButton.setCompoundDrawablesWithIntrinsicBounds(
                        ContextCompat.getDrawable(this, R.drawable.ic_thumb_up_white_18dp),
                        null, null, null);
                likeButton.setText(getResources().getString(R.string.like_text));
            }
        }

        // Hide count container if like and comment count are 0.
        if (likeCount == 0 && commentCount == 0) {
            counterView.setVisibility(View.GONE);
        } else {
            counterView.setVisibility(View.VISIBLE);
        }


        if (mReactionsEnabled == 1 && PreferencesUtils.isNestedCommentEnabled(mContext)) {

            // Show 3 popular reactions
            if (mPhotoReactions != null && mPhotoReactions.length() != 0) {
                popularReactionsView.removeAllViews();
                JSONArray reactionIds = mPhotoReactions.names();
                popularReactionsView.setVisibility(View.VISIBLE);
                for (int j = 0; j < mPhotoReactions.length() && j < 3; j++) {
                    String imageUrl = mPhotoReactions.optJSONObject(reactionIds.optString(j)).
                            optString("reaction_image_icon");
                    int reactionId = mPhotoReactions.optJSONObject(reactionIds.optString(j)).
                            optInt("reactionicon_id");

                    ImageView imageView = CustomViews.generateReactionImageView(this, reactionId, imageUrl);

                    popularReactionsView.addView(imageView);
                }
            } else {
                popularReactionsView.setVisibility(View.GONE);
            }
        } else {
            popularReactionsView.setVisibility(View.GONE);
        }

        // Showing photos like count.
        if (likeCount > 0) {
            likeCountView.setVisibility(View.VISIBLE);

            // Set Like Count
            if (mReactionsEnabled == 1 && PreferencesUtils.isNestedCommentEnabled(mContext)) {
                // You and count others
                if (isLiked) {
                    if (likeCount == 1) {
                        likeCountView.setText(mContext.getResources().getString(R.string.reaction_string));
                    } else {
                        String likeText = mContext.getResources().getQuantityString(R.plurals.others,
                                likeCount - 1, likeCount - 1);
                        likeCountView.setText(String.format(mContext.getResources().getString(R.string.reaction_text_format),
                                getResources().getString(R.string.you_and_text), likeText
                        ));
                    }
                } else {
                    // Only count
                    likeCountView.setText(Integer.toString(likeCount));
                }
            } else {
                String likeText = mContext.getResources().getQuantityString(R.plurals.profile_page_like,
                        likeCount);
                likeCountView.setText(String.format(getResources().getString(R.string.like_count_text),
                        likeCount, likeText));
            }
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mCommentCount.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
            commentCountView.setLayoutParams(layoutParams);

        } else {
            likeCountView.setVisibility(View.GONE);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mCommentCount.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_END, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
            commentCountView.setLayoutParams(layoutParams);
        }

        // Showing the comment count for each image view.
        if (commentCount != 0) {
            commentCountView.setVisibility(View.VISIBLE);
            String commentText = mContext.getResources().getQuantityString(R.plurals.profile_page_comment,
                    commentCount);
            commentCountView.setText(Html.fromHtml(String.format(
                    mContext.getResources().getString(R.string.comment_count_text),
                    commentCount, commentText)));
        } else {
            commentCountView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {

            case ConstantVariables.VIEW_LIGHT_BOX:
                // When there is single image at single feed page
                // and any changes occured at photoLightBox page then update the like/comment count.
                if (resultCode == ConstantVariables.LIGHT_BOX_EDIT && data != null) {
                    Bundle bundle = data.getExtras();
                    int feedPosition = bundle.getInt(ConstantVariables.ITEM_POSITION);
                    int likeCount = bundle.getInt(ConstantVariables.PHOTO_LIKE_COUNT);
                    int commentCount = bundle.getInt(ConstantVariables.PHOTO_COMMENT_COUNT);
                    int isLike = bundle.getBoolean(ConstantVariables.IS_LIKED) ? 1 : 0;

                    if (mFeedItemsList != null && mFeedItemsList.size() != 0) {
                        FeedList selectedFeedRow = (FeedList) mFeedItemsList.get(feedPosition);

                        // When the single feed page is loaded from feed list
                        // and the comment is updated on single photo comment page.
                        if (mFeedList == null && selectedFeedRow.getmCommentCount() != commentCount) {
                            sendRequestToServer(mCommentListUrl);
                        }
                        selectedFeedRow.setmCommentCount(commentCount);
                        selectedFeedRow.setmLikeCount(likeCount);
                        selectedFeedRow.setmIsLike(isLike);
                        mFeedAdapter.updatePhotoLikeCommentCount(feedPosition);
                        mFeedAdapter.notifyItemChanged(feedPosition);
                    }
                }
                break;

            case ConstantVariables.VIEW_COMMENT_PAGE_CODE:
                // When comment page is opened from any image(from multiple images) then update the comment count.
                if (resultCode == ConstantVariables.VIEW_COMMENT_PAGE_CODE && data != null) {
                    Bundle bundle = data.getExtras();
                    int feedPosition = bundle.getInt(ConstantVariables.ITEM_POSITION);

                    if (bundle.getBoolean(ConstantVariables.IS_PHOTO_COMMENT)) {
                        setClickedPhotoCommentCount(bundle);

                    } else if (mFeedItemsList != null && mFeedItemsList.size() != 0) {
                        FeedList feedList = (FeedList) mFeedItemsList.get(feedPosition);
                        feedList.setmCommentCount(bundle.getInt(ConstantVariables.PHOTO_COMMENT_COUNT));
                        mFeedAdapter.updatePhotoLikeCommentCount(feedPosition);
                        mFeedAdapter.notifyItemChanged(feedPosition);
                    }
                    break;
                }
                break;

            case ConstantVariables.VIEW_PAGE_CODE:
                // When user clicked on a single image from multiple images
                // and any changes occured at photolightbox then update the like/comment count for images.
                if (resultCode == ConstantVariables.LIGHT_BOX_EDIT && data != null) {
                    mPhotoDetails = (ArrayList<PhotoListDetails>) data.getSerializableExtra(PhotoLightBoxActivity.EXTRA_IMAGE_URL_LIST);

                    if (mPhotoDetails.size() > 1) {
                        updateClickedFeedData(0);
                    }
                    setPhotosLikeCommentCount();
                }
                break;

            case ConstantVariables.STICKER_STORE_REQUEST:
                if (resultCode == ConstantVariables.STICKER_STORE_REQUEST) {
                    JSONObject jsonObject = ConstantVariables.STICKERS_STORE_ARRAY;
                    JSONArray collectionIds = jsonObject.names();
                    for (int i = 0; i < jsonObject.length(); i++) {
                        JSONObject stickerStoreObject = jsonObject.optJSONObject(collectionIds.optString(i));
                        String stickerAction = stickerStoreObject.optString("action");
                        int collectionId = stickerStoreObject.optInt("collection_id");

                        if (stickerAction.equals("add")) {
                            StickersGridView stickersGridView = new StickersGridView(mContext, collectionId, mStickersPopup);
                            mStickersPopup.viewsList.add(stickersGridView);
                            mStickersPopup.stickerGridViewList.put(collectionId, stickersGridView);
                            ((PagerAdapter) mStickersPopup.myAdapter).notifyDataSetChanged();
                            mStickersPopup.mCollectionsList.put(stickerStoreObject);
                            mStickersPopup.setupTabIcons();
                        } else {
                            mStickersPopup.viewsList.remove(mStickersPopup.stickerGridViewList.get(collectionId));
                            mStickersPopup.stickerGridViewList.remove(collectionId);
                            ((PagerAdapter) mStickersPopup.myAdapter).notifyDataSetChanged();
                            for (int j = 0; j < mStickersPopup.mCollectionsList.length(); j++) {
                                JSONObject collectionObject = mStickersPopup.mCollectionsList.optJSONObject(j);
                                int collection_id = collectionObject.optInt("collection_id");

                                if (collection_id == collectionId) {
                                    mStickersPopup.mCollectionsList.remove(j);
                                    break;
                                }
                            }
                            mStickersPopup.setupTabIcons();
                        }
                    }
                    ConstantVariables.STICKERS_STORE_ARRAY = new JSONObject();
                }
                break;

            case ConstantVariables.REQUEST_IMAGE:
                if (resultCode == RESULT_OK) {

                    if (mSelectPath != null) {
                        mSelectPath.clear();
                    }
                    // Getting image path from uploaded images.
                    mSelectPath = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
                    //Checking if there is any image or not.
                    if (mSelectPath != null) {
                        showSelectedImages(mSelectPath);
                    }

                } else if (resultCode != RESULT_CANCELED) {
                    // failed to capture image
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.image_capturing_failed),
                            Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    private void updateClickedFeedData(int photoPosition) {

        try {
            if (photoPosition == 0 && mFeedItemsList != null && mFeedItemsList.size() != 0) {
                PhotoListDetails photoDetails = mPhotoDetails.get(photoPosition);
                FeedList selectedFeedRow = (FeedList) mFeedItemsList.get(1);
                selectedFeedRow.setmCommentCount(photoDetails.getImageCommentCount());
                selectedFeedRow.setmLikeCount(photoDetails.getImageLikeCount());
                selectedFeedRow.setmIsLike(photoDetails.isLiked() ? 1 : 0);
                String reactions = photoDetails.getmReactionsObject();
                if (reactions != null) {
                    JSONObject reactionsObject = new JSONObject(reactions);
                    selectedFeedRow.setmFeedReactions(reactionsObject.optJSONObject("feed_reactions"));
                    selectedFeedRow.setmMyFeedReactions(reactionsObject.optJSONObject("my_feed_reaction"));
                }
                mFeedAdapter.notifyItemChanged(1);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to show like and comment count if there are multiple images are showing on single feed.
     */
    public void setPhotosLikeCommentCount() {

        if (mPhotoDetails != null) {
            for (int i = 0; i < mPhotoDetails.size(); i++) {
                PhotoListDetails photoDetails = mPhotoDetails.get(i);
                setSinglePhotoLikeAndCommentCount(photoDetails, i);
            }
        }
    }

    /**
     * Method to updated comment count for the image on which comment is clicked.
     *
     * @param bundle Bundle which contains the data.
     */
    public void setClickedPhotoCommentCount(Bundle bundle) {

        int photoPosition = bundle.getInt(ConstantVariables.PHOTO_POSITION);
        int photoCommentCount = bundle.getInt(ConstantVariables.PHOTO_COMMENT_COUNT);
        PhotoListDetails photoListDetails = mPhotoDetails.get(photoPosition);
        photoListDetails.setmImageCommentCount(photoCommentCount);

        // Getting view for the clicked position.
        View view = mPhotosBlock.findViewById(photoPosition);
        ThemedTextView commentCountView = (ThemedTextView) view.findViewById(R.id.comment_count);

        // Showing the comment count on counter container view.
        if (photoCommentCount != 0) {
            String commentText = mContext.getResources().getQuantityString(R.plurals.profile_page_comment,
                    photoCommentCount);
            commentCountView.setText(Html.fromHtml(String.format(
                    mContext.getResources().getString(R.string.comment_count_text),
                    photoCommentCount, commentText
            )));
            commentCountView.setVisibility(View.VISIBLE);
            view.findViewById(R.id.counts_container).setVisibility(View.VISIBLE);
        } else {
            commentCountView.setVisibility(View.GONE);
        }


        // Update Feeds Comment Count if the comment is posted on first photo
        if (photoPosition == 0 && mFeedItemsList != null && mFeedItemsList.size() != 0) {
            FeedList feedList = (FeedList) mFeedItemsList.get(1);
            feedList.setmCommentCount(bundle.getInt(ConstantVariables.PHOTO_COMMENT_COUNT));
            mFeedAdapter.notifyItemChanged(1);
        }
    }

    /**
     * Method to updated attachment array if there are multiple images in the list.
     *
     * @param feedList FeedList which contains all the feed data.
     * @return Returns the updated attachment array.
     */
    public JSONArray updateAttachmentArrayOnBackPressed(FeedList feedList) {

        int photoAttachmentCount = feedList.getmPhotoAttachmentCount();
        JSONArray feedAttachmentArray = feedList.getmFeedAttachmentArray();

        // Updating the attachment array if it is for multiple images.
        if (photoAttachmentCount > 1 && mPhotoDetails != null && mPhotoDetails.size() > 0
                && feedAttachmentArray != null && feedAttachmentArray.length() > 0) {

            for (int i = 0; i < feedAttachmentArray.length(); i++) {
                JSONObject singleAttachmentObject = feedAttachmentArray.optJSONObject(i);
                PhotoListDetails photoListDetails = mPhotoDetails.get(i);
                try {
                    singleAttachmentObject.put("likes_count", photoListDetails.getImageLikeCount());
                    singleAttachmentObject.put("comment_count", photoListDetails.getImageCommentCount());
                    singleAttachmentObject.put("is_like", (photoListDetails.isLiked() ? 1 : 0));
                    String reactions = photoListDetails.getmReactionsObject();
                    if (mReactionsEnabled == 1 && PreferencesUtils.isNestedCommentEnabled(mContext) && reactions != null) {
                        singleAttachmentObject.put("reactions", new JSONObject(reactions));
                    }
                    feedAttachmentArray.put(i, singleAttachmentObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            feedList.setmFeedAttachmentArray(feedAttachmentArray);
        }
        return feedList.getmFeedAttachmentArray();
    }

    @Override
    public void onFeedDisableComment(boolean isCommentEnabled) {
        if (isCommentEnabled) {
            mCommentPostBlock.setVisibility(View.VISIBLE);
        } else {
            mCommentPostBlock.setVisibility(View.GONE);
        }

    }

    private void postComment(String commentBody, String stickerGuid, String stickerImage) {

        Bitmap bitmap = null;
        if (mStickersEnabled == 1) {
            mCommentPostButton.setText("\uf118");
        } else {
            mCommentPostButton.setText("\uf1d8");
        }
        if(mStickersParentView.getVisibility() == View.GONE){
            mCommentPostButton.setTextColor(ContextCompat.getColor(this, R.color.gray_stroke_color));
        }

        try {

            if (stickerGuid != null) {
                params.put("attachment_type", "sticker");
                params.put("attachment_id", stickerGuid);
            } else if (mSelectPath != null && !mSelectPath.isEmpty()) {
                params.put("attachment_type", mAttachmentType);
                bitmap = BitmapUtils.decodeSampledBitmapFromFile(this, mSelectPath.get(0),
                        width, (int) getResources().getDimension(R.dimen.feed_attachment_image_height), false);
            }

            // Show Comment instantly when user post it without like/delete options.

            if (PreferencesUtils.getUserDetail(this) != null) {

                JSONObject userDetail = new JSONObject(PreferencesUtils.getUserDetail(this));
                String profileIconImage = userDetail.getString("image_profile");
                int mLoggedInUserId = userDetail.getInt("user_id");
                String userName = userDetail.optString("displayname");
                mCommentListItems.add(new CommentList(mLoggedInUserId, userName, profileIconImage,
                        commentBody, stickerImage, true, bitmap));
                mCommentAdapter.notifyDataSetChanged();

                // Scroll scrollview to the bottom when any new comment is posted
                mCommentsListView.post( new Runnable() {
                    @Override
                    public void run() {
                        //call smooth scroll
                        ((ScrollView)findViewById(R.id.scrollview)).fullScroll(View.FOCUS_DOWN);
                    }
                });
            }

            if (commentBody != null) {
                params.put("body", commentBody);
            }
            params.put("send_notification", "0");
            params.put(ConstantVariables.ACTION_ID, String.valueOf(mActionId));

            // Playing postSound effect when comment is posted.
            if (PreferencesUtils.isSoundEffectEnabled(this)) {
                SoundUtil.playSoundEffectOnPost(this);
            }

            if (mSelectPath != null && mSelectPath.size() != 0) {
                mSelectedImageBlock.setVisibility(View.GONE);

                // Uploading files in background with the details.
                new UploadAttachmentUtil(SingleFeedPage.this, mCommentPostUrl, params,
                        mSelectPath).execute();
            } else{

                mAppConst.postJsonResponseForUrl(mCommentPostUrl, params, new OnResponseListener() {
                    @Override
                    public void onTaskCompleted(JSONObject jsonObject) {
                        addCommentToList(jsonObject);
                    }

                    @Override
                    public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                        mCommentListItems.remove(mCommentAdapter.getCount() - 1);
                        SnackbarUtils.displaySnackbar(findViewById(R.id.main_content), message);
                    }

                });
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addCommentToList(JSONObject jsonObject) {

        int commentId = 0;
        String stickerImage = null;

        try {
            int userId = jsonObject.optInt("user_id");
            commentId = jsonObject.getInt("comment_id");
            String authorPhoto = jsonObject.getString("image_icon");
            String authorTitle = jsonObject.getString("author_title");
            String commentDate = jsonObject.getString("comment_date");
            int likeCount = jsonObject.optInt("like_count");
            JSONObject deleteJsonObject = jsonObject.optJSONObject("delete");
            JSONObject likeJsonObject = jsonObject.optJSONObject("like");
            JSONObject attachmentObject = jsonObject.optJSONObject("attachment");
            if (attachmentObject != null) {
                stickerImage = attachmentObject.optString("image_profile");
            }

            /* CODE STARTS FOR  PREPARING Tags and comments body */
            String commentBody = jsonObject.getString("comment_body");
            JSONArray tagsJsonArray = jsonObject.optJSONArray("userTag");
            JSONObject paramsJsonObject = jsonObject.optJSONObject("params");

            if (tagsJsonArray != null && tagsJsonArray.length() != 0) {
                commentBody = getCommentsBody(commentBody, tagsJsonArray, paramsJsonObject);
            }


            int isLike = 0;
            if (likeJsonObject != null)
                isLike = likeJsonObject.optInt("isLike");
            /*
                Remove the instantly added comment from the adapter after comment posted
                successfully and add a comment in adapter with full details.
            */
            mCommentListItems.remove(mCommentListItems.size() - 1);
            mCommentListItems.add(new CommentList(userId, commentId, likeCount,
                    isLike, authorPhoto, authorTitle, commentBody, mClickableParts, commentDate,
                    deleteJsonObject, likeJsonObject, stickerImage));

            mCommentList.setmTotalCommmentCount(mCommentList.getmTotalCommmentCount() + 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mCommentAdapter.notifyDataSetChanged();

        // Scroll scrollview to the bottom when any new comment is posted
        mCommentsListView.post( new Runnable() {
            @Override
            public void run() {
                //call smooth scroll
                ((ScrollView)findViewById(R.id.scrollview)).fullScroll(View.FOCUS_DOWN);
            }
        });

        params.put("comment_id", String.valueOf(commentId));
        params.remove("body");
        params.remove("send_notification");

        mAppConst.postJsonResponseForUrl(mCommentNotificationUrl, params, new OnResponseListener() {
            @Override
            public void onTaskCompleted(JSONObject jsonObject) {

            }

            @Override
            public void onErrorInExecutingTask(String message, boolean isRetryOption) {

            }
        });
    }

    /**
     * Method to show selected images.
     *
     * @param mSelectPath list of selected images.
     */
    public void showSelectedImages(final ArrayList<String> mSelectPath) {

        mAttachmentType = "photo";
        mCommentPostButton.setText("\uf1d8");
        mCommentPostButton.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        mSelectedImageBlock.setVisibility(View.VISIBLE);

        for (final String imagePath : mSelectPath) {

            // Getting Bitmap from its real path.
            Bitmap bitmap = BitmapUtils.decodeSampledBitmapFromFile(this, imagePath,
                    (int) getResources().getDimension(R.dimen.profile_image_width_height),
                    (int) getResources().getDimension(R.dimen.profile_image_width_height), false);

            // If there is any null image then remove from image path.
            if (bitmap != null) {
                // Creating ImageView & params for this and adding selected images in this view.
                mSelectedImageView.setImageBitmap(bitmap);

                // Setting OnClickListener on cancelImage.
                mCancelImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mSelectPath.remove(imagePath);

                        // If canceled all the selected images then hide photoBlockLayout
                        // and enabled other attachement click.
                        if (mSelectPath.isEmpty()) {
                            mAttachmentType = null;
                            mSelectedImageBlock.setVisibility(View.GONE);
                            if (mCommentBox.getText().toString().trim().isEmpty()) {
                                if (mStickersEnabled == 1) {
                                    mCommentPostButton.setText("\uf118");
                                } else {
                                    mCommentPostButton.setText("\uf1d8");
                                }
                                mCommentPostButton.setTextColor(ContextCompat.getColor(mContext, R.color.gray_stroke_color));
                            }

                        }
                    }
                });
            }
        }
    }

    @Override
    public boolean onLongClick(View v) {
        int[] location = new int[2];

        final int clickedPosition = (int) v.getTag();
        View singlePhotoBlock = mPhotosBlock.findViewById(clickedPosition);
        final PhotoListDetails photoDetails = mPhotoDetails.get(clickedPosition);
        final String reactions = photoDetails.getmReactionsObject();
        RelativeLayout counterView = (RelativeLayout) singlePhotoBlock.findViewById(R.id.counts_container);
        counterView.getLocationOnScreen(location);
        RecyclerView reactionsRecyclerView = new RecyclerView(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        reactionsRecyclerView.setHasFixedSize(true);
        reactionsRecyclerView.setLayoutManager(linearLayoutManager);
        reactionsRecyclerView.setItemAnimator(new DefaultItemAnimator());

        final PopupWindow popUp = new PopupWindow(reactionsRecyclerView, LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        popUp.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.shape));
        popUp.setTouchable(true);
        popUp.setFocusable(true);
        popUp.setOutsideTouchable(true);
        popUp.setAnimationStyle(R.style.customDialogAnimation);

        // Playing popup effect when user long presses on like button of a feed.
        if (PreferencesUtils.isSoundEffectEnabled(this)) {
            SoundUtil.playSoundEffectOnReactionsPopup(this);
        }
        popUp.showAtLocation(reactionsRecyclerView, Gravity.TOP, location[0], location[1]);

        if (mReactions != null && mReactionsArray != null) {

            reactionsImages = new ArrayList<>();
            for (int i = 0; i < mReactionsArray.size(); i++) {
                JSONObject reactionObject = mReactionsArray.get(i);
                String reaction_image_url = reactionObject.optJSONObject("icon").
                        optString("reaction_image_icon");
                String caption = reactionObject.optString("caption");
                String reaction = reactionObject.optString("reaction");
                int reactionId = reactionObject.optInt("reactionicon_id");
                String reactionIconUrl = reactionObject.optJSONObject("icon").
                        optString("reaction_image_icon");
                reactionsImages.add(new ImageViewList(reaction_image_url, caption,
                        reaction, reactionId, reactionIconUrl));
            }

            ImageAdapter reactionsAdapter = new ImageAdapter(this, reactionsImages, true,
                    new OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {


                            ImageViewList imageViewList = reactionsImages.get(position);
                            String reaction = imageViewList.getmReaction();
                            String caption = imageViewList.getmCaption();
                            String reactionIcon = imageViewList.getmReactionIcon();
                            int reactionId = imageViewList.getmReactionId();
                            popUp.dismiss();

                            /**
                             * If the user Presses the same reaction again then don't do anything
                             */
                            JSONObject myReactions = null;
                            if (reactions != null) {
                                try {
                                    JSONObject reactionsObject = new JSONObject(reactions);
                                    myReactions = reactionsObject.optJSONObject("my_feed_reaction");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            if (myReactions != null) {
                                if (myReactions.optInt("reactionicon_id") != reactionId) {
                                    doLikeUnlike(photoDetails, reaction, true, reactionId, reactionIcon,
                                            caption, clickedPosition);
                                }
                            } else {
                                doLikeUnlike(photoDetails, reaction, false, reactionId, reactionIcon,
                                        caption, clickedPosition);
                            }
                        }
                    });

            reactionsRecyclerView.setAdapter(reactionsAdapter);
        }
        return true;
    }

    @Override
    public void onAsyncSuccessResponse(JSONObject response, boolean isRequestSuccessful, boolean isAttachFileRequest) {

        if (BitmapUtils.isImageRotated)
            BitmapUtils.deleteImageFolder();
        if (response != null) {
            JSONObject bodyObject = response.optJSONObject("body");
            addCommentToList(bodyObject);
            mSelectPath.clear();
        }
    }

}
