/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */

package com.unitedgungroup.mobiapp.classes.modules.advancedActivityFeeds;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.text.util.Linkify;

import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.activities.CreateNewEntry;
import com.unitedgungroup.mobiapp.classes.common.adapters.AddPeopleAdapter;
import com.unitedgungroup.mobiapp.classes.common.dialogs.AlertDialogWithAction;
import com.unitedgungroup.mobiapp.classes.common.dialogs.CheckInLocationDialog;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnCancelClickListener;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnAsyncFacebookResponseListener;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnCheckInLocationResponseListener;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnResponseListener;
import com.unitedgungroup.mobiapp.classes.common.multiimageselector.MultiImageSelectorActivity;
import com.unitedgungroup.mobiapp.classes.common.utils.AddPeopleList;
import com.unitedgungroup.mobiapp.classes.common.utils.BitmapUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.EmojiUtil;
import com.unitedgungroup.mobiapp.classes.common.utils.GlobalFunctions;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.utils.LogUtils;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnAsyncResponseListener;
import com.unitedgungroup.mobiapp.classes.common.utils.PreferencesUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.Smileys;
import com.unitedgungroup.mobiapp.classes.common.utils.SnackbarUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.SoundUtil;
import com.unitedgungroup.mobiapp.classes.common.utils.UploadAttachmentUtil;
import com.unitedgungroup.mobiapp.classes.common.utils.UrlUtil;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;
import com.unitedgungroup.mobiapp.classes.core.ConstantVariables;
import com.unitedgungroup.mobiapp.classes.common.ui.PredicateLayout;
import com.unitedgungroup.mobiapp.classes.core.LoginActivity;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;

import github.ankushsachdeva.emojicon.EmojiconEditText;


public class Status extends AppCompatActivity implements View.OnClickListener, TextWatcher,
        OnAsyncResponseListener, OnCheckInLocationResponseListener, OnCancelClickListener,
        OnAsyncFacebookResponseListener, AdapterView.OnItemClickListener {

    private TextView mAddPeople, mAddLocation, mAddPhoto, mAddVideo, mAddLink, mAddPrivacy, mAddMusic;
    private Typeface fontIcon;
    private Context mContext;
    private String mAttachPostUrl, mAttachType = "";
    private PredicateLayout mTaggedFriendsLayout;
    private LinearLayout mLocationsLayout, mAddLinkLayout, mAddVideoLayout,mAddPhotoBlock, mLinkAttachmentBlock;

    private Map<String, String> selectedFriends;
    private Map<String, String> selectedLocation;
    private TextView mLocationPrefix, mLocationLabel, mAddLinkText, mAddVideoText, musicAddedMessage,
            mEmojiButton;
    private EditText mEnterLinkText;
    private EmojiconEditText mStatusBodyField;
    private Button mAttachLinkButton;
    private ArrayList<String> mSelectPath = new ArrayList<>(), popupMenuList = new ArrayList<>();;
    private JSONObject mFeedPostMenus;
    private AppConstant mAppConst;
    private JSONObject mSelectedLocationObject, mUserPrivacyObject, mUserListObject;
    private JSONArray mPrivacyKeys;
    private String mSelectedPrivacyKey, mSubjectType, mUploadingOption;
    private boolean mShowPhotoBlock, mOpenCheckIn;
    private int mSubjectId, width;

    private boolean isAttachmentAttached = false, isExternalShare = false, isFacebookPost = false, isTwitterPost = false;
    private ImageView mVideoAttachmentIcon, mLinkAttachmentImage, mCancelSelectedLocation;
    private TextView mVideoAttachmentTitle, mVideoAttachmentBody, mLinkAttachmentTitle, mLinkAttachmentDescription,
            mLinkAttachmentUrl, mShareFacebook, mShareTwitter;
    private TreeMap<String, String> statusBodyFieldUrlList;
    private Bundle mSelectedLocationBundle;
    private CallbackManager callbackManager;
    String fb_uid, fbAccessToken;
    String videoAttachedUrl, mStatusPostUrl;
    private String mSelectedMusicFile, uriText = null;
    private int mVideoId, mSongId;
    private AlertDialogWithAction mAlertDialogWithAction;

    Toolbar mToolbar;
    boolean isPosted = false;
    private String searchText;
    private AddPeopleAdapter mAddPeopleAdapter;
    private List<AddPeopleList> mAddPeopleList;
    private String tagString;
    private ListView mUserListView;
    private JSONObject tagObject;
    private PopupWindow popupWindow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        mContext = this;
        mAppConst = new AppConstant(this);
        mAlertDialogWithAction = new AlertDialogWithAction(mContext);
        fontIcon = GlobalFunctions.getFontIconTypeFace(mContext);
        statusBodyFieldUrlList = new TreeMap<>(Collections.reverseOrder());
        mAddPeopleList = new ArrayList<>();

        //Setting up the action bar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.title_activity_status));
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        CustomViews.createMarqueeTitle(this, mToolbar);

        mStatusBodyField = (EmojiconEditText) findViewById(R.id.statusBody);
        mStatusBodyField.setHint(mContext.getResources().getString(R.string.status_box_default_text) + "...");

        mShareFacebook = (TextView) findViewById(R.id.shareFacebook);
        mShareTwitter = (TextView) findViewById(R.id.shareTwitter);
        mAddPeople = (TextView) findViewById(R.id.addPeople);
        // CheckIN Code Commented, will implement in future.
        mAddLocation = (TextView) findViewById(R.id.addLocation);
        mAddPhoto = (TextView) findViewById(R.id.addPhoto);
        mAddVideo = (TextView) findViewById(R.id.addVideo);
        mAddLink = (TextView) findViewById(R.id.addLink);
        mAddPrivacy = (TextView) findViewById(R.id.addPrivacy);
        mAddMusic = (TextView) findViewById(R.id.addMusic);
        musicAddedMessage = (TextView) findViewById(R.id.music_added_msg);
        mEmojiButton = (TextView) findViewById(R.id.emojiIcon);

        selectedFriends = new HashMap<>();
        selectedLocation = new HashMap<>();

        mAddVideoText = (TextView) findViewById(R.id.addVideoText);
        mAddVideoLayout = (LinearLayout) findViewById(R.id.addVideoLayout);
        mTaggedFriendsLayout = (PredicateLayout) findViewById(R.id.taggedFriends);
        mLocationsLayout = (LinearLayout) findViewById(R.id.Locations);
        mAddPhotoBlock = (LinearLayout) findViewById(R.id.addPhotoBlock);

        mLocationPrefix = (TextView) findViewById(R.id.locationPrefix);
        mLocationLabel = (TextView) findViewById(R.id.name);
        mCancelSelectedLocation = (ImageView) findViewById(R.id.cancel);
        mCancelSelectedLocation.setOnClickListener(this);

        mAddLinkLayout = (LinearLayout) findViewById(R.id.addLinkBlock);
        mEnterLinkText = (EditText) findViewById(R.id.enterLinkText);
        mAttachLinkButton = (Button) findViewById(R.id.attachLinkButton);
        mAddLinkText = (TextView) findViewById(R.id.addLinkText);

        //Link Attachment Info
        mLinkAttachmentBlock = (LinearLayout) findViewById(R.id.linkAttachment);
        mLinkAttachmentImage = (ImageView) findViewById(R.id.linkAttachmentImage);
        mLinkAttachmentTitle = (TextView) findViewById(R.id.linkAttachmentTitle);
        mLinkAttachmentDescription = (TextView) findViewById(R.id.linkAttachmentDescription);
        mLinkAttachmentUrl = (TextView) findViewById(R.id.linkAttachmentUrl);

        //Video attachment info.
        mVideoAttachmentIcon = (ImageView) findViewById(R.id.attachmentIcon);
        mVideoAttachmentTitle = (TextView) findViewById(R.id.attachmentTitle);
        mVideoAttachmentBody = (TextView) findViewById(R.id.attachmentBody);

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        /**
         * Check if user is logged-out in case of External share
         * Redirect to login first and send the intent data to Login Activity
         * After successful login load the intent data here
         */
        if(PreferencesUtils.getAuthToken(this) == null || PreferencesUtils.getAuthToken(this).isEmpty()) {

            Intent loginIntent = new Intent(this, LoginActivity.class);
            loginIntent.putExtras(intent.getExtras());
            loginIntent.setAction(action);
            loginIntent.setType(type);
            finish();
            startActivity(loginIntent);
        }

        if(action != null && type != null){
            switch (action) {
                case Intent.ACTION_SEND:
                    isExternalShare = true;
                    if (type.equals("text/plain")) {
                        handleSendText(intent); // Handle text being sent
                    } else if (type.startsWith("image/")) {
                        if(!mAppConst.checkManifestPermission(Manifest.permission.READ_EXTERNAL_STORAGE)){
                            mAppConst.requestForManifestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                                    ConstantVariables.READ_EXTERNAL_STORAGE);
                        }else{
                            handleSendImage(intent); // Handle single image being sent
                        }
                    }
                    break;
                case Intent.ACTION_SEND_MULTIPLE:
                    isExternalShare = true;
                    if (type.startsWith("image/")) {
                        if(!mAppConst.checkManifestPermission(Manifest.permission.READ_EXTERNAL_STORAGE)){
                            mAppConst.requestForManifestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                                    ConstantVariables.READ_EXTERNAL_STORAGE);
                        }else{
                            handleSendMultipleImages(intent); // Handle multiple images being sent
                        }
                    }
                    break;
            }
        }else{
            try {
                if(intent.getStringExtra("feedPostMenus") != null) {
                    mFeedPostMenus = new JSONObject(intent.getStringExtra("feedPostMenus"));
                }
                mShowPhotoBlock = intent.getBooleanExtra("showPhotoBlock", true);
                mOpenCheckIn = intent.getBooleanExtra("openCheckIn", false);
                mSubjectType = intent.getStringExtra(ConstantVariables.SUBJECT_TYPE);
                mSubjectId = intent.getIntExtra(ConstantVariables.SUBJECT_ID, 0);

                if(mOpenCheckIn){
                    if(!mAppConst.checkManifestPermission(Manifest.permission.ACCESS_FINE_LOCATION)){
                        mAppConst.requestForManifestPermission(Manifest.permission.ACCESS_FINE_LOCATION,
                                ConstantVariables.ACCESS_FINE_LOCATION);
                    }else{
                        addLocation(true);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        // Show attachment Options in bottom
        if(mFeedPostMenus != null && mFeedPostMenus.length() != 0) {

            int checkIn = mFeedPostMenus.optInt("checkin", 0);
            int withTags = mFeedPostMenus.optInt("withtags", 0);
            int photo = mFeedPostMenus.optInt("photo", 0);
            int emotions = mFeedPostMenus.optInt("emotions", 0);
            int link = mFeedPostMenus.optInt("link", 0);
            int video = mFeedPostMenus.optInt("video", 0);
            int music = mFeedPostMenus.optInt("music", 0);

            mUserPrivacyObject = mFeedPostMenus.optJSONObject("userprivacy");
            mUserListObject = mFeedPostMenus.optJSONObject("userlists");

            String facebook = getResources().getString(R.string.facebook_post);
            String twitter = getResources().getString(R.string.twitter_post);

            /*
             Show facebook icon for status post on facebook
            */
            if (facebook.equals("1")) {
                mShareFacebook.setVisibility(View.VISIBLE);
                mShareFacebook.setOnClickListener(this);
                mShareFacebook.setTypeface(fontIcon);
                mShareFacebook.setText("\uf082");
            }

            /*
             Show twitter icon for status post on twitter
            */
            if (twitter.equals("1")) {
                mShareTwitter.setVisibility(View.VISIBLE);
                mShareTwitter.setOnClickListener(this);
                mShareTwitter.setTypeface(fontIcon);
                mShareTwitter.setText("\uf081");
            }

               /*
             Show Add People Option
            */
            if (withTags == 1) {

                mAddPeople.setVisibility(View.VISIBLE);
                mAddPeople.setTypeface(fontIcon);
                mAddPeople.setText("\uf234");
                mAddPeople.setOnClickListener(this);
            }

            /*
             Add emotions Option
            */
            if(emotions == 1) {
                mEmojiButton.setVisibility(View.VISIBLE);
                mEmojiButton.setTypeface(fontIcon);
                mEmojiButton.setText("\uf118");
                mEmojiButton.setOnClickListener(this);
            }

            /*
             Add Location Option
            */

            //Todo in future
            if(checkIn == 1 && ! getResources().getString(R.string.places_api_key).isEmpty()) {
                mAddLocation.setVisibility(View.VISIBLE);
                mAddLocation.setTypeface(fontIcon);
                mAddLocation.setText("\uf041");
                mAddLocation.setOnClickListener(this);
            }

            /*
             Add Photo Option
            */

            if (photo == 1) {
                mAddPhoto.setVisibility(View.VISIBLE);
                mAddPhoto.setTypeface(fontIcon);
                mAddPhoto.setText("\uf030");
                mAddPhoto.setOnClickListener(this);
            }

            /*
             Add Link Option
            */

            if (link == 1) {
                mAddLink.setVisibility(View.VISIBLE);
                mAddLink.setTypeface(fontIcon);
                mAddLink.setText("\uf0c1");
                mAddLink.setOnClickListener(this);
                mStatusBodyField.addTextChangedListener(this);
            }

            /*
             Add Video Option
             Show video attachement only when video module is enabled and feedPostMenu has video option
            */

            if (video == 1 && GlobalFunctions.isModuleEnabled("video")) {

                mAddVideo.setVisibility(View.VISIBLE);
                mAddVideo.setTypeface(fontIcon);
                mAddVideo.setText("\uf03d");
                mAddVideo.setOnClickListener(this);
            }

            /*
             Add Music Option
             Show music attachment only when music module is enabled and feedPostMenu has music option
            */

            if (music == 1 && GlobalFunctions.isModuleEnabled("music")) {

                mAddMusic.setVisibility(View.VISIBLE);
                mAddMusic.setTypeface(fontIcon);
                mAddMusic.setText("\uf001");
                mAddMusic.setOnClickListener(this);
            }

            if (emotions != 1) {
                mStatusBodyField.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
            }

            if (mUserPrivacyObject != null && mUserPrivacyObject.length() != 0) {
                mAddPrivacy.setVisibility(View.VISIBLE);
                mAddPrivacy.setTypeface(fontIcon);
                mAddPrivacy.setText("\uf0ac");
                mAddPrivacy.setOnClickListener(this);
            }
        }

        /*
         Open Photo Gallery If Photos Clicked From Status Menu
         */
        if(mShowPhotoBlock) {
            if(!mAppConst.checkManifestPermission(Manifest.permission.READ_EXTERNAL_STORAGE)){
                mAppConst.requestForManifestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                        ConstantVariables.READ_EXTERNAL_STORAGE);
            }else{
                addPhotoBlock(true);
            }
        }

        /* To create a Emoji popup with all emoticons of keyboard height */
        EmojiUtil.createEmojiPopup(this, findViewById(R.id.rootView), mStatusBodyField);
        width = AppConstant.getDisplayMetricsWidth(mContext);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_title, menu);
        menu.findItem(R.id.submit).setTitle(mContext.getResources().getString(R.string.post_status_button_text));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch(id){

            case android.R.id.home:
                onBackPressed();
                // Playing backSound effect when user tapped on back button from tool bar.
                if (PreferencesUtils.isSoundEffectEnabled(mContext)) {
                    SoundUtil.playSoundEffectOnBackPressed(mContext);
                }
                return true;

            case R.id.submit:
                String statusBodyText = mStatusBodyField.getText().toString();

                //Checking and allow to post only if post is not empty when there is no attachment.
                //If there is any attachment then allow to post.
                if((statusBodyText.length() > 0 && !statusBodyText.trim().isEmpty()) || !mAttachType.isEmpty()
                        || !selectedLocation.isEmpty()) {
                    mAppConst.hideKeyboard();
                    mStatusPostUrl = AppConstant.DEFAULT_URL + "advancedactivity/feeds/post";
                    if(mSubjectType != null && !mSubjectType.isEmpty() && mSubjectId != 0) {
                        mStatusPostUrl += "?subject_id=" + mSubjectId + "&subject_type=" + mSubjectType;
                    }

                    if (isFacebookPost ) {
                        UpdateStatusOnFacebook();
                    } else {
                        uploadFilesAndData();
                    }


                }else {
                    SnackbarUtils.displaySnackbar(findViewById(R.id.rootView),
                            getResources().getString(R.string.status_empty_msg));
                }

        }

        return super.onOptionsItemSelected(item);
    }

    private void uploadFilesAndData() {
        // Uploading files in background with the status post.
        new UploadAttachmentUtil(Status.this, mStatusPostUrl,
                getAttachmentPostParams(new HashMap<String, String>()),
                mSelectPath).execute();

    }

    private void UpdateStatusOnFacebook() {
        LoginManager loginManager = LoginManager.getInstance();
        loginManager.logInWithPublishPermissions(Status.this, Arrays.asList("publish_actions"));

        loginManager.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        fbAccessToken = loginResult.getAccessToken().getToken();
                        GraphRequest graphRequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                fb_uid = object.optString("id");

                                String updateStatusUrl;
                                HashMap<String, String> fbPostParams = new HashMap<>();

                                // TODO
                                if (mAttachType.equals("video")) {
                                    updateStatusUrl = "https://graph.facebook.com/" + fb_uid+"/feed?access_token=" + fbAccessToken;
                                    fbPostParams.put("message",videoAttachedUrl);
                                } else if (mSelectPath != null && !mSelectPath.isEmpty()) {
                                    fbPostParams.put("photo", mSelectPath.get(0));
                                    fbPostParams.put("caption", mStatusBodyField.getText().toString().trim());
                                    updateStatusUrl = "https://graph.facebook.com/" + fb_uid+"/photos?access_token=" + fbAccessToken;
                                } else {
                                    updateStatusUrl = "https://graph.facebook.com/" + fb_uid+"/feed?access_token=" + fbAccessToken;
                                    fbPostParams.put("message",mStatusBodyField.getText().toString().trim());
                                    if (selectedLocation != null) {
                                        fbPostParams.put("place", selectedLocation.toString());
                                        if (selectedFriends != null && selectedFriends.size() != 0) {
                                            fbPostParams.put("tags", selectedFriends.toString());
                                        }
                                    }
                                    if (uriText != null) {
                                        fbPostParams.put("link", uriText);
                                    }
                                }

                                new UploadAttachmentUtil(Status.this, updateStatusUrl, fbPostParams, mSelectPath, isFacebookPost).execute();

                            }
                        });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email");
                        graphRequest.setParameters(parameters);
                        graphRequest.executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        uploadFilesAndData();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        uploadFilesAndData();
                    }
                });

    }


    @Override
    public void onClick(View view) {

        int id = view.getId();

        switch (id){

            case R.id.addPeople:

                Intent addPeopleIntent = new Intent(Status.this, AddPeople.class);

                Set<String> keySet = selectedFriends.keySet();
                Bundle bundle = new Bundle();

                for (String key : keySet) {
                    String value = selectedFriends.get(key);
                    bundle.putString(key, value);
                }

                addPeopleIntent.putExtras(bundle);
                startActivityForResult(addPeopleIntent, ConstantVariables.ADD_PEOPLE_CODE);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                break;

            case R.id.addLocation:
                if(!mAppConst.checkManifestPermission(Manifest.permission.ACCESS_FINE_LOCATION)){
                    mAppConst.requestForManifestPermission(Manifest.permission.ACCESS_FINE_LOCATION,
                            ConstantVariables.ACCESS_FINE_LOCATION);
                }else{
                    addLocation(false);
                }
                break;

            case R.id.addPhoto:
                mUploadingOption = "photo";
                if(!mAppConst.checkManifestPermission(Manifest.permission.READ_EXTERNAL_STORAGE)){
                    mAppConst.requestForManifestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                            ConstantVariables.READ_EXTERNAL_STORAGE);
                }else{
                    addPhotoBlock(false);
                }
                break;

            case R.id.addLink:

                addLinkBlock();
                mEnterLinkText.setVisibility(View.VISIBLE);
                mEnterLinkText.setText("");
                mAttachLinkButton.setVisibility(View.VISIBLE);

                mAttachLinkButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String text = mEnterLinkText.getText().toString().trim();
                        attachLink(text);
                    }
                });
                break;

            case R.id.emojiIcon:

                /* To show Emoji Keyboard */
                EmojiUtil.showEmojiKeyboard(this, mStatusBodyField);
                break;

            case R.id.addPrivacy:
                showPopup(view);
                break;

            case R.id.addVideo:
                String url;
                List<String> enabledModuleList = null;
                if (PreferencesUtils.getEnabledModuleList(mContext) != null) {
                    enabledModuleList = new ArrayList<>(Arrays.asList(PreferencesUtils.getEnabledModuleList(mContext).split("\",\"")));
                }
                if (enabledModuleList != null && enabledModuleList.contains("sitevideo")
                        && !Arrays.asList(ConstantVariables.DELETED_MODULES).contains("sitevideo")) {
                    url = AppConstant.DEFAULT_URL + "advancedvideos/create?post_attach=1";
                } else {
                    url = AppConstant.DEFAULT_URL + "videos/create?post_attach=1";
                }
                Intent attachVideoIntent = new Intent(Status.this, CreateNewEntry.class);
                attachVideoIntent.putExtra(ConstantVariables.ATTACH_VIDEO, "attach_video");
                attachVideoIntent.putExtra(ConstantVariables.EXTRA_MODULE_TYPE, "home");
                attachVideoIntent.putExtra(ConstantVariables.CREATE_URL, url);
                startActivityForResult(attachVideoIntent, ConstantVariables.REQUEST_VIDEO);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;

            case R.id.addMusic:
                mUploadingOption = "music";
                /* Check if permission is granted or not */
                if(!mAppConst.checkManifestPermission(Manifest.permission.READ_EXTERNAL_STORAGE)){
                    mAppConst.requestForManifestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                            ConstantVariables.READ_EXTERNAL_STORAGE);
                } else {
                    //Visible music block and disable other attachments until link option is not canceled.
                    GlobalFunctions.addMusicBlock(Status.this);
                    makeAndRemoveAttachmentClickable(mAddVideo, mAddPhoto, mAddLink, mAddMusic, false);
                }
                break;

            case R.id.shareFacebook:
                if (isFacebookPost) {
                    isFacebookPost = false;
                    mShareFacebook.setTextColor(ContextCompat.getColor(mContext, R.color.dark_gray));
                } else {
                    isFacebookPost = true;
                    callbackManager = CallbackManager.Factory.create();
                    FacebookSdk.sdkInitialize(getApplicationContext());
                    FacebookSdk.setApplicationId(getResources().getString(R.string.facebook_app_id));
                    mShareFacebook.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                }
                break;

            case R.id.shareTwitter:
                if (isTwitterPost) {
                    isTwitterPost = false;
                    mShareTwitter.setTextColor(ContextCompat.getColor(mContext, R.color.dark_gray));
                } else {
                    isTwitterPost = true;
                    mShareTwitter.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                }

            // Remove selected check-in location.
            case R.id.cancel:
                selectedLocation.clear();
                mLocationsLayout.setVisibility(View.GONE);
                break;
        }

    }

    public Map<String, String> getAttachmentPostParams(Map<String, String> postParams) {

        // Add Tagged Friends in post params
        if (selectedFriends != null && selectedFriends.size() != 0) {
            String mToValues = "";
            int j = 0;
            Set<String> mKeySet = selectedFriends.keySet();
            for (String key : mKeySet) {
                j++;
                if (j < selectedFriends.size()) {
                    mToValues += key + ",";
                } else {
                    mToValues += key;
                }
            }
            postParams.put("toValues", mToValues);
        }

        // Adding loaction params
        postParams.put("locationLibrary", "client");

        // Adding status body.
        postParams.put("body", Smileys.getEmojiFromString(mStatusBodyField.getText().toString().trim()));

        if (tagObject != null && tagObject.length() > 0) {
            postParams.put("composer", tagObject.toString());
        }

        // Adding post privacy option.
        if (mSelectedPrivacyKey == null) {
            mSelectedPrivacyKey = "everyone";
        }
        postParams.put("auth_view", mSelectedPrivacyKey);

        // Adding check-in option in params
        if (selectedLocation != null && selectedLocation.size() != 0) {
            try {

                JSONObject checkInObject = new JSONObject();
                checkInObject.put("checkin", mSelectedLocationObject);
                postParams.put("composer", checkInObject.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        // Adding attachment info if any.
        if (mAttachType != null && !mAttachType.isEmpty()) {
            postParams = UploadAttachmentUtil.getAttachmentPostParams(postParams, mAttachType,
                    uriText, mSongId, mVideoId);
        }

        return postParams;
    }

    /**
     * Method to show link block when user click on link icon.
     */
    public void addLinkBlock() {

        //Visible link block and disable other attachments until link option is not canceled.
        mAddLinkLayout.setVisibility(View.VISIBLE);
        makeAndRemoveAttachmentClickable(mAddLink, mAddVideo, mAddPhoto, mAddMusic, false);
        isAttachmentAttached = true;

        String addLinkText = getResources().getString(R.string.add_link_text) + " " +
                "(" + getResources().getString(R.string.cancel_attachment) + ")";
        int i1 = addLinkText.indexOf("(");
        int i2 = addLinkText.indexOf(")");

        mAddLinkText.setMovementMethod(LinkMovementMethod.getInstance());
        mAddLinkText.setText(addLinkText, TextView.BufferType.SPANNABLE);
        Spannable photoTextSpannable = (Spannable) mAddLinkText.getText();
        ClickableSpan myClickSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {

                // When cancel the link attachment option then setting the default text color.
                // and setting the attach type to null value & enable other attachment options.
                mAddLink.setTextColor(ContextCompat.getColor(mContext, R.color.body_text_2));
                mAttachType = "";
                isAttachmentAttached = false;
                mAddLinkLayout.setVisibility(View.GONE);
                mLinkAttachmentBlock.setVisibility(View.GONE);
                makeAndRemoveAttachmentClickable(mAddLink, mAddVideo, mAddPhoto, mAddMusic, true);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            }
        };

        photoTextSpannable.setSpan(myClickSpan, i1 + 1, i2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

    }

    /**
     * Method to make attachment clickable and non-clickable according to request.
     * @param attachment1 TextView of 1st attachment.
     * @param attachment2 TextView of 2nd attachment.
     * @param attachment3 TextView of 3rd attachment.
     * @param attachment4 TextView of 4th attachment.
     * @param isMakeTextViewClickableRequest if true then make all text view clickable else not.
     */
    public void makeAndRemoveAttachmentClickable(TextView attachment1, TextView attachment2,
                                                 TextView attachment3, TextView attachment4,
                                                 boolean isMakeTextViewClickableRequest) {
        if (isMakeTextViewClickableRequest) {
            attachment1.setClickable(true);
            attachment2.setClickable(true);
            attachment3.setClickable(true);
            // Checking this condition when user click on photo attachment.
            //because when click on photo attachment then don't disable photo attachment click
            // and allow user to chose more photo.
            if (attachment4 != null)
                attachment4.setClickable(true);
        } else {
            attachment1.setClickable(false);
            attachment2.setClickable(false);
            attachment3.setClickable(false);
            // Checking this condition when user click on photo attachment.
            //because when click on photo attachment then don't disable photo attachment click
            // and allow user to chose more photo.
            if (attachment4 != null)
                attachment4.setClickable(false);
        }
    }

    /**
     * Method to show selected images.
     * @param mSelectPath list of selected images.
     */
    public void showSelectedImages(final ArrayList<String> mSelectPath) {

        ArrayList<String> removeImageList = new ArrayList<>();
        for (final String imagePath : mSelectPath) {

            // Getting Bitmap from its real path.
            Bitmap bitmap = BitmapUtils.decodeSampledBitmapFromFile(Status.this, imagePath, width,
                    (int) getResources().getDimension(R.dimen.feed_attachment_image_height), false);

            // If there is any null image then remove from image path.
            if (bitmap != null) {

                // Creating ImageView & params for this and adding selected images in this view.
                ImageView selectedImage = new ImageView(this);
                selectedImage.setLayoutParams(CustomViews.getCustomWidthHeightLayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, bitmap.getHeight()));
                selectedImage.setScaleType(ImageView.ScaleType.FIT_XY);
                selectedImage.setImageBitmap(bitmap);

                // Creating cancel ImageView & params for this and adding close icon in this view.
                RelativeLayout.LayoutParams cancelImageParams = CustomViews.getWrapRelativeLayoutParams();
                ImageView cancelImage = new ImageView(this);
                cancelImage.setImageResource(R.drawable.ic_clear_grey);
                // Adding cancel ImageView on top-right of the selected image.
                cancelImageParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                cancelImageParams.addRule(RelativeLayout.ALIGN_TOP, selectedImage.getId());
                cancelImage.setLayoutParams(cancelImageParams);

                // Creating a Relative layout & params for this
                // and adding selected images and cancel images in it.
                LinearLayout.LayoutParams layoutParams = CustomViews.getFullWidthHeightLayoutParams();
                layoutParams.setMargins((int) getResources().getDimension(R.dimen.margin_10dp),
                        (int) getResources().getDimension(R.dimen.margin_10dp),
                        (int) getResources().getDimension(R.dimen.margin_10dp),
                        (int) getResources().getDimension(R.dimen.margin_10dp));
                final RelativeLayout selectedImageLayout = new RelativeLayout(this);

                selectedImageLayout.setLayoutParams(layoutParams);
                selectedImageLayout.addView(selectedImage);
                selectedImageLayout.addView(cancelImage);
                mAddPhotoBlock.addView(selectedImageLayout);
                mAddPhotoBlock.setVisibility(View.VISIBLE);
                mAddPhoto.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                mAttachType = "photo";
                isAttachmentAttached = true;

                // Setting OnClickListener on cancelImage.
                cancelImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectedImageLayout.setVisibility(View.GONE);
                        mSelectPath.remove(imagePath);

                        // If canceled all the selected images then hide photoBlockLayout
                        // and enabled other attachement click.
                        if (mSelectPath.isEmpty()) {
                            mAddPhotoBlock.setVisibility(View.GONE);
                            mAttachType = "";
                            isAttachmentAttached = false;
                            mAddPhoto.setTextColor(ContextCompat.getColor(mContext, R.color.body_text_2));
                            makeAndRemoveAttachmentClickable(mAddLink, mAddVideo, mAddMusic, null, true);
                        }
                    }
                });
            } else if (imagePath != null) {
                //When there are null images then add them in remove list.
                removeImageList.add(imagePath);
            }
        }
        // Checking if there is any null images then remove them from the mSelectPath
        if (!removeImageList.isEmpty()) {
            for (String image : removeImageList) {
                mSelectPath.remove(image);
            }
            if (mSelectPath.isEmpty()) {
                mAddPhotoBlock.setVisibility(View.GONE);
                mAttachType = "";
                isAttachmentAttached = false;
                mAddPhoto.setTextColor(ContextCompat.getColor(mContext, R.color.body_text_2));
                makeAndRemoveAttachmentClickable(mAddLink, mAddVideo, mAddMusic, null, true);
            }
        }
    }

    // Get image from bitmap for real path
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    /**
     * Method to show video attachment.
     * @param mVideoTitle title of video attachment.
     * @param mVideoImage image of video attachment.
     * @param mVideoDescription description of video attachment.
     */
    public void showAttachedVideo(String mVideoTitle, String mVideoImage,
                                  String mVideoDescription) {

        if (mVideoImage != null && !mVideoImage.isEmpty()) {
            Picasso.with(mContext)
                    .load(mVideoImage)
                    .into(mVideoAttachmentIcon);
        }
        mVideoAttachmentTitle.setText(Html.fromHtml(mVideoTitle));
        mVideoAttachmentBody.setText(Html.fromHtml(mVideoDescription));

        mAddVideoLayout.setVisibility(View.VISIBLE);
        mAddVideo.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        makeAndRemoveAttachmentClickable(mAddLink, mAddPhoto, mAddMusic, mAddVideo, false);
        mAttachType = "video";
        isAttachmentAttached = true;

        String addVideoText = getResources().getString(R.string.add_video_text)  + " " +
                "(" + getResources().getString(R.string.cancel_attachment) + ")";
        int index1 = addVideoText.indexOf("(");
        int index2 = addVideoText.indexOf(")");

        mAddVideoText.setMovementMethod(LinkMovementMethod.getInstance());
        mAddVideoText.setText(addVideoText, TextView.BufferType.SPANNABLE);
        Spannable mAddVideoTextSpannable = (Spannable) mAddVideoText.getText();
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {

                // When cancel the video attachment option then setting the default text color.
                // and setting the attach type to null value & enable other attachment options.
                mAddVideo.setTextColor(ContextCompat.getColor(mContext, R.color.body_text_2));
                mAddVideoLayout.setVisibility(View.GONE);
                mAttachType = "";
                isAttachmentAttached = false;
                makeAndRemoveAttachmentClickable(mAddLink, mAddPhoto, mAddMusic, mAddVideo, true);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            }
        };

        mAddVideoTextSpannable.setSpan(clickableSpan, index1 + 1, index2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    public void showPopup(View v) {

        PopupMenu popup = new PopupMenu(mContext, v);


        if (mUserPrivacyObject != null && mUserPrivacyObject.length() != 0) {
            mPrivacyKeys = mUserPrivacyObject.names();

            for (int i = 0; i < mUserPrivacyObject.length(); i++) {
                String key = mPrivacyKeys.optString(i);
                popupMenuList.add(key);
                String privacyLabel = mUserPrivacyObject.optString(key);
                if (mSelectedPrivacyKey != null && mSelectedPrivacyKey.equals(key)) {
                    popup.getMenu().add(Menu.NONE, i, Menu.NONE, privacyLabel).setCheckable(true).setChecked(true);
                }else if(mSelectedPrivacyKey == null && key.equals("everyone")) {
                    popup.getMenu().add(Menu.NONE, i, Menu.NONE, privacyLabel).setCheckable(true).setChecked(true);
                }else {
                    popup.getMenu().add(Menu.NONE, i, Menu.NONE, privacyLabel);
                }
            }
        }

        if (mUserListObject != null && mUserListObject.length() != 0) {
            mPrivacyKeys = mUserListObject.names();

            for (int i = 0; i < mUserListObject.length(); i++) {
                String key = mPrivacyKeys.optString(i);
                popupMenuList.add(key);
                String privacyLabel = mUserListObject.optString(key);
                if (mSelectedPrivacyKey != null && mSelectedPrivacyKey.equals(key)) {
                    popup.getMenu().add(Menu.NONE, i+mUserPrivacyObject.length(), Menu.NONE,
                            privacyLabel).setCheckable(true).setChecked(true);
                } else {
                    popup.getMenu().add(Menu.NONE, i+mUserPrivacyObject.length(), Menu.NONE, privacyLabel);
                }
            }
        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                    mSelectedPrivacyKey = popupMenuList.get(id);
                    switch (mSelectedPrivacyKey) {
                        case "everyone":
                            mAddPrivacy.setText("\uf0ac");
                            break;
                        case "networks":
                            mAddPrivacy.setText("\uf0c0");
                            break;
                        case "friends":
                            mAddPrivacy.setText("\uf007");
                            break;
                        case "onlyme":
                            mAddPrivacy.setText("\uf023");
                            break;
                        default:
                            mAddPrivacy.setText("\uf007");
                            break;
                    }
                return true;
            }
        });
        popup.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mAppConst.hideKeyboard();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case ConstantVariables.REQUEST_IMAGE:
                if(resultCode == RESULT_OK){

                    if (mSelectPath != null) {
                        mSelectPath.clear();
                        mAddPhotoBlock.removeAllViews();
                    }
                    // Getting image path from uploaded images.
                    mSelectPath = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
                    //Checking if there is any image or not.
                    if (mSelectPath != null)
                        showSelectedImages(mSelectPath);

                }else if (resultCode != RESULT_CANCELED) {
                    // failed to capture image
                    makeAndRemoveAttachmentClickable(mAddLink, mAddVideo, mAddMusic, null, true);
                    isAttachmentAttached = false;
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.image_capturing_failed),
                            Toast.LENGTH_SHORT).show();
                } else {
                    // User cancel the process
                    /**
                     * Finish this activity if Photo Option get clicked from Main Feed page
                     * And if user press back button on photoUploading, so as to show Feedpage again
                     */
                    if(data != null && data.hasExtra(MultiImageSelectorActivity.OPEN_PHOTO_BLOCK)){
                        boolean isOpenPhotoBlock = data.getExtras().getBoolean(MultiImageSelectorActivity.
                                OPEN_PHOTO_BLOCK, false);
                        if(isOpenPhotoBlock){
                            finish();
                        }
                    }else if (mSelectPath == null || mSelectPath.isEmpty()) {
                        mAddPhotoBlock.setVisibility(View.GONE);
                        mAttachType = "";
                        isAttachmentAttached = false;
                        mAddPhoto.setTextColor(ContextCompat.getColor(mContext, R.color.body_text_2));
                        makeAndRemoveAttachmentClickable(mAddLink, mAddVideo, mAddMusic, null, true);
                    }
                }
                break;

            case ConstantVariables.REQUEST_MUSIC:
                if(resultCode == RESULT_OK && data != null){

                    Uri selectedMusicUri = data.getData();
                    if (selectedMusicUri != null) {

                        // getting real path of music file
                        mSelectedMusicFile = GlobalFunctions.getMusicFilePathFromURI(this, selectedMusicUri);
                        mAttachType = "music";
                        mAttachPostUrl = AppConstant.DEFAULT_URL + "music/playlist/add-song";
                        // Getting attachment details.
                        new UploadAttachmentUtil(Status.this, mAttachPostUrl, mSelectedMusicFile).execute();
                    }

                }else if (resultCode != RESULT_CANCELED) {
                    makeAndRemoveAttachmentClickable(mAddLink, mAddPhoto, mAddVideo, mAddMusic, true);
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.music_capturing_failed),
                            Toast.LENGTH_SHORT).show();
                } else {
                    // User cancel the process
                    if (mSelectedMusicFile == null || mSelectedMusicFile.isEmpty()) {
                        mAttachType = "";
                        makeAndRemoveAttachmentClickable(mAddLink, mAddPhoto, mAddVideo, mAddMusic, true);
                    }
                }
                break;

            // When video is uploaded from my device option then loading aaf.
            case ConstantVariables.REQUEST_VIDEO:
                if (resultCode == ConstantVariables.CREATE_REQUEST_CODE) {
                    isPosted = true;
                    Intent intent = new Intent();
                    intent.putExtra("isPosted", isPosted);
                    setResult(ConstantVariables.FEED_REQUEST_CODE, intent);
                    finish();
                }
                break;
        }

        Bundle bundle = null;

        if(data != null) {
            bundle = data.getExtras();
        }

        // Check Result from Add People and Add Location Activities
        switch (resultCode){

            case ConstantVariables.ADD_PEOPLE_CODE:
                Set<String> searchArgumentSet = bundle.keySet();
                selectedFriends.clear();
                mTaggedFriendsLayout.removeAllViews();
                // Add Text "with" if some friends are being tagged
                if(searchArgumentSet != null && searchArgumentSet.size() != 0){
                    LinearLayout.LayoutParams layoutParams = CustomViews.getWrapLayoutParams();
                    layoutParams.setMargins(5,5,5,0);
                    TextView tagTextView = new TextView(mContext);
                    tagTextView.setText(" -  " + getResources().getString(R.string.with_text) + "  ");
                    TextViewCompat.setTextAppearance(tagTextView, R.style.CaptionView);
                    tagTextView.setLayoutParams(layoutParams);
                    tagTextView.setTextColor(ContextCompat.getColor(this, R.color.gray_text_color));
                    mTaggedFriendsLayout.addView(tagTextView);

                    CustomViews.setmOnCancelClickListener(this);
                    for (String key : searchArgumentSet) {
                        String value = bundle.getString(key);
                        selectedFriends.put(key, value);
                        selectedFriends = CustomViews.createSelectedUserLayout(mContext, Integer.parseInt(key), value,
                                mTaggedFriendsLayout, selectedFriends, 1);
                    }
                }
                break;

            case ConstantVariables.REQUEST_VIDEO:

                if(bundle != null) {
                    // Getting video attachment info.
                    mVideoId = bundle.getInt(ConstantVariables.CONTENT_ID);
                    String mVideoTitle = bundle.getString(ConstantVariables.CONTENT_TITLE);
                    if (mVideoId != 0 && mVideoTitle != null) {
                        String mVideoDescription = bundle.getString(ConstantVariables.DESCRIPTION);
                        String mVideoImage = bundle.getString(ConstantVariables.IMAGE);
                        if (bundle.containsKey(ConstantVariables.VIDEO_URL))
                            videoAttachedUrl = bundle.getString(ConstantVariables.VIDEO_URL);
                        showAttachedVideo(mVideoTitle, mVideoImage, mVideoDescription);
                    }
                }
                break;
        }

        if (isFacebookPost) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    // TODO in future
    public void createSelectedLocationLayout(String label) {

        mLocationLabel.setText(label);
        mLocationsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mAppConst.checkManifestPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    mAppConst.requestForManifestPermission(Manifest.permission.ACCESS_FINE_LOCATION,
                            ConstantVariables.ACCESS_FINE_LOCATION);
                } else {
                    addLocation(false);
                }
            }
        });
        mLocationsLayout.setVisibility(View.VISIBLE);
    }

    /**
     * Method to start ImageUploadingActivity (MultiImageSelector)
     * @param context Context of Class.
     * @param selectedMode Selected mode i.e. multi images or single image.
     * @param showCamera Whether to display the camera.
     * @param maxNum Max number of images allowed to pick in case of MODE_MULTI.
     */
    public void startImageUploadActivity(Context context, int selectedMode, boolean showCamera, int maxNum,
                                         boolean isOpenPhotoBlock){

        Intent intent;

        intent = new Intent(context, MultiImageSelectorActivity.class);
        // Whether photoshoot
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA, showCamera);
        // The maximum number of selectable image
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_COUNT, maxNum);
        // Select mode
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE, selectedMode);
        // The default selection
        if (mSelectPath != null && mSelectPath.size() > 0) {
            intent.putExtra(MultiImageSelectorActivity.EXTRA_DEFAULT_SELECTED_LIST, mSelectPath);
        }
        intent.putExtra(MultiImageSelectorActivity.OPEN_PHOTO_BLOCK, isOpenPhotoBlock);
        ((Activity) context).startActivityForResult(intent, ConstantVariables.REQUEST_IMAGE);

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

        //Checking if there is no attachment already attached.
        if (start > 0 && charSequence.toString().trim().length() > 1 && !isAttachmentAttached) {
            //Getting all Url's of mStatusBodyField.
            Spannable mSpannableUrl = mStatusBodyField.getText();
            URLSpan[] mUrlSpans = mSpannableUrl.getSpans(0, mSpannableUrl.length(), URLSpan.class);

            char last = charSequence.charAt(charSequence.length() - 1);
            //Checking last entered character is space or not.
            if (' ' == last) {
                statusBodyFieldUrlList.clear();
                for (URLSpan span : mUrlSpans) {
                    String url = span.getURL().replace("http://", "");
                    statusBodyFieldUrlList.put("", url);
                }
                // Checking map is not null & empty and checking url is valid or not.
                if (mUrlSpans.length >= 1 &&
                        statusBodyFieldUrlList != null && !statusBodyFieldUrlList.isEmpty() &&
                        GlobalFunctions.isValidUrl(statusBodyFieldUrlList.lastEntry().getValue())) {

                    addLinkBlock();
                    mEnterLinkText.setVisibility(View.GONE);
                    mEnterLinkText.setText(statusBodyFieldUrlList.lastEntry().getValue());
                    mAttachLinkButton.setVisibility(View.GONE);
                    mAttachPostUrl = AppConstant.DEFAULT_URL + "advancedactivity/feeds/attach-link";
                    mAttachType = "link";
                    uriText = mEnterLinkText.getText().toString().trim();
                    mAppConst.hideKeyboard();
                    new UploadAttachmentUtil(Status.this, mAttachPostUrl, uriText, mAttachType).execute();
                }
            }
        }


        // Check user start typing text with @ or not if yes than show users search list for tag them in status post content
        if (charSequence != null && !charSequence.toString().trim().isEmpty() && charSequence.toString().contains("@")) {
            String chr = charSequence.toString().substring(charSequence.toString().indexOf("@"), charSequence.length());
            if (chr.length() > 1) {
                StringBuilder stringBuilder = new StringBuilder(chr);
                searchText = stringBuilder.deleteCharAt(0).toString();
                getFriendList(UrlUtil.GET_FRIENDS_LIST + "?search=" + searchText);
            } else {
                if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                }
            }
        } else {
            if (popupWindow != null && popupWindow.isShowing()) {
                popupWindow.dismiss();
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        // For detecting url's in edit text.
        Linkify.addLinks(s, Linkify.WEB_URLS);
    }

    public void getFriendList(String url) {
        mAppConst.getJsonResponseFromUrl(url, new OnResponseListener() {
            @Override
            public void onTaskCompleted(JSONObject body) {
                if (body != null && body.length() != 0) {

                    mAddPeopleList.clear();
                    JSONArray guestListResponse = body.optJSONArray("response");

                    initFriendsListView(guestListResponse.length());

                    for (int i = 0; i < guestListResponse.length(); i++) {
                        JSONObject friendObject = guestListResponse.optJSONObject(i);
                        String username = friendObject.optString("label");
                        int userId = friendObject.optInt("id");
                        String userImage = friendObject.optString("image_icon");

                        mAddPeopleList.add(new AddPeopleList(userId, username, userImage));

                    }
                    mAddPeopleAdapter.notifyDataSetChanged();
                } else {
                    if (popupWindow != null && !popupWindow.isShowing()) {
                        popupWindow.dismiss();
                    }
                }
            }

            @Override
            public void onErrorInExecutingTask(String message, boolean isRetryOption) {

            }
        });

    }

    // Create popup window for showing user search list
    private void initFriendsListView(int length) {

        if (popupWindow != null && popupWindow.isShowing()) {
            popupWindow.dismiss();
        }

        mUserListView = new ListView(mContext);
        mUserListView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        mAddPeopleAdapter = new AddPeopleAdapter(this, R.layout.list_friends, mAddPeopleList);
        mUserListView.setAdapter(mAddPeopleAdapter);
        mUserListView.setOnItemClickListener(this);
        mUserListView.setBackground(new ColorDrawable(ContextCompat.getColor(mContext, R.color.gray_light)));

        popupWindow = new PopupWindow(mUserListView, RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        popupWindow.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(mContext, R.color.gray_light)));
        if (length > 3) {
            popupWindow.setHeight(300);
        }
        popupWindow.showAsDropDown(mStatusBodyField);

    }

    @Override
    public void onAsyncSuccessResponse(JSONObject response, boolean isRequestSuccessful, boolean isAttachFileRequest) {
        try {
            LogUtils.LOGD(Status.class.getSimpleName(), "isRequestSuccessful: " + isRequestSuccessful + ", isAttachFileRequest: " + isAttachFileRequest + ", response: " + response);

            // If response is for post status and uploading files to server.
            if (!isAttachFileRequest) {
                if (isRequestSuccessful) {
                    isPosted = true;
                    Intent intent = new Intent();
                    if (isTwitterPost) {
                        intent.putExtra("isTwitterPost", true);
                        intent.putExtra("mStatusBodyText", mStatusBodyField.getText().toString().trim());
                        if (mSelectPath != null && !mSelectPath.isEmpty()) {
                            intent.putExtra("imagePath", mSelectPath.get(0));
                        } else if (videoAttachedUrl != null && !videoAttachedUrl.isEmpty()) {
                            intent.putExtra(ConstantVariables.VIDEO_URL, videoAttachedUrl);
                        }
                    } else {
                        intent.putExtra("isTwitterPost", false);
                    }

                    intent.putExtra("isPosted", isPosted);
                    intent.putExtra("isExternalShare", isExternalShare);
                    setResult(ConstantVariables.FEED_REQUEST_CODE, intent);
                    finish();
                } else {
                    isPosted = false;
                    SnackbarUtils.displaySnackbarLongWithListener(findViewById(R.id.rootView),
                            response.optString("message"), new SnackbarUtils.OnSnackbarDismissListener() {
                                @Override
                                public void onSnackbarDismissed() {
                                    finish();
                                }
                            });
                }

            } else {
                if (isRequestSuccessful) {
                    JSONObject mBody = response.optJSONObject("body");
                    if (mBody == null) {
                        JSONArray mBodyArray = response.optJSONArray("body");
                        mBody = mAppConst.convertToJsonObject(mBodyArray);
                    }
                    switch (mAttachType) {
                        case "music":

                            mSongId = mBody.optInt("song_id");
                            String mSongTitle = mBody.optString("title");

                            //Showing uploaded music name and cancel option
                            musicAddedMessage.setVisibility(View.VISIBLE);
                            mAddMusic.setClickable(false);
                            isAttachmentAttached = true;
                            mAddMusic.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));

                            String musicUploadMessage = getResources().
                                    getQuantityString(R.plurals.music_uploading_message_custom, 1, 1) +
                                    " : " + mSongTitle + " " + "(" + getResources().getString(R.string.cancel_attachment)
                                    + ")";
                            int index1 = musicUploadMessage.length() - 7;
                            int index2 = musicUploadMessage.length() - 1;

                            musicAddedMessage.setMovementMethod(LinkMovementMethod.getInstance());
                            musicAddedMessage.setText(musicUploadMessage, TextView.BufferType.SPANNABLE);
                            Spannable mAddVideoTextSpannable = (Spannable) musicAddedMessage.getText();
                            ClickableSpan clickableSpan = new ClickableSpan() {
                                @Override
                                public void onClick(View widget) {

                                    // When cancel the music option then setting the default text color.
                                    // and setting the attach type to null value & enable other attachment options.
                                    mAddMusic.setTextColor(ContextCompat.getColor(mContext, R.color.body_text_2));
                                    mAttachType = "";
                                    isAttachmentAttached = false;
                                    musicAddedMessage.setVisibility(View.GONE);
                                    makeAndRemoveAttachmentClickable(mAddLink, mAddVideo, mAddPhoto, mAddMusic, true);
                                    mSelectedMusicFile = "";
                                }

                                @Override
                                public void updateDrawState(TextPaint ds) {
                                    super.updateDrawState(ds);
                                    ds.setUnderlineText(false);
                                    ds.setColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                                }
                            };
                            mAddVideoTextSpannable.setSpan(clickableSpan, index1, index2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            break;

                        case "link":
                            String url = mBody.optString("url");
                            String title = mBody.optString("title");
                            String description = mBody.optString("description");
                            String image = mBody.optString("thumb");
                            mLinkAttachmentUrl.setText(Html.fromHtml(url));
                            mLinkAttachmentTitle.setText(Html.fromHtml(title));

                            //Checking if image is coming in response or not.
                            // It will return value "null" in some case, so checking this condition also.
                            if (image != null && !image.isEmpty() && !image.equals("null")) {
                                mLinkAttachmentImage.setVisibility(View.VISIBLE);
                                Picasso.with(mContext)
                                        .load(image)
                                        .into(mLinkAttachmentImage);
                            } else {
                                mLinkAttachmentImage.setVisibility(View.GONE);
                            }

                            //Checking if description is coming in response or not.
                            // It will return value "null" in some case, so checking this condition also.
                            if (description != null && !description.equals("null")) {
                                mLinkAttachmentDescription.setVisibility(View.VISIBLE);
                                mLinkAttachmentDescription.setText(Html.fromHtml(description));
                            } else {
                                mLinkAttachmentDescription.setVisibility(View.GONE);
                            }
                            mAddLinkLayout.setVisibility(View.VISIBLE);
                            mEnterLinkText.setVisibility(View.GONE);
                            mAttachLinkButton.setVisibility(View.GONE);
                            mLinkAttachmentBlock.setVisibility(View.VISIBLE);
                            mAddLink.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                            break;
                    }
                } else {
                    //When error occured enable other attachments clickable.
                    makeAndRemoveAttachmentClickable(mAddLink, mAddVideo, mAddPhoto, mAddMusic, true);
                    mAddLinkLayout.setVisibility(View.GONE);
                    mAttachType = "";
                    mSelectedMusicFile = "";
                    if (response != null) {
                        SnackbarUtils.displaySnackbar(findViewById(R.id.rootView),
                                response.optString("message"));
                    }
                }

            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /*
        Method to show photo uploading block.
     */
    public void addPhotoBlock(boolean isOpenPhotoBlock){
        makeAndRemoveAttachmentClickable(mAddVideo, mAddLink, mAddMusic, null, false);
        startImageUploadActivity(mContext, MultiImageSelectorActivity.MODE_MULTI, true, 10, isOpenPhotoBlock);
    }

    // ToDo in future
    public void addLocation(boolean isOpenCheckIn){
        Bundle bundle = new Bundle();
        bundle.putBoolean("openCheckIn", isOpenCheckIn);
        if(selectedLocation != null && selectedLocation.size() != 0){
            bundle.putBundle("locationsBundle", mSelectedLocationBundle);
        }
        CheckInLocationDialog checkInLocationDialog = new CheckInLocationDialog(mContext, bundle);
        checkInLocationDialog.show();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ConstantVariables.READ_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission granted, proceed to the normal flow
                    if (mUploadingOption != null && mUploadingOption.equals("music")) {
                        GlobalFunctions.addMusicBlock(Status.this);
                        makeAndRemoveAttachmentClickable(mAddVideo, mAddPhoto, mAddLink, mAddMusic, false);
                    } else {
                        if (isExternalShare) {
                            Intent intent = getIntent();
                            String action = intent.getAction();
                            String type = intent.getType();

                            if(action != null && type != null){
                                switch (action) {
                                    case Intent.ACTION_SEND:
                                        if (type.startsWith("image/")) {
                                            handleSendImage(intent); // Handle single image being sent
                                        }
                                        break;
                                    case Intent.ACTION_SEND_MULTIPLE:
                                        if (type.startsWith("image/")) {
                                            handleSendMultipleImages(intent); // Handle multiple images being sent
                                        }
                                        break;
                                }
                            }

                        } else {
                            addPhotoBlock(mShowPhotoBlock);
                        }
                    }
                } else{
                    // If user press deny in the permission popup
                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) mContext,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {

                        // Show an expanation to the user After the user
                        // sees the explanation, try again to request the permission.

                        mAlertDialogWithAction.showDialogForAccessPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                                ConstantVariables.READ_EXTERNAL_STORAGE);

                    }else{
                        // If user pressed never ask again on permission popup
                        // show snackbar with open app info button
                        // user can revoke the permission from Permission section of App Info.

                        SnackbarUtils.displaySnackbarOnPermissionResult(mContext, findViewById(R.id.rootView),
                                ConstantVariables.READ_EXTERNAL_STORAGE);

                    }
                }
                break;

            case ConstantVariables.ACCESS_FINE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission granted, proceed to the normal flow
                    addLocation(mOpenCheckIn);
                } else {
                    // If user press deny in the permission popup
                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) mContext,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {

                        // Show an expanation to the user After the user
                        // sees the explanation, try again to request the permission.

                        mAlertDialogWithAction.showDialogForAccessPermission(Manifest.permission.ACCESS_FINE_LOCATION,
                                ConstantVariables.ACCESS_FINE_LOCATION);

                    }else{
                        // If user pressed never ask again on permission popup
                        // show snackbar with open app info button
                        // user can revoke the permission from Permission section of App Info.

                        SnackbarUtils.displaySnackbarOnPermissionResult(mContext, findViewById(R.id.rootView),
                                ConstantVariables.ACCESS_FINE_LOCATION);

                    }
                }
                break;

        }
    }

    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);

        if (sharedText != null) {
            // Update UI to reflect text being shared
            Matcher m = Patterns.WEB_URL.matcher(sharedText);
            if (m.find()) {
                String url = m.group();
                attachLink(url);
            }else{
                mStatusBodyField.setText(sharedText);
            }
        }
    }

    void handleSendImage(Intent intent) {
        Uri imageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            InputStream inputStream = null;
            try {
                inputStream = getContentResolver().openInputStream(imageUri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                // Method to get the uri from bitmap
                Uri tempUri = getImageUri(mContext, bitmap);

                // get real path from uri and add it to mSelectPath
                mSelectPath.add(getRealPathFromURI(tempUri));

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            showSelectedImages(mSelectPath);
        }
    }

    void handleSendMultipleImages(Intent intent) {
        ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        if (imageUris != null) {
            for(int i = 0; i < imageUris.size(); i++){
                InputStream inputStream = null;
                try {
                    inputStream = getContentResolver().openInputStream(imageUris.get(i));
                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                    // Method to get the uri from bitmap
                    Uri tempUri = getImageUri(mContext, bitmap);

                    // get real path from uri and add it to mSelectPath
                    mSelectPath.add(getRealPathFromURI(tempUri));

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
            showSelectedImages(mSelectPath);
        }
    }

    /**
     * Method to retrieve the path of an image URI
     * @param contentUri uri of image.
     * @return returns the real path of image.
     */
    private String getRealPathFromURI(Uri contentUri) {
        String result;
        String[] projection = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(mContext, contentUri, projection, null, null, null);
        Cursor cursor = loader.loadInBackground();
        if(cursor == null){
            result = contentUri.getPath();
        }else {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
            cursor.close();
        }
        return result;
    }

    private void attachLink(String link){
        // Hide Enter Link Text Block in case of External share.
        if(isExternalShare){
            mEnterLinkText.setVisibility(View.GONE);
            mAttachLinkButton.setVisibility(View.GONE);
            mAddLinkText.setVisibility(View.GONE);
        }
        if (!link.isEmpty()) {
            //Checking if link is valid or not.
            if (GlobalFunctions.isValidUrl(link)) {
                mAttachPostUrl = AppConstant.DEFAULT_URL + "advancedactivity/feeds/attach-link";
                mAttachType = "link";
                uriText = link;
                mAppConst.hideKeyboard();
                new UploadAttachmentUtil(Status.this, mAttachPostUrl, uriText, mAttachType).execute();
            } else {
                mEnterLinkText.setError(getResources().getString(R.string.url_not_valid));
            }

        } else {
            mEnterLinkText.setError(getResources().getString(R.string.empty_url_text));
        }
    }

    @Override
    public void onCheckInLocationChanged(Bundle data) {
        selectedLocation.clear();
        if(data != null && !data.isEmpty()) {
            mSelectedLocationBundle = data;
            String locationId = data.getString("placeId");
            String locationLabel = data.getString("locationLabel");
            try {
                mSelectedLocationObject = new JSONObject(data.getString("locationObject"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mLocationPrefix.setText(getResources().getString(R.string.at) + ":");
            selectedLocation.put(locationId, locationLabel);
            createSelectedLocationLayout(locationLabel);
        } else {
            mLocationsLayout.setVisibility(View.GONE);
            /* Finish this Activity
            *  if CheckIn Option Clicked from Main Feed
            *  and If no location is selected from dialog.
            * */
            if(mOpenCheckIn) {
                finish();
            }
        }
    }

    @Override
    public void onCancelButtonClicked(int userId){

        View canceledView=mTaggedFriendsLayout.findViewById(userId);
        if(canceledView!=null){
        mTaggedFriendsLayout.removeView(canceledView);
        selectedFriends.remove(Integer.toString(userId));
        }

        if(selectedFriends == null || selectedFriends.size() ==0){
        mTaggedFriendsLayout.setVisibility(View.GONE);
        }
     }

    public void onAsyncSuccessFacebookResponse(boolean isFacebookPost) {
        uploadFilesAndData();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

        popupWindow.dismiss();

        AddPeopleList addPeopleList = mAddPeopleList.get(position);
        String label = addPeopleList.getmUserLabel();
        int userId = addPeopleList.getmUserId();

        String string = "user_" + userId + "=" + label;

        if (tagString != null && tagString.length() > 0) {
            tagString =  tagString + "&" + string;
        } else {
            tagString = string;
        }

        tagObject = new JSONObject();
        try {
            tagObject.put("tag", tagString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(mAddPeopleAdapter != null){
            mAddPeopleAdapter.clear();
        }
        mStatusBodyField.setText(mStatusBodyField.getText().toString().replace("@" + searchText, label));
        mStatusBodyField.setSelection(mStatusBodyField.getText().length());
    }
}
