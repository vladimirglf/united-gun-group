package com.unitedgungroup.mobiapp.classes.modules.store.utils;


import org.json.JSONArray;

public class OrderInfoModel {
    int orderId,totalDownloads,remainingDownloads,totalOrderCount;
    double orderAmount;
    String orderDate,orderStatus,fileTitle,fileOption,defaultCurrency;
    JSONArray orderOptions;

    public OrderInfoModel(){

    }
    public OrderInfoModel(int orderId,String orderDate,String orderStatus, double orderAmount,
                          JSONArray orderOptions,String defaultCurrency){
        this.orderId =  orderId;
        this.orderDate = orderDate;
        this.orderStatus = orderStatus;
        this.orderAmount = orderAmount;
        this.orderOptions = orderOptions;
        this.defaultCurrency = defaultCurrency;
    }
    public OrderInfoModel(int orderId,String fileTitle,String fileOption, int totalDownloads,
                          int remainingDownloads){
        this.orderId =  orderId;
        this.fileTitle = fileTitle;
        this.fileOption = fileOption;
        this.totalDownloads = totalDownloads;
        this.remainingDownloads = remainingDownloads;
    }
    public int getOrderId() {
        return orderId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public double getOrderAmount() {
        return orderAmount;
    }

    public JSONArray getOrderOptions() {
        return orderOptions;
    }

    public int getTotalDownloads() {
        return totalDownloads;
    }

    public int getRemainingDownloads() {
        return remainingDownloads;
    }

    public String getFileTitle() {
        return fileTitle;
    }

    public String getFileOption() {
        return fileOption;
    }

    public int getTotalOrderCount() {
        return totalOrderCount;
    }

    public void setTotalOrderCount(int totalOrderCount) {
        this.totalOrderCount = totalOrderCount;
    }

    public String getDefaultCurrency() {
        return defaultCurrency;
    }
}
