/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */

package com.unitedgungroup.mobiapp.classes.modules.user.profile;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.activities.PhotoUploadingActivity;
import com.unitedgungroup.mobiapp.classes.common.adapters.ViewPageFragmentAdapter;
import com.unitedgungroup.mobiapp.classes.common.dialogs.AlertDialogWithAction;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnOptionItemClickResponseListener;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnUploadResponseListener;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.utils.BrowseListItems;
import com.unitedgungroup.mobiapp.classes.common.utils.GlobalFunctions;
import com.unitedgungroup.mobiapp.classes.common.utils.GutterMenuUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.LogUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.PreferencesUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.SnackbarUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.SoundUtil;
import com.unitedgungroup.mobiapp.classes.common.utils.UploadFileToServerUtils;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;
import com.unitedgungroup.mobiapp.classes.core.ConstantVariables;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnResponseListener;
import com.unitedgungroup.mobiapp.classes.common.activities.FragmentLoadActivity;
import com.unitedgungroup.mobiapp.classes.modules.photoLightBox.PhotoLightBoxActivity;
import com.unitedgungroup.mobiapp.classes.modules.photoLightBox.PhotoListDetails;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class userProfile extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener,
        OnUploadResponseListener, OnOptionItemClickResponseListener{

    private String userProfileUrl, displayName, mPreviousSelectedModule, mCoverImageUrl;
    private int mPreviousSelectedModuleListingTypeId;
    static int mUserId, index = 0;
    private AppConstant mAppConst;
    private GutterMenuUtils mGutterMenuUtils;
    private BrowseListItems mBrowseList;
    private Toolbar mToolBar;
    private CollapsingToolbarLayout collapsingToolbar;
    private AppBarLayout appBar;
    private ProgressBar mProgressBar;
    private JSONObject mBody, mResponseObject;
    private JSONArray mGutterMenus, mProfileTabs, mUserProfileTabs, coverPhotoMenuArray, profilePhotoMenuArray;
    private ImageView mCoverImage, mProfileImage;
    private TextView mProfileImageMenus, mCoverImageMenus, mContentTitle, mVerifiedIcon;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private Context mContext;
    private ViewPageFragmentAdapter mViewPageFragmentAdapter;
    private String successMessage;
    private CoordinatorLayout mMainContent;
    private ArrayList<PhotoListDetails> mPhotoDetails, mProfilePhotoDetail;
    private Boolean isOrganizerProfile = false, isCoverRequest = false;
    private TextView mToolBarTitle;
    private AlertDialogWithAction mAlertDialogWithAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_pages);

        mAppConst = new AppConstant(this);
        mContext = this;
        mGutterMenuUtils = new GutterMenuUtils(this);
        mGutterMenuUtils.setOnOptionItemClickResponseListener(this);
        mAlertDialogWithAction = new AlertDialogWithAction(mContext);
        mProfileTabs = mUserProfileTabs = new JSONArray();
        mUserId = getIntent().getExtras().getInt(ConstantVariables.USER_ID);

        String profileType = getIntent().getExtras().getString(ConstantVariables.PROFILE_TYPE);
        if (profileType != null && !profileType.isEmpty() && profileType.equals("organizer_profile")) {
            isOrganizerProfile = true;
        }

        mPreviousSelectedModule = PreferencesUtils.getCurrentSelectedModule(mContext);
        if (mPreviousSelectedModule != null && mPreviousSelectedModule.equals("sitereview_listing")) {
            mPreviousSelectedModuleListingTypeId = PreferencesUtils.getCurrentSelectedListingId(mContext);
        }

        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        mToolBarTitle = (TextView) findViewById(R.id.toolbar_title);
        mToolBarTitle.setSelected(true);
        setSupportActionBar(mToolBar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getResources().getString(R.string.blank_string));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mPhotoDetails = new ArrayList<>();
        mProfilePhotoDetail = new ArrayList<>();

        findViewById(R.id.cover_layout).getLayoutParams().height = mContext.getResources().
                getDimensionPixelSize(R.dimen.app_bar_height_medium);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mContentTitle = (TextView) findViewById(R.id.content_title);
        mVerifiedIcon = (TextView) findViewById(R.id.verified_icon);
        mVerifiedIcon.setTypeface(GlobalFunctions.getFontIconTypeFace(mContext));
        mCoverImage = (ImageView) findViewById(R.id.coverImage);
        mProfileImage = (ImageView) findViewById(R.id.profile_image);
        mCoverImageMenus = (TextView) findViewById(R.id.cover_image_menus);
        mProfileImageMenus = (TextView) findViewById(R.id.profile_image_menus);
        mCoverImageMenus.setTypeface(GlobalFunctions.getFontIconTypeFace(mContext));
        mProfileImageMenus.setTypeface(GlobalFunctions.getFontIconTypeFace(mContext));

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mTabLayout = (TabLayout) findViewById(R.id.slidingTabs);
        mMainContent = (CoordinatorLayout) findViewById(R.id.main_content);
        appBar = (AppBarLayout) findViewById(R.id.appbar);
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        if (isOrganizerProfile) {
            userProfileUrl = AppConstant.DEFAULT_URL + "advancedevents/organizer/" + mUserId;
        } else {
            userProfileUrl = AppConstant.DEFAULT_URL + "user/profile/" + mUserId;
        }

        // IF profile photo is uploaded via album view page.
        if (getIntent().getBooleanExtra(ConstantVariables.IS_PHOTO_UPLOADED, false)) {
            mAppConst.refreshUserData();
        }
        makeRequest();
    }

    public void  makeRequest(){

        try {
            mAppConst.getJsonResponseFromUrl(userProfileUrl, new OnResponseListener() {
                @Override
                public void onTaskCompleted(JSONObject jsonObject) {
                    mBody = jsonObject;
                    mProgressBar.setVisibility(View.GONE);

                    if (mBody != null) {
                        mGutterMenus = mBody.optJSONArray("gutterMenu");
                        mUserProfileTabs = mBody.optJSONArray("profile_tabs");
                        if (mUserProfileTabs == null) {
                            mUserProfileTabs = mBody.optJSONArray("profileTabs");
                        }
                        mResponseObject = mBody.optJSONObject("response");
                        if (mResponseObject == null) {
                            mResponseObject = mBody;

                        }

                        if (mResponseObject != null) {
                            mUserId = mResponseObject.optInt("user_id");
                            String userImageProfile = null;
                            if (isOrganizerProfile) {
                                mUserId = mResponseObject.optInt("organizer_id");
                                displayName = jsonObject.optString("title");
                                userImageProfile = jsonObject.optString("owner_image");
                                if (userImageProfile == null || userImageProfile.isEmpty() )
                                    userImageProfile = jsonObject.optString("image");

                                if (userImageProfile != null && !userImageProfile.isEmpty()) {
                                    Picasso.with(getApplicationContext())
                                            .load(userImageProfile)
                                            .into(mCoverImage);
                                }

                            } else {
                                displayName = mResponseObject.optString("displayname");
                                userImageProfile = mResponseObject.optString("image");
                                // When the user name or profile image is changed then Updating the user data.
                                if (!mAppConst.isLoggedOutUser()
                                        && PreferencesUtils.getUserDetail(mContext) != null) {
                                    try {
                                        JSONObject userDetail = new JSONObject(PreferencesUtils.getUserDetail(mContext));
                                        String userName = userDetail.optString("displayname");
                                        String coverImageUrl = userDetail.optString("image");
                                        if (userDetail.optInt("user_id") == mUserId && (!displayName.equals(userName) ||
                                                !userImageProfile.equals(coverImageUrl))) {
                                            userDetail.put("displayname", displayName);
                                            userDetail.put("image", userImageProfile);
                                            userDetail.put("image_profile", mResponseObject.optString("image_profile"));
                                            PreferencesUtils.updateUserPreferences(mContext, userDetail.toString(),
                                                    PreferencesUtils.getUserPreferences(mContext).getString("oauth_secret", null),
                                                    PreferencesUtils.getUserPreferences(mContext).getString("oauth_token", null));
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            mBrowseList = new BrowseListItems("", mUserId, displayName);

                            mPhotoDetails.clear();
                            mCoverImageUrl = mResponseObject.optString("cover");

                            //Showing content title
                            mContentTitle.setText(displayName);
                            collapsingToolbar.setTitle(displayName);
                            mToolBarTitle.setText(displayName);
                            CustomViews.setCollapsingToolBarTitle(collapsingToolbar);

                            // If not null then showing user cover image plugin views.
                            if (mCoverImageUrl != null && !mCoverImageUrl.isEmpty()) {
                                mProfileImage.setVisibility(View.VISIBLE);
                                coverPhotoMenuArray = mResponseObject.optJSONArray("coverPhotoMenu");
                                profilePhotoMenuArray = mResponseObject.optJSONArray("mainPhotoMenu");
                                if (coverPhotoMenuArray != null && coverPhotoMenuArray.length() > 0) {
                                    mCoverImageMenus.setVisibility(View.VISIBLE);
                                    mCoverImageMenus.setText("\uf030");
                                }
                                if (profilePhotoMenuArray != null && profilePhotoMenuArray.length() > 0) {
                                    mProfileImageMenus.setVisibility(View.VISIBLE);
                                    mProfileImageMenus.setText("\uf030");
                                }

                                //TODO in future.
//                                int verified = mResponseObject.optInt("verified");
//                                if (verified == 1) {
//                                    mVerifiedIcon.setVisibility(View.VISIBLE);
//                                    mVerifiedIcon.setText("\uf058");
//                                }

                                //Showing profile image.
                                if (userImageProfile != null && !userImageProfile.isEmpty()) {
                                    Picasso.with(getApplicationContext())
                                            .load(userImageProfile)
                                            .placeholder(R.drawable.default_user_profile)
                                            .error(R.drawable.default_user_profile)
                                            .into(mProfileImage);
                                }

                                //Showing Cover image.
                                Picasso.with(getApplicationContext())
                                        .load(mCoverImageUrl)
                                        .into(mCoverImage);

                                mProfilePhotoDetail.clear();
                                mProfilePhotoDetail.add(new PhotoListDetails(userImageProfile));
                                mPhotoDetails.add(new PhotoListDetails(mCoverImageUrl));

                            } else {
                                if (userImageProfile != null && !userImageProfile.isEmpty()) {
                                    Picasso.with(getApplicationContext())
                                            .load(userImageProfile)
                                            .into(mCoverImage);
                                }
                                mPhotoDetails.add(new PhotoListDetails(userImageProfile));
                            }

                            mCoverImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (mCoverImageUrl != null && !mCoverImageUrl.isEmpty() &&
                                            coverPhotoMenuArray != null && coverPhotoMenuArray.length() > 0) {
                                        isCoverRequest = true;
                                        mGutterMenuUtils.showPopup(mCoverImageMenus, coverPhotoMenuArray,
                                                mBrowseList, ConstantVariables.USER_MENU_TITLE);
                                    } else {
                                        openLightBox(true);
                                    }

                                }
                            });

                            mProfileImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (mCoverImageUrl != null && !mCoverImageUrl.isEmpty() &&
                                            profilePhotoMenuArray != null && profilePhotoMenuArray.length() > 0) {
                                        isCoverRequest = false;
                                        mGutterMenuUtils.showPopup(mProfileImageMenus, profilePhotoMenuArray,
                                                mBrowseList, ConstantVariables.USER_MENU_TITLE);

                                    } else {
                                        openLightBox(false);
                                    }
                                }
                            });

                            if (mUserProfileTabs != null) {
                                if(mProfileTabs != null && mProfileTabs.length() != 0){
                                    mProfileTabs = new JSONArray();
                                }
                                for (int i = 0; i < mUserProfileTabs.length(); i++) {
                                    JSONObject singleJsonObject = mUserProfileTabs.optJSONObject(i);
                                    String tabName = singleJsonObject.optString("name");
                                    int totalItemCount = singleJsonObject.optInt("totalItemCount");

                                    if (!((!(tabName.equals("update") || tabName.equals("info") ||
                                            tabName.equals("organizer_info") || tabName.equals("organizer_events")) &&
                                            (totalItemCount == 0)) || (tabName.equals("forum_topics")))) {
                                        mProfileTabs.put(singleJsonObject);
                                    }
                                }

                                Bundle bundle = new Bundle();
                                bundle.putString(ConstantVariables.SUBJECT_TYPE, "user");
                                bundle.putInt(ConstantVariables.SUBJECT_ID, mUserId);
                                bundle.putString(ConstantVariables.MODULE_NAME, "userProfile");
                                bundle.putString(ConstantVariables.EXTRA_MODULE_TYPE, "userProfile");
                                bundle.putInt(ConstantVariables.USER_ID, mUserId);
                                bundle.putString(ConstantVariables.RESPONSE_OBJECT, mResponseObject.toString());

                                // Setup the viewPager
                                mViewPageFragmentAdapter = new ViewPageFragmentAdapter(mContext,
                                        getSupportFragmentManager(), mProfileTabs, bundle);
                                mViewPager.setAdapter(mViewPageFragmentAdapter);
                                mViewPager.setOffscreenPageLimit(mViewPageFragmentAdapter.getCount() + 1);
                                // This method ensures that tab selection events update the ViewPager and page changes update the selected tab.
                                mTabLayout.setupWithViewPager(mViewPager);

                                mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                                    @Override
                                    public void onTabSelected(TabLayout.Tab tab) {
                                        try {
                                            JSONObject jsonObject = mProfileTabs.getJSONObject(tab.getPosition());
                                            PreferencesUtils.setCurrentSelectedListingId(mContext, jsonObject.optInt(ConstantVariables.LISTING_TYPE_ID));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    @Override
                                    public void onTabUnselected(TabLayout.Tab tab) {

                                    }

                                    @Override
                                    public void onTabReselected(TabLayout.Tab tab) {

                                    }
                                });
                            }
                        }

                        /* Call OnPrepareOptionsMenu forceFully to set Menu items */

                        if (mGutterMenus != null) {
                            invalidateOptionsMenu();
                        }
                    }
                }

                @Override
                public void onErrorInExecutingTask(String message, boolean isRetryOption) {

                    // Show Privacy Message to user if not authorized to view this
                    mProgressBar.setVisibility(View.GONE);
                    SnackbarUtils.displaySnackbarLongWithListener(mMainContent, message,
                            new SnackbarUtils.OnSnackbarDismissListener() {
                                @Override
                                public void onSnackbarDismissed() {
                                    if(!isFinishing()){
                                        finish();
                                    }
                                }
                            });
                }
            });
        }catch (IllegalStateException|NullPointerException e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.default_menu_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            // Playing backSound effect when user tapped on back button from tool bar.
            if (PreferencesUtils.isSoundEffectEnabled(mContext)) {
                SoundUtil.playSoundEffectOnBackPressed(mContext);
            }
        }else{
            if(mGutterMenus != null) {

                mGutterMenuUtils.onMenuOptionItemSelected(mMainContent, findViewById(item.getItemId()),
                        id, mGutterMenus);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Method to open photoLightBox when user click on view image
     * @param isCoverRequest parameter to decide whether it is user profile or user cover.
     */
    public void openLightBox(boolean isCoverRequest) {

        Bundle bundle = new Bundle();
        if (isCoverRequest) {
            bundle.putSerializable(PhotoLightBoxActivity.EXTRA_IMAGE_URL_LIST, mPhotoDetails);
        } else {
            bundle.putSerializable(PhotoLightBoxActivity.EXTRA_IMAGE_URL_LIST, mProfilePhotoDetail);
        }
        Intent i = new Intent(mContext, PhotoLightBoxActivity.class);
        i.putExtra(ConstantVariables.TOTAL_ITEM_COUNT, 1);
        i.putExtra(ConstantVariables.SHOW_OPTIONS, false);
        i.putExtras(bundle);
        startActivityForResult(i, ConstantVariables.VIEW_LIGHT_BOX);

    }

    private void startImageUploading(){
        Intent intent = new Intent(mContext, PhotoUploadingActivity.class);
        intent.putExtra("selection_mode", true);
        intent.putExtra(ConstantVariables.IS_PHOTO_UPLOADED, true);
        startActivityForResult(intent, ConstantVariables.PAGE_EDIT_CODE);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        menu.clear();
        if(mGutterMenus != null){
            mGutterMenuUtils.showOptionMenus(menu, mGutterMenus, ConstantVariables.USER_MENU_TITLE,
                    mBrowseList);
        }
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {

        index = i;
        CustomViews.showMarqueeTitle(i, collapsingToolbar, mToolBar, mToolBarTitle, displayName);
    }

    @Override
    public void onUploadResponse(JSONObject jsonObject, boolean isRequestSuccessful) {

        if (isRequestSuccessful) {
            SnackbarUtils.displaySnackbarLongTime(mMainContent, successMessage);
            mProgressBar.setVisibility(View.VISIBLE);
            mProgressBar.bringToFront();
            mAppConst.refreshUserData();
            makeRequest();

        } else {
            SnackbarUtils.displaySnackbarLongTime(mMainContent, jsonObject.optString("message"));
        }

    }

    @Override
    public void onItemDelete(String successMessage) {

    }

    @Override
    public void onOptionItemActionSuccess(Object itemList, String menuName) {
        mBrowseList = (BrowseListItems) itemList;

        switch (menuName) {
            case "view_profile_photo":
                    case "view_cover_photo":
                        openLightBox(isCoverRequest);
                        break;

                    case "upload_cover_photo":
                    case "upload_photo":
                        if(!mAppConst.checkManifestPermission(Manifest.permission.READ_EXTERNAL_STORAGE)){
                            mAppConst.requestForManifestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                                    ConstantVariables.READ_EXTERNAL_STORAGE);
                        }else{
                            startImageUploading();
                }
                break;

            case "choose_from_album":
                Bundle bundle = new Bundle();
                bundle.putString(ConstantVariables.FRAGMENT_NAME, "album");
                bundle.putString(ConstantVariables.CONTENT_TITLE, mBrowseList.getmBrowseListTitle());
                bundle.putBoolean(ConstantVariables.IS_WAITING, false);
                bundle.putInt("user_id", mUserId);
                bundle.putBoolean("isMemberAlbums", true);
                bundle.putBoolean("isCoverRequest", isCoverRequest);
                if (isOrganizerProfile) {
                    bundle.putString(ConstantVariables.PROFILE_TYPE, "organizer_profile");
                }

                Intent newIntent = new Intent(mContext, FragmentLoadActivity.class);
                newIntent.putExtras(bundle);
                startActivityForResult(newIntent, ConstantVariables.PAGE_EDIT_CODE);
                ((Activity)mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;

            default:
                finish();
                startActivity(getIntent());
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        appBar.addOnOffsetChangedListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        appBar.removeOnOffsetChangedListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mPreviousSelectedModule != null) {
            PreferencesUtils.updateCurrentModule(mContext, mPreviousSelectedModule);
            if (mPreviousSelectedModule.equals("sitereview_listing") && mPreviousSelectedModuleListingTypeId != 0) {
                PreferencesUtils.setCurrentSelectedListingId(mContext, mPreviousSelectedModuleListingTypeId);
            }
        }
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){

            case ConstantVariables.VIEW_PAGE_CODE:
                makeRequest();
                break;

            case ConstantVariables.PAGE_EDIT_CODE:
                if (resultCode == ConstantVariables.PAGE_EDIT_CODE && data != null) {
                    ArrayList<String> resultList = data.getStringArrayListExtra(ConstantVariables.PHOTO_LIST);
                    String postUrl;
                    if (isCoverRequest) {
                        postUrl = AppConstant.DEFAULT_URL + "user/profilepage/upload-cover-photo/user_id/" +
                                mUserId + "/special/cover";
                        successMessage = mContext.getResources().getString(R.string.cover_photo_updated);
                    } else {
                        postUrl = AppConstant.DEFAULT_URL + "user/profilepage/upload-cover-photo/user_id/" +
                                mUserId + "/special/profile";
                        successMessage = mContext.getResources().getString(R.string.profile_photo_updated);
                    }
                    new UploadFileToServerUtils(userProfile.this, postUrl, resultList, this).execute();
                }
                break;

            case ConstantVariables.USER_PROFILE_CODE:
                if (resultCode == ConstantVariables.USER_PROFILE_CODE) {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mProgressBar.bringToFront();
                    mAppConst.refreshUserData();
                    makeRequest();
                }
                break;

            case ConstantVariables.FEED_REQUEST_CODE:
                /* Reset the view pager adapter if any status is posted using AAF */
                if(resultCode == ConstantVariables.FEED_REQUEST_CODE && mViewPager != null) {
                    mViewPager.setAdapter(mViewPageFragmentAdapter);
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case ConstantVariables.READ_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, proceed to the normal flow.
                    startImageUploading();
                } else {
                    // If user deny the permission popup
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Show an explanation to the user, After the user
                        // sees the explanation, try again to request the permission.

                        mAlertDialogWithAction.showDialogForAccessPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                                ConstantVariables.READ_EXTERNAL_STORAGE);

                    }else{
                        // If user pressed never ask again on permission popup
                        // show snackbar with open app info button
                        // user can revoke the permission from Permission section of App Info.

                        SnackbarUtils.displaySnackbarOnPermissionResult(mContext, mMainContent,
                                ConstantVariables.READ_EXTERNAL_STORAGE);

                    }
                }
                break;

        }
    }

}
