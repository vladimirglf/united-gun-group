/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */

package com.unitedgungroup.mobiapp.classes.common.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.method.LinkMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.unitedgungroup.mobiapp.classes.common.activities.CreateNewEntry;
import com.unitedgungroup.mobiapp.classes.common.ui.BaseButton;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.utils.GlobalFunctions;
import com.unitedgungroup.mobiapp.classes.common.utils.SnackbarUtils;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;
import com.unitedgungroup.mobiapp.classes.core.ConstantVariables;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnResponseListener;
import com.unitedgungroup.mobiapp.classes.modules.user.profile.userProfile;
import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.utils.BrowseListItems;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BrowseMemberAdapter extends ArrayAdapter<BrowseListItems> {

    private Context mContext;
    private int mLayoutResID;
    private List<BrowseListItems> mBrowseItemlist;
    private AppConstant mAppConst;
    private View mRootView;
    private BrowseListItems listItems;
    private Typeface fontIcon;
    private int mItemPosition;
    private boolean mIsMemberFriends;
    public static int sFriendId;

    public BrowseMemberAdapter(Context context, int layoutResourceID, List<BrowseListItems> listItem,
                               boolean isMemberFriends) {

        super(context, layoutResourceID, listItem);

        mContext = context;
        mLayoutResID = layoutResourceID;
        mBrowseItemlist = listItem;
        mAppConst = new AppConstant(context);

        mIsMemberFriends = isMemberFriends;
        fontIcon = GlobalFunctions.getFontIconTypeFace(mContext);
    }

    public View getView(int position, final View convertView, ViewGroup parent) {

        mRootView = convertView;
        final ListItemHolder listItemHolder;

        if (mRootView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            listItemHolder = new ListItemHolder();
            mRootView = inflater.inflate(mLayoutResID, parent, false);
            listItemHolder.mOwnerName = (TextView) mRootView.findViewById(R.id.ownerTitle);
            listItemHolder.mMemberOptions = (TextView) mRootView.findViewById(R.id.memberOption);
            listItemHolder.mMemberOptions.setTypeface(fontIcon);
            listItemHolder.mMemberOptions.setMovementMethod(LinkMovementMethod.getInstance());
            listItemHolder.mListImage = (ImageView) mRootView.findViewById(R.id.ownerImage);
            listItemHolder.mAge = (TextView) mRootView.findViewById(R.id.age);
            listItemHolder.mLocation = (TextView) mRootView.findViewById(R.id.location);
            listItemHolder.mMutualFriendCount = (TextView) mRootView.findViewById(R.id.mutualFriendCount);
            listItemHolder.mAgeIcon = (TextView) mRootView.findViewById(R.id.age_icon);
            listItemHolder.mLocationIcon = (TextView) mRootView.findViewById(R.id.location_icon);
            listItemHolder.mMutualFriendIcon = (TextView) mRootView.findViewById(R.id.mutualFriendIcon);
            listItemHolder.mMemberVerifiedIcon = (TextView) mRootView.findViewById(R.id.verified_icon);
            listItemHolder.mMemberOnlineIcon = (TextView) mRootView.findViewById(R.id.online_icon);
            listItemHolder.mAgeIcon.setTypeface(GlobalFunctions.getFontIconTypeFace(mContext));
            listItemHolder.mLocationIcon.setTypeface(GlobalFunctions.getFontIconTypeFace(mContext));
            listItemHolder.mMutualFriendIcon.setTypeface(GlobalFunctions.getFontIconTypeFace(mContext));
            listItemHolder.mMemberVerifiedIcon.setTypeface(GlobalFunctions.getFontIconTypeFace(mContext));
            listItemHolder.mMemberOnlineIcon.setTypeface(GlobalFunctions.getFontIconTypeFace(mContext));

            listItemHolder.mAddToList = (BaseButton) mRootView.findViewById(R.id.add_to_list);
            mRootView.findViewById(R.id.option_icon_layout).setVisibility(View.GONE);
            mRootView.setTag(listItemHolder);
        } else {
            listItemHolder = (ListItemHolder) mRootView.getTag();
        }

        listItemHolder.mMemberOptions.setGravity(Gravity.CENTER_VERTICAL);
        listItemHolder.mMemberOptions.setTag(position);

        listItems = this.mBrowseItemlist.get(position);

        listItemHolder.mUserId = listItems.getmUserId();

        /*
        Set Data in the List View Items
         */

        if (listItems.getmBrowseImgUrl() != null && !listItems.getmBrowseImgUrl().isEmpty()) {
            Picasso.with(mContext)
                    .load(listItems.getmBrowseImgUrl())
                    .placeholder(R.drawable.default_user_profile)
                    .into(listItemHolder.mListImage);
        }
        listItemHolder.mListImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent userProfileIntent = new Intent(mContext, userProfile.class);
                userProfileIntent.putExtra("user_id", listItemHolder.mUserId);
                ((Activity)mContext).startActivityForResult(userProfileIntent, ConstantVariables.USER_PROFILE_CODE);
                ((Activity)mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        listItemHolder.mOwnerName.setText(listItems.getmBrowseListOwnerTitle());
        listItemHolder.mOwnerName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent userProfileIntent = new Intent(mContext, userProfile.class);
                userProfileIntent.putExtra("user_id", listItemHolder.mUserId);
                ((Activity)mContext).startActivityForResult(userProfileIntent, ConstantVariables.USER_PROFILE_CODE);
                ((Activity)mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        if (listItems.getmCanAddToList() == 1) {
            listItemHolder.mAddToList.setVisibility(View.VISIBLE);

            RelativeLayout.LayoutParams layoutParams = CustomViews.getWrapRelativeLayoutParams();
            layoutParams.addRule(RelativeLayout.RIGHT_OF, R.id.ownerImage);

            int margin = (int) mContext.getResources().getDimension(R.dimen.margin_10dp);

            layoutParams.setMargins(margin,margin,margin,margin);
            listItemHolder.mOwnerName.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams buttonLayoutParams = CustomViews.getWrapRelativeLayoutParams();
            buttonLayoutParams.addRule(RelativeLayout.RIGHT_OF, R.id.ownerImage);
            buttonLayoutParams.addRule(RelativeLayout.BELOW, R.id.ownerTitle);
            buttonLayoutParams.setMargins(margin, 0, margin, margin);

            listItemHolder.mAddToList.setLayoutParams(buttonLayoutParams);


            listItemHolder.mAddToList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sFriendId = listItemHolder.mUserId;
                    String url = AppConstant.DEFAULT_URL + "user/list-add?friend_id=" +
                            listItemHolder.mUserId;
                    Intent addToList = new Intent(mContext, CreateNewEntry.class);
                    addToList.putExtra("friend_id", listItemHolder.mUserId);
                    addToList.putExtra(ConstantVariables.CREATE_URL, url);
                    addToList.putExtra(ConstantVariables.EXTRA_MODULE_TYPE, "add_to_friend_list");
                    ((Activity)mContext).startActivityForResult(addToList, ConstantVariables.CREATE_REQUEST_CODE);
                    ((Activity)mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            });

        } else {
            listItemHolder.mAddToList.setVisibility(View.GONE);
        }

        listItemHolder.menuJsonArray  = listItems.getmMemberMenus();

        if(listItemHolder.menuJsonArray  == null){
            listItemHolder.mMemberOptions.setVisibility(View.GONE);
        }else {
            listItemHolder.mMemberOptions.setVisibility(View.VISIBLE);
            String menuName = listItemHolder.menuJsonArray.optString("name");
            if(menuName != null){
                switch (menuName){
                    case "add_friend":
                    case "accept_request":
                    case "member_follow":
                        listItemHolder.mMemberOptions.setText("\uf234");
                        break;
                    case "remove_friend":
                    case "member_unfollow":
                        listItemHolder.mMemberOptions.setText("\uf235");
                        break;
                    case "cancel_request":
                    case "cancel_follow":
                        listItemHolder.mMemberOptions.setText("\uf00d");
                        break;
                }
            }
            listItemHolder.mMemberOptions.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if(mIsMemberFriends)
                        listItemHolder.mMemberOptions.setText("\uf110");
                    mItemPosition = (int) v.getTag();
                    performLinkAction(mItemPosition);
                }
            });
        }

        // Checking condition for AdvancedMember and show data accordingly.
        if (listItems.getmIsSiteMember() == 1) {

            // Showing age of the member if exists.
            if (listItems.getmAge() != 0) {
                listItemHolder.mAgeIcon.setText("\uf1fd");
                listItemHolder.mAge.setText(mContext.getResources().
                        getQuantityString(R.plurals.age_text,
                                listItems.getmAge(),
                                listItems.getmAge()));
                listItemHolder.mAgeIcon.setVisibility(View.VISIBLE);
                listItemHolder.mAge.setVisibility(View.VISIBLE);
            } else {
                listItemHolder.mAge.setVisibility(View.GONE);
                listItemHolder.mAgeIcon.setVisibility(View.GONE);
            }

            //Showing location if exists
            if (listItems.getmLocation() != null && !listItems.getmLocation().isEmpty()) {
                listItemHolder.mLocationIcon.setText("\uf041");
                listItemHolder.mLocation.setText(listItems.getmLocation());
                listItemHolder.mLocation.setVisibility(View.VISIBLE);
                listItemHolder.mLocationIcon.setVisibility(View.VISIBLE);
            } else {
                listItemHolder.mLocationIcon.setVisibility(View.GONE);
                listItemHolder.mLocation.setVisibility(View.GONE);
            }

            // Showing mutual friend count of the member if exists.
            if (listItems.getmMutualFriendCount() >= 1) {
                listItemHolder.mMutualFriendIcon.setText("\uf0c0");
                listItemHolder.mMutualFriendCount.setText(mContext.getResources().
                        getQuantityString(R.plurals.mutual_friend_text,
                                listItems.getmMutualFriendCount(),
                                listItems.getmMutualFriendCount()));
                listItemHolder.mMutualFriendIcon.setText("\uf0c0");
                listItemHolder.mMutualFriendCount.setVisibility(View.VISIBLE);
                listItemHolder.mMutualFriendIcon.setVisibility(View.VISIBLE);
            } else {
                listItemHolder.mMutualFriendCount.setVisibility(View.GONE);
                listItemHolder.mMutualFriendIcon.setVisibility(View.GONE);
            }

            // TODO in future.
            // Showing verified icon if the member is verified.
//            if (listItems.getmIsVerified() == 1) {
//                listItemHolder.mMemberVerifiedIcon.setText("\uf058");
//                listItemHolder.mMemberVerifiedIcon.setVisibility(View.VISIBLE);
//            } else {
//                listItemHolder.mMemberVerifiedIcon.setVisibility(View.GONE);
//            }

            //Showing online icon if the member is online.
            if (listItems.getmIsMemberOnline() == 1) {
                listItemHolder.mMemberOnlineIcon.setText("\uf111");
                listItemHolder.mMemberOnlineIcon.setVisibility(View.VISIBLE);
            } else {
                listItemHolder.mMemberOnlineIcon.setVisibility(View.GONE);
            }

            //Increasing height and width of image as we are showing more info.
            listItemHolder.mListImage.getLayoutParams().height = mContext.getResources().
                    getDimensionPixelSize(R.dimen.host_image_width_height);
            listItemHolder.mListImage.getLayoutParams().width = mContext.getResources().
                    getDimensionPixelSize(R.dimen.host_image_width_height);

        } else {
            listItemHolder.mAge.setVisibility(View.GONE);
            listItemHolder.mLocation.setVisibility(View.GONE);
            listItemHolder.mMutualFriendCount.setVisibility(View.GONE);
            listItemHolder.mAgeIcon.setVisibility(View.GONE);
            listItemHolder.mLocationIcon.setVisibility(View.GONE);
            listItemHolder.mMutualFriendIcon.setVisibility(View.GONE);
            listItemHolder.mMemberVerifiedIcon.setVisibility(View.GONE);
        }

        return mRootView;
    }

    private static class ListItemHolder{
        ImageView mListImage;
        TextView mOwnerName, mMemberOptions, mAge, mLocation, mMutualFriendCount, mAgeIcon, mLocationIcon,
                mMutualFriendIcon, mMemberVerifiedIcon, mMemberOnlineIcon;
        JSONObject menuJsonArray;
        BaseButton mAddToList;
        int mUserId;
    }

    public void performLinkAction(int itemPosition){

        String dialogueMessage = null, dialogueTitle = null, buttonTitle = null, successMessage = null;
        String changedMenuName = null, changedMenuUrl = null;
        final Map<String, String> postParams = new HashMap<>();
        final BrowseListItems memberList = mBrowseItemlist.get(itemPosition);
        final JSONObject menuArray = memberList.getmMemberMenus();
        final String menuName = menuArray.optString("name");

        final String url = AppConstant.DEFAULT_URL + menuArray.optString("url");
        JSONObject urlParamsJsonObject = menuArray.optJSONObject("urlParams");
        if(urlParamsJsonObject != null && urlParamsJsonObject.length() != 0){
            JSONArray urlParamsNames = urlParamsJsonObject.names();
            for(int i = 0; i < urlParamsJsonObject.length(); i++){

                String name = urlParamsNames.optString(i);
                String value = urlParamsJsonObject.optString(name);
                    postParams.put(name, value);
            }
        }

        switch (menuName){
            case "add_friend":
                dialogueMessage = mContext.getResources().getString(R.string.add_friend_message);
                dialogueTitle = mContext.getResources().getString(R.string.add_friend_title);
                buttonTitle = mContext.getResources().getString(R.string.add_friend_button);
                successMessage = mContext.getResources().getString(R.string.add_friend_success_message);
                if (memberList.getIsFriendshipVerified() == 1) {
                    changedMenuUrl = "user/cancel";
                    changedMenuName = "cancel_request";
                } else {
                    changedMenuUrl = "user/remove";
                    changedMenuName = "remove_friend";
                }
                break;
            case "member_follow":
                dialogueMessage = mContext.getResources().getString(R.string.add_friend_message);
                dialogueTitle = mContext.getResources().getString(R.string.add_friend_title);
                buttonTitle = mContext.getResources().getString(R.string.add_friend_button);
                successMessage = mContext.getResources().getString(R.string.add_friend_success_message);
                if (memberList.getIsFriendshipVerified() == 1) {
                    changedMenuUrl = "user/cancel";
                    changedMenuName = "cancel_follow";
                } else {
                    changedMenuUrl = "user/remove";
                    changedMenuName = "member_unfollow";
                }
                break;
            case "accept_request":
                dialogueMessage = mContext.getResources().getString(R.string.accept_friend_request_message);
                dialogueTitle = mContext.getResources().getString(R.string.accept_friend_request_title);
                buttonTitle = mContext.getResources().getString(R.string.accept_friend_request_button);
                successMessage = mContext.getResources().getString(R.string.accept_friend_request_success_message);
                changedMenuName = "remove_friend";
                changedMenuUrl = "user/remove";
                break;
            case "remove_friend":
            case "member_unfollow":
                dialogueMessage = mContext.getResources().getString(R.string.remove_friend_message);
                dialogueTitle = mContext.getResources().getString(R.string.remove_friend_title);
                buttonTitle = mContext.getResources().getString(R.string.remove_friend_button);
                successMessage = mContext.getResources().getString(R.string.remove_friend_success_message);
                changedMenuUrl = "user/add";
                if (menuName.equals("member_unfollow")) {
                    changedMenuName = "member_follow";
                } else {
                    changedMenuName = "add_friend";
                }
                break;
            case "cancel_request":
            case "cancel_follow":
                dialogueMessage = mContext.getResources().getString(R.string.cancel_friend_request_message);
                dialogueTitle = mContext.getResources().getString(R.string.cancel_friend_request_title);
                buttonTitle = mContext.getResources().getString(R.string.cancel_friend_request_button);
                successMessage = mContext.getResources().getString(R.string.cancel_friend_request_success_message);
                changedMenuUrl = "user/add";
                if (menuName.equals("cancel_follow")) {
                    changedMenuName = "member_follow";
                } else {
                    changedMenuName = "add_friend";
                }
                break;
        }

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(mContext);

        alertBuilder.setMessage(dialogueMessage);
        alertBuilder.setTitle(dialogueTitle);

        final String finalSuccessMessage = successMessage;
        final String finalChangedMenuName = changedMenuName;
        final String finalChangedMenuUrl = changedMenuUrl;
        alertBuilder.setPositiveButton(buttonTitle, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                mAppConst.postJsonResponseForUrl(url, postParams, new OnResponseListener() {
                    @Override
                    public void onTaskCompleted(JSONObject jsonObject) {

                        View view = ((Activity)mContext).getCurrentFocus();
                        SnackbarUtils.displaySnackbar(view, finalSuccessMessage);
                        try {
                            menuArray.put("name", finalChangedMenuName);
                            menuArray.put("url", finalChangedMenuUrl);
                            memberList.setmMemberMenus(menuArray);
                            memberList.setCanAddToList(0);
                            notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                        View view = ((Activity)mContext).getCurrentFocus();
                        SnackbarUtils.displaySnackbar(view, message);
                    }
                });
            }});
        alertBuilder.setNegativeButton(mContext.getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                try {
                    menuArray.put("name", menuName);
                    memberList.setmMemberMenus(menuArray);
                    notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.cancel();
            }
        });
        alertBuilder.create().show();

    }
}
