/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */

package com.unitedgungroup.mobiapp.classes.modules.likeNComment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.unitedgungroup.mobiapp.classes.common.dialogs.AlertDialogWithAction;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnMenuClickResponseListener;
import com.unitedgungroup.mobiapp.classes.common.ui.CircularImageView;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.utils.GlobalFunctions;
import com.unitedgungroup.mobiapp.classes.common.utils.GutterMenuUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.Smileys;
import com.unitedgungroup.mobiapp.classes.common.utils.SnackbarUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.UrlUtil;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;


import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.ui.ExpandableTextView;
import com.unitedgungroup.mobiapp.classes.core.ConstantVariables;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnResponseListener;
import com.unitedgungroup.mobiapp.classes.modules.user.profile.userProfile;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;


public class CommentAdapter extends ArrayAdapter<CommentList> implements OnMenuClickResponseListener {

    private Context mContext;
    private List<CommentList> mCommentListItems;
    private CommentList mCommentList, mListComment;
    private int mLayoutResID, mPosition, itemPosition, mSubjectId, mActionId, mLikeCountPosition;
    private View mRootView;
    private JSONObject mLikeOptionArray, mDeleteOptionArray;
    private String mLikeCommentUrl, mDeleteCommentUrl, mSubjectType;
    private AppConstant mAppConst;
    private boolean isCommentPage;
    private boolean likeUnlikeAction = true;
    private GutterMenuUtils mGutterMenuUtils;
    private AlertDialogWithAction mAlertDialogWithAction;

    public CommentAdapter(Context context, int resource, List<CommentList> listItems,
                          CommentList commentList, boolean commentPage, String subject_type,
                          int subject_id, int action_id) {

        super(context, resource, listItems);
        mContext = context;
        mCommentListItems = listItems;
        mListComment = commentList;
        mLayoutResID = resource;
        isCommentPage = commentPage;

        mSubjectType = subject_type;
        mSubjectId = subject_id;
        mActionId = action_id;

        mAppConst = new AppConstant(context);
        mGutterMenuUtils = new GutterMenuUtils(context);
        mAlertDialogWithAction = new AlertDialogWithAction(context);

    }

    public CommentAdapter(Context context, int resource, List<CommentList> listItems, CommentList commentList,
                          boolean commentPage) {

        super(context, resource, listItems);
        mContext = context;
        mCommentListItems = listItems;
        mListComment = commentList;
        mLayoutResID = resource;
        isCommentPage = commentPage;

        mAppConst = new AppConstant(context);
        mGutterMenuUtils = new GutterMenuUtils(context);
        mAlertDialogWithAction = new AlertDialogWithAction(context);
    }

    public View getView(int position, View convertView, ViewGroup parent){

        mRootView = convertView;
        mCommentList = mCommentListItems.get(position);
        final ListItemHolder listItemHolder;
        if(mRootView == null){

            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            listItemHolder = new ListItemHolder();
            mRootView = inflater.inflate(mLayoutResID, parent, false);

            listItemHolder.mAuthorImage = (ImageView) mRootView.findViewById(R.id.authorImage);
            listItemHolder.mAuthorTitle = (TextView) mRootView.findViewById(R.id.authorTitle);
            listItemHolder.mCommentBody = (ExpandableTextView) mRootView.findViewById(R.id.commentBody);
            listItemHolder.mCommentDate = (TextView) mRootView.findViewById(R.id.commentDate);
            listItemHolder.mPostingText = (TextView) mRootView.findViewById(R.id.postingText);
            listItemHolder.mStickerImage = (ImageView) mRootView.findViewById(R.id.stickerImage);

            listItemHolder.mCommentOptionsBlock = (LinearLayout) mRootView.findViewById(R.id.commentOptionsBlock);
            listItemHolder.mCommentLikeCount = (TextView) mRootView.findViewById(R.id.commentLikeCount);
            listItemHolder.mCommentLikeCount.setTag(position);

            listItemHolder.mMemberOptions = (TextView) mRootView.findViewById(R.id.memberOption);
            listItemHolder.mMemberOptions.setTag(position);
            listItemHolder.mMemberOptions.setTypeface(GlobalFunctions.getFontIconTypeFace(mContext));
            listItemHolder.mLikeOption = (TextView) mRootView.findViewById(R.id.likeOption);
            listItemHolder.mLikeOption.setTag(position);

            listItemHolder.mDeleteOption = (TextView) mRootView.findViewById(R.id.deleteOption);
            listItemHolder.mDeleteOption.setTag(position);

            listItemHolder.mReactionImage = (CircularImageView) mRootView.findViewById(R.id.reactionIcon);

            mRootView.setTag(listItemHolder);
        } else {
            listItemHolder = (ListItemHolder)mRootView.getTag();
            listItemHolder.mLikeOption.setTag(position);
            listItemHolder.mMemberOptions.setTag(position);
            listItemHolder.mCommentLikeCount.setTag(position);
            if (listItemHolder.mDeleteOption != null) {
                listItemHolder.mDeleteOption.setTag(position);
            }
            if (listItemHolder.mCommentBody != null) {
                listItemHolder.mCommentBody.setVisibility(View.GONE);
            }
        }
        
        listItemHolder.mLikeOption.setClickable(true);
        listItemHolder.mLikeOption.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));

        listItemHolder.mCommentLikeCount.setClickable(true);
        listItemHolder.mCommentLikeCount.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));

        listItemHolder.mDeleteOption.setClickable(true);
        listItemHolder.mDeleteOption.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));


        /*
        Set Data in the List View Items
         */

        final int userId = mCommentList.getmUserId();

        if (mCommentList.getmAuthorPhoto() != null && !mCommentList.getmAuthorPhoto().isEmpty()) {
            Picasso.with(mContext)
                    .load(mCommentList.getmAuthorPhoto())
                    .placeholder(R.drawable.default_user_profile)
                    .into(listItemHolder.mAuthorImage);
        }

        listItemHolder.mAuthorTitle.setText(mCommentList.getmAuthorTitle());

        if(isCommentPage){

            listItemHolder.mMemberOptions.setVisibility(View.GONE);
            listItemHolder.mAuthorImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent userProfileIntent = new Intent(mContext, userProfile.class);
                    userProfileIntent.putExtra("user_id", userId);
                    ((Activity) mContext).startActivityForResult(userProfileIntent, ConstantVariables.
                            USER_PROFILE_CODE);
                    ((Activity)mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                }
            });

            listItemHolder.mAuthorTitle.setClickable(true);
            listItemHolder.mAuthorTitle.setMovementMethod(LinkMovementMethod.getInstance());
            listItemHolder.mAuthorTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent userProfileIntent = new Intent(mContext, userProfile.class);
                    userProfileIntent.putExtra("user_id", userId);
                    ((Activity) mContext).startActivityForResult(userProfileIntent, ConstantVariables.
                            USER_PROFILE_CODE);
                    ((Activity)mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            });
        } else {
            int marginTop = (int) (mContext.getResources().getDimension(R.dimen.margin_40dp) /
                    mContext.getResources().getDisplayMetrics().density);

            LinearLayout.LayoutParams layoutParams = CustomViews.getFullWidthLayoutParams();
            layoutParams.setMargins(0, marginTop, 0, 0);
            listItemHolder.mAuthorTitle.setLayoutParams(layoutParams);

            if(mCommentList.getmReactionIcon() != null && !mCommentList.getmReactionIcon().isEmpty()){
                Picasso.with(mContext)
                        .load(mCommentList.getmReactionIcon())
                        .into(listItemHolder.mReactionImage);
                listItemHolder.mReactionImage.setVisibility(View.VISIBLE);
            } else{
                listItemHolder.mReactionImage.setVisibility(View.GONE);
            }

        }


        listItemHolder.mCommentLikeCount.setTypeface(GlobalFunctions.getFontIconTypeFace(mContext));
        if(isCommentPage) {
            listItemHolder.mMemberOptions.setVisibility(View.GONE);

            if(mCommentList.getmCommentBody() != null && !mCommentList.getmCommentBody().isEmpty()){
                listItemHolder.mCommentBody.setVisibility(View.VISIBLE);
                listItemHolder.mCommentBody.setMovementMethod(LinkMovementMethod.getInstance());

                HashMap<String, String> clickableParts = mCommentList.getmClickableStringsList();

                // Show Clickable Parts and Apply Click Listener to redirect
                if (clickableParts != null && clickableParts.size() != 0) {
                    CharSequence title = Smileys.getEmojiFromString(Html.fromHtml(mCommentList.getmCommentBody().replaceAll("\n", "<br/>")).toString());
                    SpannableString text = new SpannableString(title);
                    SortedSet<String> keys = new TreeSet<>(clickableParts.keySet());

                    int lastIndex = 0;
                    for (String key : keys) {

                        String[] keyParts = key.split("-");
                        final int attachment_id = Integer.parseInt(keyParts[2]);
                        final String value = clickableParts.get(key);

                        if (value != null && !value.isEmpty()) {
                            int i1 = title.toString().indexOf(value, lastIndex);
                            if (i1 != -1) {
                                int i2 = i1 + value.length();
                                if (lastIndex != -1) {
                                    lastIndex += value.length();
                                }
                                ClickableSpan myClickableSpan = new ClickableSpan() {
                                    @Override
                                    public void onClick(View widget) {
                                        redirectToActivity(attachment_id);
                                    }

                                    @Override
                                    public void updateDrawState(TextPaint ds) {
                                        super.updateDrawState(ds);
                                        ds.setUnderlineText(false);
                                        ds.setColor(ContextCompat.getColor(mContext, R.color.black));
                                        ds.setFakeBoldText(true);
                                    }
                                };
                                text.setSpan(myClickableSpan, i1, i2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            }
                        }
                    }
                    listItemHolder.mCommentBody.setText(text);
                } else {
                    listItemHolder.mCommentBody.setText(Smileys.getEmojiFromString(
                        Html.fromHtml(mCommentList.getmCommentBody().replaceAll("\n", "<br/>")).toString()));
                }


            } else{
                listItemHolder.mCommentBody.setVisibility(View.GONE);
            }

            if(mCommentList.getmImageBitmap() != null){
                listItemHolder.mStickerImage.setVisibility(View.VISIBLE);
                listItemHolder.mStickerImage.setImageBitmap(mCommentList.getmImageBitmap());
            } else if(mCommentList.getmStickerImage() != null && !mCommentList.getmStickerImage().isEmpty()){
                listItemHolder.mStickerImage.setVisibility(View.VISIBLE);
                Picasso.with(mContext)
                        .load(mCommentList.getmStickerImage())
                        .placeholder(R.drawable.background_image)
                        .into(listItemHolder.mStickerImage);
            } else{
                listItemHolder.mStickerImage.setVisibility(View.GONE);
            }


            if(mCommentList.isShowPosting()){
                listItemHolder.mPostingText.setVisibility(View.VISIBLE);
                listItemHolder.mPostingText.setText(mContext.getResources().getString(R.string.comment_posting)
                        + "...");
                listItemHolder.mCommentOptionsBlock.setVisibility(View.GONE);

            }else{
                listItemHolder.mPostingText.setVisibility(View.GONE);
                listItemHolder.mCommentOptionsBlock.setVisibility(View.VISIBLE);

                String convertedDate = AppConstant.convertDateFormat(mContext.getResources(),
                        mCommentList.getmCommentDate());
                listItemHolder.mCommentDate.setText(convertedDate);

                if(mCommentList.getmLikeCount() > 0) {
                    listItemHolder.mCommentLikeCount.setText(String.format("\uF087 %d", mCommentList.getmLikeCount()));
                    listItemHolder.mCommentLikeCount.setVisibility(View.VISIBLE);
                }else{
                    listItemHolder.mCommentLikeCount.setVisibility(View.GONE);
                }

                listItemHolder.mLikeJsonObject = mCommentList.getmLikeJsonObject();
                if (listItemHolder.mLikeJsonObject != null) {
                    listItemHolder.mLikeOption.setVisibility(View.VISIBLE);
                    if (mCommentList.getmIsLike() == 0) {
                        listItemHolder.mLikeOption.setText(mContext.getString(R.string.like_text));
                    } else {
                        listItemHolder.mLikeOption.setText(mContext.getString(R.string.unlike));
                    }
                }else{
                    listItemHolder.mLikeOption.setVisibility(View.GONE);
                }

                listItemHolder.mDeleteJsonObject = mCommentList.getmDeleteJsonObject();
                if(mCommentList.getmDeleteJsonObject() != null) {
                    listItemHolder.mDeleteOption.setVisibility(View.VISIBLE);
                    listItemHolder.mDeleteOption.setText(mContext.getString(R.string.delete_dialogue_button));

                    listItemHolder.mDeleteOption.setClickable(true);
                }else {
                    listItemHolder.mDeleteOption.setVisibility(View.GONE);
                }
            }

            listItemHolder.mLikeOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    itemPosition = (int) view.getTag();
                    CommentList commentList = mCommentListItems.get(itemPosition);
                    int isLike = commentList.getmIsLike();

                    if (likeUnlikeAction) {
                        if (isLike == 0) {
                            listItemHolder.mLikeOption.setText(mContext.getString(R.string.unlike));
                            listItemHolder.mCommentLikeCount.setVisibility(View.VISIBLE);
                            listItemHolder.mCommentLikeCount.setText(
                                    String.format("\uF087 %d", commentList.getmLikeCount() + 1));

                        } else {
                            listItemHolder.mLikeOption.setText(mContext.getString(R.string.like_text));
                            listItemHolder.mCommentLikeCount.setText(
                                    String.format("\uF087 %d", commentList.getmLikeCount() - 1));
                            if(commentList.getmLikeCount() == 1){
                                listItemHolder.mCommentLikeCount.setVisibility(View.GONE);
                            }
                        }
                        doLikeUnlike();
                        likeUnlikeAction = false;
                    }
                }
            });

            listItemHolder.mDeleteOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mPosition = (int) view.getTag();
                    deleteComment();
                }
            });

            listItemHolder.mCommentLikeCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mLikeCountPosition = (int) view.getTag();
                    viewCommentLikes();
                }
            });
        } else {
            listItemHolder.mCommentBody.setVisibility(View.GONE);
            listItemHolder.mPostingText.setVisibility(View.GONE);
            listItemHolder.mCommentOptionsBlock.setVisibility(View.GONE);

            // Showing friendship options if condition matches.
            if (mCommentList.getmFriendshipType() != null && !mCommentList.getmFriendshipType().isEmpty() &&
                    mCommentList.getmUserId() != 0) {
                listItemHolder.mMemberOptions.setVisibility(View.VISIBLE);
                switch (mCommentList.getmFriendshipType()) {
                    case "add_friend":
                    case "accept_request":
                    case "member_follow":
                        listItemHolder.mMemberOptions.setText("\uf234");
                        break;
                    case "remove_friend":
                    case "member_unfollow":
                        listItemHolder.mMemberOptions.setText("\uf235");
                        break;
                    case "cancel_request":
                    case "cancel_follow":
                        listItemHolder.mMemberOptions.setText("\uf00d");
                        break;
                }
                mGutterMenuUtils.setOnMenuClickResponseListener(this);
                listItemHolder.mMemberOptions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int position = (int) view.getTag();
                        CommentList commentList = mCommentListItems.get(position);
                        mGutterMenuUtils.setPopUpForFriendShipType(position, null, commentList, null);
                    }
                });
            } else {
                listItemHolder.mMemberOptions.setVisibility(View.GONE);
            }
        }

        mRootView.setId(mCommentList.getmCommentId());
        return mRootView;
    }

    private void redirectToActivity(int attachment_id) {

        Intent viewIntent = new Intent(mContext, userProfile.class);
        viewIntent.putExtra(ConstantVariables.USER_ID, attachment_id);
        ((Activity) mContext).startActivityForResult(viewIntent, ConstantVariables.USER_PROFILE_CODE);
        ((Activity) mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }


    @Override
    public void onItemDelete(int position) {

    }

    @Override
    public void onItemActionSuccess(int position, Object itemList, String menuName) {
        if (menuName.equals("friendship_type")) {
            mCommentListItems.set(position, (CommentList) itemList);
            notifyDataSetChanged();
        }
    }

    private static class ListItemHolder{

        ImageView mAuthorImage, mStickerImage;
        TextView mAuthorTitle,  mCommentDate, mLikeOption, mDeleteOption, mCommentLikeCount,
                mPostingText, mMemberOptions;
        JSONObject mLikeJsonObject, mDeleteJsonObject;
        ExpandableTextView mCommentBody;
        LinearLayout mCommentOptionsBlock;
        CircularImageView mReactionImage;
    }

    public void doLikeUnlike(){

        final CommentList commentList = mCommentListItems.get(itemPosition);
        mLikeOptionArray = commentList.getmLikeJsonObject();
                        try {
                            final String likeUrl = mLikeOptionArray.getString("url");
                            final int isLikeValue = mLikeOptionArray.optInt("isLike");
                            final String likeLabel = mLikeOptionArray.getString("label");
                            final String likeName = mLikeOptionArray.getString("name");

                            if (mSubjectType.equals("activity_action")) {
                                mLikeCommentUrl = AppConstant.DEFAULT_URL + "advancedactivity/" + likeUrl;
                            } else {
                                mLikeCommentUrl = AppConstant.DEFAULT_URL + likeUrl;
                            }
                            JSONObject urlParamsJsonObject = mLikeOptionArray.getJSONObject("urlParams");

                            Map<String, String> likeUnlikeParams = new HashMap<>();

                            JSONArray urlParamsKeys = urlParamsJsonObject.names();

                            for (int i = 0; i < urlParamsJsonObject.length(); i++) {
                                String keyName = urlParamsKeys.getString(i);
                                String value = urlParamsJsonObject.getString(keyName);
                                likeUnlikeParams.put(keyName, value);
                            }

                            mAppConst.postJsonResponseForUrl(mLikeCommentUrl, likeUnlikeParams,
                                    new OnResponseListener() {
                                @Override
                                public void onTaskCompleted(JSONObject jsonObject) {

                                    int isLike = commentList.getmIsLike();
                                    if (isLike == 0) {
                                        commentList.setmIsLike(1);
                                        commentList.setmLikeCount(commentList.getmLikeCount() + 1);
                                    }else {
                                        commentList.setmIsLike(0);
                                        commentList.setmLikeCount(commentList.getmLikeCount() - 1);
                                    }

                                    try {
                                        if (isLikeValue == 0)
                                            mLikeOptionArray.put("isLike", 1);
                                        else
                                            mLikeOptionArray.put("isLike", 0);
                                        if (likeLabel.equals("Like"))
                                            mLikeOptionArray.put("label", "Unlike");
                                        else
                                            mLikeOptionArray.put("label", "Like");
                                        if (likeUrl.equals("like"))
                                            mLikeOptionArray.put("url", "unlike");
                                        else
                                            mLikeOptionArray.put("url", "like");
                                        if (likeName.equals("unlike"))
                                            mLikeOptionArray.put("name", "like");
                                        else
                                            mLikeOptionArray.put("name", "unlike");

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    commentList.setmLikeJsonObject(mLikeOptionArray);
                                    notifyDataSetChanged();
                                    likeUnlikeAction = true;

                                }

                                @Override
                                public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                                    notifyDataSetChanged();
                                    likeUnlikeAction = true;
                                    SnackbarUtils.displaySnackbar(mRootView, message);
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

    }

    public void deleteComment() {

        mAlertDialogWithAction.showAlertDialogWithAction(mContext.getResources().getString(R.string.delete_comment_dialogue_title),
                mContext.getResources().getString(R.string.delete_comment_dialogue_message),
                mContext.getResources().getString(R.string.delete_comment_dialogue_delete_button),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {

                        final CommentList commentList = mCommentListItems.get(mPosition);
                        mDeleteOptionArray = commentList.getmDeleteJsonObject();
                        if (mDeleteOptionArray != null && mDeleteOptionArray.length() != 0) {
                            try {
                                if (mSubjectType.equals("activity_action")) {
                                    mDeleteCommentUrl = AppConstant.DEFAULT_URL + "advancedactivity/delete";
                                } else {
                                    mDeleteCommentUrl = AppConstant.DEFAULT_URL + mDeleteOptionArray.getString("url");
                                }
                                JSONObject urlParamsJsonObject = mDeleteOptionArray.getJSONObject("urlParams");
                                JSONArray urlParamsKeys = urlParamsJsonObject.names();
                                Map<String, String> deleteParams = new HashMap<>();

                                for (int i = 0; i < urlParamsJsonObject.length(); i++) {
                                    String keyName = urlParamsKeys.getString(i);
                                    String value = urlParamsJsonObject.getString(keyName);
                                    deleteParams.put(keyName, value);
                                }

                                if (deleteParams.size() != 0) {
                                    mDeleteCommentUrl = mAppConst.buildQueryString(mDeleteCommentUrl, deleteParams);
                                }
                                mAppConst.showProgressDialog();
                                mAppConst.deleteResponseForUrl(mDeleteCommentUrl, null, new OnResponseListener() {
                                    @Override
                                    public void onTaskCompleted(JSONObject jsonObject) {
                                        mAppConst.hideProgressDialog();
                                        /* Notify the adapter After Deleting the Entry */
                                        mCommentListItems.remove(mPosition);
                                        mListComment.setmTotalCommmentCount(mListComment.getmTotalCommmentCount() - 1);
                                        notifyDataSetChanged();
                                    }

                                    @Override
                                    public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                                        mAppConst.hideProgressDialog();
                                        SnackbarUtils.displaySnackbar(mRootView, message);
                                    }
                                });
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }

    public void viewCommentLikes(){

        CommentList commentList = mCommentListItems.get(mLikeCountPosition);
        String viewAllCommentLikesUrl;
        if (mSubjectType.equals("activity_action")) {
            viewAllCommentLikesUrl = UrlUtil.AAF_VIEW_LIKES_URL + "&comment_id=" + commentList.getmCommentId() +
            "&action_id=" +mActionId;
        } else {
            viewAllCommentLikesUrl = UrlUtil.VIEW_LIKES_URL + "&subject_type=" + mSubjectType + "&subject_id=" + mSubjectId +
                    "&comment_id=" + commentList.getmCommentId();
        }
        Intent viewAllLikesIntent = new Intent(mContext, Likes.class);
        viewAllLikesIntent.putExtra("ViewAllLikesUrl", viewAllCommentLikesUrl);
        mContext.startActivity(viewAllLikesIntent);
        ((Activity)mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

}
