package com.unitedgungroup.mobiapp.classes.common.formgenerator;

import android.content.Context;
import android.support.v7.widget.AppCompatSeekBar;
import android.widget.SeekBar;

public class FormRangeBar extends FormWidget {
    protected AppCompatSeekBar rangeBar;
    public FormRangeBar(Context context, String name, boolean hasValidator) {
        super(context, name, hasValidator);

        rangeBar = new AppCompatSeekBar(context);
        rangeBar.setMax(1000);
        rangeBar.setLayoutParams(FormActivity.defaultLayoutParams);

        rangeBar.getThumb();
        rangeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                seekBar.setProgress(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        _layout.addView(rangeBar);
    }

    @Override
    public String getValue() {
        return String.valueOf(rangeBar.getProgress());
    }
}
