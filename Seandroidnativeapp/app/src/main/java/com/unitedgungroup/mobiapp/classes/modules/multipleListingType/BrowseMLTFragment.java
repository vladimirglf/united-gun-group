/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *    You may not use this file except in compliance with the
 *    SocialEngineAddOns License Agreement.
 *    You may obtain a copy of the License at:
 *    https://www.socialengineaddons.com/android-app-license
 *    The full copyright and license information is also mentioned
 *    in the LICENSE file that was distributed with this
 *    source code.
 */

package com.unitedgungroup.mobiapp.classes.modules.multipleListingType;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdsManager;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.adapters.RecyclerViewAdapter;
import com.unitedgungroup.mobiapp.classes.common.adapters.SpinnerAdapter;
import com.unitedgungroup.mobiapp.classes.common.ads.admob.AdFetcher;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnCommunityAdsLoadedListener;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnItemClickListener;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.ui.SelectableTextView;
import com.unitedgungroup.mobiapp.classes.common.utils.BrowseListItems;
import com.unitedgungroup.mobiapp.classes.common.utils.CommunityAdsList;
import com.unitedgungroup.mobiapp.classes.common.utils.DataStorage;
import com.unitedgungroup.mobiapp.classes.common.utils.GlobalFunctions;
import com.unitedgungroup.mobiapp.classes.common.utils.GridSpacingItemDecorationUtil;
import com.unitedgungroup.mobiapp.classes.common.utils.LinearDividerItemDecorationUtil;
import com.unitedgungroup.mobiapp.classes.common.utils.LogUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.PreferencesUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.SnackbarUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.UrlUtil;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;
import com.unitedgungroup.mobiapp.classes.core.ConstantVariables;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class BrowseMLTFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
        NativeAdsManager.Listener, AdapterView.OnItemSelectedListener, OnCommunityAdsLoadedListener {

    private View mRootView, mHeaderView;
    private AppConstant mAppConst;
    private AdFetcher mAdFetcher;
    private Context mContext;
    private TextView errorIcon;
    private SelectableTextView errorMessage;
    private ProgressBar mProgressBar;
    private LinearLayout messageLayout;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mBrowseMLTAdapter;
    private List<Object> mBrowseItemList;
    private BrowseListItems mBrowseList, mFeaturedBrowseList;
    private boolean isLoading = false , isSearchTextSubmitted = false, isMemberMLT = false,
            isAdLoaded = false, isAdvGroupsMLT = false, isVisibleToUser = false;
    private int mLoadingPageNo = 1, NUM_OF_COLUMNS;
    private String mBrowseMLTUrl, mListingLabel, mListingIcon, mGroupMLTUrl, mCurrentSelectedModule;
    private int mUserId, mListingTypeId, mMLTBrowseType;
    private Snackbar snackbar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private HashMap<String, String> searchParams = new HashMap<>();
    private NativeAdsManager listNativeAdsManager;
    private String mListingFilter = "";
    private int mCategoryId;
    Spinner spinner, subCategorySpinner, subSubCategorySpinner;
    SpinnerAdapter adapter, subCategoryAdapter, subSubCategoryAdapter;
    private String mSubCategoryId, mSubSubCategoryId;
    private CardView subCategoryLayout, subSubCategoryLayout;
    private int mSelectedItem = -1, mSubsubcategorySelectedItem = -1;
    private HashMap<String, String> postParams = new HashMap<>();
    private JSONArray mDataResponse, mSubCategoryResponse = null, mSubSubCategoryResponse = null;
    private boolean isCategoryResults = false, isLoadSubCategory = true, isLoadSubSubcategory = true, isFirstRequest = true;
    private LayoutInflater layoutInflater;
    private JSONObject sliderDataObject, mBody;
    private boolean isShowHeader = true, isCommunityAds = false;
    private int mFeaturedCount = 0;
    private JSONArray mAdvertisementsArray;


    public static BrowseMLTFragment newInstance(Bundle bundle){
        BrowseMLTFragment fragment = new BrowseMLTFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);

        if (visible && !isVisibleToUser && mContext != null) {
            updateListingParams();
            makeRequest();
        }
        if (!isVisible() && snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mAppConst = new AppConstant(getActivity());
        mAppConst.setOnCommunityAdsLoadedListener(this);
        mContext = getContext();
        NUM_OF_COLUMNS = AppConstant.getNumOfColumns(mContext);
        mBrowseItemList = new ArrayList<>();
        mBrowseList = new BrowseListItems();
        layoutInflater = inflater;

        // Updating current selected module
        mCurrentSelectedModule = PreferencesUtils.getCurrentSelectedModule(mContext);
        if(mCurrentSelectedModule != null && !mCurrentSelectedModule.equals("sitereview_listing")){
            PreferencesUtils.updateCurrentModule(mContext, "sitereview_listing");
            mCurrentSelectedModule = PreferencesUtils.getCurrentSelectedModule(mContext);
        }


        if(getArguments() != null && getArguments().containsKey(ConstantVariables.LISTING_TYPE_ID)){
            PreferencesUtils.setCurrentSelectedListingId(mContext, getArguments().getInt(ConstantVariables.
                    LISTING_TYPE_ID));
            getArguments().remove(ConstantVariables.LISTING_TYPE_ID);
        }

        // Inflating recycler layout.
        mRootView = inflater.inflate(R.layout.recycler_view_layout, container, false);
        mHeaderView = inflater.inflate(R.layout.spinner_view, null, false);
        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);

        // Getting arguments from search query
        if(getArguments() != null) {

            Bundle bundle = getArguments();
            mRecyclerView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));

            // If The Fragment Being called from User profile page.
            isMemberMLT = bundle.getBoolean("isMemberMLT");
            isAdvGroupsMLT = bundle.getBoolean("isAdvGroupsMLT");
            mUserId = bundle.getInt("user_id");
            isCategoryResults = bundle.getBoolean(ConstantVariables.IS_CATEGORY_BASED_RESULTS, false);
            mCategoryId = bundle.getInt(ConstantVariables.VIEW_PAGE_ID, 0);
            postParams.put("category_id", String.valueOf(mCategoryId));
            if(mUserId != 0){
                mBrowseMLTUrl += "&user_id=" + mUserId;
            }
            if (isMemberMLT || isAdvGroupsMLT) {
                mGroupMLTUrl = bundle.getString(ConstantVariables.URL_STRING);
                mHeaderView.findViewById(R.id.spinnerCardView).setVisibility(View.GONE);
            }

            if(!isMemberMLT && !isAdvGroupsMLT && !isCategoryResults){
                updateListingParams();
                Set<String> searchArgumentSet = getArguments().keySet();
                for (String key : searchArgumentSet) {
                    String value = getArguments().getString(key);
                    if (value != null && !value.isEmpty()) {
                        searchParams.put(key, value);
                    }
                }
            }
            isShowHeader = false;
            getViews();
        } else {
            getViews();
            updateListingParams();
            sendRequestForFeaturedContent();
        }

        // Hide Filter View if Fragment is being called from user profile, group profile or search page
        if((searchParams != null && searchParams.size() != 0) || isMemberMLT || isAdvGroupsMLT){
            isSearchTextSubmitted = true;
            mHeaderView.setVisibility(View.GONE);
            mBrowseMLTUrl = mAppConst.buildQueryString(mBrowseMLTUrl, searchParams);
        }

        /**
         * Show Order by Spinner when fragment is loaded from dashboard.
         */
        if(!isCategoryResults){

            adapter = new SpinnerAdapter(mContext, R.layout.simple_text_view, mSelectedItem);

            /* Add events filter type to spinner using adpter */
            adapter.add(mContext.getResources().getString(R.string.browse_event_filter_sell_all));
            adapter.add(mContext.getResources().getString(R.string.featured));
            adapter.add(mContext.getResources().getString(R.string.sponsored));

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
            spinner.setSelection(0, false);
            spinner.setTag("listingFilter");
        }else{

            subCategoryAdapter = new SpinnerAdapter(mContext, R.layout.simple_text_view, mSelectedItem);
            subCategoryAdapter.add(getResources().getString(R.string.select_sub_category_text));

            //    ArrayAdapter adapter = new ArrayAdapter(this,R.layout.simple_spinner_item,list);
            subCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            subCategorySpinner.setAdapter(subCategoryAdapter);
            subCategorySpinner.setSelection(0, false);
            subCategorySpinner.setTag("subCategory");

            subSubCategoryAdapter = new SpinnerAdapter(mContext, R.layout.simple_text_view, mSubsubcategorySelectedItem);
            subSubCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            subSubCategorySpinner.setAdapter(subSubCategoryAdapter);
            subSubCategorySpinner.setSelection(0, false);
            subSubCategorySpinner.setTag("subSubCategory");
        }


        // Showing adds if ADS enabled for MLT.
        if(ConstantVariables.ENABLE_MLT_ADS == 1) {
            switch (ConstantVariables.MLT_ADS_TYPE){
                case ConstantVariables.TYPE_FACEBOOK_ADS:
                    listNativeAdsManager = new NativeAdsManager(mContext,
                            mContext.getResources().getString(R.string.facebook_placement_id),
                            ConstantVariables.DEFAULT_AD_COUNT);
                    listNativeAdsManager.setListener(this);
                    listNativeAdsManager.loadAds(NativeAd.MediaCacheFlag.ALL);
                    break;
                case ConstantVariables.TYPE_GOOGLE_ADS:
                    mAdFetcher =  new AdFetcher(mContext);
                    mAdFetcher.loadAds(mBrowseItemList,mBrowseMLTAdapter,ConstantVariables.MLT_ADS_POSITION);
                    break;
                default:
                    isCommunityAds = true;
                    break;
            }
        }

        // When its loading for the Browse fragment from the dashboard or from the category selection
        // then making server call.
        if ((!isMemberMLT && !isAdvGroupsMLT && searchParams != null && searchParams.size() != 0)
                || isCategoryResults) {
            updateListingParams();
            makeRequest();
        }
        return mRootView;
    }

    private void sendRequestForFeaturedContent() {
        String featuredContentUrl = UrlUtil.BROWSE_MLT_URL + "limit=" + AppConstant.FEATURED_CONTENT_LIMIT +
                "&listingtype_id=" + mListingTypeId + "&page=1&listing_filter=featured";

        mBrowseItemList.clear();
        String tempData = DataStorage.getResponseFromLocalStorage(mContext,DataStorage.MLT_FEATURED_CONTENT + mListingTypeId);
        if (tempData != null) {
            try {
                JSONObject jsonObject = new JSONObject(tempData);
                addDataToFeaturedList(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        mAppConst.getJsonResponseFromUrl(featuredContentUrl, new OnResponseListener() {
            @Override
            public void onTaskCompleted(JSONObject jsonObject) {
                mBrowseItemList.clear();
                addDataToFeaturedList(jsonObject);
                DataStorage.createTempFile(mContext,DataStorage.MLT_FEATURED_CONTENT + mListingTypeId, jsonObject.toString());
            }

            @Override
            public void onErrorInExecutingTask(String message, boolean isRetryOption) {

            }
        });
    }

    private void addDataToFeaturedList(JSONObject jsonObject) {
        JSONArray mDataResponse = jsonObject.optJSONArray("response");
        if(mDataResponse != null && mDataResponse.length() > 0) {
            sliderDataObject = jsonObject;
            mFeaturedCount = jsonObject.optInt("totalItemCount");
            mFeaturedBrowseList = new BrowseListItems(sliderDataObject, mListingTypeId);
            mBrowseItemList.add(0, mFeaturedBrowseList);
            isShowHeader = true;
        } else {
            isShowHeader = false;
        }

        // Initializing layout manager and adapter according to the browse view type.
        if (mMLTBrowseType != 0)
            setLayoutManager(mMLTBrowseType);
        else
            setLayoutManager(2);

        mBrowseMLTAdapter.notifyDataSetChanged();

        makeRequest();
    }

    public void getViews() {
        mRecyclerView.setHasFixedSize(true);
        swipeRefreshLayout = (SwipeRefreshLayout) mRootView.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mProgressBar = (ProgressBar) mRootView.findViewById(R.id.progressBar);

        // No data message views
        messageLayout = (LinearLayout) mRootView.findViewById(R.id.message_layout);
        errorIcon = (TextView) mRootView.findViewById(R.id.error_icon);
        errorMessage = (SelectableTextView) mRootView.findViewById(R.id.error_message);
        errorIcon.setTypeface(GlobalFunctions.getFontIconTypeFace(mContext));

        // getting header views.

        if(isCategoryResults){
            mHeaderView = layoutInflater.inflate(R.layout.layout_category_block, null, false);
            subCategoryLayout = (CardView) mHeaderView.findViewById(R.id.categoryFilterLayout);
            subSubCategoryLayout = (CardView) mHeaderView.findViewById(R.id.subCategoryFilterLayout);
            subCategorySpinner = (Spinner) subCategoryLayout.findViewById(R.id.filter_view);
            subSubCategorySpinner = (Spinner) subSubCategoryLayout.findViewById(R.id.filter_view);
            mHeaderView.findViewById(R.id.mlt_category_block).setVisibility(View.VISIBLE);
            mHeaderView.findViewById(R.id.toolbar).setVisibility(View.GONE);
            // Adding header view to main view.
            RelativeLayout mainView = (RelativeLayout) mRootView.findViewById(R.id.main_view_recycler);
            mainView.addView(mHeaderView);
            CustomViews.addHeaderView(R.id.mlt_category_block, swipeRefreshLayout);
            mHeaderView.findViewById(R.id.mlt_category_block).getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        } else{
            mHeaderView = layoutInflater.inflate(R.layout.spinner_view, null, false);
            spinner = (Spinner) mHeaderView.findViewById(R.id.filter_view);
            // Adding header view to main view.
            RelativeLayout mainView = (RelativeLayout) mRootView.findViewById(R.id.main_view_recycler);
            mainView.addView(mHeaderView);
            CustomViews.addHeaderView(R.id.spinnerCardView, swipeRefreshLayout);
            mHeaderView.findViewById(R.id.spinnerCardView).getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAppConst.hideKeyboard();
    }

    /**
     * Method to initialize listing variables.
     */
    public void updateListingParams() {

        if (spinner != null)
            spinner.setOnItemSelectedListener(this);

        if (subCategorySpinner  != null)
            subCategorySpinner.setOnItemSelectedListener(this);

        if (subSubCategorySpinner != null)
            subSubCategorySpinner.setOnItemSelectedListener(this);

        // Getting current Listing variables from preferences.
        mListingTypeId = PreferencesUtils.getCurrentSelectedListingId(mContext);
        mListingLabel = PreferencesUtils.getCurrentSelectedListingSingularLabel(mContext, mListingTypeId);
        mMLTBrowseType = PreferencesUtils.getCurrentSelectedListingBrowseType(mContext, mListingTypeId);
        mListingIcon = PreferencesUtils.getCurrentSelectedListingIcon(mContext, mListingTypeId);

        mBrowseMLTUrl = UrlUtil.BROWSE_MLT_URL + "limit=" + AppConstant.LIMIT + "&listingtype_id=" + mListingTypeId + "&page=" + mLoadingPageNo +
                "&listing_filter=" + mListingFilter;

        // setting up the listing icon.
        if (mListingIcon != null && !mListingIcon.isEmpty()) {
            try {
                mListingIcon = new String(Character.toChars(Integer.parseInt(mListingIcon, 16)));
            } catch (NumberFormatException e) {
                mListingIcon = GlobalFunctions.getItemIcon(mCurrentSelectedModule);
            }
        } else {
            mListingIcon = GlobalFunctions.getItemIcon(mCurrentSelectedModule);
        }

        // Initializing layout manager and adapter according to the browse view type.
        if (mMLTBrowseType != 0) {
            setLayoutManager(mMLTBrowseType);
        } else {
            setLayoutManager(2);
        }
    }

    /**
     * Method to send request to server to get browse page data.
     */
    public void makeRequest() {

        mLoadingPageNo = 1;

        if (!isSearchTextSubmitted && !isMemberMLT && (mListingFilter.isEmpty() ||
                mListingFilter.equals("all")) && !isAdvGroupsMLT && !isCategoryResults) {
            mProgressBar.setVisibility(View.VISIBLE);

            try {
                mBrowseItemList.clear();
                if (mFeaturedBrowseList != null && !mBrowseItemList.contains(mFeaturedBrowseList)) {
                    mBrowseItemList.add(0, mFeaturedBrowseList);
                }
                mBrowseMLTUrl = UrlUtil.BROWSE_MLT_URL + "limit=" + AppConstant.LIMIT + "&listingtype_id=" + mListingTypeId + "&page=" +
                        mLoadingPageNo + "&listing_filter=" + mListingFilter;

                // Don't show data in case of searching and User Profile Tabs.
                String tempData = DataStorage.getResponseFromLocalStorage(mContext, DataStorage.MLT_FILE +
                        mListingLabel);

                if (tempData != null) {
                    swipeRefreshLayout.post(new Runnable() {
                        @Override
                        public void run() {
                            swipeRefreshLayout.setRefreshing(true);
                        }
                    });
                    mProgressBar.setVisibility(View.GONE);
                    JSONObject jsonObject = new JSONObject(tempData);
                    addItemsToList(jsonObject);
                    mBrowseMLTAdapter.notifyDataSetChanged();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (isMemberMLT) {
            mBrowseMLTUrl = UrlUtil.BROWSE_MLT_URL + "limit=" + AppConstant.LIMIT +  "&listingtype_id=" + mListingTypeId +
                    "&page=" + mLoadingPageNo + "&user_id=" + mUserId;

        } else if (isAdvGroupsMLT) {
            mBrowseMLTUrl = mGroupMLTUrl + "&limit=" + AppConstant.LIMIT + "&page=" + mLoadingPageNo;

        } else if (!mListingFilter.isEmpty()) {
            swipeRefreshLayout.setRefreshing(true);
            mBrowseMLTUrl = UrlUtil.BROWSE_MLT_URL  + "limit=" + AppConstant.LIMIT + "&listingtype_id=" + mListingTypeId + "&page=" + mLoadingPageNo
                    + "&listing_filter=" + mListingFilter;
        }else if (isCategoryResults){
            mBrowseMLTUrl = UrlUtil.BROWSE_CATEGORIES_MLT_URL + "&listingtype_id=" + mListingTypeId + "&page=" + mLoadingPageNo
                    + "&showListings=1";
            mBrowseMLTUrl = mAppConst.buildQueryString(mBrowseMLTUrl, postParams);
        }

        if(searchParams != null && searchParams.size() != 0){
            mBrowseMLTUrl = mAppConst.buildQueryString(mBrowseMLTUrl, searchParams);
        }

        if (!swipeRefreshLayout.isRefreshing()) {
            mProgressBar.setVisibility(View.VISIBLE);
            mProgressBar.bringToFront();
        }
        mAppConst.getJsonResponseFromUrl(mBrowseMLTUrl, new OnResponseListener() {
            @Override
            public void onTaskCompleted(JSONObject jsonObject) {
                mProgressBar.setVisibility(View.GONE);
                mBody = jsonObject;
                isVisibleToUser = true;
                if (snackbar != null && snackbar.isShown()) {
                    snackbar.dismiss();
                }
                mBrowseItemList.clear();
                if (sliderDataObject != null) {
                    mBrowseItemList.add(0, new BrowseListItems(sliderDataObject, mListingTypeId));
                }

                addItemsToList(jsonObject);
                if(isCommunityAds){
                    mAppConst.getCommunityAds(ConstantVariables.MLT_ADS_POSITION,
                            ConstantVariables.MLT_ADS_TYPE);
                }
                mBrowseMLTAdapter.notifyDataSetChanged();

                // Don't save data in cashing in case of searching and user profile tabs.
                if (!isSearchTextSubmitted && !isMemberMLT && (mListingFilter.isEmpty() ||
                        mListingFilter.equals("all")) && !isAdvGroupsMLT && !isCategoryResults) {
                    DataStorage.createTempFile(mContext, DataStorage.MLT_FILE + mListingLabel, jsonObject.toString());
                }
                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                mProgressBar.setVisibility(View.GONE);
                swipeRefreshLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, ConstantVariables.REFRESH_DELAY_TIME);
                if (isRetryOption) {
                    snackbar = SnackbarUtils.displaySnackbarWithAction(getActivity(), mRootView, message,
                            new SnackbarUtils.OnSnackbarActionClickListener() {
                                @Override
                                public void onSnackbarActionClick() {
                                    mProgressBar.setVisibility(View.VISIBLE);
                                    makeRequest();
                                }
                            });
                } else {
                    SnackbarUtils.displaySnackbar(mRootView, message);
                }
            }
        });

    }


    private CommunityAdsList addCommunityAddsToList(int j) {

        JSONObject singleAdObject = mAdvertisementsArray.optJSONObject(j);
        int adId = singleAdObject.optInt("userad_id");
        String ad_type = singleAdObject.optString("ad_type");
        String cads_title = singleAdObject.optString("cads_title");
        String cads_body = singleAdObject.optString("cads_body");
        String cads_url = singleAdObject.optString("cads_url");
        String image = singleAdObject.optString("image");
        return new CommunityAdsList(adId, ad_type, cads_title, cads_body,
                cads_url, image);
    }

    /**
     *Method to load more data(if exists) on scrolling.
     *
     * @param url Url to load next page data
     */
    private void loadMoreData(String url){
        //add null , so the adapter will check view_type and show progress bar at bottom
        mBrowseItemList.add(null);
        mBrowseMLTAdapter.notifyItemInserted(mBrowseItemList.size() - 1);

        mAppConst.getJsonResponseFromUrl(url, new OnResponseListener() {
            @Override
            public void onTaskCompleted(JSONObject jsonObject) {
                mBody = jsonObject;
                if(isCommunityAds){
                    mAppConst.getCommunityAds(ConstantVariables.MLT_ADS_POSITION,
                            ConstantVariables.MLT_ADS_TYPE);
                } else {
                    //   remove progress item
                    mBrowseItemList.remove(mBrowseItemList.size() - 1);
                    mBrowseMLTAdapter.notifyItemRemoved(mBrowseItemList.size());
                    addItemsToList(jsonObject);
                }

                mBrowseMLTAdapter.notifyItemInserted(mBrowseItemList.size());
                isLoading = false;
                isFirstRequest = false;
            }

            @Override
            public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                SnackbarUtils.displaySnackbar(mRootView, message);
            }
        });
    }

    /**
     *Method to add data to the list.
     *
     * @param jsonObject JsonObject by which getting the response
     */
    int j = 0;
    public void addItemsToList(JSONObject jsonObject) {


        int mTotalItemCount = jsonObject.optInt("totalItemCount");
        mBrowseList.setmTotalItemCount(mTotalItemCount);

        if(isCategoryResults){
            /**
             * Show Sub Categories of the selected category
             */
            if (isLoadSubCategory) {
                mSubCategoryResponse = jsonObject.optJSONArray("subCategories");

                if (mSubCategoryResponse != null && mSubCategoryResponse.length() != 0) {
                    subCategoryLayout.setVisibility(View.VISIBLE);
                    for (int k = 0; k < mSubCategoryResponse.length(); k++) {
                        JSONObject object = mSubCategoryResponse.optJSONObject(k);
                        String sub_cat_name = object.optString("sub_cat_name");
                        subCategoryAdapter.add(sub_cat_name);
                    }
                } else {
                    subCategoryLayout.setVisibility(View.GONE);
                }

                isLoadSubCategory = false;
            }

            /**
             * Show 3rd level categories when sub category will be selected
             */
            if (jsonObject.has("subsubCategories") && isLoadSubSubcategory) {

                mSubSubCategoryResponse = jsonObject.optJSONArray("subsubCategories");
                if (mSubSubCategoryResponse != null && mSubSubCategoryResponse.length() != 0) {
                    subSubCategoryLayout.setVisibility(View.VISIBLE);

                    for (int k = 0; k < mSubSubCategoryResponse.length(); k++) {
                        JSONObject object = mSubSubCategoryResponse.optJSONObject(k);
                        String sub_sub_cat_name = object.optString("tree_sub_cat_name");
                        subSubCategoryAdapter.add(sub_sub_cat_name);
                    }
                } else {
                    subSubCategoryLayout.setVisibility(View.GONE);
                }

                isLoadSubSubcategory = false;
            }

            mDataResponse = jsonObject.optJSONArray("listings");
            if(mTotalItemCount == 0 && isFirstRequest){
                subCategoryLayout.setVisibility(View.GONE);
                subSubCategoryLayout.setVisibility(View.GONE);
            }
        }else{
            mDataResponse = jsonObject.optJSONArray("response");
        }

        if(mDataResponse != null && mDataResponse.length() > 0) {
            messageLayout.setVisibility(View.GONE);

            for (int i = 0; i < mDataResponse.length(); i++) {
                if ((isAdLoaded || AdFetcher.isAdLoaded()) && mBrowseItemList.size() != 0
                        && mBrowseItemList.size() % ConstantVariables.MLT_ADS_POSITION == 0) {
                    switch (ConstantVariables.MLT_ADS_TYPE){
                        case ConstantVariables.TYPE_FACEBOOK_ADS:
                            NativeAd ad = this.listNativeAdsManager.nextNativeAd();
                            mBrowseItemList.add(ad);
                            break;
                        case ConstantVariables.TYPE_GOOGLE_ADS:
                            if(mAdFetcher.getAdList() != null && !mAdFetcher.getAdList().isEmpty()){
                                if(j < mAdFetcher.getAdList().size()) {
                                    NativeAppInstallAd nativeAppInstallAd = (NativeAppInstallAd) mAdFetcher.getAdList().get(j);
                                    j++;
                                    mBrowseItemList.add(nativeAppInstallAd);
                                }else {
                                    j = 0;
                                }
                            }
                            break;
                        default:
                            if(mAdvertisementsArray != null){
                                if(j < mAdvertisementsArray.length()){
                                    mBrowseItemList.add(addCommunityAddsToList(j));
                                    j++;
                                } else {
                                    j = 0;
                                }
                            }
                            break;
                    }
                }
                JSONObject jsonDataObject = mDataResponse.optJSONObject(i);
                int listingId = jsonDataObject.optInt("listing_id");
                int listingTypeId = jsonDataObject.optInt("listingtype_id");
                String title = jsonDataObject.optString("title");
                String image = jsonDataObject.optString("image");
                String ownerTitle = jsonDataObject.optString("owner_title");
                int featured  = isCategoryResults ? 0 : jsonDataObject.optInt("featured");
                int sponsored = isCategoryResults ? 0: jsonDataObject.optInt("sponsored");
                String creationDate = jsonDataObject.optString("creation_date");
                String location = jsonDataObject.optString("location");
                String price = jsonDataObject.optString("price");
                String currency = jsonDataObject.optString("currency");
                int allowToView = jsonDataObject.optInt("allow_to_view");
                int isClosed = jsonDataObject.optInt("closed");
                if (allowToView == 1) {
                    mBrowseItemList.add(new BrowseListItems(listingId, listingTypeId, title, image, ownerTitle,
                            creationDate, location, price, currency, true, isClosed, featured, sponsored));
                } else {
                    mBrowseItemList.add(new BrowseListItems(listingId, listingTypeId, title, image, ownerTitle,
                            creationDate, location, price, currency, false, isClosed, featured, sponsored));
                }

            }
            // Show End of Result Message when there are less results
            if(mTotalItemCount <= AppConstant.LIMIT * mLoadingPageNo){
                mBrowseItemList.add(ConstantVariables.FOOTER_TYPE);
            }
        }else {
            String message = mContext.getResources().getString(R.string.no_text) + " " + mListingLabel.toLowerCase() + " " +
                    mContext.getResources().getString(R.string.available_text);

            if (mFeaturedCount > 0) {
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
                messageLayout.setLayoutParams(layoutParams);

                messageLayout.setPadding(0, (int) mContext.getResources().getDimension(R.dimen.error_view_top_margin), 0 ,0);

                if (PreferencesUtils.getDefaultLocation(mContext) != null && !PreferencesUtils.getDefaultLocation(mContext).isEmpty()) {
                    message = message + " " + mContext.getResources().getString(R.string.for_this_location_text);
                }
            }

            messageLayout.setVisibility(View.VISIBLE);
            messageLayout.bringToFront();
            errorIcon.setTypeface(GlobalFunctions.getFontIconTypeFace(mContext));
            errorIcon.setText(mListingIcon);
            errorMessage.setText(message);
        }
    }

    /**
     * Method to set layout manager according to type.
     * @param mMLTBrowseType type of view to show MLT.
     */
    public void setLayoutManager(final int mMLTBrowseType) {
        try {
            LinearLayoutManager mLinearLayoutManager;
            switch (mMLTBrowseType) {
                case ConstantVariables.GRID_VIEW:
                    GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), NUM_OF_COLUMNS);
                    if (NUM_OF_COLUMNS > 1) {
                        mRecyclerView.addItemDecoration(new GridSpacingItemDecorationUtil(mContext,
                                R.dimen.loading_bar_height, mRecyclerView, true));
                    } else {
                        mRecyclerView.addItemDecoration(new GridSpacingItemDecorationUtil(mContext,
                                R.dimen.margin_2dp, mRecyclerView, false));
                    }
                    mRecyclerView.setLayoutManager(mLayoutManager);
                    break;
                case ConstantVariables.MATRIX_VIEW:
                    mLayoutManager = new GridLayoutManager(getActivity(), 2);
                    mRecyclerView.setLayoutManager(mLayoutManager);
                    mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int position) {
                            switch (mBrowseMLTAdapter.getItemViewType(position)) {
                                case RecyclerViewAdapter.VIEW_ITEM:
                                case RecyclerViewAdapter.TYPE_FB_AD:
                                case RecyclerViewAdapter.TYPE_COMMUNITY_ADS:
                                case RecyclerViewAdapter.REMOVE_COMMUNITY_ADS:
                                    return 1;
                                case RecyclerViewAdapter.VIEW_PROG:
                                case RecyclerViewAdapter.HEADER_TYPE:
                                    return 2; //number of columns of the grid
                                default:
                                    return -1;
                            }
                        }
                    });
                    break;
                case ConstantVariables.LIST_VIEW:
                    mLinearLayoutManager = new LinearLayoutManager(getActivity());
                    mRecyclerView.setLayoutManager(mLinearLayoutManager);
                    mRecyclerView.addItemDecoration(new LinearDividerItemDecorationUtil(mContext));
                    break;
            }
            mBrowseMLTAdapter = new RecyclerViewAdapter(getActivity(), mBrowseItemList, true, isShowHeader, mMLTBrowseType,
                    ConstantVariables.MLT_MENU_TITLE,
                    new OnItemClickListener() {

                        @Override
                        public void onItemClick(View view, int position) {

                            BrowseListItems listItems = (BrowseListItems) mBrowseItemList.get(position);
                            boolean isAllowedToView = listItems.isAllowToView();

                            if (!isAllowedToView) {
                                SnackbarUtils.displaySnackbar(mRootView,
                                        mContext.getResources().getString(R.string.unauthenticated_view_message));
                            } else {
                                Intent mainIntent = GlobalFunctions.getIntentForModule(mContext, listItems.getmListItemId(),
                                        mCurrentSelectedModule, null);
                                mainIntent.putExtra(ConstantVariables.LISTING_TYPE_ID, mListingTypeId);
                                startActivityForResult(mainIntent, ConstantVariables.VIEW_PAGE_CODE);
                                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            }

                        }
                    });
            mRecyclerView.setAdapter(mBrowseMLTAdapter);
            addScrollListener(mMLTBrowseType);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to add Scroll listener according to view type.
     * @param mMLTViewType type of view to show MLT.
     */
    public void addScrollListener(final int mMLTViewType) {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int firstVisibleItem = 0, totalItemCount = 0, lastVisibleCount, visibleItemCount = 0;

                switch (mMLTViewType) {
                    case ConstantVariables.LIST_VIEW:
                        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                                .getLayoutManager();
                        firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleCount = linearLayoutManager.findLastVisibleItemPosition() + 1;
                        visibleItemCount = lastVisibleCount - firstVisibleItem;
                        break;
                    case ConstantVariables.GRID_VIEW:
                    case ConstantVariables.MATRIX_VIEW:
                        final GridLayoutManager layoutManager = (GridLayoutManager) mRecyclerView
                                .getLayoutManager();
                        firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                        totalItemCount = layoutManager.getItemCount();
                        lastVisibleCount = layoutManager.findLastVisibleItemPosition() + 1;
                        visibleItemCount = lastVisibleCount - firstVisibleItem;
                        break;
                }
                int limit = firstVisibleItem + visibleItemCount;

                if (limit == totalItemCount && !isLoading) {

                    //if (limit >= AppConstant.LIMIT && (AppConstant.LIMIT * mLoadingPageNo) < mBrowseList.getmTotalItemCount()) {

                        mLoadingPageNo = mLoadingPageNo + 1;
                        String url;
                        if(isAdvGroupsMLT){
                            url = mGroupMLTUrl + "&limit=" + AppConstant.LIMIT + "&page=" + mLoadingPageNo;
                        } else if (isCategoryResults && limit >= mBrowseList.getmTotalItemCount()){

                               url = UrlUtil.BROWSE_CATEGORIES_MLT_URL + "&listingtype_id=" + mListingTypeId + "&page=" + mLoadingPageNo
                                       + "&showListings=1" + "&paginator=1";
                               url = mAppConst.buildQueryString(url, postParams);

                        }else {
                            url = UrlUtil.BROWSE_MLT_URL + "limit=" + AppConstant.LIMIT + "&listingtype_id=" + mListingTypeId + "&page=" + mLoadingPageNo;
                        }

                        if (isMemberMLT) {
                            url += "&user_id=" + mUserId;
                        }

                        if (!mListingFilter.isEmpty()) {
                            url += "&listing_filter=" + mListingFilter;
                        }
                        isLoading = true;
                        // Adding Search Params in the scrolling url
                        if(searchParams != null && searchParams.size() != 0){
                            url = mAppConst.buildQueryString(url, searchParams);
                        }
                        loadMoreData(url);
                    }
               // }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ConstantVariables.VIEW_PAGE_CODE) {
            PreferencesUtils.updateCurrentModule(mContext,"sitereview_listing");
            if(resultCode == ConstantVariables.VIEW_PAGE_CODE){
                makeRequest();
            }
        }
    }

    @Override
    public void onRefresh() {
        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
                isAdLoaded = false;
                makeRequest();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        if(mRecyclerView != null){
            mRecyclerView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onAdsLoaded() {
        isAdLoaded = true;
        for (int i = 0 ; i <= mBrowseItemList.size(); i++) {
            if (i != 0 && i % ConstantVariables.MLT_ADS_POSITION == 0) {
                NativeAd ad = this.listNativeAdsManager.nextNativeAd();
                mBrowseItemList.add(i, ad);
                mBrowseMLTAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onAdError(AdError adError) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        postParams.clear();


        LogUtils.LOGD(BrowseMLTFragment.class.getSimpleName(), " parent.getTag().toString() "+ parent.getTag().toString());
        switch (parent.getTag().toString()){
            case "listingFilter":
                mSelectedItem = position;
                adapter.getCustomView(position, view, parent, mSelectedItem);
                switch (position) {
                    case 0:
                        mListingFilter = "all";
                        break;
                    case 1:
                        mListingFilter = "featured";
                        break;
                    case 2:
                        mListingFilter = "sponsored";
                        break;
                }
                swipeRefreshLayout.setRefreshing(true);
                makeRequest();
                break;

            case "subCategory":
                isFirstRequest = false;
                mSelectedItem = position;
                subCategoryAdapter.getCustomView(position, view, parent, mSelectedItem);
                if (position != 0){
                    isLoadSubSubcategory = true;
                    subSubCategoryAdapter.clear();
                    subSubCategoryAdapter.add(getResources().getString(R.string.select_3rd_level_category_text));
                    JSONObject object = mSubCategoryResponse.optJSONObject(position - 1);
                    mSubCategoryId = object.optString("sub_cat_id");
                    postParams.put("subCategory_id", mSubCategoryId);
                    postParams.put("category_id", String.valueOf(mCategoryId));
                    swipeRefreshLayout.setRefreshing(true);
                    makeRequest();

                } else {
                    subCategoryLayout.setVisibility(View.VISIBLE);
                    subSubCategoryLayout.setVisibility(View.GONE);
                    postParams.put("category_id", String.valueOf(mCategoryId));
                    swipeRefreshLayout.setRefreshing(true);
                    makeRequest();
                }
                break;

            case "subSubCategory":
                isFirstRequest = false;
                mSubsubcategorySelectedItem = position;
                subSubCategoryAdapter.getCustomView(position, view, parent, mSubsubcategorySelectedItem);
                if (position != 0) {
                    JSONObject object = mSubSubCategoryResponse.optJSONObject(position - 1);
                    mSubSubCategoryId = object.optString("tree_sub_cat_id");
                    postParams.put("subCategory_id", mSubCategoryId);
                    postParams.put("category_id", String.valueOf(mCategoryId));
                    postParams.put("subsubcategory_id", mSubSubCategoryId);
                    swipeRefreshLayout.setRefreshing(true);
                    makeRequest();

                } else {
                    postParams.put("subCategory_id", mSubCategoryId);
                    postParams.put("category_id", String.valueOf(mCategoryId));
                    swipeRefreshLayout.setRefreshing(true);
                    makeRequest();
                }
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onCommunityAdsLoaded(JSONArray advertisementsArray) {

        mAdvertisementsArray = advertisementsArray;

        if(!isAdLoaded && mAdvertisementsArray != null){
            isAdLoaded = true;
            int j = 0;
            for (int i = 0 ; i <= mBrowseItemList.size(); i++) {
                if (i != 0 && i % ConstantVariables.MLT_ADS_POSITION == 0 &&
                        j < mAdvertisementsArray.length()) {
                    mBrowseItemList.add(i, addCommunityAddsToList(j));
                    j++;
                    mBrowseMLTAdapter.notifyDataSetChanged();
                }
            }
        } else if (mAdvertisementsArray != null && mBrowseItemList.size() > 0) {
            mBrowseItemList.remove(mBrowseItemList.size() - 1);
            mBrowseMLTAdapter.notifyItemRemoved(mBrowseItemList.size());
            addItemsToList(mBody);
        }
    }
}
