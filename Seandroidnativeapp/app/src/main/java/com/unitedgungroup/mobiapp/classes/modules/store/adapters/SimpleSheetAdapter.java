package com.unitedgungroup.mobiapp.classes.modules.store.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.utils.LogUtils;
import com.unitedgungroup.mobiapp.classes.modules.store.utils.SheetItemModel;

import java.util.List;

public class SimpleSheetAdapter extends RecyclerView.Adapter<SimpleSheetAdapter.ItemHolder> {
    private List<SheetItemModel> list;
    private OnItemClickListener onItemClickListener;

    public OnItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(SheetItemModel value, int position);
    }

    public SimpleSheetAdapter(List<SheetItemModel> list) {
        this.list = list;
    }

    @Override
    public SimpleSheetAdapter.ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.hidden_feeds, parent, false);
        return new ItemHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(final SimpleSheetAdapter.ItemHolder holder, int position) {
        final SheetItemModel item = list.get(position);
        if(item.getName() != null) {
            holder.textView.setText(item.getName());
        }else {
            if(item.getKeyObject() == null){
                LogUtils.LOGD("Adapter","Key Object Null");
            }
            holder.textView.setText(item.getKeyObject().optString("label"));
        }
        holder.mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final OnItemClickListener listener = holder.adapter.getOnItemClickListener();
                if (listener != null) {
                    listener.onItemClick(item, holder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }


    public static class ItemHolder extends RecyclerView.ViewHolder {

        private SimpleSheetAdapter adapter;
        TextView textView;
        View mainView;

        public ItemHolder(View itemView, SimpleSheetAdapter parent) {
            super(itemView);
            mainView = itemView;
            this.adapter = parent;
            textView = (TextView) itemView.findViewById(R.id.hiddenFeedOptions);
        }



    }
}
