/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */

package com.unitedgungroup.mobiapp.classes.common.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.utils.AddPeopleList;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AddPeopleAdapter extends ArrayAdapter<AddPeopleList> {

    Context mContext;
    List<AddPeopleList> mAddPeopleList;
    AddPeopleList mListItem;
    int mLayoutResID;
    View mRootView;

    public AddPeopleAdapter(Context context, int layoutResourceId, List<AddPeopleList> friendsList) {

        super(context, layoutResourceId, friendsList);

        mContext = context;
        mAddPeopleList = friendsList;
        mLayoutResID = layoutResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent){

        mRootView = convertView;
        final ListItemHolder listItemHolder;

        if(mRootView == null){

            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            listItemHolder = new ListItemHolder();
            mRootView = inflater.inflate(mLayoutResID, parent, false);

            listItemHolder.mFriendImage = (ImageView) mRootView.findViewById(R.id.friendImage);
            listItemHolder.mFriendLabel = (TextView) mRootView.findViewById(R.id.friendLabel);
            mRootView.setTag(listItemHolder);

        }else {
            listItemHolder = (ListItemHolder)mRootView.getTag();

        }

        mListItem = mAddPeopleList.get(position);

        /*
        Set Data in the List View Items
         */

        if (mListItem.getmUserPhoto() != null && !mListItem.getmUserPhoto().isEmpty()) {
            Picasso.with(mContext)
                    .load(mListItem.getmUserPhoto())
                    .placeholder(R.drawable.default_user_profile)
                    .into(listItemHolder.mFriendImage);
        }
        listItemHolder.mFriendLabel.setText(mListItem.getmUserLabel());

        return mRootView;
    }

    private static class ListItemHolder{

        ImageView mFriendImage;
        TextView mFriendLabel;
    }
}
