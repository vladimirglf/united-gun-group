/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */

package com.unitedgungroup.mobiapp.classes.modules.user.settings;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnSuccessListener;
import com.unitedgungroup.mobiapp.classes.common.utils.NetworkList;
import com.unitedgungroup.mobiapp.classes.common.ui.SelectableTextView;
import com.unitedgungroup.mobiapp.classes.common.utils.SnackbarUtils;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;
import com.unitedgungroup.mobiapp.classes.common.adapters.NetworkAdapter;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class NetworkFragment extends Fragment implements OnSuccessListener {

    private View mRootView;
    private String mNetworkUrl;
    private AppConstant mAppConst;
    private Context mContext;
    private int mJoinedNetworkCount;
    private ListView myNetworkListView, mAvailableNetworkView;
    private SelectableTextView mNoAvailableMessage, mCountMyNetowrks;
    private JSONArray mJoinedNetworks, mAvailableNetworks;
    private List<NetworkList> mJoinedNetworkList, mAvailableNetworksList;
    private NetworkAdapter mJoinedNetworkAdapter, mAvailableNetworkAdapter;

    public NetworkFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_network, container, false);
        mJoinedNetworkList = new ArrayList<>();
        mAvailableNetworksList = new ArrayList<>();


        if (getArguments() != null) {
            mNetworkUrl = getArguments().getString("url");
        }

        mContext = getActivity();

        mAppConst = new AppConstant(mContext);

        myNetworkListView = (ListView) mRootView.findViewById(R.id.myNetworks);
        mAvailableNetworkView = (ListView) mRootView.findViewById(R.id.availableNetworks);
        mCountMyNetowrks = (SelectableTextView) mRootView.findViewById(R.id.countMyNetowrks);

        mJoinedNetworkAdapter = new NetworkAdapter(mContext, R.layout.list_networks, mJoinedNetworkList, true, this);
        mAvailableNetworkAdapter = new NetworkAdapter(mContext, R.layout.list_networks, mAvailableNetworksList, false, this);

        myNetworkListView.setAdapter(mJoinedNetworkAdapter);
        mAvailableNetworkView.setAdapter(mAvailableNetworkAdapter);

        makeRequest();

        return mRootView;
    }

    /**
     * Method to make server call to get the user network response.
     */
    private void makeRequest() {
        if(mNetworkUrl != null){

            mRootView.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
            mRootView.findViewById(R.id.networksInfo).setVisibility(View.GONE);
            mAppConst.getJsonResponseFromUrl(mNetworkUrl, new OnResponseListener() {
                @Override
                public void onTaskCompleted(JSONObject jsonObject) {
                    mRootView.findViewById(R.id.progressBar).setVisibility(View.GONE);
                    mRootView.findViewById(R.id.networksInfo).setVisibility(View.VISIBLE);
                    if(jsonObject != null){
                        try {
                            mJoinedNetworks = jsonObject.getJSONArray("joinedNetworks");

                            if(mJoinedNetworks != null){

                                mJoinedNetworkCount = mJoinedNetworks.length();
                                for(int i = 0; i < mJoinedNetworks.length(); i++){

                                    JSONObject networkInfo = mJoinedNetworks.getJSONObject(i);

                                    int networkId = networkInfo.getInt("network_id");
                                    String networkTitle = networkInfo.getString("title");
                                    int memberCount = networkInfo.getInt("member_count");

                                    mJoinedNetworkList.add(new NetworkList(networkId, memberCount, networkTitle));
                                }

                                String networksText = mContext.getResources().getQuantityString(R.plurals.user_network,
                                        mJoinedNetworkCount);
                                mCountMyNetowrks.setText(String.format(
                                        mContext.getResources().getString(R.string.joined_network_text_format),
                                        mContext.getResources().getString(R.string.joined_network_text),
                                        mJoinedNetworkCount, networksText
                                ));
                            }

                            mAvailableNetworks = jsonObject.getJSONArray("availableNetworks");

                            if(mAvailableNetworks != null && mAvailableNetworks.length() != 0){
                                for(int i = 0; i < mAvailableNetworks.length(); i++){

                                    JSONObject networkInfo = mAvailableNetworks.getJSONObject(i);

                                    int networkId = networkInfo.getInt("network_id");
                                    String networkTitle = networkInfo.getString("title");
                                    int memberCount = networkInfo.getInt("member_count");

                                    mAvailableNetworksList.add(new NetworkList(networkId, memberCount, networkTitle));
                                }
                            }else{
                                mNoAvailableMessage = (SelectableTextView) mRootView.findViewById(R.id.noAvailableMessage);
                                mNoAvailableMessage.setVisibility(View.VISIBLE);
                                mNoAvailableMessage.setText(mContext.getResources().getString(R.string.no_network_available_message));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        mJoinedNetworkAdapter.notifyDataSetChanged();
                        mAvailableNetworkAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                    mRootView.findViewById(R.id.progressBar).setVisibility(View.GONE);
                    SnackbarUtils.displaySnackbarLongWithListener(mRootView, message,
                            new SnackbarUtils.OnSnackbarDismissListener() {
                                @Override
                                public void onSnackbarDismissed() {
                                    getActivity().finish();
                                }
                            });
                }
            });
        }
    }

    @Override
    public void onSuccess() {
        // Making server call on successful operation only if the fragment is currently added to the activity.
        if (isAdded()) {
            mJoinedNetworkList.clear();
            mAvailableNetworksList.clear();
            mJoinedNetworkAdapter.notifyDataSetChanged();
            mAvailableNetworkAdapter.notifyDataSetChanged();
            makeRequest();
        }

    }
}
