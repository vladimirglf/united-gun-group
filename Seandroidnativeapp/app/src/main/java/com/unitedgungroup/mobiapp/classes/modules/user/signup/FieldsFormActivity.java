/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */

package com.unitedgungroup.mobiapp.classes.modules.user.signup;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.activities.WebViewActivity;
import com.unitedgungroup.mobiapp.classes.common.dialogs.AlertDialogWithAction;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.utils.GlobalFunctions;
import com.unitedgungroup.mobiapp.classes.common.utils.PreferencesUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.SnackbarUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.SocialLoginUtil;
import com.unitedgungroup.mobiapp.classes.common.utils.SoundUtil;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;
import com.unitedgungroup.mobiapp.classes.core.ConstantVariables;
import com.unitedgungroup.mobiapp.classes.core.MainActivity;
import com.unitedgungroup.mobiapp.classes.common.formgenerator.FormActivity;
import com.unitedgungroup.mobiapp.classes.common.utils.DataStorage;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class FieldsFormActivity extends FormActivity {

    private Toolbar mToolBar;
    private ProgressBar mProgressBar;
    private JSONObject mFieldsJsonObject;
    private RelativeLayout mAccountFormView;
    private boolean isPhotoStep, mHasProfileFields = true;
    private AppConstant mAppConst;
    private Map<String, String> singupFieldsParams, postParams;
    private HashMap<String, String> mAccountFormValues, mSignupParams;
    private Bundle mFbTwitterBundle;
    private Context mContext;
    private AlertDialogWithAction mAlertDialogWithAction;
    private String loginType, emailAddress, password, mProfileType, mPackageId, first_name,
            last_name, picture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_creation_view);

        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolBar);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        mContext = this;
        mAppConst = new AppConstant(this);
        mAlertDialogWithAction = new AlertDialogWithAction(mContext);

        Intent loginIntent = getIntent();
        mFbTwitterBundle = loginIntent.getBundleExtra("fb_twitter_info");
        if (mFbTwitterBundle != null) {
            loginType = mFbTwitterBundle.getString("loginType");
            if (loginType != null && loginType.equals("facebook")) {
                first_name = mFbTwitterBundle.getString("firstName");
                last_name = mFbTwitterBundle.getString("lastName");
                picture = mFbTwitterBundle.getString("picture");
            }
        }
        mProfileType = loginIntent.getStringExtra("selectedProfileType");
        mPackageId = loginIntent.getStringExtra("package_id");

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        CustomViews.createMarqueeTitle(this, mToolBar);

        mAccountFormView = (RelativeLayout) findViewById(R.id.form_view);

        try {
            mFieldsJsonObject = new JSONObject(getIntent().getStringExtra("fields"));
            mAccountFormValues = (HashMap<String, String>) getIntent().getSerializableExtra("account_fields");
            emailAddress = mAccountFormValues.get("email");
            password = mAccountFormValues.get("password");
            isPhotoStep = getIntent().getBooleanExtra("isPhotoStep", false);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Checking if there is any profile fields present or not. If present then load in view,
        // otherwise sent the sign up post request.
        if(mFieldsJsonObject != null && mFieldsJsonObject.length() > 0) {
            mProgressBar.setVisibility(View.GONE);
            mAccountFormView.addView(generateForm(mFieldsJsonObject, false, "signup_fields", first_name, last_name));
        } else {
            mHasProfileFields = false;
            mAppConst.showProgressDialog();
            validateFieldsForm();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_with_action_icon, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch(id){
            case android.R.id.home:
                onBackPressed();
                // Playing backSound effect when user tapped on back button from tool bar.
                if (PreferencesUtils.isSoundEffectEnabled(mContext)) {
                    SoundUtil.playSoundEffectOnBackPressed(mContext);
                }
                break;

            case R.id.submit:
                validateFieldsForm();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem addItem = menu.findItem(R.id.submit);

        if(isPhotoStep){

//            Drawable drawable = ContextCompat.getDrawable
//                    (mContext, R.drawable.ic_arrow_forward_24dp);
//            DrawableCompat.setTint(drawable, ContextCompat.getColor(mContext, R.color.white));
            Drawable drawable = ContextCompat.getDrawable(mContext, R.drawable.ic_arrow_forward_24dp);
            drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(mContext, R.color.white),
                    PorterDuff.Mode.SRC_ATOP));
            addItem.setIcon(drawable);
        }else{
            addItem.setTitle(mContext.getResources().getString(R.string.edit_title_dialogue_button));
            addItem.setIcon(ContextCompat.getDrawable(mContext, R.drawable.ic_done_white_24dp));
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("accountFormValues", mAccountFormValues);
        setResult(ConstantVariables.SIGN_UP_CODE, intent);
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void validateFieldsForm(){

        String accountValidationUrl = AppConstant.DEFAULT_URL + "signup/validations?account_validation=0";

         /* Check signUp type, facebook and add required parameters to Validation Url*/
        if (loginType != null && !loginType.isEmpty()) {
            accountValidationUrl = mAppConst.buildQueryString(accountValidationUrl,
                    SocialLoginUtil.getFacebookTwitterParams());
        }

        singupFieldsParams = save();

        mSignupParams = new HashMap<>();
        mSignupParams = (HashMap<String, String>) singupFieldsParams;

        if(singupFieldsParams != null){
            if (mProfileType != null) {
                singupFieldsParams.put("profile_type", mProfileType);
            }

            mAppConst.hideKeyboard();
            mProgressBar.bringToFront();
            mProgressBar.setVisibility(View.VISIBLE);
            mAppConst.postJsonResponseForUrl(accountValidationUrl, singupFieldsParams, new OnResponseListener() {
                @Override
                public void onTaskCompleted(JSONObject jsonObject) {
                    mProgressBar.setVisibility(View.GONE);

                    if(isPhotoStep){
                        Intent photoIntent = new Intent(mContext, SignupPhotoActivity.class);

                        /* facebook and send details to SignUpPhotoActivity */

                        if (mFbTwitterBundle != null && !mFbTwitterBundle.isEmpty()) {
                            photoIntent.putExtra("fb_twitter_info", mFbTwitterBundle);
                        }

                        photoIntent.putExtra("package_id", mPackageId);
                        photoIntent.putExtra("account_form_values", mAccountFormValues);
                        photoIntent.putExtra("field_form_values", mSignupParams);
                        if (mHasProfileFields) {
                            startActivity(photoIntent);
                        } else {
                            mAppConst.hideProgressDialog();
                            startActivityForResult(photoIntent, ConstantVariables.SIGN_UP_CODE);
                        }
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }else{
                        postSignupForm();
                    }

                }

                @Override
                public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                    try {
                        JSONObject validationMessagesObject = new JSONObject(message);
                        showValidations(validationMessagesObject);
                        mProgressBar.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

    }

    public void postSignupForm(){

        mAppConst.showProgressDialog();
        postParams = new HashMap<>();

        if(mPackageId != null){
            postParams.put("package_id", mPackageId);
        }

        if(mAccountFormValues != null){
            Set<String> keySet = mAccountFormValues.keySet();

            for (String key : keySet) {
                String value =  mAccountFormValues.get(key);
                postParams.put(key, value);
            }
        }

        if(singupFieldsParams != null){

            Set<String> keySet = singupFieldsParams.keySet();

            for (String key : keySet) {
                String value =  singupFieldsParams.get(key);
                postParams.put(key, value);
            }
        }

        postParams.put("ip", GlobalFunctions.getLocalIpAddress());

        String postSignupUrl = AppConstant.DEFAULT_URL + "signup?subscriptionForm=1";

        if (loginType != null && !loginType.isEmpty()) {
            postSignupUrl = mAppConst.buildQueryString(postSignupUrl,
                    SocialLoginUtil.getFacebookTwitterParams());
        }

        mAppConst.postLoginSignUpRequest(postSignupUrl, postParams, new OnResponseListener() {
            @Override
            public void onTaskCompleted(JSONObject jsonObject) {

                /**
                 * Check If there user has chosen a paid subscription
                 * redirect to the web view activity on the url which is coming with body string in response.
                 * else user will be logged-in
                 */
                if(jsonObject != null){
                    if(jsonObject.has("body")){
                        mAppConst.hideProgressDialog();
                        Intent intent = new Intent(mContext, WebViewActivity.class);
                        intent.putExtra("isSubscription", true);
                        intent.putExtra("email", emailAddress);
                        intent.putExtra("password", password);
                        intent.putExtra("url", jsonObject.optString("body"));
                        if(mFbTwitterBundle != null && !mFbTwitterBundle.isEmpty()){
                            intent.putExtra("fbtTwitterInfo", mFbTwitterBundle);
                        }
                        startActivityForResult(intent, ConstantVariables.SIGN_UP_WEBVIEW_CODE);
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                    }else{
                        PreferencesUtils.clearSharedPreferences(mContext);
                        PreferencesUtils.clearDashboardData(mContext);
                        DataStorage.clearApplicationData(mContext);

                        String OauthToken = jsonObject.optString("oauth_token");
                        String oauth_secret = jsonObject.optString("oauth_secret");
                        JSONObject userDetail = jsonObject.optJSONObject("user");
                        if(userDetail != null){
                            String user_language = userDetail.optString("language");
                            if(user_language.equals("English")) {
                                user_language = "en";
                            }
                            PreferencesUtils.updateDashBoardData(mContext,
                                    PreferencesUtils.CURRENT_LANGUAGE, user_language);
                            PreferencesUtils.updateUserPreferences(mContext, userDetail.toString(),
                                    oauth_secret, OauthToken);
                            // Save email and base64 encrypted password in SharedPreferences
                            PreferencesUtils.UpdateLoginInfoPref(mContext, emailAddress,
                                    password, userDetail.optInt("user_id"));
                        }
                        Intent intent = new Intent(mContext, MainActivity.class);
                        intent.putExtra("isSetLocation", true);
                        finish();
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        mAppConst.hideProgressDialog();
                    }
                }
            }

            @Override
            public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                mAppConst.hideProgressDialog();
                mProgressBar.setVisibility(View.GONE);

                switch (message) {
                    case "email_not_verified":
                    case "not_approved":
                        SocialLoginUtil.clearFbTwitterInstances(mContext, loginType);
                        mAlertDialogWithAction.showAlertDialogForSignUpError(message);
                        break;

                    default:
                        SnackbarUtils.displaySnackbar(mAccountFormView, message);
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ConstantVariables.SIGN_UP_WEBVIEW_CODE){

            /**
             * Clear Twitter and Facebook instances if subscription
             * payment is not completed
             */
            SocialLoginUtil.clearFbTwitterInstances(this, loginType);

            mAlertDialogWithAction.showAlertDialogForSignUpError("payment_error");
        } else if (requestCode == ConstantVariables.SIGN_UP_CODE) {
            onBackPressed();
        }
    }
}
