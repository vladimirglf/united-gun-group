/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */

package com.unitedgungroup.mobiapp.classes.core;


import android.Manifest;
import android.app.Dialog;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.activities.WebViewActivity;
import com.unitedgungroup.mobiapp.classes.common.dialogs.AlertDialogWithAction;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnCheckInLocationResponseListener;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnResponseListener;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnUploadResponseListener;
import com.unitedgungroup.mobiapp.classes.common.formgenerator.FormActivity;
import com.unitedgungroup.mobiapp.classes.common.ui.BadgeView;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.ui.fab.CustomFloatingActionButton;
import com.unitedgungroup.mobiapp.classes.common.ui.fab.FloatingActionMenu;
import com.unitedgungroup.mobiapp.classes.common.multiimageselector.MultiImageSelectorActivity;
import com.unitedgungroup.mobiapp.classes.common.utils.CustomTabUtil;
import com.unitedgungroup.mobiapp.classes.common.utils.GlobalFunctions;
import com.unitedgungroup.mobiapp.classes.common.utils.LogUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.PreferencesUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.SnackbarUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.SocialShareUtil;
import com.unitedgungroup.mobiapp.classes.common.utils.UploadFileToServerUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.UrlUtil;
import com.unitedgungroup.mobiapp.classes.common.dialogs.CheckInLocationDialog;
import com.unitedgungroup.mobiapp.classes.modules.advancedActivityFeeds.FeedHomeFragment;
import com.unitedgungroup.mobiapp.classes.modules.advancedActivityFeeds.SingleFeedPage;
import com.unitedgungroup.mobiapp.classes.common.activities.CreateNewEntry;
import com.unitedgungroup.mobiapp.classes.common.activities.SearchActivity;
import com.unitedgungroup.mobiapp.classes.common.adapters.SelectAlbumListAdapter;

import com.unitedgungroup.mobiapp.classes.common.utils.BrowseListItems;


import com.unitedgungroup.mobiapp.classes.common.utils.DataStorage;
import com.unitedgungroup.mobiapp.classes.modules.messages.CreateNewMessage;
import com.unitedgungroup.mobiapp.classes.modules.packages.SelectPackage;
import com.unitedgungroup.mobiapp.classes.modules.store.CartView;
import com.unitedgungroup.mobiapp.classes.modules.pushnotification.MyFcmListenerService;
import com.unitedgungroup.mobiapp.classes.modules.user.profile.userProfile;
import com.unitedgungroup.mobiapp.classes.modules.user.settings.SettingsListActivity;
import com.facebook.login.LoginManager;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.wordpress.android.util.ToastUtils;

import java.io.IOException;
import java.io.File;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


import static android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS;
import static android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL;

public class MainActivity extends FormActivity implements FragmentDrawer.FragmentDrawerListener,
        View.OnClickListener, OnCheckInLocationResponseListener, OnUploadResponseListener,  GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private final int TYPE_HOME = 1,TYPE_MODULE = 2,TYPE_OTHER = 3;
    private boolean isHomePage = false, mIsCanView = true;
    private String mAppTitle;
    private String mUploadPhotoUrl, mIcon, mSingularLabel;
    private ArrayList<String> mSelectPath;
    private BadgeView mCartCountBadge;
    private Menu optionMenu=null;
    private ProgressDialog progressDialog;
    private AppConstant mAppConst;
    private String currentSelectedOption;
    private FragmentDrawer drawerFragment;
    private SelectAlbumListAdapter listAdapter;
    private ArrayAdapter<String> locationAdapter;
    private Map<String, String> mSelectedLocationInfo;
    private Context mContext;
    private IntentFilter intentFilter;
    private SocialShareUtil socialShareUtil;
    Toolbar toolbar;
    CustomFloatingActionButton fabAlbumCreate,fabAlbumUpload;
    private FloatingActionButton mFabCreate;
    private FloatingActionMenu mFabMenu;
    private LinearLayout mHomeTabs,mFooterTabs;
    private CardView mEventFilters;
    private TabLayout mTabHost;
    private AppBarLayout.LayoutParams mToolbarParams;
    private AlertDialogWithAction mAlertDialogWithAction;
    private int mListingTypeId, mBrowseType, mViewType;
    private int mPackagesEnabled,mStoreWishListEnabled,mMLTWishListEnabled;
    private BottomSheetBehavior<View> behavior;
    public static FirebaseAnalytics mFirebaseAnalytics;
    private GoogleApiClient mGoogleApiClient;
    private CheckInLocationDialog checkInLocationDialog;


    /* Broadcast receiver for receiving broadcasting intent action for
    viewing message and notification module (view all)    */

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if( intent!=null){
                switch (intent.getAction()) {
                    case ConstantVariables.ACTION_VIEW_ALL_MESSAGES:
                        selectItem("core_mini_messages",
                                getResources().getString(R.string.message_tab_name), null, null, 1);
                        break;
                    case ConstantVariables.ACTION_VIEW_ALL_NOTIFICATIONS:
                        if(optionMenu != null){
                            optionMenu.findItem(R.id.action_search).setVisible(false);
                        }
                        selectItem("core_mini_notification",
                                getResources().getString(R.string.notification_drawer), null, null, 1);
                        break;
                }
            }
        }
    };

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        intentFilter = new IntentFilter();
        intentFilter.addAction(ConstantVariables.ACTION_VIEW_ALL_MESSAGES);
        intentFilter.addAction(ConstantVariables.ACTION_VIEW_ALL_NOTIFICATIONS);
        registerReceiver(broadcastReceiver, intentFilter);

        mAppConst = new AppConstant(this);
        socialShareUtil = new SocialShareUtil(mContext);
        mAlertDialogWithAction = new AlertDialogWithAction(mContext);

        if(!mAppConst.checkManifestPermission(Manifest.permission.WAKE_LOCK)){
            mAppConst.requestForManifestPermission(Manifest.permission.WAKE_LOCK,
                    ConstantVariables.PERMISSION_WAKE_LOCK);
        }else{
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        }

        // Load the App in the previous saved Language
        mAppConst.changeAppLocale(PreferencesUtils.getCurrentLanguage(this), false);

        setContentView(R.layout.activity_main);
        View bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setHideable(true);
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        mAppTitle =  getApplicationContext().getResources().getString(R.string.app_name);
        mHomeTabs = (LinearLayout) findViewById(R.id.home_tab_layout);
        mFooterTabs = (LinearLayout) findViewById(R.id.quick_return_footer_ll);
        mEventFilters = (CardView) findViewById(R.id.eventFilterBlock);
        mTabHost = (TabLayout) findViewById(R.id.materialTabHost);
        mFabMenu = (FloatingActionMenu) findViewById(R.id.fab_menu);
        mFabCreate = (FloatingActionButton) findViewById(R.id.create_fab);
        mFabCreate.hide();
        mFooterTabs.setVisibility(View.GONE);
        fabAlbumCreate = (CustomFloatingActionButton) findViewById(R.id.create_new_album);
        fabAlbumCreate = (CustomFloatingActionButton) findViewById(R.id.create_new_album);
        fabAlbumUpload = (CustomFloatingActionButton) findViewById(R.id.edit_album);
        mFabCreate.setOnClickListener(this);
        fabAlbumUpload.setOnClickListener(this);
        fabAlbumCreate.setOnClickListener(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mToolbarParams = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();

        //drawer layout settings
        drawerFragment = (FragmentDrawer) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout),toolbar);
        drawerFragment.setDrawerListener(MainActivity.this);

        // Updating dashboard data.
        mAppConst.getJsonResponseFromUrl(UrlUtil.DASHBOARD_URL + "?browse_as_guest=1",
                new OnResponseListener() {
            @Override
            public void onTaskCompleted(JSONObject jsonObject) throws JSONException {

                // Updating location & guest user info.
                PreferencesUtils.updateLocationEnabledSetting(mContext,
                        jsonObject.optInt("location"));
                PreferencesUtils.updateGuestUserSettings(mContext,
                        jsonObject.optString("browse_as_guest"));

                JSONArray enabledModules = jsonObject.optJSONArray("enable_modules");
                if (enabledModules != null) {
                    String modules = enabledModules.toString().replace("[\"", "");
                    modules = modules.replace("\"]", "");
                    PreferencesUtils.updateNestedCommentEnabled(mContext,
                            enabledModules.toString().contains("nestedcomment") ? 1 : 0);
                    PreferencesUtils.updateSiteContentCoverPhotoEnabled(mContext,
                            enabledModules.toString().contains("sitecontentcoverphoto") ? 1 : 0);
                    PreferencesUtils.setEnabledModuleList(mContext, modules);
                }
                invalidateOptionsMenu();
                // Saving dashboard response into preferences.
                mAppConst.saveDashboardValues(jsonObject);


                // Updating drawer.
                drawerFragment.drawerUpdate();
            }

            @Override
            public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                // Updating drawer.
                drawerFragment.drawerUpdate();
            }
        });

        // Auto detect device location work
        if (getIntent().hasExtra("isSetLocation") && AppConstant.isDeviceLocationEnable == 1 &&
                AppConstant.mLocationType.equals("notspecific")) {
            if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
            }
        }

        Bundle extras = getIntent().getExtras();
        // Check is there any push notification intent coming from MyFcmListenerService Class
        if(extras != null) {
            int id = extras.getInt("id");
            int isSingleNotification = extras.getInt("is_single_notification");
            String type = extras.getString("type");
            String notificationViewUrl = extras.getString("notification_view_url");
            String headerTitle = extras.getString("headerTitle");
            String message = extras.getString("message");

            if (type != null && notificationViewUrl != null) {
                if (isSingleNotification == 1) {
                    int listingTypeId = extras.getInt(ConstantVariables.LISTING_TYPE_ID, 0);
                    int albumId = extras.getInt(ConstantVariables.ALBUM_ID, 0);
                    if (!type.isEmpty()) {
                        if (type.equals("album_photo") && albumId != 0) {
                            startNewActivity(type, albumId, listingTypeId, notificationViewUrl, headerTitle);
                        } else {
                            startNewActivity(type, id, listingTypeId, notificationViewUrl, headerTitle);
                        }
                    } else {
                        mAlertDialogWithAction.showPushNotificationAlertDialog(headerTitle, message);
                    }
                    selectItem("home", mAppTitle, null, null, 0);
                } else {
                    selectItem("core_mini_notification" ,getResources().
                            getString(R.string.notification_drawer), null, null, 1);
                }
            } else {
                selectItem("home", mAppTitle, null, null, 1);

            }
        } else {
            //first page selection when the app loaded first time
            selectItem("home", mAppTitle, null, null, 1);
        }
        if(!PreferencesUtils.getNotificationsCounts(this,PreferencesUtils.CART_COUNT).equals("0") &&
                !PreferencesUtils.getNotificationsCounts(this,PreferencesUtils.CART_COUNT).equals("")
                && !PreferencesUtils.getNotificationsCounts(this,PreferencesUtils.CART_COUNT).equals("null")
                && mCartCountBadge != null){
            mCartCountBadge.setVisibility(View.VISIBLE);
            mCartCountBadge.setText(PreferencesUtils.getNotificationsCounts(this,PreferencesUtils.CART_COUNT));
        }

        if(!PreferencesUtils.isAppRated(this,PreferencesUtils.APP_RATED)) {
            showRateAppDialogIfNeeded();
        }
    }
    private void showRateAppDialogIfNeeded() {
        PreferencesUtils.updateLaunchCount(this,PreferencesUtils.getLaunchCount(this)+1);
        int count =  PreferencesUtils.isAppRated(this,PreferencesUtils.NOT_RATED) ? 180 : 20;
        if(PreferencesUtils.getLaunchCount(this) % count == 0) {
            createAppRatingDialog(getString(R.string.rate_app_title) + " "+getString(R.string.app_name),
                    getString(R.string.rate_app_message)).show();
        }
    }

    private AlertDialog createAppRatingDialog(String rateAppTitle, String rateAppMessage) {
        return new AlertDialog.Builder(this,R.style.Theme_LocationDialog).setPositiveButton(getString(R.string.dialog_app_rate), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
                PreferencesUtils.updateRatePref(mContext,PreferencesUtils.APP_RATED);
                openAppInPlayStore();
            }
        }).setNegativeButton(getString(R.string.dialog_app_never), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
                PreferencesUtils.updateRatePref(mContext,PreferencesUtils.NOT_RATED);
                PreferencesUtils.updateLaunchCount(mContext,0);
                paramAnonymousDialogInterface.dismiss();
            }
        }).setMessage(rateAppMessage).setTitle(rateAppTitle).create();
    }

    public void openAppInPlayStore() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
        }
    }
    /** Swaps fragments in the main content view */

    public void selectItem(String name, String label, String headerLabel, String itemUrl, int canCreate) {

        Fragment fragment = null;
        // update the main content by replacing fragments
        if (name != null) {
            AppController.getInstance().trackEvent(name, "Dashboard Selection", label);
            switch (name) {
                case "signout":
                    PreferencesUtils.clearSharedPreferences(mContext);
                    DataStorage.clearApplicationData(mContext);
                    Twitter.getSessionManager().clearActiveSession();
                    Twitter.logOut();
                    task.execute((Void[])null);
                    break;

                case ConstantVariables.USER_SETTINGS_MENU_TITLE:
                    Intent settingsActivity = new Intent(MainActivity.this, SettingsListActivity.class);
                    startActivity(settingsActivity);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    break;

                case ConstantVariables.PRODUCT_CART_MENU_TITLE:
                    Intent cartIntent = new Intent(MainActivity.this,CartView.class);
                    startActivity(cartIntent);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    break;

                case ConstantVariables.GLOBAL_SEARCH_MENU_TITLE:
                    Intent searchActivity = new Intent(MainActivity.this, SearchActivity.class);
                    searchActivity.putExtra(ConstantVariables.IS_SEARCHED_FROM_DASHBOARD,true);
                    startActivity(searchActivity);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    break;

                case ConstantVariables.MULTI_LANGUAGES_MENU_TITLE:
                    mAppConst.changeLanguage(mContext, currentSelectedOption);
                    break;

                case ConstantVariables.LOCATION_MENU_TITLE:
                    if (!mAppConst.checkManifestPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        mAppConst.requestForManifestPermission(Manifest.permission.ACCESS_FINE_LOCATION,
                                ConstantVariables.ACCESS_FINE_LOCATION);
                    } else {
                        changeLocation();
                    }
                    break;

                case ConstantVariables.SPREAD_THE_WORD_MENU_TITLE:
                    String url = AppConstant.DEFAULT_URL.replace("api/rest/", "") + "siteapi/index/app-page";
                    socialShareUtil.shareContent(getResources().getString(R.string.spread_the_word_title) + " " +
                                    getResources().getString(R.string.app_name),
                            getResources().getString(R.string.spread_the_word_hello_text) + ",\n" +
                            getResources().getString(R.string.spread_the_word_description) + " " + url);
                    break;

                case ConstantVariables.COMET_CHAT_MENU_TITLE:
                    /**
                     * Redirect to Cometchat app if it is installed in phone
                     * else redirect to Google playstore to download install it.
                     */
                    if(isCometchatAppInstalled(ConstantVariables.COMETCHAT_PACKAGE_NAME)){
                        String userEmail = PreferencesUtils.getLoginInfo(mContext, PreferencesUtils.LOGIN_EMAIL);
                        String password = PreferencesUtils.getLoginInfo(mContext, PreferencesUtils.LOGIN_PASSWORD);
                        int userId = PreferencesUtils.getLoginUserId(mContext);
                        Intent launchIntent = getApplicationContext().getPackageManager().
                                getLaunchIntentForPackage(ConstantVariables.COMETCHAT_PACKAGE_NAME);
                        launchIntent.putExtra("username", userEmail);
                        launchIntent.putExtra("password", password);
                        launchIntent.putExtra("user_id", userId);

                        LogUtils.LOGD(MainActivity.class.getSimpleName(), " password " + password
                                + "Email = " + userEmail + " userId " + userId) ;
                        startActivityForResult(launchIntent, 1);
                    }else{
                        startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse("https://play.google.com/store/apps/details?id=" + ConstantVariables.COMETCHAT_PACKAGE_NAME)));
                    }
                    break;

                default:
                    mFooterTabs.setVisibility(View.GONE);
                    currentSelectedOption = name;
                    PreferencesUtils.updateCurrentModule(mContext, name);
                    if(mAppConst.isLoggedOutUser()){
                        if (name.equals("sitereview_listing")) {
                            PreferencesUtils.updateCurrentListingType(mContext, mListingTypeId, label,
                                    mSingularLabel, mListingTypeId, mBrowseType, mViewType, mIcon,
                                    canCreate, mPackagesEnabled, mIsCanView);
                        } else if (name.equals("home")) {
                            label = headerLabel = mAppTitle;
                            isHomePage = true;
                        }
                        if (mIsCanView) {
                            fragment = GlobalFunctions.getGuestUserFragment(name);
                        }

                        // Checking tab visibility for the logged out user.
                        // Making tool bar SCROLL_FLAG_SCROLL only for the selected modules.
                        switch (name) {
                            case ConstantVariables.MLT_MENU_TITLE:
                            case ConstantVariables.EVENT_MENU_TITLE:
                            case ConstantVariables.ADVANCED_EVENT_MENU_TITLE:
                            case ConstantVariables.SITE_PAGE_TITLE_MENU:
                            case ConstantVariables.SITE_PAGE_MENU_TITLE:
                            case ConstantVariables.ADV_GROUPS_MENU_TITLE:
                            case ConstantVariables.ADV_VIDEO_MENU_TITLE:
                            case ConstantVariables.ADV_VIDEO_CHANNEL_MENU_TITLE:
                            case ConstantVariables.PRODUCT_MENU_TITLE:
                                setTabVisibility(TYPE_MODULE);
                                break;

                            default:
                                setTabVisibility(TYPE_OTHER);
                                break;
                        }
                        mTabHost.setVisibility(View.GONE);
                    }else {
                        switch (name) {
                            case ConstantVariables.HOME_MENU_TITLE:
                                setTabVisibility(TYPE_HOME);
                                label = headerLabel = mAppTitle;
                                isHomePage = true;
                                break;

                            case ConstantVariables.SITE_PRODUCT_WISHLIST_MENU_TITLE:
                            case ConstantVariables.STORE_OFFER_MENU_TITLE:
                            case ConstantVariables.FORUM_MENU_TITLE:
                            case ConstantVariables.FRIEND_REQUEST_MENU_TITLE:
                            case ConstantVariables.USER_MENU_TITLE:
                            case ConstantVariables.CONTACT_US_MENU_TITLE:
                            case ConstantVariables.PRIVACY_POLICY_MENU_TITLE:
                            case ConstantVariables.TERMS_OF_SERVICE_MENU_TITLE:
                                mFabCreate.hide();
                                setTabVisibility(TYPE_OTHER);
                                break;

                            case ConstantVariables.ALBUM_MENU_TITLE:
                                mFabCreate.hide();
                                if(canCreate == 1) {
                                    mFabMenu.showMenu(false);
                                }
                                mFabMenu.setClosedOnTouchOutside(true);
                                setTabVisibility(TYPE_MODULE);
                                break;

                            case ConstantVariables.NOTIFICATION_MENU_TITLE:
                                mFabCreate.hide();
                                mFabMenu.hideMenu(false);
                                setTabVisibility(TYPE_MODULE);
                                break;

                            case ConstantVariables.DIARY_MENU_TITLE:
                            case ConstantVariables.MLT_WISHLIST_MENU_TITLE:
                                if(canCreate == 1) {
                                    mFabCreate.show();
                                }else {
                                    mFabCreate.hide();
                                }
                                setTabVisibility(TYPE_OTHER);
                                break;

                            case ConstantVariables.WISHLIST_MENU_TITLE:

                                mFabCreate.hide();
                                mFabMenu.hideMenu(false);
                                if (mStoreWishListEnabled == 1 && mMLTWishListEnabled == 1) {
                                    setTabVisibility(TYPE_MODULE);
                                } else {
                                    setTabVisibility(TYPE_OTHER);
                                }
                                break;

                            case ConstantVariables.MLT_MENU_TITLE:
                                PreferencesUtils.updateCurrentListingType(mContext, mListingTypeId, label,
                                        mSingularLabel, mListingTypeId, mBrowseType, mViewType, mIcon,
                                        canCreate, mPackagesEnabled, mIsCanView);

                            default:
                                mFabCreate.setImageDrawable(ContextCompat.getDrawable(
                                        this, R.drawable.ic_action_new));
                                if(canCreate == 1) {
                                    mFabCreate.show();
                                }else {
                                    mFabCreate.hide();
                                }
                                mFabMenu.hideMenu(false);
                                setTabVisibility(TYPE_MODULE);
                                break;
                        }
                        fragment = GlobalFunctions.getAuthenticateUserFragment(name,mStoreWishListEnabled,mMLTWishListEnabled);
                    }
            }

        }else if(label != null && !label.isEmpty() && itemUrl != null && !itemUrl.isEmpty()){

            AppController.getInstance().trackEvent(headerLabel, "Dashboard Selection", "Webview Loaded");
            if(ConstantVariables.WEBVIEW_ENABLE == 1) {
                Intent webViewActivity = new Intent(MainActivity.this, WebViewActivity.class);
                webViewActivity.putExtra("headerText", headerLabel);
                webViewActivity.putExtra("url", itemUrl);
                startActivityForResult(webViewActivity, ConstantVariables.WEB_VIEW_ACTIVITY_CODE);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }else {
                LogUtils.LOGD("MainActivity", "GlobalFunctions.getWebViewUrl(itemUrl, this) " +
                        GlobalFunctions.getWebViewUrl(itemUrl, this));
                CustomTabUtil.launchCustomTab(this, GlobalFunctions.getWebViewUrl(itemUrl, this));
            }
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.main_content, fragment).commit();
            // Show Header label as Toolbar title if set
            if(headerLabel != null && !headerLabel.isEmpty()) {
                setTitle(headerLabel);
            } else {
                setTitle(label);
            }
        } else if (!mIsCanView && mAppConst.isLoggedOutUser()) {
            Intent mainIntent = new Intent(this,LoginActivity.class);
            startActivity(mainIntent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }

        CustomViews.createMarqueeTitle(mContext, toolbar);
    }

    @Override
    public void setTitle(CharSequence title) {
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_main);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.optionMenu=menu;

        if (AppConstant.isLocationEnable && currentSelectedOption.equals("home") &&
                PreferencesUtils.isLocationSettingEnabled(mContext)) {
            menu.findItem(R.id.action_location).setVisible(true);
        } else {
            menu.findItem(R.id.action_location).setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar actions click
        switch (item.getItemId()) {

            case R.id.action_search:

                //calling the search activity
                Intent searchActivity = new Intent(MainActivity.this, SearchActivity.class);
                searchActivity.putExtra(ConstantVariables.IS_SEARCHED_FROM_DASHBOARD,false);
                searchActivity.putExtra(ConstantVariables.CATEGORY_FORUM_TOPIC,
                        getResources().getString(R.string.query_search_forum_topics));
                startActivity(searchActivity);
                return true;

            case R.id.action_location:
                if (!mAppConst.checkManifestPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    mAppConst.requestForManifestPermission(Manifest.permission.ACCESS_FINE_LOCATION,
                            ConstantVariables.ACCESS_FINE_LOCATION);
                } else {
                    changeLocation();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem searchItem = menu.findItem(R.id.action_search);
        MenuItem cartItem = menu.findItem(R.id.action_cart);
        if(behavior.getState() == BottomSheetBehavior.STATE_EXPANDED){
            behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }

        // Getting enabled module array to check store cart icon is need to show or not.
        List<String> enabledModuleList = null;
        if (PreferencesUtils.getEnabledModuleList(mContext) != null) {
            enabledModuleList = new ArrayList<>(Arrays.asList(PreferencesUtils.getEnabledModuleList(mContext).split("\",\"")));
        }

        if (enabledModuleList != null && enabledModuleList.contains("sitestore") && ConstantVariables.SHOW_CART_ICON) {
            cartItem.setVisible(true);
            cartItem.getActionView().setOnClickListener(this);
            mCartCountBadge = (BadgeView) cartItem.getActionView().findViewById(R.id.cart_item_count);
            if(!PreferencesUtils.getNotificationsCounts(this,PreferencesUtils.CART_COUNT).equals("0") &&
                    !PreferencesUtils.getNotificationsCounts(this,PreferencesUtils.CART_COUNT).equals("")
                    && !PreferencesUtils.getNotificationsCounts(this,PreferencesUtils.CART_COUNT).equals("null")){
                mCartCountBadge.setVisibility(View.VISIBLE);
                mCartCountBadge.setText(PreferencesUtils.getNotificationsCounts(this,PreferencesUtils.CART_COUNT));
            }
        } else {
            cartItem.setVisible(false);
        }

        if (currentSelectedOption != null) {

            switch (currentSelectedOption) {
                case ConstantVariables.NOTIFICATION_MENU_TITLE:
                case ConstantVariables.USER_SETTINGS_MENU_TITLE:
                case ConstantVariables.CONTACT_US_MENU_TITLE:
                case ConstantVariables.PRIVACY_POLICY_MENU_TITLE:
                case ConstantVariables.TERMS_OF_SERVICE_MENU_TITLE:
                case ConstantVariables.MESSAGE_MENU_TITLE:
                case ConstantVariables.FRIEND_REQUEST_MENU_TITLE:
                case ConstantVariables.PRODUCT_MENU_TITLE:
                    searchItem.setVisible(false);
                    break;

                case ConstantVariables.BLOG_MENU_TITLE:
                case ConstantVariables.CLASSIFIED_MENU_TITLE:
                case ConstantVariables.POLL_MENU_TITLE:
                case ConstantVariables.GROUP_MENU_TITLE:
                case ConstantVariables.VIDEO_MENU_TITLE:
                case ConstantVariables.EVENT_MENU_TITLE:
                case ConstantVariables.MUSIC_MENU_TITLE:
                case ConstantVariables.ADVANCED_EVENT_MENU_TITLE:
                case ConstantVariables.ALBUM_MENU_TITLE:
                case ConstantVariables.MLT_MENU_TITLE:
                case ConstantVariables.MLT_WISHLIST_MENU_TITLE:
                case ConstantVariables.FORUM_MENU_TITLE:
                case ConstantVariables.USER_MENU_TITLE:
                case ConstantVariables.ADV_GROUPS_MENU_TITLE:
                case ConstantVariables.SITE_PAGE_TITLE_MENU:
                    break;
                 default:
                    break;

            }

        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onDrawerItemSelected(View view, String name, String label, String headerLabel,
                                     String singularLabel, String itemUrl, int listingId, int browseType,
                                     int viewType, String icon, int canCreate, int packagesEnabled,
                                     int siteStoreEnabled, int listingEnabled, boolean canView) {
        mListingTypeId = listingId;
        mBrowseType = browseType;
        mViewType = viewType;
        mIcon = icon;
        mSingularLabel = singularLabel;
        mPackagesEnabled = packagesEnabled;
        mStoreWishListEnabled = siteStoreEnabled;
        mMLTWishListEnabled = listingEnabled;
        mIsCanView = canView;

        selectItem(name, label, headerLabel, itemUrl, canCreate);
    }


    //Showing list of albums for uploading photos in a particular album
    public void showListOfAlbums(){

        final Dialog albumListDialog = new Dialog(this, R.style.Theme_CustomDialog);
        final ListView listViewAlbumTitle = new ListView(this);
        final List<BrowseListItems> mBrowseItemList = new ArrayList<>();
        listAdapter =
                new SelectAlbumListAdapter(this, R.layout.simple_text_view, mBrowseItemList);
        listViewAlbumTitle.setAdapter(listAdapter);
        ProgressBar progressBar = new ProgressBar(this);
        albumListDialog.setContentView(progressBar);
        albumListDialog.setTitle(getApplicationContext().getResources()
                .getString(R.string.select_album_dialog_title));
        albumListDialog.setCancelable(true);
        albumListDialog.show();
        mAppConst.getJsonResponseFromUrl(AppConstant.DEFAULT_URL + "albums/upload",
                new OnResponseListener() {
                    @Override
                    public void onTaskCompleted(JSONObject jsonObject) throws JSONException {
                        if (jsonObject != null) {
                            albumListDialog.setContentView(listViewAlbumTitle);
                            JSONArray body = jsonObject.optJSONArray("response");
                            if (body != null && body.length() > 2) {
                                JSONObject albumList = body.optJSONObject(0);
                                if (albumList.optString("name").equals("album")) {
                                    try {
                                        if (albumList.optJSONObject("multiOptions") != null) {
                                            Iterator<String> iter = albumList.optJSONObject("multiOptions").keys();
                                            while (iter.hasNext()) {
                                                String key = iter.next();
                                                String value = albumList.optJSONObject("multiOptions").getString(key);
                                                if (!key.equals("0"))
                                                    mBrowseItemList.add(new BrowseListItems(key, value));
                                                listAdapter.notifyDataSetChanged();
                                            }
                                        } else {
                                            mBrowseItemList.add(new BrowseListItems("", getResources().
                                                    getString(R.string.no_album_error_message)));
                                            listAdapter.notifyDataSetChanged();
                                            listViewAlbumTitle.setOnItemClickListener(null);
                                        }
                                    } catch (NullPointerException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }


                        }
                    }

                    @Override
                    public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                        albumListDialog.dismiss();
                    }
                });


        listViewAlbumTitle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mFabMenu.close(true);
                String url = AppConstant.DEFAULT_URL + "albums/upload?album_id="
                        + mBrowseItemList.get(i).getAlbumId();
                albumListDialog.cancel();
                Intent uploadPhoto = new Intent(MainActivity.this, MultiImageSelectorActivity.class);
                // Whether photo shoot
                uploadPhoto.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA, true);
                // The maximum number of selectable image
                uploadPhoto.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_COUNT,
                        ConstantVariables.FILE_UPLOAD_LIMIT);
                // Select mode
                uploadPhoto.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE,
                        MultiImageSelectorActivity.MODE_MULTI);
                uploadPhoto.putExtra(MultiImageSelectorActivity.EXTRA_URL, url);

                startActivityForResult(uploadPhoto, ConstantVariables.REQUEST_IMAGE);
            }
        });

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Fragment loadFragment;
            if (requestCode == ConstantVariables.TWITTER_REQUEST_CODE) {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
                if (fragment != null) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }

            Bundle bundle = null;
            if (data != null) {
                bundle = data.getExtras();
            }

            if (resultCode != 0) {
                switch (resultCode) {

                    case ConstantVariables.CREATE_REQUEST_CODE:
                        loadFragment = GlobalFunctions.getAuthenticateUserFragment(currentSelectedOption, mStoreWishListEnabled, mMLTWishListEnabled);
                        if (loadFragment != null) {
                            loadFragment.setArguments(bundle);
                            FragmentManager fragmentManager = getSupportFragmentManager();
                            FragmentTransaction ft = fragmentManager.beginTransaction();
                            ft.replace(R.id.main_content, loadFragment);
                            ft.commitAllowingStateLoss();
                        }
                        break;

                    // Refreshing the fragment when creating any content.
                    case ConstantVariables.VIEW_PAGE_CODE:
                        loadFragment = GlobalFunctions.getAuthenticateUserFragment(currentSelectedOption, mStoreWishListEnabled, mMLTWishListEnabled);
                        if (requestCode == ConstantVariables.CREATE_REQUEST_CODE && loadFragment != null) {
                            PreferencesUtils.updateCurrentModule(mContext, currentSelectedOption);
                            loadFragment.setArguments(bundle);
                            FragmentManager fragmentManager = getSupportFragmentManager();
                            FragmentTransaction ft = fragmentManager.beginTransaction();
                            ft.replace(R.id.main_content, loadFragment);
                            ft.commitAllowingStateLoss();
                        }
                        break;

                    case ConstantVariables.REQUEST_IMAGE:
                        mSelectPath = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
                        mUploadPhotoUrl = data.getStringExtra(MultiImageSelectorActivity.EXTRA_URL);
                        StringBuilder sb = new StringBuilder();
                        for (String p : mSelectPath) {
                            sb.append(p);
                            sb.append("\n");
                        }
                        // uploading the photos to server
                        new UploadFileToServerUtils(mContext, findViewById(R.id.main_content),
                                mUploadPhotoUrl, mSelectPath, this).execute();
                        break;

                    case ConstantVariables.FEED_REQUEST_CODE:

                        if (bundle != null) {
                            if (bundle.getBoolean("isPosted", false)) {
                                if (bundle.getBoolean("isExternalShare", false)) {
                                    ToastUtils.showToast(mContext, mContext.getResources().getString(R.string.post_sharing_success_message));
                                } else {
                                    loadFragment = new FeedHomeFragment();
                                    loadFragment.setArguments(bundle);
                                    FragmentManager fragmentManager = getSupportFragmentManager();
                                    FragmentTransaction ft = fragmentManager.beginTransaction();
                                    ft.replace(R.id.main_content, loadFragment);
                                    ft.commitAllowingStateLoss();
                                }
                            }

                            if (bundle.getBoolean("isTwitterPost", false)) {
                                TweetComposer.Builder builder = new TweetComposer.Builder(this);
                                builder.text(bundle.getString("mStatusBodyText", ""));
                                if (bundle.containsKey("imagePath")) {
                                    String imagePath = bundle.getString("imagePath");
                                    File myImageFile = new File(imagePath);
                                    Uri imageUri = Uri.fromFile(myImageFile);
                                    builder.image(imageUri);
                                } else if (bundle.containsKey(ConstantVariables.VIDEO_URL)) {
                                    URL url = new URL(bundle.getString(ConstantVariables.VIDEO_URL));
                                    builder.url(url);
                                }
                                builder.show();
                            }

                        }

                        break;

                }
            } else {
                switch (requestCode) {
                    case ConstantVariables.REQUEST_IMAGE:

                        if (data != null) {
                            mSelectPath = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
                            mUploadPhotoUrl = data.getStringExtra(MultiImageSelectorActivity.EXTRA_URL);
                            StringBuilder sb = new StringBuilder();
                            for (String p : mSelectPath) {
                                sb.append(p);
                                sb.append("\n");
                            }

                            // uploading the photos to server
                            new UploadFileToServerUtils(mContext, findViewById(R.id.main_content),
                                    mUploadPhotoUrl, mSelectPath, this).execute();
                        }

                        break;

                    case ConstantVariables.WEB_VIEW_ACTIVITY_CODE:
                        if(isHomePage) {
                            selectItem("home", mAppTitle, null, null, 0);
                        }
                        break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mCartCountBadge != null) {
            if (!PreferencesUtils.getNotificationsCounts(this, PreferencesUtils.CART_COUNT).equals("0") &&
                    !PreferencesUtils.getNotificationsCounts(this, PreferencesUtils.CART_COUNT).equals("")
                    && !PreferencesUtils.getNotificationsCounts(this, PreferencesUtils.CART_COUNT).equals("null")) {
                mCartCountBadge.setVisibility(View.VISIBLE);
                mCartCountBadge.setText(PreferencesUtils.getNotificationsCounts(this, PreferencesUtils.CART_COUNT));
            } else {
                mCartCountBadge.setVisibility(View.GONE);
            }
        }
        registerReceiver(broadcastReceiver, intentFilter);
        if (currentSelectedOption != null && currentSelectedOption.equals("home")) {
            if(mTabHost.getVisibility() == View.VISIBLE) {
                mTabHost.setVisibility(View.GONE);
            }
            if(mFooterTabs.getVisibility() == View.VISIBLE){
                mFooterTabs.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onBackPressed() {

        // if drawer is open, close it.
        if(drawerFragment.mDrawerLayout != null
                && drawerFragment.mDrawerLayout.isDrawerOpen(GravityCompat.START) ){

            drawerFragment.mDrawerLayout.closeDrawers();

        }else if(behavior.getState() == BottomSheetBehavior.STATE_EXPANDED){
            behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        } else if(isHomePage) {
            //Going to home screen
            moveTaskToBack(true);
        }else {
            invalidateOptionsMenu();
            //Going back to home screen
            selectItem("home", mAppTitle, null, null, 0);

        }


    }


    @Override
    public void onClick(View view) {

        LogUtils.LOGD("MainActivity",String.valueOf(view.getId()));
        switch (view.getId()){

            case R.id.create_fab:
                //create page url settings
                Intent createEntry;
                String url = null;

                if(currentSelectedOption != null) {

                    createEntry = new Intent(MainActivity.this, CreateNewEntry.class);

                    switch (currentSelectedOption) {
                        case ConstantVariables.HOME_MENU_TITLE:
                            if(view.getTag().equals("core_main_message")){
                                createEntry = new Intent(MainActivity.this, CreateNewMessage.class);
                            }else {
                                createEntry = new Intent(this, SettingsListActivity.class);
                            }
                            break;

                        case ConstantVariables.MESSAGE_MENU_TITLE:
                            createEntry = new Intent(MainActivity.this, CreateNewMessage.class);
                            break;

                        case ConstantVariables.BLOG_MENU_TITLE:
                            url = AppConstant.DEFAULT_URL + "/blogs/create";
                            break;

                        case ConstantVariables.CLASSIFIED_MENU_TITLE:
                            url = AppConstant.DEFAULT_URL + "/classifieds/create";
                            break;

                        case ConstantVariables.GROUP_MENU_TITLE:
                            url = AppConstant.DEFAULT_URL + "groups/create";
                            break;

                        case ConstantVariables.VIDEO_MENU_TITLE:
                            url = AppConstant.DEFAULT_URL + "videos/create";
                            break;

                        case ConstantVariables.EVENT_MENU_TITLE:
                            url = AppConstant.DEFAULT_URL + "events/create";
                            break;

                        case ConstantVariables.MUSIC_MENU_TITLE:
                            url = AppConstant.DEFAULT_URL + "music/create";
                            break;

                        case ConstantVariables.POLL_MENU_TITLE:
                            url = AppConstant.DEFAULT_URL + "/polls/create";
                            break;

                        case ConstantVariables.MLT_MENU_TITLE:
                            if(mPackagesEnabled == 1){
                                createEntry = new Intent(this, SelectPackage.class);
                                url = UrlUtil.MLT_PACKAGE_LIST_URL + "&listingtype_id=" +
                                        PreferencesUtils.getCurrentSelectedListingId(mContext);
                            }else{
                                url = AppConstant.DEFAULT_URL + "listings/create?listingtype_id=" +
                                        PreferencesUtils.getCurrentSelectedListingId(mContext);
                            }
                            createEntry.putExtra(ConstantVariables.LISTING_TYPE_ID,
                                    PreferencesUtils.getCurrentSelectedListingId(mContext));
                            break;

                        case ConstantVariables.MLT_WISHLIST_MENU_TITLE:
                            url = AppConstant.DEFAULT_URL + "listings/wishlist/create";
                            break;

                        case ConstantVariables.ADVANCED_EVENT_MENU_TITLE:
                            if(mPackagesEnabled == 1) {
                                createEntry = new Intent(this, SelectPackage.class);
                                url = UrlUtil.ADV_EVENTS_PACKAGE_LIST_URL;
                            } else {
                                url = AppConstant.DEFAULT_URL + "advancedevents/create";
                            }
                            break;

                        case ConstantVariables.DIARY_MENU_TITLE:
                            url = AppConstant.DEFAULT_URL + "advancedevents/diaries/create";
                            createEntry.putExtra(ConstantVariables.FORM_TYPE, "create_new_diary");
                            break;

                        case ConstantVariables.SITE_PAGE_MENU_TITLE:
                        case ConstantVariables.SITE_PAGE_TITLE_MENU:
                            if(mPackagesEnabled == 1) {
                                createEntry = new Intent(this, SelectPackage.class);
                                url = UrlUtil.SITE_PAGE_PACKAGE_LIST_URL;
                            } else
                                url = AppConstant.DEFAULT_URL + "sitepages/create";
                            break;

                        case ConstantVariables.ADV_GROUPS_MENU_TITLE:
                            if(mPackagesEnabled == 1) {
                                createEntry = new Intent(this, SelectPackage.class);
                                url = UrlUtil.ADV_GROUP_PACKAGE_LIST_URL;
                            } else
                                url = AppConstant.DEFAULT_URL + "advancedgroups/create";
                            break;

                        case ConstantVariables.ADV_VIDEO_MENU_TITLE:
                            url = AppConstant.DEFAULT_URL + "advancedvideos/create";
                            break;

                        case ConstantVariables.ADV_VIDEO_CHANNEL_MENU_TITLE:
                            url = AppConstant.DEFAULT_URL + "advancedvideos/channel/create";
                            break;

                        case ConstantVariables.ADV_VIDEO_PLAYLIST_MENU_TITLE:
                            url = AppConstant.DEFAULT_URL + "advancedvideos/playlist/create";
                            break;

                        default:
                            currentSelectedOption = PreferencesUtils.getCurrentSelectedModule(mContext);
                            break;

                    }

                    createEntry.putExtra(ConstantVariables.CREATE_URL, url);
                    startActivityForResult(createEntry, ConstantVariables.CREATE_REQUEST_CODE);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
                break;

            case R.id.create_new_album:
                mFabMenu.close(true);
                url=AppConstant.DEFAULT_URL+"albums/upload";
                createEntry = new Intent(MainActivity.this, CreateNewEntry.class);
                createEntry.putExtra(ConstantVariables.CREATE_URL, url);
                startActivityForResult(createEntry, ConstantVariables.CREATE_REQUEST_CODE);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;

            case R.id.edit_album:
                if(!mAppConst.checkManifestPermission(Manifest.permission.READ_EXTERNAL_STORAGE)){
                    mAppConst.requestForManifestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                            ConstantVariables.READ_EXTERNAL_STORAGE);
                }else{
                    showListOfAlbums();
                }
                break;
            default:
                Intent intent = new Intent(this, CartView.class);
                startActivity(intent);


        }

    }

    public void setTabVisibility(int viewType){

        switch (viewType){
            case TYPE_HOME:
                mToolbarParams.setScrollFlags(SCROLL_FLAG_SCROLL| SCROLL_FLAG_ENTER_ALWAYS);
                mHomeTabs.setVisibility(View.VISIBLE);
                mEventFilters.setVisibility(View.GONE);
                mTabHost.setVisibility(View.GONE);
                mFabCreate.setVisibility(View.GONE);
                mFabMenu.hideMenu(false);
                break;
            case TYPE_MODULE:
                mToolbarParams.setScrollFlags(SCROLL_FLAG_SCROLL| SCROLL_FLAG_ENTER_ALWAYS);
                isHomePage = false;
                mHomeTabs.setVisibility(View.GONE);
                mEventFilters.setVisibility(View.GONE);
                break;
            case TYPE_OTHER:
                mToolbarParams.setScrollFlags(0);
                isHomePage = false;
                mHomeTabs.setVisibility(View.GONE);
                mEventFilters.setVisibility(View.GONE);
                mTabHost.setVisibility(View.GONE);
                mFabMenu.hideMenu(false);
                break;
        }
    }

    @Override
    public void onCheckInLocationChanged(final Bundle data) {
        final Fragment loadFragment = GlobalFunctions.getAuthenticateUserFragment(currentSelectedOption, mStoreWishListEnabled, mMLTWishListEnabled);
        SnackbarUtils.displaySnackbarShortWithListener(findViewById(R.id.main_content),
                mContext.getResources().getString(R.string.change_location_success_message),
                new SnackbarUtils.OnSnackbarDismissListener() {
                    @Override
                    public void onSnackbarDismissed() {
                        if (loadFragment != null && data.getBoolean("isNewLocation", true)) {
                            FragmentManager fragmentManager = getSupportFragmentManager();
                            FragmentTransaction ft = fragmentManager.beginTransaction();
                            ft.replace(R.id.main_content, loadFragment);
                            ft.commitAllowingStateLoss();
                        }
                    }
                });

    }

    @Override
    public void onUploadResponse(JSONObject jsonObject, boolean isRequestSuccessful) {
        String message;
        if (isRequestSuccessful) {
            message = getResources().
                    getQuantityString(R.plurals.photo_upload_msg,
                            mSelectPath.size(),
                            mSelectPath.size());
        } else {
            message = jsonObject.optString("message");
        }
        SnackbarUtils.displaySnackbar(findViewById(R.id.main_content), message);

    }

    /**
     * Method to start new activity according to push notification type.
     * @param type type of push notification.
     * @param id id of content page.
     * @param listingTypeId listingtypeId in case of sitereview.
     * @param notificationViewUrl web view url.
     * @param headerTitle title of push notification.
     */
    public void startNewActivity(String type, int id, int listingTypeId, String notificationViewUrl, String headerTitle){
        Intent viewIntent;
        MyFcmListenerService.clearPushNotification();

        switch (type) {
            case "user":
                viewIntent = new Intent(getApplicationContext(), userProfile.class);
                viewIntent.putExtra("user_id", id);
                startActivityForResult(viewIntent, ConstantVariables.USER_PROFILE_CODE);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                break;
            case "activity_action":
            case "activity_comment":
                viewIntent = new Intent(getApplicationContext(), SingleFeedPage.class);
                viewIntent.putExtra(ConstantVariables.ACTION_ID, id);
                startActivity(viewIntent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
            case "messages_conversation":
                viewIntent = GlobalFunctions.getIntentForModule(getApplicationContext(), id, type, null);
                viewIntent.putExtra("UserName", headerTitle);
                startActivity(viewIntent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
            default:
                viewIntent = GlobalFunctions.getIntentForModule(getApplicationContext(), id, type, null);
                if (viewIntent != null
                        && !Arrays.asList(ConstantVariables.DELETED_MODULES).contains(type)) {

                    if (type.equals("sitereview_listing") || type.equals("sitereview_review")) {
                        viewIntent.putExtra(ConstantVariables.LISTING_TYPE_ID, listingTypeId);
                    }

                    startActivityForResult(viewIntent, ConstantVariables.VIEW_PAGE_CODE);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else if (notificationViewUrl != null && !notificationViewUrl.isEmpty()) {
                    if(ConstantVariables.WEBVIEW_ENABLE == 0) {
                        CustomTabUtil.launchCustomTab(this, GlobalFunctions.
                                getWebViewUrl(notificationViewUrl, mContext));
                    }else {
                        Intent webViewActivity = new Intent(MainActivity.this, WebViewActivity.class);
                        webViewActivity.putExtra("headerText", headerTitle);
                        webViewActivity.putExtra("url", notificationViewUrl);
                        startActivityForResult(webViewActivity, ConstantVariables.WEB_VIEW_ACTIVITY_CODE);
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                }
                break;
        }
    }


    public void changeLocation() {
        if (AppConstant.mLocationType != null && AppConstant.mLocationType.equals("specific")) {

            try {
                mSelectedLocationInfo = new HashMap<>();
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                alertBuilder.setTitle(getResources().getString(R.string.location_popup_title));
                JSONObject multiLocations = null;
                int selectedPosition = 0;
                locationAdapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_singlechoice);
                String locations = PreferencesUtils.getLocations(mContext);

                // Put Location key and label in adapter and hashmap
                if (locations != null && !locations.isEmpty()) {
                    multiLocations = new JSONObject(locations);
                    JSONArray localeNames = multiLocations.names();

                    for (int i = 0; i < multiLocations.length(); i++) {
                        String locationKey = localeNames.getString(i);
                        locationAdapter.add(multiLocations.getString(locationKey));
                        mSelectedLocationInfo.put(multiLocations.getString(locationKey), locationKey);
                    }
                }

                // Show the previously selected location selected in AlertBox
                if (multiLocations != null && multiLocations.has(PreferencesUtils.getDefaultLocation(mContext))) {
                    String defaultLang = multiLocations.optString(PreferencesUtils.getDefaultLocation(mContext));
                    selectedPosition = locationAdapter.getPosition(defaultLang);

                }

                alertBuilder.setSingleChoiceItems(locationAdapter, selectedPosition,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String location = mSelectedLocationInfo.get(locationAdapter.getItem(which));
                                if (location != null && !location.isEmpty()) {
                                    PreferencesUtils.updateDashBoardData(mContext,
                                            PreferencesUtils.DASHBOARD_DEFAULT_LOCATION,
                                            location);
                                }
                                dialog.cancel();
                            }
                        });

                alertBuilder.create().show();
            }catch (JSONException|NullPointerException e) {
                e.printStackTrace();
            }

        } else {
            if (AppConstant.isDeviceLocationChange == 1) {
                Bundle bundle = new Bundle();
                bundle.putBoolean(ConstantVariables.CHANGE_DEFAULT_LOCATION, true);
                checkInLocationDialog = new CheckInLocationDialog(MainActivity.this, bundle);
                checkInLocationDialog.show();
            } else {
                SnackbarUtils.displaySnackbar(findViewById(R.id.main_content), getResources().getString(R.string.change_location_permission_message));
            }
        }
    }

    /* Executing background task for sending post request for sing out */
    AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
        @Override
        protected void onPreExecute() {

            progressDialog = ProgressDialog.show(MainActivity.this, "",
                    getResources().getString(R.string.progress_dialog_message) + "....",
                    true, false);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            mAppConst.postJsonRequest(AppConstant.DEFAULT_URL + "logout", null);
            MyFcmListenerService.clearPushNotification();
            try {
                // Delete existing FirebaseInstanceId and generate a new one for the new user.
                FirebaseInstanceId.getInstance().deleteInstanceId();
                FirebaseInstanceId.getInstance().getToken();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (progressDialog != null) {
                progressDialog.dismiss();
                LoginManager.getInstance().logOut();
                Intent homeScreen = new Intent(MainActivity.this, LoginActivity.class);
                homeScreen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeScreen);
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ConstantVariables.READ_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, proceed to the normal flow.
                    showListOfAlbums();
                } else {
                    // If user deny the permission popup
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {

                        // Show an explanation to the user, After the user
                        // sees the explanation, try again to request the permission.

                        mAlertDialogWithAction.showDialogForAccessPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                                ConstantVariables.READ_EXTERNAL_STORAGE);

                    }else{
                        // If user pressed never ask again on permission popup
                        // show snackbar with open app info button
                        // user can revoke the permission from Permission section of App Info.

                        SnackbarUtils.displaySnackbarOnPermissionResult(mContext,
                                findViewById(R.id.main_content), ConstantVariables.READ_EXTERNAL_STORAGE);

                    }
                }
                break;

            case ConstantVariables.PERMISSION_WAKE_LOCK:

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, proceed to the normal flow.
                    mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
                } else {
                    // If user deny the permission popup
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.WAKE_LOCK)) {

                        // Show an explanation to the user, After the user
                        // sees the explanation, try again to request the permission.

                        mAlertDialogWithAction.showDialogForAccessPermission(Manifest.permission.WAKE_LOCK,
                                ConstantVariables.PERMISSION_WAKE_LOCK);

                    }else{
                        // If user pressed never ask again on permission popup
                        // show snackbar with open app info button
                        // user can revoke the permission from Permission section of App Info.

                        SnackbarUtils.displaySnackbarOnPermissionResult(mContext,
                                findViewById(R.id.main_content), ConstantVariables.PERMISSION_WAKE_LOCK);

                    }
                }
                break;

            case ConstantVariables.ACCESS_FINE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    changeLocation();
                } else {
                    // If user press deny in the permission popup
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {

                        // Show an expanation to the user After the user
                        // sees the explanation, try again to request the permission.

                        AlertDialogWithAction mAlertDialogWithAction = new AlertDialogWithAction(this);
                        mAlertDialogWithAction.showDialogForAccessPermission(Manifest.permission.ACCESS_FINE_LOCATION,
                                ConstantVariables.ACCESS_FINE_LOCATION);

                    }else{
                        // If user pressed never ask again on permission popup
                        // show snackbar with open app info button
                        // user can revoke the permission from Permission section of App Info.

                        SnackbarUtils.displaySnackbarOnPermissionResult(this, findViewById(R.id.rootView),
                                ConstantVariables.ACCESS_FINE_LOCATION);

                    }
                }
                break;

        }
    }

    public boolean isCometchatAppInstalled(String packageName) {
        try {
            getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

        String street = null;
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (addresses != null && addresses.size() > 0) {
                Address returnedAddress = addresses.get(0);
                int size = returnedAddress.getMaxAddressLineIndex();
                StringBuilder strReturnedAddress = new StringBuilder();

                /*
                * Uncomment this code if need complete/accurate address/location
                * */

//                for (int j = 0; j < returnedAddress.getMaxAddressLineIndex(); j++) {
//                    strReturnedAddress.append(returnedAddress.getAddressLine(j)).append(" ");
//                }

                strReturnedAddress.append(returnedAddress.getAddressLine(size-1)).append(" ");
                street = strReturnedAddress.toString();

                Toast.makeText(mContext, getResources().getString(R.string.current_location_text) + street,
                        Toast.LENGTH_SHORT).show();

                PreferencesUtils.updateDashBoardData(mContext,
                        PreferencesUtils.DASHBOARD_DEFAULT_LOCATION, street);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }
}
