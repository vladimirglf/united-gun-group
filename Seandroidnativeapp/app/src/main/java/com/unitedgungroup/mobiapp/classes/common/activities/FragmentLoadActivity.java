/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */

package com.unitedgungroup.mobiapp.classes.common.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.unitedgungroup.mobiapp.classes.common.fragments.AdvReviewFragment;
import com.unitedgungroup.mobiapp.classes.common.fragments.MemberFragment;
import com.unitedgungroup.mobiapp.classes.common.fragments.PhotoFragment;
import com.unitedgungroup.mobiapp.classes.common.fragments.UserReviewFragment;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.utils.PreferencesUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.SoundUtil;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;
import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.core.ConstantVariables;
import com.unitedgungroup.mobiapp.classes.modules.advancedGroups.AdvGroupUtil;
import com.unitedgungroup.mobiapp.classes.modules.advancedEvents.AdvEventGuestDetailsFragment;
import com.unitedgungroup.mobiapp.classes.modules.advancedEvents.AdvEventsUtil;
import com.unitedgungroup.mobiapp.classes.modules.advancedVideos.AdvVideoUtil;
import com.unitedgungroup.mobiapp.classes.modules.directoryPages.SitePageUtil;
import com.unitedgungroup.mobiapp.classes.modules.multipleListingType.MLTUtil;
import com.unitedgungroup.mobiapp.classes.modules.offers.BrowseOffersFragment;
import com.unitedgungroup.mobiapp.classes.modules.store.utils.StoreUtil;
import com.unitedgungroup.mobiapp.classes.modules.user.profile.MemberInfoFragment;
import com.unitedgungroup.mobiapp.classes.modules.album.AlbumUtil;
import com.unitedgungroup.mobiapp.classes.modules.video.VideoUtil;

import static com.unitedgungroup.mobiapp.R.id.action_message;
import static com.unitedgungroup.mobiapp.R.id.search_occurrence;


public class FragmentLoadActivity extends AppCompatActivity {

    private String mFragmentName;
    private int mTotalItemCount, listingId, listingTypeId;
    private String mContentTitle, mTabLabel;
    private Toolbar mToolbar;
    private Context mContext;
    private Bundle bundle;
    private String currentSelectedOption = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_container);

        try {
            //Setting up the action bar
            mToolbar = (Toolbar) findViewById(R.id.toolbar);
            mContext = this;
            bundle = getIntent().getExtras();

            mFragmentName = bundle.getString(ConstantVariables.FRAGMENT_NAME);
            mContentTitle = bundle.getString(ConstantVariables.CONTENT_TITLE);
            mTotalItemCount = bundle.getInt(ConstantVariables.TOTAL_ITEM_COUNT, 0);
            currentSelectedOption = bundle.getString(ConstantVariables.EXTRA_MODULE_TYPE);

            //Getting intent data from MLT.
            listingTypeId = bundle.getInt(ConstantVariables.LISTING_TYPE_ID, 0);
            listingId = bundle.getInt(ConstantVariables.LISTING_ID, 0);
            mTabLabel = getIntent().getStringExtra(ConstantVariables.TAB_LABEL);

            Fragment loadFragment = null;

            switch (mFragmentName) {
                case "members":
                    switch (currentSelectedOption) {
                        case ConstantVariables.GROUP_MENU_TITLE:
                        case ConstantVariables.ADV_GROUPS_MENU_TITLE:
                            mToolbar.setTitle(getResources().getString(R.string.action_bar_title_members)
                                    + " (" + mTotalItemCount + ")" + ": " + mContentTitle);
                            loadFragment = new MemberFragment();
                            break;
                        case ConstantVariables.EVENT_MENU_TITLE:
                            mToolbar.setTitle(getResources().getString(R.string.action_bar_title_guests)
                                    + " (" + mTotalItemCount + ")" + ": " + mContentTitle);
                            loadFragment = new MemberFragment();
                            break;

                        case ConstantVariables.ADVANCED_EVENT_MENU_TITLE:
                            mToolbar.setTitle(getResources().getString(R.string.action_bar_title_guests)
                                    + " (" + mTotalItemCount + ")" + ": " + mContentTitle);
                            loadFragment = new AdvEventGuestDetailsFragment();
                            break;

                    }
                    break;

                case "album":
                    if (bundle.containsKey("isCoverRequest")) {
                        mToolbar.setTitle(mContentTitle);
                    }
                    loadFragment = AlbumUtil.getBrowsePageInstance();
                    break;

                case "isMemberDiaries":
                    mToolbar.setTitle(mContentTitle);
                    loadFragment = AdvEventsUtil.getBrowseDiariesInstance();
                    bundle.putBoolean("isMemberDiaries", true);
                    break;

                case "waiting_member":
                    switch (currentSelectedOption) {
                        case ConstantVariables.GROUP_MENU_TITLE:
                        case ConstantVariables.ADV_GROUPS_MENU_TITLE:
                            mToolbar.setTitle(getResources().getString(R.string.waiting_members_text));
                            break;

                        case ConstantVariables.EVENT_MENU_TITLE:
                        case ConstantVariables.ADVANCED_EVENT_MENU_TITLE:
                            mToolbar.setTitle(getResources().getString(R.string.waiting_guests_text));
                            break;
                    }
                    loadFragment = new MemberFragment();
                    bundle.putBoolean("isWatingMember", true);
                    break;

                case "announcement":
                    mToolbar.setTitle(mContentTitle + " (" + mTotalItemCount + ")");
                    loadFragment = new MemberInfoFragment();
                    bundle.putString(ConstantVariables.FORM_TYPE, "announcement");
                    break;

                case "search_by_date":
                    mToolbar.setTitle(getResources().getString(R.string.action_bar_title_event) + ": " + mContentTitle);
                    loadFragment = AdvEventsUtil.getBrowsePageInstance();
                    bundle.putBoolean("isSearchByDate", true);
                    break;

                case "group_categories":
                    mToolbar.setTitle(mContentTitle + ": " + mTotalItemCount);
                    loadFragment = AdvGroupUtil.getBrowsePageInstance();
                    bundle.putBoolean(ConstantVariables.IS_CATEGORY_BASED_RESULTS, true);
                    break;

                case "adv_events_categories":
                    mToolbar.setTitle(mContentTitle + ": " + mTotalItemCount);
                    loadFragment = AdvEventsUtil.getBrowsePageInstance();
                    bundle.putBoolean(ConstantVariables.IS_CATEGORY_BASED_RESULTS, true);
                    break;

                case "site_page_categories":
                    mToolbar.setTitle(mContentTitle + ": " + mTotalItemCount);
                    loadFragment = SitePageUtil.getBrowsePageInstance();
                    bundle.putBoolean(ConstantVariables.IS_CATEGORY_BASED_RESULTS, true);
                    break;

                case "mlt_categories":
                    mToolbar.setTitle(mContentTitle + ": " + mTotalItemCount);
                    loadFragment = MLTUtil.getBrowsePageInstance();
                    bundle.putBoolean(ConstantVariables.IS_CATEGORY_BASED_RESULTS, true);
                    break;

                case "store_product_categories":
                    mToolbar.setTitle(mContentTitle + ": " + mTotalItemCount);
                    loadFragment = StoreUtil.getProductsBrowsePageInstance();
                    bundle.putBoolean(ConstantVariables.IS_CATEGORY_BASED_RESULTS, true);
                    break;

                case "adv_videos_categories":
                    mToolbar.setTitle(mContentTitle + ": " + mTotalItemCount);
                    loadFragment = AdvVideoUtil.getBrowsePageInstance();
                    bundle.putBoolean(ConstantVariables.IS_CATEGORY_BASED_RESULTS, true);
                    break;

                case "adv_videos_channel_categories":
                    mToolbar.setTitle(mContentTitle + ": " + mTotalItemCount);
                    loadFragment = AdvVideoUtil.getChannelBrowsePageInstance();
                    bundle.putBoolean(ConstantVariables.IS_CATEGORY_BASED_RESULTS, true);
                    break;

                case "reviews":
                    switch (currentSelectedOption) {
                        case ConstantVariables.MLT_MENU_TITLE:
                        case ConstantVariables.ADVANCED_EVENT_MENU_TITLE:
                        case ConstantVariables.PRODUCT_MENU_TITLE:
                            if (mTabLabel == null || mTabLabel.isEmpty()) {
                                mTabLabel = getResources().getString(R.string.user_review);
                                bundle.putString(ConstantVariables.TAB_LABEL, mTabLabel);
                            }
                            mToolbar.setTitle(mTabLabel);
                            loadFragment = new UserReviewFragment();
                            break;

                        case ConstantVariables.ADV_GROUPS_MENU_TITLE:
                        case ConstantVariables.SITE_PAGE_MENU_TITLE:
                            mToolbar.setTitle(getResources().getString(R.string.user_review));
                            loadFragment = new AdvReviewFragment();
                            break;
                    }
                    break;

                case "offer":
                    mToolbar.setTitle(getResources().getString(R.string.action_bar_title_offers) + " (" + mTotalItemCount + ")" + ": " + mContentTitle);
                    loadFragment = new BrowseOffersFragment();
                    break;

                case "photos":
                    if (bundle.containsKey("isCoverRequest")) {
                        mToolbar.setTitle(mContentTitle);
                    } else {
                        mToolbar.setTitle(getResources().getString(R.string.action_bar_title_photos) +
                                " (" + mTotalItemCount + ")" + ": " + mContentTitle);
                    }
                    loadFragment = new PhotoFragment();
                    break;

                case "occurence_index":
                    mToolbar.setTitle(mContentTitle);
                    loadFragment = new AdvEventGuestDetailsFragment();
                    break;

                case "video":
                case "videos":
                    mToolbar.setTitle(mContentTitle + " (" + mTotalItemCount + ")");
                    loadFragment = VideoUtil.getBrowsePageInstance();
                    break;

                default:
                    break;
            }

            if (loadFragment != null) {
                loadFragment.setArguments(bundle);
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.replace(R.id.main_content, loadFragment);
                ft.commit();
            }

            setSupportActionBar(mToolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
            CustomViews.createMarqueeTitle(mContext, mToolbar);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_fragment_load, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                // Playing backSound effect when user tapped on back button from tool bar.
                if (PreferencesUtils.isSoundEffectEnabled(mContext)) {
                    SoundUtil.playSoundEffectOnBackPressed(mContext);
                }
                return true;

            case R.id.add_entry:
                // TODO in future
                String redirectUrl = AppConstant.DEFAULT_URL + "listings/video/create/" + listingId +
                        "?listingtype_id=" + listingTypeId;
                Intent createEntry = new Intent(FragmentLoadActivity.this, CreateNewEntry.class);
                createEntry.putExtra(ConstantVariables.CREATE_URL, redirectUrl);
                createEntry.putExtra(ConstantVariables.FORM_TYPE, "add_video");
                createEntry.putExtra(ConstantVariables.EXTRA_MODULE_TYPE, ConstantVariables.MLT_MENU_TITLE);
                startActivityForResult(createEntry, ConstantVariables.CREATE_REQUEST_CODE);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;

            case R.id.search_occurrence:
                return false;

            case R.id.action_message:
                return false;

        }

        return super.onOptionsItemSelected(item);
    }

    /***
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem addEntry = menu.findItem(R.id.add_entry);
        MenuItem searchOccurrence = menu.findItem(search_occurrence);
        MenuItem composeMessage = menu.findItem(action_message);
        MenuItem search = menu.findItem(R.id.action_search);

        if(mFragmentName != null){
            if (mFragmentName.equals("members") || mFragmentName.equals("wating_member")) {
                addEntry.setVisible(false);
                search.setVisible(true);
                if (currentSelectedOption.equals("core_main_siteevent")){
                    composeMessage.setVisible(true);
                }

            } else if (mFragmentName.equals("occurence_index")) {
                search.setVisible(false);
                composeMessage.setVisible(true);
                searchOccurrence.setVisible(true);
            }
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == ConstantVariables.USER_PROFILE_CODE) {
            PreferencesUtils.updateCurrentModule(mContext, currentSelectedOption);
        }
    }

}
