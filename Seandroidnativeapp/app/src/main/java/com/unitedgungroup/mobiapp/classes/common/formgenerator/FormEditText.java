
/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 *
 */

package com.unitedgungroup.mobiapp.classes.common.formgenerator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.adapters.AddPeopleAdapter;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnResponseListener;
import com.unitedgungroup.mobiapp.classes.common.ui.BaseButton;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.ui.NestedListView;
import com.unitedgungroup.mobiapp.classes.common.ui.SelectableTextView;
import com.unitedgungroup.mobiapp.classes.common.utils.AddPeopleList;
import com.unitedgungroup.mobiapp.classes.common.utils.PreferencesUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.SelectedFriendList;
import com.unitedgungroup.mobiapp.classes.common.utils.SnackbarUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.UrlUtil;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;
import com.unitedgungroup.mobiapp.classes.core.ConstantVariables;
import com.unitedgungroup.mobiapp.classes.modules.advancedEvents.AdvEventsInfoTabFragment;
import com.unitedgungroup.mobiapp.classes.modules.editor.NewEditorActivity;
import com.unitedgungroup.mobiapp.classes.modules.messages.SelectedFriendListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class FormEditText extends FormWidget implements TextWatcher, AdapterView.OnItemClickListener, View.OnClickListener {
    protected SelectableTextView _label;
    protected AppCompatEditText _input;
    protected TextInputLayout _inputWrapper;
    protected BaseButton _checkUrlButton;
    protected RelativeLayout _urlFieldBlock;
    protected ImageView imageview;

    protected AppCompatAutoCompleteTextView _locationField;

    protected String fieldName;

    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";

    FormWidget widget;
    Context mContext;

    //Advanced Events Search guest code

    public static final String SCHEMA_KEY_SEND_INVITES = "user_ids";
    private AppConstant mAppConst;
    private NestedListView mGuestListView;
    private RecyclerView mAddedFriendListView;

    private SelectedFriendListAdapter mSelectedGuestListAdapter;
    private List<SelectedFriendList> mSelectedGuestList;
    private List<AddPeopleList> mAddPeopleList;
    private Map<String, String> selectedGuest, mPostParams;

    private JSONArray guestListResponse;
    private Map<Integer, String> showNonSelectedGuest;

    private int hostPosition;
    private boolean isSendInviteRequest = false, isSearchHost = false, isCreateForm;
    private TextView cancelTextView, hostTextView;
    FormActivity formActivity;
    ArrayList<FormWidget> _widgetsNew;
    Map<String, FormWidget> _mapNew;
    String mCurrentSelectedOption;
    private int mContentId;
    ProgressBar progressBar;
    static Context staticContext;


    public FormEditText(Context context, String property, String description, boolean hasValidator, String type,
                        ArrayList<FormWidget> _widgets, Map<String, FormWidget> _map, int contentId, boolean createForm,
                        String value, String selectedOption) {

        super(context, property, hasValidator);

        staticContext = mContext = context;
        fieldName = property;
        _widgetsNew = _widgets;
        _mapNew = _map;
        formActivity = new FormActivity();
        isCreateForm = createForm;

        mContentId = contentId;

        mAppConst = new AppConstant(mContext);
        mCurrentSelectedOption = selectedOption;

        _inputWrapper = new TextInputLayout(context);
        _inputWrapper.setLayoutParams(FormActivity.defaultLayoutParams);

        _urlFieldBlock = new RelativeLayout(context);
        _urlFieldBlock.setLayoutParams(new LayoutParams( LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

        _input = new AppCompatEditText(context);
        _input.setTag(property);
        _input.setId(R.id.property);

        _label = new SelectableTextView(context);
        _label.setText(getDisplayText());
        _label.setLayoutParams(FormActivity.defaultLayoutParams);

        _checkUrlButton = new BaseButton(context);
        _checkUrlButton.setTextColor(ContextCompat.getColor(context, R.color.white));
        _checkUrlButton.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
        _checkUrlButton.setText(context.getResources().getString(R.string.check_url));

        imageview = new ImageView(context);
        FormActivity.reset = 0;

        if (property.equals("overview") && (mCurrentSelectedOption.equals("sitereview_listing")
                || mCurrentSelectedOption.equals("core_main_siteevent"))) {

            _input.setGravity(Gravity.START | Gravity.TOP);
            _input.setMinHeight(100);
            _input.setId(R.id.overview);
            _input.setFocusableInTouchMode(false);
            _inputWrapper.addView(_input);
            _input.setTag("editor");
            _input.setOnClickListener(this);

        } else if (type.equals(FormActivity.SCHEMA_KEY_TextArea) || type.equals(FormActivity.SCHEMA_KEY_TextArea_LOWER)) {

            _input.setMinHeight(100);
            _input.setGravity(Gravity.START | Gravity.TOP);
            _input.setSingleLine(false);
            _inputWrapper.addView(_input);

        } else if (property.equals("location")) {
            String defaultLocation = PreferencesUtils.getDefaultLocation(mContext);
            _locationField = new AppCompatAutoCompleteTextView(context);
            if (defaultLocation != null && !defaultLocation.isEmpty()) {
                _locationField.setText(defaultLocation);
            }
            _locationField.setId(R.id.locationAutoComplete);
            _locationField
                    .setAdapter(new GooglePlacesAutocompleteAdapter(context, R.layout.simple_text_view));
            _locationField.setSingleLine(true);

            _inputWrapper.addView(_locationField);

            _label = new SelectableTextView(context);
            _label.setText(description);
            _label.setLayoutParams(FormActivity.defaultLayoutParams);
            _inputWrapper.addView(_label);


        } else if ((mCurrentSelectedOption.equals(ConstantVariables.ADV_GROUPS_MENU_TITLE)
                && property.equals("group_uri"))
                || (mCurrentSelectedOption.equals(ConstantVariables.ADV_VIDEO_CHANNEL_MENU_TITLE)
                && property.equals("channel_uri"))) {
            _input.setLayoutParams(new LayoutParams( LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            _input.setId(R.id.editTextUrl);
            _input.addTextChangedListener(this);
            _urlFieldBlock.addView(_input);

            int margin = (int) (context.getResources().getDimension(R.dimen.margin_10dp) /
                    context.getResources().getDisplayMetrics().density);

            LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            layoutParams.setMargins(0, 0, margin, 0);
            _checkUrlButton.setPadding(margin, margin, margin, margin);
            imageview.setPadding(margin, margin, margin, margin);

            _checkUrlButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!_input.getText().toString().trim().isEmpty()) {
                        checkUrlAvailability(_input.getText().toString());
                    } else {
                        SnackbarUtils.displaySnackbar(v, mContext.getResources().getString(R.string.empty_url_text));
                    }
                }
            });
            _urlFieldBlock.addView(_checkUrlButton, layoutParams);
            _urlFieldBlock.addView(imageview, layoutParams);
            imageview.setVisibility(View.GONE);
            _layout.addView(_urlFieldBlock);

        } else if ((mCurrentSelectedOption.equals("core_main_video")
                || mCurrentSelectedOption.equals("home")
                || mCurrentSelectedOption.equals(ConstantVariables.ADV_VIDEO_MENU_TITLE)
                || mCurrentSelectedOption.equals("message"))
                && property.equals("url")) {
            _input.setId(R.id.editTextUrl);
            _inputWrapper.addView(_input);

        } else if (type.equals(FormActivity.SCHEMA_KEY_PASSWORD)) {
            _input.setGravity(Gravity.START | Gravity.TOP);
            _input.setSingleLine(true);
            _input.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
            _input.setTransformationMethod(PasswordTransformationMethod.getInstance());
            _inputWrapper.addView(_input);

        } else if (property.equals("searchGuests") || property.equals("toValues")) {
            searchGuest();
            _input.setGravity(Gravity.START | Gravity.TOP);
            _input.setSingleLine(true);
            _inputWrapper.addView(_input);

        } else if (mCurrentSelectedOption.equals("add_to_diary") || mCurrentSelectedOption.equals("add_wishlist") ||
                mCurrentSelectedOption.equals("add_to_friend_list") || mCurrentSelectedOption.equals("add_to_playlist")) {
            if (!FormActivity.sIsCreateDiaryDescription) {
                AppCompatTextView createDiaryTextView = new AppCompatTextView(context);
                int padding  = (int) mContext.getResources().getDimension(R.dimen.padding_6dp);
                createDiaryTextView.setPadding(0,padding,0,0);
                createDiaryTextView.setText(FormActivity.createDiaryDescription);
                createDiaryTextView.setTextColor(ContextCompat.getColor(context, R.color.body_text_1));

                _layout.addView(createDiaryTextView);
                FormActivity.sIsCreateDiaryDescription = true;
            }
            _input.setGravity(Gravity.START | Gravity.TOP);
            _input.setSingleLine(true);
            _inputWrapper.addView(_input);

        } else if (property.equals("host_auto")) {

            isSearchHost = true;

            _input.setGravity(Gravity.START | Gravity.TOP);
            _input.setSingleLine(true);
            _input.setLayoutParams(FormActivity.defaultLayoutParams);

            RelativeLayout relativeLayout = new RelativeLayout(mContext);
            LayoutParams layoutParams = new LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            relativeLayout.setLayoutParams(layoutParams);

            progressBar = new ProgressBar(mContext, null, android.R.attr.progressBarStyleSmall);
            layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE);
            layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);

            progressBar.setLayoutParams(layoutParams);
            progressBar.setVisibility(View.GONE);

            relativeLayout.addView(_input);
            relativeLayout.addView(progressBar);

            _inputWrapper.addView(relativeLayout);

            searchGuest();

        } else if (property.equals("capacity") || property.equals("username")) {

            _label = new SelectableTextView(context);
            _label.setText(description);
            _label.setLayoutParams(FormActivity.defaultLayoutParams);
            _inputWrapper.addView(_label);

            _input.setGravity(Gravity.START | Gravity.TOP);
            _input.setSingleLine(true);
            _inputWrapper.addView(_input);
            
        } else {
            _input.setGravity(Gravity.START | Gravity.TOP);
            _input.setSingleLine(true);
            _inputWrapper.addView(_input);

        }

        if (property.equals("host_title")) {

            int margin  = (int) mContext.getResources().getDimension(R.dimen.margin_10dp);

            LinearLayout linearLayout = new LinearLayout(context);

            LinearLayout.LayoutParams layoutParams = CustomViews.getWrapLayoutParams();
            layoutParams.setMargins(margin,0,0,0);
            linearLayout.setLayoutParams(layoutParams);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);

            hostTextView = new TextView(context);
            hostTextView.setText(mContext.getResources().getString(R.string.add_new_host_text));

            cancelTextView = new TextView(context);
            cancelTextView.setPadding(margin,margin,margin,margin);
            cancelTextView.setId(R.id.cancel);
            cancelTextView.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            cancelTextView.setText(" [" + mContext.getResources().getString(R.string.cancel) + "]");

            linearLayout.addView(hostTextView);
            linearLayout.addView(cancelTextView);

           _layout.addView(linearLayout);

            cancelTextView.setOnClickListener(this);
        }

        if (!(mCurrentSelectedOption.equals("core_main_sitegroup") && property.equals("group_uri"))
                && !(mCurrentSelectedOption.equals(ConstantVariables.ADV_VIDEO_CHANNEL_MENU_TITLE)
                && property.equals("channel_uri"))) {
            _layout.addView(_inputWrapper);
        }

        if(((FormHostChange.sIsEditHost &&  FormHostChange.sIsCreateNewHost) ||
                mCurrentSelectedOption.equals("signup_fields")) && value != null){
            _input.setText(Html.fromHtml(value));
        }

    }

    private void searchGuest() {
        mAppConst = new AppConstant(mContext);
        showNonSelectedGuest = new HashMap<>();

        mAddPeopleList = new ArrayList<>();
        mSelectedGuestList = new ArrayList<>();
        selectedGuest = new HashMap<>();

        //Views

        mGuestListView = new NestedListView(mContext);
        mGuestListView.setLayoutParams(CustomViews.getFullWidthLayoutParams());

        FormActivity.mAddPeopleAdapter = new AddPeopleAdapter(mContext, R.layout.list_friends, mAddPeopleList);
        mGuestListView.setAdapter(FormActivity.mAddPeopleAdapter);

        mAddedFriendListView = new RecyclerView(mContext);

        mAddedFriendListView.setLayoutParams(CustomViews.getCustomWidthHeightLayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, 100));

        LinearLayoutManager layoutManager= new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mAddedFriendListView.setLayoutManager(layoutManager);
        mSelectedGuestListAdapter = new SelectedFriendListAdapter(mSelectedGuestList,
                mAddedFriendListView,selectedGuest, showNonSelectedGuest, isSendInviteRequest);
        mAddedFriendListView.setAdapter(mSelectedGuestListAdapter);

        //Setting the listeners
        _input.addTextChangedListener(this);
        mGuestListView.setOnItemClickListener(this);


        _inputWrapper.addView(mGuestListView);

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence searchText, int start, int before, int count) {

        if((mCurrentSelectedOption.equals(ConstantVariables.ADV_GROUPS_MENU_TITLE)
                && fieldName.equals("group_uri"))
                || (mCurrentSelectedOption.equals(ConstantVariables.ADV_VIDEO_CHANNEL_MENU_TITLE)
                && fieldName.equals("channel_uri"))) {
            _input.setError(null);
            imageview.setVisibility(View.GONE);
            _checkUrlButton.setText(mContext.getResources().getString(R.string.check_url));
            _checkUrlButton.setVisibility(View.VISIBLE);
        }else{
            if(searchText != null && searchText.length() != 0) {
                String getFriendsUrl;
                if (isSearchHost) {
                    getFriendsUrl = AppConstant.DEFAULT_URL + "advancedevents/get-hosts?host_type_select=" +
                            FormActivity.hostKey+ "&host_auto=" + searchText;
                } else if(mContentId != 0){
                    getFriendsUrl = AppConstant.DEFAULT_URL + "advancedgroups/members/getusers/" + mContentId +
                            "?limit=10" + "&text=" + searchText;
                }else {
                    getFriendsUrl = AppConstant.DEFAULT_URL + "advancedevents/member-suggest?limit=10" +
                            "&subject=" + AdvEventsInfoTabFragment.sGuid + "&value=" + searchText;
                }
                getFriendList(getFriendsUrl);
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public void getFriendList(String url) {
        progressBar.setVisibility(View.VISIBLE);

        mAppConst.getJsonResponseFromUrl(url, new OnResponseListener() {
            @Override
            public void onTaskCompleted(JSONObject body) {
                progressBar.setVisibility(View.GONE);

                InputMethodManager imm = (InputMethodManager)mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(_input.getWindowToken(), 0);

                if (body != null && body.length() != 0) {
                    mAddPeopleList.clear();
                    FormActivity.mAddPeopleAdapter.clear();
                    guestListResponse = body.optJSONArray("response");
                    for (int i = 0; i < guestListResponse.length(); i++) {
                        JSONObject friendObject = guestListResponse.optJSONObject(i);
                        try {
                            if (!isSearchHost) {
                                String username = friendObject.optString("label");
                                int userId = friendObject.optInt("id");
                                String userImage = friendObject.optString("image_icon");

                                if (!showNonSelectedGuest.isEmpty()) {
                                    if (!showNonSelectedGuest.containsKey(userId)) {
                                        mAddPeopleList.add(new AddPeopleList(userId, username, userImage));
                                    }
                                } else {
                                    mAddPeopleList.add(new AddPeopleList(userId, username, userImage));
                                }
                            } else {
                                String username = friendObject.optString("host_title");
                                int userId = friendObject.optInt("host_id");
                                String userImage = friendObject.optString("image_icon");
                                mAddPeopleList.add(new AddPeopleList(userId, username, userImage, friendObject));
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    FormActivity.mAddPeopleAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onErrorInExecutingTask(String message, boolean isRetryOption) {

            }
        });

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        AddPeopleList addPeopleList = mAddPeopleList.get(position);
        String label = addPeopleList.getmUserLabel();
        int userId = addPeopleList.getmUserId();

        if( FormActivity.mAddPeopleAdapter != null){
            FormActivity.mAddPeopleAdapter.clear();
        }


        if (!isSearchHost) {
            mAddedFriendListView.setVisibility(View.VISIBLE);
            if(!selectedGuest.containsKey(Integer.toString(userId))) {
                selectedGuest.put(Integer.toString(userId), label);
                FormActivity.selectedGuest = selectedGuest;
                showNonSelectedGuest.put(userId, label);
                mSelectedGuestList.add(new SelectedFriendList(userId, label));
                mSelectedGuestListAdapter.notifyDataSetChanged();
            }

            _inputWrapper.addView(mAddedFriendListView);
            _input.setText("");
        } else {

            for (int j = 0; j < _widgetsNew.size(); j++) {
                if (_widgetsNew.get(j).getPropertyName().equals("host_type_select")) {
                    position = j;
                }

                if (_widgetsNew.get(j).getPropertyName().contains("host")) {
                    _widgetsNew.remove(j);
                    j--;
                }
            }

            JSONObject jsonObject = addPeopleList.getmHostObject();
            String fieldName = FormHostChange.sEventHost;
            String fieldLabel = jsonObject.optString("host_title");

            widget = formActivity.getWidget(mContext, fieldName, jsonObject, fieldLabel, false, true,
                    null, _widgetsNew, _mapNew, mCurrentSelectedOption, null,
                    null, null, null, null, null);

            if (jsonObject.has(FormActivity.SCHEMA_KEY_HINT))
                widget.setHint(jsonObject.optString(FormActivity.SCHEMA_KEY_HINT));

            _widgetsNew.add(position, widget);
            _mapNew.put(fieldName, widget);

            FormActivity._layout.removeAllViews();
            for (int k = 0; k < _widgetsNew.size(); k++) {
                FormActivity._layout.addView(_widgetsNew.get(k).getView());
            }
        }

        _input.setText("");
    }

    @Override
    public String getValue(){

        if(fieldName.equals("location"))
            return _locationField.getText().toString();
        else
            if(_input.getTag().equals("body")) {
                return Html.toHtml(_input.getText());
            }else {
                return _input.getText().toString();
            }
    }

    @Override
    public void setValue( String value ) {

        if(fieldName.equals("location"))
            _locationField.setText(value);
        else
            _input.setText(Html.fromHtml(value));
    }

    @Override
    public void setHint( String value ) {
        if ((mCurrentSelectedOption.equals("core_main_sitegroup") && fieldName.equals("group_uri"))
                || (mCurrentSelectedOption.equals(ConstantVariables.ADV_VIDEO_CHANNEL_MENU_TITLE)
                && fieldName.equals("channel_uri"))) {
            _input.setHint(value);
        } else {
            _inputWrapper.setHint(value);
        }
    }

    public void setErrorMessage(String errorMessage) {

        if(fieldName.equals("location")) {
            _locationField.setError(errorMessage);
        }
        else {
            _input.setError(errorMessage);
        }

        if (FormActivity.reset == 0) {
            if(fieldName.equals("location")) {
                _locationField.setFocusable(true);
                _locationField.requestFocus();
            }
            else {
                _input.setFocusable(true);
                _input.requestFocus();
            }
            FormActivity.reset++;
        }
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.cancel) {
            if (FormHostChange.sIsEditHost) {
                FormHostChange.sIsEditHost = false;
                for (int k = 0; k < _widgetsNew.size(); k++) {
                    if (_widgetsNew.get(k).getPropertyName().equals(FormHostChange.sEventHost)) {
                        _widgetsNew.get(k).getView().setVisibility(View.VISIBLE);
                    } else if (_widgetsNew.get(k).getPropertyName().equals("host_title") ||
                            _widgetsNew.get(k).getPropertyName().equals("host_description") ||
                            _widgetsNew.get(k).getPropertyName().equals("host_photo") ||
                            _widgetsNew.get(k).getPropertyName().equals("host_link") ||
                            _widgetsNew.get(k).getPropertyName().equals("host_facebook") ||
                            _widgetsNew.get(k).getPropertyName().equals("host_twitter") ||
                            _widgetsNew.get(k).getPropertyName().equals("host_website")) {
                        _widgetsNew.get(k).getView().setVisibility(View.GONE);

                    }

                }

            } else if (FormHostChange.sIsAddNewHost) {
                FormActivity.loadEditHostForm = 0;
                FormHostChange.sIsAddNewHost = false;

                for (int j = 0; j < _widgetsNew.size(); j++) {
                    if (_widgetsNew.get(j).getPropertyName().contains("host") &&
                            !_widgetsNew.get(j).getPropertyName().equals(FormHostChange.sEventHost)) {
                        _widgetsNew.remove(j);
                        j--;
                    }
                }

                for (int j = 0; j < _widgetsNew.size(); j++) {

                    if (_widgetsNew.get(j).getPropertyName().equals(FormHostChange.sEventHost)) {
                        hostPosition = j;
                    }
                }

                hostPosition++;

                for (int i = 0; i < FormActivity.mHostSelectionForm.length(); i++) {

                    JSONObject jsonObject = FormActivity.mHostSelectionForm.optJSONObject(i);
                    String fieldName = jsonObject.optString("name");
                    String fieldLabel = jsonObject.optString("label");

                    widget = formActivity.getWidget(mContext, fieldName, jsonObject, fieldLabel, false, isCreateForm,
                            null, _widgetsNew, _mapNew, mCurrentSelectedOption, null,
                            null, null, null, null, null);

                    if (widget == null) continue;

                    if (jsonObject.has(FormActivity.SCHEMA_KEY_HINT))
                        widget.setHint(jsonObject.optString(FormActivity.SCHEMA_KEY_HINT));

                    _widgetsNew.add(hostPosition + i, widget);
                    _mapNew.put(fieldName, widget);

                }

                FormActivity._layout.removeAllViews();
                for (int i = 0; i < _widgetsNew.size(); i++) {
                    if (_widgetsNew.get(i).getPropertyName().equals(FormHostChange.sEventHost)) {
                        _widgetsNew.get(i).getView().setVisibility(View.GONE);
                    }

                    FormActivity._layout.addView(_widgetsNew.get(i).getView());
                }


            }
        } else if (v.getTag().toString().equals("editor")) {
            Intent intent = new Intent(mContext, NewEditorActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(NewEditorActivity.TITLE_PARAM, "");
            bundle.putString(NewEditorActivity.CONTENT_PARAM, _input.getText().toString());
            bundle.putString(NewEditorActivity.TITLE_PLACEHOLDER_PARAM,
                    mContext.getResources().getString(R.string.example_post_title_placeholder));
            bundle.putString(NewEditorActivity.CONTENT_PLACEHOLDER_PARAM,
                    mContext.getResources().getString(R.string.post_content_placeholder) + "…");
            bundle.putInt(NewEditorActivity.EDITOR_PARAM, NewEditorActivity.USE_NEW_EDITOR);
            bundle.putString(ConstantVariables.EXTRA_MODULE_TYPE, mCurrentSelectedOption);
            bundle.putBoolean("isOverview", true);
            intent.putExtras(bundle);
            ((Activity)mContext).startActivityForResult(intent, ConstantVariables.OVERVIEW_REQUEST_CODE);
            ((Activity)mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String>
            implements Filterable {
        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context,
                                               int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }

                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint,
                                              FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

    public static ArrayList<String> autocomplete(String input) {

        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            String sb = UrlUtil.PLACES_API_BASE
                    + TYPE_AUTOCOMPLETE + OUT_JSON + "?key=" + staticContext.getResources().getString(R.string.places_api_key) +
                    "&input=" + URLEncoder.encode(input, "utf8");
            // sb.append("&components=country:gr");
 //           sb.append("&types=(cities)");

            URL url = new URL(sb);

            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            return resultList;
        } catch (IOException e) {
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());

            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");
            // Extract the Place descriptions from the results
            resultList = new ArrayList<>(predsJsonArray.length());
            resultList = new ArrayList<>(predsJsonArray.length());

            for (int i = 0; i< predsJsonArray.length(); i++) {

                JSONObject list = predsJsonArray.optJSONObject(i);
                String value = list.optString("description");
                resultList.add(value);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return resultList;
    }

    protected void checkUrlAvailability(String url){

        _checkUrlButton.setText(mContext.getResources().getString(R.string.checking_url) + "...");
        mPostParams = new HashMap<>();
        String actionUrl;
        if (mCurrentSelectedOption.equals(ConstantVariables.ADV_GROUPS_MENU_TITLE)) {
            actionUrl = AppConstant.DEFAULT_URL + "advancedgroups/groupurlvalidation";
            mPostParams.put("group_uri", url);
        } else {
            actionUrl = AppConstant.DEFAULT_URL + "advancedvideos/channel/channelurl-validation";
            mPostParams.put("channel_uri", url);
        }

        mAppConst.getJsonResponseFromUrl(mAppConst.buildQueryString(actionUrl, mPostParams), new OnResponseListener() {
                    @Override
                    public void onTaskCompleted(JSONObject jsonObject) {
                        _checkUrlButton.setVisibility(View.GONE);
                        imageview.setImageResource(R.drawable.ic_done_white_24dp);
                        imageview.setColorFilter(ContextCompat.getColor(mContext, R.color.light_green));
                        imageview.setVisibility(View.VISIBLE);
                        SnackbarUtils.displaySnackbarLongTime(_layout,
                                mContext.getResources().getString(R.string.url_available));
                    }

                    @Override
                    public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                        _checkUrlButton.setVisibility(View.GONE);
                        imageview.setImageResource(R.drawable.ic_clear_white);
                        imageview.setColorFilter(ContextCompat.getColor(mContext, R.color.red),
                                PorterDuff.Mode.SRC_IN);
                        imageview.setVisibility(View.VISIBLE);
                        SnackbarUtils.displaySnackbarLongTime(_layout, message);
                    }
                }

        );
    }
}
